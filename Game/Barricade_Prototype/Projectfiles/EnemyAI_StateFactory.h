//
//  EnemyAI_StateFactory.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyAI_Protocol.h" 

@class Barricade;
@class GameEnemy;
@class GamePlayer;

@interface EnemyAI_StateFactory : NSObject
{
    Barricade* barricadeRef;
    GamePlayer* playerRef;
}

-(id) initWithDependencies: (Barricade*) barricade
                          : (GamePlayer*) playerRefIn;

-(EnemyState*) createWalkingStateForParent: (GameEnemy*) parentIn;
-(EnemyState*) createApproachPlayerStateForParent: (GameEnemy*) parentIn;
-(EnemyState*) createAttackingStateForParent: (GameEnemy*) parentIn;

@end
