//
//  AI_Walk.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyAI_Protocol.h"

@class GameEnemy;

@interface AI_Walk : NSObject<EnemyAI_Protocol>
{
    GameEnemy*  parent;
}

-(void) update: (ccTime)deltaTime;
-(id) initWithParent: (GameEnemy*) parentIn;



@end
