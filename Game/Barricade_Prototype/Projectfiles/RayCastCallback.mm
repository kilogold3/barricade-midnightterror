//
//  RayCastCallback.cpp
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "RayCastCallback.h"
#include "GameEnemy.h"

float32 RayCastCallback::ReportFixture(	b2Fixture* fixture, 
                                        const b2Vec2& point,
                                        const b2Vec2& normal, 
                                        float32 fraction )
{
    //Acquire the body.
    b2Body* fixtureBody = fixture->GetBody();
    void* userData = fixtureBody->GetUserData();
    
    if( NULL != userData )
    {
        //If the fixture's body is the player's...
        if([GameEnemy class] == [((id)userData) class] )
        {
            //CCLOG(@"ENEMY HIT!");
            
            [ gameEnemy reposition:ccp( 0, (rand() % 320) ) ];
            
            //stop ray casting
            return 0;
        }
        else
        {
            //NSString* className = NSStringFromClass( [ ( (id) userData ) class] );
            //CCLOG( className );
        }
    }
    
    
    //Keep searching...
    return 1;
    
}