//
//  Barricade.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2Body;
class b2World;

@interface Barricade : CCNode 
{
    b2Body* barricadeBody;
    b2World* physicsWorld;
    
    int health;
    BOOL isDestroyed;
    
}
@property (readonly) int Health;
@property (readonly) BOOL IsDestroyed;

-(id) initAtPosition: (CGPoint) positionIn 
           withWorld: (b2World*) world;

-(void) setHealth: (int) nVal;

-(void) setDestroyedState;


@end
