//
//  RayCastCallback.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef PhysicsEditor_BarricadeTest_RayCastCallback_h
#define PhysicsEditor_BarricadeTest_RayCastCallback_h

#include "Box2D.h"

@class GameEnemy;

class RayCastCallback : public b2RayCastCallback
{
public:    
    RayCastCallback( GameEnemy* gameEnemyIn )
    {
        gameEnemy = gameEnemyIn; 
    }
    

    float32 ReportFixture(	b2Fixture* fixture, 
                            const b2Vec2& point,
                            const b2Vec2& normal, 
                            float32 fraction );
private:
    GameEnemy* gameEnemy;
};


#endif
