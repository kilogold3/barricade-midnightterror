//
//  AI_Attack.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AI_Attack.h"
#import "GameEnemy.h"
#import "Barricade.h"
#import "EnemyAI_StateFactory.h"
@implementation AI_Attack

-(void) update: (float)deltaTime
{
    [barricadeRef setHealth: barricadeRef.Health - 1];
    
    if( barricadeRef.IsDestroyed == YES )
    {
        [parent changeState: [parent.AI_StateFactory createApproachPlayerStateForParent:parent]];
    }

}

-(void) enter
{
    
}

-(void) exit
{
    
}

-(id) initWithParent: (GameEnemy*) parentIn
        andBarricade: (Barricade*) barricadeIn
{
    self = [super init];
    
    if (self != nil) 
    {
        parent = parentIn;
        barricadeRef = barricadeIn;
    }
    
    return self;
}


@end
