//
//  WeaponAim.mm
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/18/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "WeaponAim.h"


@implementation WeaponAim
-(id) init
{
    self = [super init];
    
    if( nil != self )
    {
        aimSprite = [CCSprite spriteWithFile:@"Reticle.png"];
        [self addChild:aimSprite];
    }
    
    return self;
}

@end
