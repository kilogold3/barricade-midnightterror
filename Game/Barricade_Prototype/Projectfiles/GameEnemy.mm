//
//  GameEnemy.mm
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameEnemy.h"
#import "Box2D.h"
#import "GB2ShapeCache.h"
#import "EnemyAI_StateFactory.h"


extern const float PTM_RATIO;


@implementation GameEnemy
@synthesize MovementSpeed = movementSpeed;
@synthesize AI_StateFactory=enemyAI_StateFactory;

-(id) initWithDependencies: (b2World*) world
                  position: (CGPoint) position
            aiStateFactory: (EnemyAI_StateFactory*) aiStateFactoryIn
{
    self = [super init];
    
    if (self != nil) 
    {
        movementSpeed = 80.0f;
        enemySprite = [CCSprite spriteWithFile:@"crabenemy.png"];
        [self addChild:enemySprite];
        [self scheduleUpdate];
        [self setPosition:position];
        
        
        // Create a body definition and set it to be a dynamic body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        
        // position must be converted to meters
        bodyDef.position = b2Vec2(position.x / PTM_RATIO, position.y / PTM_RATIO);
        
        // assign the sprite as userdata so it's easy to get to the sprite when working with the body
        bodyDef.userData = self;
        enemyBody = world->CreateBody(&bodyDef);
        
        // position must be converted to meters
        bodyDef.position = b2Vec2(position.x / PTM_RATIO, position.y / PTM_RATIO);
        
        // assign the sprite as userdata so it's easy to get to the sprite when working with the body
        bodyDef.userData = self;
        enemyBody = world->CreateBody(&bodyDef);
        
        // Define the dynamic body fixture.
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyBody forShapeName:@"crabenemy"];
        
        //Set state
        enemyAI_StateFactory = aiStateFactoryIn;
        [enemyAI_StateFactory retain];
        [self changeState: [enemyAI_StateFactory createWalkingStateForParent:self]];

    }
    
    return self;
}
-(void) dealloc
{
    [enemyState release];
    [enemyAI_StateFactory release];
    [super dealloc];
}

-(void) update: (ccTime) delta
{
    [enemyState update:delta];
}

-(void) reposition: (CGPoint) position
{
    [self setPosition: ccp( position.x , position.y) ];  
    
    b2Vec2 positionVec2( position.x / PTM_RATIO, position.y / PTM_RATIO);
    enemyBody->SetTransform(positionVec2, 0);
}

-(void) beginContactWithBarricade: (GB2Contact*)contact
{
    ++contactWithBarricadeCounter;
    if( 1 == contactWithBarricadeCounter )
    {
        CCLOG(@"Touch!!");
        [self changeState: [enemyAI_StateFactory createAttackingStateForParent:self] ];
    }
}

-(void) endContactWithBarricade: (GB2Contact*)contact
{
    --contactWithBarricadeCounter;
    if( 0 == contactWithBarricadeCounter )
    {
        CCLOG(@"End Touch!!!");
    }
}

-(void) changeState: (EnemyState*) newState
{
    [enemyState exit];
    [enemyState release];
    enemyState = newState;
    [enemyState enter];
}



@end
