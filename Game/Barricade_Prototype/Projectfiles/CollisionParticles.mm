//
//  CollisionParticles.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "CollisionParticles.h"
#import "Box2D.h"

extern const float PTM_RATIO;


@implementation CollisionParticles
-(id) initWithPhysicsWorld: (b2World*) world
{
    self = [super initWithTotalParticles:10];
    
    if( NULL != self )
    {
        particleBodies = new b2Body*[particleCount];
        for ( NSUInteger curParticle = 0; curParticle < allocatedParticles; ++curParticle ) 
        {
            // Create a body definition and set it to be a dynamic body
            b2BodyDef bodyDef;
            bodyDef.type = b2_dynamicBody;
            
            // position must be converted to meters
            b2Vec2 newPosition(
            particles[curParticle].pos.x/PTM_RATIO,
            particles[curParticle].pos.y/PTM_RATIO
            ); 
            
            bodyDef.position = newPosition;
            
            particleBodies[curParticle] = world->CreateBody(&bodyDef);
            
            // Define another box shape for our dynamic bodies.
            b2PolygonShape dynamicBox;
            
            float tileInMeters = 30.0f / PTM_RATIO;
            
            dynamicBox.SetAsBox(tileInMeters * 0.5f, tileInMeters * 0.5f);
            
            // Define the dynamic body fixture.
            b2FixtureDef fixtureDef;
            fixtureDef.shape = &dynamicBox;	
            fixtureDef.isSensor = true;
            particleBodies[curParticle]->CreateFixture(&fixtureDef);
            
        }
    }
    
    return self;
}

-(void) dealloc
{
    delete[] particleBodies;
    [super dealloc];
}

-(void) update:(ccTime)dt
{
    [super update:dt];
    
//    for ( NSUInteger curParticle = 0; curParticle < allocatedParticles; ++curParticle ) 
//    {
//        b2Assert(particleBodies[curParticle] != NULL);
//        b2Vec2 newPosition( 
//                           particles[curParticle].pos.x/PTM_RATIO, 
//                           particles[curParticle].pos.y/PTM_RATIO );
//        particleBodies[curParticle]->SetTransform(newPosition, 0);
//        
//    }
}

@end
