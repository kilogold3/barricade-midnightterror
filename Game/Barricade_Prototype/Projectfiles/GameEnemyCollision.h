//
//  GameEnemyCollision.h
//  Barricade_Prototype
//
//  Created by Kelvin Bonilla on 10/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2Body;
class b2World;

@interface GameEnemyCollision : CCNode {
    CCSprite* enemySprite;
    b2Body* enemyBody;
}

-(id) initWithPosition: (CGPoint) position
       andPhysicsWorld: (b2World*) world;
@end
