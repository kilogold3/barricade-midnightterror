//
//  Barricade.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Barricade.h"
#import "Box2D.h"

extern const float PTM_RATIO;

@implementation Barricade
@synthesize Health=health;
@synthesize IsDestroyed=isDestroyed;

-(id) initAtPosition: (CGPoint) positionIn
           withWorld: (b2World*) world
{
    self = [super init];
    
    if (self != nil) 
    {
        physicsWorld = world;
        isDestroyed = NO;
        
        // Create a body definition and set it to be a dynamic body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        
        // position must be converted to meters
        b2Vec2 newPosition(
                           positionIn.x/PTM_RATIO,
                           positionIn.y/PTM_RATIO
                           ); 
        
        bodyDef.position = newPosition;
        bodyDef.userData = self;
        
        barricadeBody = world->CreateBody(&bodyDef);
        
        // Define another box shape for our dynamic bodies.
        b2PolygonShape dynamicBox;
        
        CGSize screenSize = [[CCDirector sharedDirector] winSize];
        
        const float heightInMeters = screenSize.height / PTM_RATIO;
        const float widthInMeters = 30.0f / PTM_RATIO;
        
        dynamicBox.SetAsBox(widthInMeters * 0.5f, heightInMeters * 0.5f);
        
        // Define the dynamic body fixture.
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &dynamicBox;	
        fixtureDef.isSensor = true;
        barricadeBody->CreateFixture(&fixtureDef);
    }

    return self;
}

-(void) setDestroyedState
{
    isDestroyed = YES;
    physicsWorld->DestroyBody(barricadeBody);
}


-(void) setHealth:(int)nVal
{
    health = nVal;
    
    if( (nVal <= 0) and (NO == isDestroyed) )
    {
        CCLOG(@"BarricadeDead");
        [self setDestroyedState];
    }
}
@end
