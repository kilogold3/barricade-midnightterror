//
//  EnemyAI_StateFactory.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyAI_StateFactory.h"
#import "AI_Walk.h"
#import "AI_Attack.h"
#import "AI_ApproachPlayer.h"
#import "GamePlayer.h"

@implementation EnemyAI_StateFactory
-(id) initWithDependencies: (Barricade*) barricade
                          : (GamePlayer*) playerRefIn
{
    self = [super init];
    
    if (self != nil) 
    {
        barricadeRef = barricade;
        playerRef = playerRefIn;
    }
    
    return self;
}

-(EnemyState*) createWalkingStateForParent: (GameEnemy*) parentIn
{
    return [[AI_Walk alloc] initWithParent:parentIn];
}

-(EnemyState*) createApproachPlayerStateForParent: (GameEnemy*) parentIn;
{
    return [[AI_ApproachPlayer alloc] initWithParent:parentIn 
                                              player:playerRef];
}

-(EnemyState*) createAttackingStateForParent: (GameEnemy*) parentIn
{
    return [[AI_Attack alloc] initWithParent:parentIn
                                andBarricade:barricadeRef];   
}

@end
