//
//  AI_ApproachPlayer.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AI_ApproachPlayer.h"
#import "GameEnemy.h"
#import "EnemyAI_StateFactory.h"
#import "GamePlayer.h"
@implementation AI_ApproachPlayer

-(void) update: (float)deltaTime
{
    
    CGPoint toPlayer = ccpSub(playerRef.position, parent.position);
    
    if( ccpLength(toPlayer) > 15.0f )
    {
        CGPoint toPlayer_Normalized = ccpNormalize(toPlayer);
        
        CGPoint newPosition = ccp(
                                  parent.position.x + toPlayer_Normalized.x * (parent.MovementSpeed * deltaTime),
                                  parent.position.y + toPlayer_Normalized.y * (parent.MovementSpeed * deltaTime));
        
        [parent reposition: newPosition ];
    }
    
}

-(void) enter
{
    
}

-(void) exit
{
    
}

-(id) initWithParent: (GameEnemy*) parentIn
              player: (GamePlayer*) gamePlayerRef
{
    self = [super init];
    
    if (self != nil) 
    {
        parent = parentIn;
        playerRef = gamePlayerRef;
    }
    
    return self;
}


@end
