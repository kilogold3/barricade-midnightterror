//
//  EnemyAI_Protocol.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EnemyAI_Protocol <NSObject>

-(void) update: (float)deltaTime;
-(void) enter;
-(void) exit;
@end

typedef NSObject<EnemyAI_Protocol> EnemyState;
