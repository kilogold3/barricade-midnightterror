//
//  GamePlayer.mm
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GamePlayer.h"
#import "Box2D.h"
#import "GB2ShapeCache.h"

extern const float PTM_RATIO;


@implementation GamePlayer
@synthesize MovementDestination=movementDestination;

-(id) initWithDependencies: (b2World*) world: (CGPoint) position
{
    self = [super init];
    
    if (self != nil) 
    {
        movementSpeed = 80.0f;
        movementDestination = position;
        enemySprite = [CCSprite spriteWithFile:@"crabenemy.png"];
        collisionCounter = 0;
        [self addChild:enemySprite];
        [self scheduleUpdate];
        [self setPosition:position];
        
        // Create a body definition and set it to be a dynamic body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        
        // position must be converted to meters
        bodyDef.position = b2Vec2(position.x / PTM_RATIO, position.y / PTM_RATIO);
        
        // assign the sprite as userdata so it's easy to get to the sprite when working with the body
        bodyDef.userData = self;
        enemyBody = world->CreateBody(&bodyDef);
        
        // Define the dynamic body fixture.
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyBody forShapeName:@"crabenemy"];
    }
    
    return self;
}

-(void) update: (ccTime) delta
{
    //get direction from current position to destination position
    CGPoint CurrentPositionToDestionation = ccpSub( movementDestination, self.position );
    
    //If we're outside the deadzone
    if( ccpLength(CurrentPositionToDestionation) > 15 )
    {
        CGPoint movementOffset;
        
        //Get direction
        movementOffset= ccpNormalize(CurrentPositionToDestionation);
        
        //Scale based on speed & time
        movementOffset = ccpMult(movementOffset, movementSpeed * delta);
        
        //Calculate the new position
        movementOffset = ccpAdd(movementOffset, self.position);
        
        //Move
        [self reposition: movementOffset];
    }
    
}

-(void) reposition: (CGPoint) position
{
    [self setPosition: ccp( position.x , position.y) ];  
    
    b2Vec2 positionVec2( position.x / PTM_RATIO, position.y / PTM_RATIO);
    enemyBody->SetTransform(positionVec2, 0);
}

-(void) beginContactWithGameEnemy: (GB2Contact*) contact
{
    ++collisionCounter;
    
    if (1 == collisionCounter) 
    {
        CCLOG(@"Touched Enemy");
    }
}

-(void) endContactWithGameEnemy: (GB2Contact*) contact
{
    --collisionCounter;
    if( 0 == collisionCounter )
    {
        CCLOG(@"Released Enemy");
    }
}
@end
