//
//  AI_Walk.m
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AI_Walk.h"
#import "GameEnemy.h"

@implementation AI_Walk

-(void) update: (float)deltaTime
{
    [parent reposition: ccp( parent.position.x + parent.MovementSpeed * deltaTime , parent.position.y) ];
    
    if( parent.position.x > [[CCDirector sharedDirector] screenSize].width )
    {
        [parent reposition: ccp( 0 , parent.position.y) ];     
    }
}

-(void) enter
{
    
}

-(void) exit
{
    
}

-(id) initWithParent: (GameEnemy*) parentIn
{
    self = [super init];
    
    if (self != nil) 
    {
        parent = parentIn;
    }
    
    return self;
}


@end
