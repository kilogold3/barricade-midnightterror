//
//  GameEnemyCollision.m
//  Barricade_Prototype
//
//  Created by Kelvin Bonilla on 10/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameEnemyCollision.h"
#import "Box2D.h"
#import "GB2ShapeCache.h"


extern const float PTM_RATIO;

@implementation GameEnemyCollision

-(id) initWithPosition: (CGPoint) position
       andPhysicsWorld: (b2World*) world
{
    self = [super init];
    
    if (self != nil) 
    {
        enemySprite = [CCSprite spriteWithFile:@"zombieCollisions.png"];
        enemySprite.anchorPoint = ccp(0.5f,0);
        
        [self addChild:enemySprite];
        
        
        // Create a body definition and set it to be a dynamic body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        
        // position must be converted to meters
        bodyDef.position = b2Vec2(position.x / PTM_RATIO, position.y / PTM_RATIO);
        
        // assign the sprite as userdata so it's easy to get to the sprite when working with the body
        bodyDef.userData = self;
        enemyBody = world->CreateBody(&bodyDef);
        
        // position must be converted to meters
        bodyDef.position = b2Vec2(position.x / PTM_RATIO, position.y / PTM_RATIO);
        
        // assign the sprite as userdata so it's easy to get to the sprite when working with the body
        bodyDef.userData = self;
        enemyBody = world->CreateBody(&bodyDef);
        
        // Define the dynamic body fixture.
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyBody forShapeName:@"zombiewalk_01"];
    }
    
    return self;
}

@end
