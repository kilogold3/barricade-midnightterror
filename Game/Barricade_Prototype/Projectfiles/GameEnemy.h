//
//  GameEnemy.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyAI_Protocol.h"
#import "cocos2d.h"

class b2Body;
class b2World;
@class GB2Contact;
@class EnemyAI_StateFactory;

@interface GameEnemy : CCNode {
    //Sprite representation of the enemy
    CCSprite* enemySprite;
    
    //Collision detection mechanism
    b2Body* enemyBody;
    
    //Movement speed
    float movementSpeed;
    
    //Box2D same-body polygon collsion counters
    int contactWithBarricadeCounter;
    
    //AI state
    EnemyState* enemyState;
    EnemyAI_StateFactory* enemyAI_StateFactory;

}
@property float MovementSpeed;
@property (readonly) EnemyAI_StateFactory* AI_StateFactory;

-(id) initWithDependencies: (b2World*) world
                  position: (CGPoint) position
            aiStateFactory: (EnemyAI_StateFactory*) aiStateFactoryIn;

-(void) update: (ccTime) delta;
-(void) reposition: (CGPoint) position;

-(void) beginContactWithBarricade: (GB2Contact*)contact;
-(void) endContactWithBarricade: (GB2Contact*)contact;

-(void) changeState: (EnemyState*) newState;

@end
