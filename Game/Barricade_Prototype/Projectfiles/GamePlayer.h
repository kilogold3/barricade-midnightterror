//
//  GamePlayer.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/13/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2Body;
class b2World;
@class GB2Contact;

@interface GamePlayer : CCNode {
    //Sprite representation of the enemy
    CCSprite* enemySprite;
    
    //Collision detection mechanism
    b2Body* enemyBody;
    
    //Movement speed
    float movementSpeed;
    
    //Target movement destination
    CGPoint movementDestination;
    
    //Collision counters
    int collisionCounter;
}
@property CGPoint MovementDestination;

-(id) initWithDependencies: (b2World*) world: (CGPoint) position;
-(void) update: (ccTime) delta;
-(void) reposition: (CGPoint) position;


//collision
-(void) beginContactWithGameEnemy: (GB2Contact*) contact;
-(void) endContactWithGameEnemy: (GB2Contact*) contact;


@end
