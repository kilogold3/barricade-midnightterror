//
//  CollisionParticles.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2Body;

@interface CollisionParticles : CCParticleFireworks 
{
    b2Body** particleBodies;
    
}
-(id) initWithPhysicsWorld:(b2World*) world;
-(void) update:(ccTime)dt;

@end
