/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"

#import "GB2WorldContactListener.h"

extern const float PTM_RATIO;

@class GamePlayer;
@class WeaponAim;
@class SneakyButton;
@class GameEnemy;
@class SneakyButtonSkinnedBase;

enum
{
	kTagBatchNode,
};

@interface PhysicsLayer : CCLayer
{
	b2World* world;
	GB2WorldContactListener* contactListener;
	GLESDebugDraw* debugDraw;
    GamePlayer* gamePlayer;
    WeaponAim* weaponAim;
    SneakyButton* fireButton;
    GameEnemy* gameEnemy;
}

@end
