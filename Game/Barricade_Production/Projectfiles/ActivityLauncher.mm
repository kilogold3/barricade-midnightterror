//
//  ActivityLauncher.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/13/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ActivityLauncher.h"
#import "NotificationCenterEventList.h"

@interface ActivityLauncher (PrivateMethods)
// declare private methods here
@end

@implementation ActivityLauncher
@synthesize ActivityToLaunch=activityToLaunch;

-(void) Interact
{
    [self LaunchActivity];
}

-(void) LaunchActivity
{
    [[NSNotificationCenter defaultCenter] postNotificationName: EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                                        object: self];
}


-(void)didLoadFromCCB
{
    CCLOG(@"Activity Launcher for %@ loaded.", activityToLaunch);
}

//We release the 'activityToLaunch' string on both dealloc
//and cleanup because the object may be released without ever having
//been added to a CCNode hierarchy, thus missing the cleanup call.
-(void) dealloc
{
    if (activityToLaunch != nil)
    {
        [activityToLaunch release];
    }
    
    [super dealloc];
}

-(void) cleanup
{
    [super cleanup];
    
    if (activityToLaunch != nil)
    {
        [activityToLaunch release];
        //Set to nil to avoid accidental over-release on dealloc call
        activityToLaunch = nil;
    }
}
@end
