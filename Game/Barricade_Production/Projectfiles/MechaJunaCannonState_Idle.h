//
//  MechaJunaState_AttackWithRightArm.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"
@class EnemyMechaJunaCannon;

@interface MechaJunaCannonState_Idle : NSObject<IEnemyState>
{
    EnemyMechaJunaCannon* enemyParentRef;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) enter;
-(void) exit;
-(void) update: (float) deltaTime;

@end