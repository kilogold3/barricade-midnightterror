//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WoundedJunaState_AttackedByJohnson.h"
#import "CCBReader.h"
#import "OverdriveSequence.h"
#import "NotificationCenterEventList.h"
#import "EnemyJuna.h"
#import "OverdriveFactory.h"
#import "EnemyStateFactory.h"

@implementation WoundedJunaState_AttackedByJohnson

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyJuna class]],@"Wrong enemy parent type");
        enemyParent = (EnemyJuna*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OverdriveAnimationFinish_EventHandler:)
                                                 name:EventList::OVERDRIVE_FINISH_ANIMATION
                                               object:nil];
    
    
    //Play Johnson's overdrive animation
    OverdriveFactory* overdriveFactory = [[[OverdriveFactory alloc] init] autorelease];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_START_ANIMATION
                                                        object:[overdriveFactory createJohnsonOverdriveAnimationNoExecution]];
    
    
    //Due to how the EnemyBase is set up, we need to unschedule the enemy's update.
    //When the animation is complete, it will resume it. If it tries to resume it while
    //it is running, we will get an assertion failure
    [enemyParent unscheduleUpdate];
}

-(void) exit
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
}

-(void) update: (float) deltaTime
{

}

-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    [enemyParent changeEnemyState: [enemyParent.AI_StateFactory createWoundedJunaState_Dead]];
}

@end
