//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyJuna;

@interface WoundedJunaState_Dead : NSObject<IEnemyState>
{
    EnemyJuna* enemyParent;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
@end
