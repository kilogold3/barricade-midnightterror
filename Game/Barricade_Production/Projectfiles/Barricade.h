//
//  Barricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
/**********************************************************************************
 *
 *
 **********************************************************************************/
#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class BarricadeSegment;

@interface Barricade : CCNode 
{
    int currentHealth;
    int maxHealth;
    BarricadeSegment* barricadeSegment;
    
    //Barricade sprite frame names
    NSString* barricadeSpriteFrameNameA;
    NSString* barricadeSpriteFrameNameB;
    NSString* barricadeSpriteFrameNameC;
    NSString* barricadeSpriteFrameNameD;
    
}
@property (readwrite, nonatomic) int CurrentHealth;
@property (readonly) int MaxHealth;
@property (readonly) BOOL IsDefending;
@property (readonly) CCSprite* BarricadeSprite;

@end
