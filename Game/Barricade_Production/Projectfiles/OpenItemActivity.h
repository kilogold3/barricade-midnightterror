//
//  OpenItemActivity.h
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "ActivityBase.h"

@class OpenItemTapObject;

@interface OpenItemActivity : ActivityBase
{
    OpenItemTapObject* objectToOpen;
}

@end
