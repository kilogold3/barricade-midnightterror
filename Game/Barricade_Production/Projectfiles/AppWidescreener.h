//
//  AppWidescreener.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/8/13.
//
//

#import <Foundation/Foundation.h>
@class CCScene;

@interface AppWidescreener : NSObject
+(void) applyLetterboxToScene: (CCNode*) scene;
+(void) centralizeSceneOnScreen: (CCNode*) scene;
+(BOOL) isWidescreenEnabled;
+(CGPoint) getWidescreenLetterboxOffset;


//Returns the size of the scene.
//During the letterboxed approach for app release, we can't reliably query
//the director for the screen size. Because of this, we will have this method
//give us the intended size of the scene (which should always be the 3.5inch
//screen resolution size in Cocos2D points: [480,320])
+(CGSize) getSceneContentSize;
@end
