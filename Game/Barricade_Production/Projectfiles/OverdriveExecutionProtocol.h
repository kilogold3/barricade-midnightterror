//
//  OverdriveProtocol.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import <Foundation/Foundation.h>
#import "CCNode.h"

@protocol OverdriveExecutionProtocol <NSObject>
-(void) executeOverdrive;
-(void) resetOverdrive;
@end

typedef CCNode<OverdriveExecutionProtocol> OverdriveExecutionObject;