//
//  CCRadioMenu.m
//  MathNinja
//
//  Created by Ray Wenderlich on 2/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CCRadioMenu.h"

//We're copying this from "CCMenuAdvanced.m".
//This declaration suppresses the "selector not declared"
//warning at compile time. We could do a runtime selector call
//but that adds more overhead.
@interface CCMenu (Private)
#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
-(CCMenuItem *) itemForTouch: (UITouch *) touch;
#elif __MAC_OS_X_VERSION_MAX_ALLOWED
-(CCMenuItem *) itemForMouseEvent: (NSEvent *) event;
#endif
@end

@implementation CCRadioMenu

-(void) resetMenu
{
    
}

- (void)setSelectedItem_:(CCMenuItem *)item {
    [_selectedItem unselected];
    _selectedItem = item;    
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
 
    if ( _state != kCCMenuStateWaiting || !self.enabled) return NO;
    
    CCMenuItem *curSelection = [self itemForTouch:touch];
    [curSelection selected];
    _curHighlighted = curSelection;
    
    if (_curHighlighted) {
        if (_selectedItem != curSelection) {
            [_selectedItem unselected];
        }
        _state = kCCMenuStateTrackingTouch;
        return YES;
    }
    return NO;
    
}

- (void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {

    NSAssert(_state == kCCMenuStateTrackingTouch, @"[Menu ccTouchEnded] -- invalid state");
	
    CCMenuItem *curSelection = [self itemForTouch:touch];
    if (curSelection != _curHighlighted && curSelection != nil) {
        [_selectedItem selected];
        [_curHighlighted unselected];
        _curHighlighted = nil;
        _state = kCCMenuStateWaiting;
        return;
    } 
    
    _selectedItem = _curHighlighted;
    [_curHighlighted activate];
    _curHighlighted = nil;
    
	_state = kCCMenuStateWaiting;
    
}

- (void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
 
    NSAssert(_state == kCCMenuStateTrackingTouch, @"[Menu ccTouchCancelled] -- invalid state");
	
	[_selectedItem selected];
    [_curHighlighted unselected];
    _curHighlighted = nil;
	
	_state = kCCMenuStateWaiting;
    
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	NSAssert(_state == kCCMenuStateTrackingTouch, @"[Menu ccTouchMoved] -- invalid state");
	
	CCMenuItem *curSelection = [self itemForTouch:touch];
    if (curSelection != _curHighlighted && curSelection != nil) {       
        [_curHighlighted unselected];
        [curSelection selected];
        _curHighlighted = curSelection;        
        return;
    }
}

@end
