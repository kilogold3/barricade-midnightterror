//
//  GameSaveData.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/23/12.
//
//

#import <Foundation/Foundation.h>

typedef NSDictionary SaveObjectPropertyList;

@interface GameSaveData : NSObject
{
    NSDictionary* saveObjectDictionary;
}

+ (GameSaveData *)sharedGameSaveData;
-(SaveObjectPropertyList*) getSaveObjectProperties: (NSString*) gameSaveCategory;
-(void) WriteToFile;
-(void) ResetSaveDataToDefaults;
-(BOOL) isSaveFileAvailable;

@end
