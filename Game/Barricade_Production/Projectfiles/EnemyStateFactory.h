//
//  EnemyStateFactory.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyBase;

@interface EnemyStateFactory : NSObject
{
    EnemyBase* enemyParent; //Enemy reference to apply all states.
}
-(id) initForEnemy: (EnemyBase*) enemyParentIn;

//Walker
-(EnemyState*) createWalkerState_ApproachBarricade;
-(EnemyState*) createWalkerState_ApproachPlayer;
-(EnemyState*) createWalkerState_Death;
-(EnemyState*) createWalkerState_AttackBarricade;
-(EnemyState*) createWalkerState_Blank;

//Vejigante
-(EnemyState*) createVejiganteState_ApproachBarricade;
-(EnemyState*) createVejiganteState_ApproachPlayer;
-(EnemyState*) createVejiganteState_Spawn;

//MechaJuna
-(EnemyState*) createMechaJunaArmState_Attack;
-(EnemyState*) createMechaJunaArmState_Idle;
-(EnemyState*) createMechaJunaArmState_Dead;
-(EnemyState*) createMechaJunaCannonState_Idle;
-(EnemyState*) createMechaJunaCannonState_Fire;
-(EnemyState*) createMechaJunaCannonState_Dead;

//RampageJuna
-(EnemyState*) createRampageJunaState_ApproachBarricade;
-(EnemyState*) createRampageJunaState_AttackedByJerome;

//WoundedJuna
-(EnemyState*) createWoundedJunaState_ApproachLeo;
-(EnemyState*) createWoundedJunaState_AttackedByJohnson;
-(EnemyState*) createWoundedJunaState_DeathShock;
-(EnemyState*) createWoundedJunaState_Dead;

//Tank
-(EnemyState*) createTankState_ApproachPlayer;
-(EnemyState*) createTankState_ApproachBarricade;
-(EnemyState*) createTankState_AttackBarricade;
-(EnemyState*) createTankState_Death;

//Flier
-(EnemyState*) createFlierState_Spawn;
-(EnemyState*) createFlierState_Death;

//Runner
-(EnemyState*) createRunnerState_ApproachBarricade;
-(EnemyState*) createRunnerState_ApproachPlayer;
-(EnemyState*) createRunnerState_Death;
-(EnemyState*) createRunnerState_AttackBarricade;
-(EnemyState*) createRunnerState_Blank;

@end
