//
//  TravelTree.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/19/13.
//
//

#include "TravelTree.h"
#import "TravelNodeButton.h"

GameTreeNode::GameTreeNode( TravelNodeButton* travelNodeButtonIn )
{
    nodeData = travelNodeButtonIn;
    nodeParent = NULL;
}

GameTreeNode* GameTreeNode::FindTreeNodeFromData( TravelNodeButton* travelNode )
{
    if (travelNode == nodeData)
    {
        return this;
    }
    
    else
    {
        for( size_t curChildNode = 0; curChildNode < nodeChildren.size(); ++curChildNode )
        {
            GameTreeNode* nodeFound = nodeChildren[curChildNode]->FindTreeNodeFromData( travelNode );
            if ( NULL != nodeFound )
            {
                return nodeFound;
            }
        }
    }
    
    return NULL;
}

void GameTreeNode::DeleteChildren()
{
    for( size_t curChildNode = 0; curChildNode < nodeChildren.size(); ++curChildNode )
    {
        nodeChildren[curChildNode]->DeleteChildren();
        delete nodeChildren[curChildNode];
    }
    
}

bool GameTreeNode::compareFunc( GameTreeNode* newNode )
{
    //If we have no parent, we are the root node.
    //No node will be added at the same level as the root node.
    if( NULL == this->nodeParent )
    {
        return false;
    }
    
    TravelNodeButton* ProgressionNodeNew = newNode->nodeData;
    TravelNodeButton* ProgressionNodeSelf = this->nodeData;
    
    //If progression values match AND
    //If the previous progression story path differs...
    //We are on a fork in the road: A suitable place to put a different path in.
    
    if( ProgressionNodeSelf.ProgressionValue == ProgressionNodeNew.ProgressionValue &&
       NO == [nodeParent->nodeData.StoryPathValue isEqualToString:
              ProgressionNodeNew.StoryPathValue] )
    {
        return true;
    }
    
    return false;
}

bool GameTreeNode::AddNode( GameTreeNode* newNode )
{
    //Run the compare function to see if we are in the suitable place to put the node ( A fork in the road )
    if( true == compareFunc( newNode ) )
    {
        //Let's attach the node to the tree in form of a fork in the road
        this->nodeParent->nodeChildren.push_back(newNode);
        newNode->nodeParent = this->nodeParent;
        
        //Let's quit the recursion
        return true;
    }
    
    //If we have children...
    if( nodeChildren.size() > 0 )
    {
        //Check to see if the children have a suitable place for the node
        for( size_t curChildNode = 0; curChildNode < nodeChildren.size(); ++curChildNode )
        {
            //If any children are on the same story path, let's look in there...
            if( YES == [nodeChildren[curChildNode]->nodeData.StoryPathValue isEqualToString:
                        newNode->nodeData.StoryPathValue] )
            {
                //Add the node down that path.
                if( true == nodeChildren[curChildNode]->AddNode( newNode ) )
                {
                    //We found a location to place the node
                    //let's exit
                    return true;
                }
            }
        }
        
        //If we are here, no children are on the same story path,
        //follow the main story path until we find the fork in the road
        if( true == nodeChildren[0]->AddNode( newNode ) )
        {
            //We found a location to place the node
            //let's exit
            return true;
        }
    }
    else //We got to the end. Let's add a leaf node
    {
        newNode->nodeParent = this;
        nodeChildren.push_back( newNode );
        return true;
    }
    
    assert(false);//should never reach here
    return true;
}
