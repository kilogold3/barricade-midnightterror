//
//  TouchControlLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

//-----------------------------------------------------------------------
//NOTE:
// This class MUST remain a layer at the origin if we expect to send the right touch 
// coordinates to other game components.
//------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Player;
@class CCRadioMenu;
@class InputTouchPanel;
@class SneakyHoldEventButton;
@class Aim;
@class HUD_DataDisplayStatsAmmo;
@class SneakyButtonSkinnedBase;

@interface TouchControlLayer : CCLayer 
{
@protected
    HUD_DataDisplayStatsAmmo* dataDisplayStatsAmmoSprite;
    SneakyHoldEventButton* pauseButton;
    SneakyHoldEventButton* fireButton;
    SneakyButtonSkinnedBase* overdriveSkinnedButton;
    SneakyButtonSkinnedBase* weaponBoostSkinnedButton;
    InputTouchPanel* movementTouchPanel;
    InputTouchPanel* aimTouchPanel;
    CCRadioMenu* weaponSwitchPanel;
    CCRadioMenu* overdrivePanel;
    CCSprite* hudPanelFrontPlateSprite;
    CCSprite* hudDataPanelBackgroundSprite;
    CCLabelTTF* healthAmountLabel;
    CGPoint previousMovementDestination;
    CGPoint previousAimDestination;
    
    //To avoid activating some buttons that are meant to remain deactivated,
    //we implement some restriction flags which we will check when executing
    //a full TouchControlLayer activation via the method 'activateSelfAndAllComponents'
    BOOL isOverdriveButtonRestricted;
    BOOL isWeaponBoostButtonRestricted;
    
@private
    Player* playerRef;
    Aim* aimRef;
}

-(id) init;
-(void) update: (ccTime) delta;
-(void) activateSelfAndAllComponents;
-(void) deactivateSelfAndAllComponents;

//Event Handlers
-(void) OverdriveJeromeStartAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveJeromeEndAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveJohnsonEndAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveJohnsonStartAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ComponentAcquireGameplaySceneTouchControlLayer_EventHandler: (NSNotification*) notification;
-(void) gameplayBarricadeHealthChanged_EventHandler: (NSNotification*) notification;
-(void) gameplayWeaponOutOfAmmo_EventHandler: (NSNotification*) notification;
-(void) ControlsRestrictOverdrive_EventHandler: (NSNotification*) notification;

@end
