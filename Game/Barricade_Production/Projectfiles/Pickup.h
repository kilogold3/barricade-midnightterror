//
//  Pickup.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ResourceGatheringInteractiveObject.h"
#import "PickupTypes.h"

@interface Pickup : ResourceGatheringInteractiveObject
{
    CCMenuItem* minimapButtonRef;
    PICKUP_TYPE pickupType;
    int pickupValue;
    
    //Can we pick the item up.
    BOOL isReadyForPickup;
}
//Can the object receive touches
@property (nonatomic, readwrite) BOOL IsReadyForPickup;

-(void) Interact;

@end
