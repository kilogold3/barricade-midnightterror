//
//  LineOfFireEventArgs.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LineOfFireEventArgs.h"

@implementation LineOfFireEventArgs
@synthesize LineOfFireOrigin=lineOfFireOrigin;
@synthesize LineOfFireDirection=lineOfFireDirection;
@synthesize FiringWeapon=firingWeapon;

-(id) initWithDependencies: (CGPoint) lineOfFireOriginIn
                          : (CGPoint) lineOfFireDirectionIn
                          : (WeaponBase*) firingWeaponIn
{
    self = [super init];
    
    if( self != nil )
    {
        lineOfFireOrigin = lineOfFireOriginIn;
        lineOfFireDirection = lineOfFireDirectionIn;
        firingWeapon = firingWeaponIn;
    }
    
    return self;
}
@end
