//
//  OverdriveExecutionJonathanLittleV1.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import "OverdriveExecutionJeromeV1.h"
#import "CCAnimation+SequenceLoader.h"
#import "NotificationCenterEventList.h"
#import "GMath.h"
#import "SimpleAudioEngine.h"
#import "CCShake.h"
#import "GameplayLayer.h"
#import "Aim.h"
#import "EnemyField.h"
#import "EnemyBase.h"

@implementation OverdriveExecutionJeromeV1

-(id) init
{
    //We'll use this init phase to invoke the event system and acquire
    //all the dependencies needed by the overdrive execution
    self = [super init];
    
    if( nil != self )
    {
        //Load explosion sprite
        explosionSprite = [CCSprite spriteWithSpriteFrameName:@"mineSprite_Idle_b.png"];
        [explosionSprite setPosition:ccp(100,100)];
        explosionSprite.visible = NO; //we only want the player to see it during the exploding animation.
        [self addChild:explosionSprite];

        
        //Load explosion animation sequence
        const short animationFrameCount = 7;
        float animationDelay = 0.020f;
        maxDetonationCount = 20;//Had to crank this number up... It only does a fraction of this value
        curDetonationCount = 0;
        
        explodeAnimation = [CCAnimation animationWithSpriteSequence:@"mineSprite_Explode_b%d.png"
                                                                       numFrames:animationFrameCount
                                                                           delay:animationDelay];
        [explodeAnimation retain];
        
        //Acquire GameplayScene's GameplayLayer
        gameplayLayerRef = nil;
        NSValue* gameplayLayerRefValue = [NSValue valueWithPointer:&gameplayLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER
                                                            object: gameplayLayerRefValue];
        NSAssert(gameplayLayerRef != nil, @"Failed to acquire GameplayLayer");
        
        //Create the shake action
        CCShake* shakeAction = [CCShake actionWithDuration:1.0f amplitude:ccp(5,5)];
        repeatingShakeScreenAction = [CCRepeatForever actionWithAction:shakeAction];
        [repeatingShakeScreenAction retain];
        
        //Grab hold of the aim reference
        aimRef = nil;
        SEND_EVENT(EventList::COMPONENT_ACQUIRE_AIM_TARGET, [NSValue valueWithPointer:&aimRef]);
        NSAssert(nil != aimRef,@"Aim reference is nil. No aim found.");
        
        //Grab hold of all the enemies on screen
        enemyFieldRef = nil;
        SEND_EVENT(EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD, [NSValue valueWithPointer:&enemyFieldRef]);
        NSAssert(nil != enemyFieldRef,@"Reference is nil. No EnemyField instance found.");
        
        //Create the array that will store the targeted enemies that will be killed.
        //We will also keep it, so we'll bring up the retain count. When we are done with it.
        //we simply empty it and reuse it next time. No need to reinstantiate it.
        targetedEnemies = [NSMutableArray array];
        [targetedEnemies retain];
    }
    
    return self;
}

-(void) executeExplosionAnimation
{
    //At this point, we will begin exploding, so let's make the explosion sprite visible and it is within the reticle
    explosionSprite.visible = YES;
    explosionSprite.position = aimRef.position;
    
    
    CCAnimate* explosionAnimateAction = [CCAnimate actionWithAnimation:explodeAnimation];
    CCCallFunc* detonationFinishCallbackAction = [CCCallFunc actionWithTarget:self
                                                                     selector:@selector( explosionAnimationFinished: )];
    
    trapDetonationSequence = [CCSequence actions:
                              explosionAnimateAction,
                              detonationFinishCallbackAction,
                              nil];
    
    //"trapDetonationSequence" is on autorelease. Upon release,
    //"detonationFinishCallbackAction" will also be release, thus,
    //decrementing "self" reference count. This must happen in order
    //to avoid memory leaks.
    [explosionSprite runAction:trapDetonationSequence];
    [gameplayLayerRef runAction: repeatingShakeScreenAction];
}

-(void) explosionAnimationFinished: (id) sender
{
    //If we've detonated all the counts...
    if( ++curDetonationCount >= maxDetonationCount )
    {
        /**************************************************************************
        * Send an event notifying that the overdrive is complete.
        * Two things should happen here:
        *
        *   1) resume game world simulation from paused state
        *   2) kill all enemies on the field
        **************************************************************************/
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_FINISH_EXECUTION
                                                            object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JEROME_DAMAGE_ENEMIES
                                                            object:targetedEnemies];
        
        //Reset the overdrive mechanism for the next use
        [self resetOverdrive];
        
        //Remove the execution so it can be added again later.
        //The execution is persistent and will rely on a 'reset'
        //feature to manage itself. The execution removes itself
        //when the player is deleted.
        [self.parent removeChild:self cleanup:YES];
        
        CCLOG(@"JEROME Overdrive Execution Complete!");
    }
    else
    {
        //Run another explosion, but the effect must only appear within the radius of the aim.
        //Since it is assumed at this point that the aim is frozen, we can safely use it's values
        //to determine the area of effect.
        //These value SHOULD be the same as the last call to the "update" method.
        const short reticleXMin = aimRef.position.x - reticleRadius;
        const short reticleXMax = aimRef.position.x + reticleRadius;
        const short reticleYMin = aimRef.position.y - reticleRadius;
        const short reticleYMax = aimRef.position.y + reticleRadius;
        
        [explosionSprite setPosition:ccp( gRangeRand(reticleXMin, reticleXMax), gRangeRand(reticleYMin, reticleYMax) )];
        [explosionSprite stopAction:trapDetonationSequence];
        [explosionSprite runAction:trapDetonationSequence];
        [[SimpleAudioEngine sharedEngine] playEffect:@"OD_SFX_MiniExplosion.wav"];
    }
}

-(void) dealloc
{
    [repeatingShakeScreenAction release];
    [explodeAnimation release];
    [targetedEnemies removeAllObjects]; //Just in case resetOverdrive is not called (eliminates Foundation retains).
    [targetedEnemies release];
    [super dealloc];
}

-(void) resetOverdrive
{
    [self unscheduleUpdate];
    [targetedEnemies removeAllObjects];
    curDetonationCount = 0;
    
    //Restore the behavior of the health displays for the enemies to their defaults.
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {
        //Only mess with the alive enemies. The dead ones have performed color modulation
        //previously, and we don't want to accidentally screw that up.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.DisplayHealthMasterOverride = NO;
            curEnemy.AllowHealthDisplay = YES;
            curEnemy.EnemySprite.color = ccWHITE;
        }
    }
    
    //This stop action MUST cause "trapDetonationSequence" to release itself.
    //(which should happen since it's on autorelease. By doing a "stopAction"
    //we remove a reference count that was put in place by "explosionSprite"
    //during the [runAction:] method. If this does not happen, we will have a
    //memory leak.
    [explosionSprite stopAction:trapDetonationSequence];
    [gameplayLayerRef stopAction:repeatingShakeScreenAction];
    [gameplayLayerRef setPosition:CGPointZero];
    
    [explosionSprite setPosition:ccp( gRangeRand(25, 300), gRangeRand(70, 300) )];
    
    //Make sure the player can't see the explosion sprite until we play the
    //exploding animation again.
    explosionSprite.visible = NO;

}




/**********************************************************************************************************
 * Upon running the OverdriveExecution, it is safe to assume that the TouchControlLayer
 * has all it's components deactivated. This is because when OVERDRIVE_FINISH_ANIMATION event
 * is sent, the game doesn't activate the components if the overdrive animation is a HIT
 * (or in other words, if the overdrive execution is expected to run right after the overdrive animation).
 * We can then activate the reticle (decativated by the TouchControlLayer) for the aiming phase of this
 * overdrive. 
 * Granted, all we need to do is fire the OVERDRIVE_JEROME_START_AIM_PHASE to enable all of this. We don't
 * perform these changes directly.
 * At the end of the Overdrive Execution, the TouchControlLayer components will reactivate. It is a proper
 * thing to do to send OVERDRIVE_JEROME_END_AIM_PHASE in order to reset the reticle to it's previous state
 * before getting automatically reenabled.
 **********************************************************************************************************/
-(void) executeOverdrive
{
    const float MAX_AIM_TIME = 4.0f;
    [self scheduleUpdate];

    
    //This is where we activate the overdrive to start moving the gears.
    //At this point, everything should be loaded, and the only thing needed to
    //do is to put the execution in motion by calling some sort of 'play' feature.
    CCLOG(@"Executing Overdrive!");
    
    //Enable the master override health display and customize behavior for the overdrive.
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {
        //Only mess with the alive enemies. The dead ones have performed color modulation
        //previously, and we don't want to accidentally screw that up.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.DisplayHealthMasterOverride = YES;
            curEnemy.AllowHealthDisplay = NO;
        }
    }
    
    //Set the timer
    currentAimTime = MAX_AIM_TIME;
    
    //Acquire the gameplay background layer to darken
    CCLayer* gameplayLayerBackground = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND, [NSValue valueWithPointer:&gameplayLayerBackground]);
    NSAssert(nil != gameplayLayerBackground,@"Gameplay layer background reference is nil. No gameplay layer background found.");
    
    //set up the actual black layer w/ all the actions
    blackLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255)
                                                      width:[CCDirector sharedDirector].screenSize.width
                                                     height:[CCDirector sharedDirector].screenSize.height];
    
    CCFadeOut* fadeOutEffect = [CCFadeOut actionWithDuration:MAX_AIM_TIME];
    
    CCCallFunc* finalizeOverdriveCallback = [CCCallFunc actionWithTarget:self
                                                                selector:@selector(finalizeOverdriveExecution)];
    
    [blackLayer runAction: [CCSequence actionOne:fadeOutEffect
                                             two:finalizeOverdriveCallback]];
    
    //Play the lock-on sound. the sound length is intended to be as long as the FadeOut time
    [[SimpleAudioEngine sharedEngine] playEffect:@"OD_SFX_JohnsonLockOn.wav"];
    
    
    //Add the layer into the engine.
    [gameplayLayerBackground addChild:blackLayer];
    
    
    //Start the aiming phase.
    //as well as allow leo to move.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JEROME_START_AIM_PHASE
                                                        object:nil];
    
    //Grab the length of the radius. In this case, we use half the width because we know that the reticle is circular
    //and that the reticle texture takes up the entire canvas of the image.
    //We do this here because it is right after the OVERDRIVE_JEROME_START_AIM_PHASE, which changes the display frame
    //of the aim to the overdrive reticle.
    reticleRadius = aimRef.displayFrame.rect.size.width/2;

}

-(void) finalizeOverdriveExecution
{
    //Let's release control of Leo and eliminate the line of fire
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JEROME_END_AIM_PHASE
                                                        object:nil];
    
    //Acquire the gameplay background layer to remove...
    CCLayer* gameplayLayerBackground = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND
                                                        object:[NSValue valueWithPointer:&gameplayLayerBackground]];
    NSAssert(nil != gameplayLayerBackground,@"Gameplay layer background reference is nil. No gameplay layer background found.");

    //Remove the darkening layer.
    [gameplayLayerBackground removeChild:blackLayer cleanup:YES];
    

    //Now we perform the explosion
    [self executeExplosionAnimation];
}

-(void) update:(ccTime)delta
{
    //We don't mess with the player aim. We only query it in order to know what enemies will get affected by
    //the overdrive explosion.
    
    //With the radius, we can now cycle through all the enemies and check their positions.
    //If they are:
    //1) Alive
    //2) Within the radius
    //Then, we tint them to a red color.
    //We need to check for distance between the enemy and the reticle radius.
    //We could do distance formula and stuff, but that's overkill for a loop running
    //within an update. Let's instead use raw bounds checking.
    const short reticleXMin = aimRef.position.x - reticleRadius;
    const short reticleXMax = aimRef.position.x + reticleRadius;
    const short reticleYMin = aimRef.position.y - reticleRadius;
    const short reticleYMax = aimRef.position.y + reticleRadius;
    
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {

        BOOL isWithinReticle = NO;
        if( curEnemy.position.x >= reticleXMin and
            curEnemy.position.x <= reticleXMax and
            curEnemy.position.y >= reticleYMin and
            curEnemy.position.y <= reticleYMax)
        {
            isWithinReticle = YES;
        }
        
        if( curEnemy.CurrentHealth > 0 )
        {
            if( YES == isWithinReticle )
            {
                //We found the enemy, let's tag it somehow, so we know to kill it
                //when the explosion animations are out.
                [targetedEnemies addObject:curEnemy];
                curEnemy.AllowHealthDisplay = YES;
                curEnemy.EnemySprite.color = ccRED;
            }
            else
            {
                //The enemy is not in the reticle.
                //Let's make sure the enemy wasn't previously in the reticle. If it was,
                //we need to remove it from the targeted list.
                NSUInteger indexOfObject = [targetedEnemies indexOfObject:curEnemy];
                if( NSNotFound != indexOfObject)
                {
                    [targetedEnemies removeObjectAtIndex:indexOfObject];
                    curEnemy.AllowHealthDisplay = NO;
                    curEnemy.EnemySprite.color = ccWHITE;
                }
            }
        }
    }
    
}

@end
