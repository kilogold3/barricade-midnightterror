//
//  StatusBarColorCycler.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/27/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "StatusBarColorCycler.h"
#import "ColorSpaceUtilities.h"


const int ACTION_TAG_HUE = 7777;
const int ACTION_TAG_BRIGHTNESS = 6666;

@implementation StatusBarColorCycler
@synthesize IsActive=isActive;
@synthesize CurrentBrightness=currentBrightness;
@synthesize CurrentHue=currentHue;
@synthesize CurrentSaturation=currentSaturation;
@synthesize MinBrightness;
@synthesize MaxBrightness;
@synthesize BrightnessTweenDuration;
@synthesize HueTweenDuration;

-(void) setIsActive:(BOOL)IsActive
{
    isActive = IsActive;
    
    if( YES == isActive )
        [self activateColorMode];
    else
        [self deactivateColorMode];
}

-(id) init
{
    self = [super init];
    if( self != nil )
    {
        //Set the starting values for the coloring
        currentBrightness = 1.0f;
        currentSaturation = 1.0f;
        currentHue = 0;
        self.MinBrightness = 0.5f;
        self.MaxBrightness = 1.0f;
        self.HueTweenDuration = 0.15f;
        self.BrightnessTweenDuration = 0.15f;
    }
    return self;
}

-(ccColor3B) getCurrentColorRGB
{
    float r,g,b;
    HSL2RGB(currentHue, currentSaturation, currentBrightness, &r, &g, &b);
    return ccc3(r*255.0f, g*255.0f, b*255.0f);
}

-(void) activateColorMode
{
    isActive = YES;
    
    //setup brightness tweening
    CCActionTween* tweenBrightness = [CCActionTween actionWithDuration:self.BrightnessTweenDuration
                                                                   key:@"currentBrightness"
                                                                  from:self.MinBrightness
                                                                    to:self.MaxBrightness];
    CCSequence* tweenBrightnessSequence = [CCSequence actionOne:tweenBrightness
                                                            two:[tweenBrightness reverse]];
    CCRepeatForever* repeatingActionBrightness = [CCRepeatForever actionWithAction:tweenBrightnessSequence];
    repeatingActionBrightness.tag = ACTION_TAG_BRIGHTNESS;
    [self runAction: repeatingActionBrightness];
    
    //setup hue tweening
    CCActionTween* tweenHue = [CCActionTween actionWithDuration:self.HueTweenDuration
                                                            key:@"currentHue"
                                                           from:0.0f
                                                             to:1.0f];
    CCSequence* tweenHueSequence = [CCSequence actionOne:tweenHue
                                                      two:[tweenHue reverse]];
    CCRepeatForever* repeatingActionHue = [CCRepeatForever actionWithAction:tweenHueSequence];
    repeatingActionHue.tag = ACTION_TAG_HUE;
    [self runAction: repeatingActionHue];
}

-(void) deactivateColorMode
{
    isActive = NO;
    
    //Stop color animation
    [self stopActionByTag:ACTION_TAG_BRIGHTNESS];
    [self stopActionByTag:ACTION_TAG_HUE];
}

@end
