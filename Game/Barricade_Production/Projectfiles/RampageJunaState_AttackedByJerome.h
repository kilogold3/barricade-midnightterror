//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"


class b2Fixture;
@class EnemyJuna;
@class GB2Contact;
@class Barricade;

@interface RampageJunaState_AttackedByJerome : NSObject<IEnemyState>
{
    EnemyJuna* enemyParent;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
@end
