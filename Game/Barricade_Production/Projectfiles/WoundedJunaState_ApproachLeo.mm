//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WoundedJunaState_ApproachLeo.h"
#import "CCBReader.h"
#import "OverdriveSequence.h"
#import "NotificationCenterEventList.h"
#import "EnemyJuna.h"
#import "EnemyStateFactory.h"
#import "Player.h"
#import "PlayerState_Alive.h"
#import "GB2ShapeCache.h"
#import "CCAnimation+SequenceLoader.h"
#import "CGPointExtension.h"
#import "TouchControlLayer.h"

@implementation WoundedJunaState_ApproachLeo

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyJuna class]],@"Wrong enemy parent type");
        enemyParent = (EnemyJuna*) enemyParentIn;
        
        //Acquire player reference
        playerRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                            object:[NSValue valueWithPointer:&playerRef]];
        NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
        
        //Acquire GameplayScene's ScreenForegroundLayer
        screenForegroundLayerRef = nil;
        NSValue* screenForegroundLayerRefValue = [NSValue valueWithPointer:&screenForegroundLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER
                                                            object: screenForegroundLayerRefValue];
        NSAssert(screenForegroundLayerRef != nil, @"Failed to acquire ScreenForegroundLayer");
        
        
        //Acquire GameplayLayer's GameplayForegroundLayer
        gameplayForegroundLayerRef = nil;
        NSValue* gameplayForegroundLayerRefValue = [NSValue valueWithPointer:&gameplayForegroundLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_FOREGROUND
                                                            object: gameplayForegroundLayerRefValue];
        NSAssert(gameplayForegroundLayerRef != nil, @"Failed to acquire ScreenForegroundLayer");
        
        enemyParent.MovementSpeed = 15;
    }
    
    return self;
}

-(void) enter
{
    //Move the player into position
    NSAssert([playerRef.CurrentPlayerState isKindOfClass:[PlayerState_Alive class]], @"Player is in wrong type of state");
    PlayerState_Alive* playerState = static_cast<PlayerState_Alive*>(playerRef.CurrentPlayerState);
    [playerState setMovementDestination:ccp(playerRef.position.x, enemyParent.position.y)];

    //Load animation data:
    //Set the display frame for the walker
    [enemyParent.EnemySprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"EvilJunaDying0.png"]];
    
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    //Load animation
    const short animationFrameCount = 6;
    float animationDelay = (25 / enemyParent.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"EvilJunaDying%d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
        
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    repeatingWalkingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody
                                           forShapeName:@"enemyWalkerWalkShape"];
    
    //Animate sprite
    [enemyParent.EnemySprite runAction:repeatingWalkingAnimationAction];
    
    //Set the current frame as the flinch frame
    flinchFrameID = enemyParent.EnemySprite.displayFrame.rect.origin;
    
    //Place the widescreen bars
    const CGSize screenSize = [CCDirector sharedDirector].screenSize;

    upperBar = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255)
                                                    width:screenSize.width
                                                   height:70];
    lowerBar = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255)
                                                    width:screenSize.width
                                                   height:70];
    
    
    [gameplayForegroundLayerRef addChild:upperBar];
    [screenForegroundLayerRef addChild:lowerBar];
    
    upperBar.ignoreAnchorPointForPosition = NO;
    lowerBar.ignoreAnchorPointForPosition = NO;
    
    [upperBar setPosition:ccp(0,screenSize.height)];
    [upperBar setAnchorPoint:ccp(0,0)];
    
    [lowerBar setAnchorPoint:ccp(0,1)];
    [lowerBar setPosition:ccp(0,0)];
    
    [self presentWidescreenBars];
    
    //At this point, we won't need to use the controls.
    //We can disable them.
    
    //Acquire GameplayScene's TouchControlLayer
    TouchControlLayer* touchControlLayerRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER
                                                        object: [NSValue valueWithPointer:&touchControlLayerRef]];
    NSAssert(touchControlLayerRef != nil, @"Failed to acquire TouchControlLayer");
    [touchControlLayerRef deactivateSelfAndAllComponents];
}

-(void) presentWidescreenBars
{
    const float widescreenBarHeight = 70;
            
    [upperBar runAction:[CCMoveBy actionWithDuration:0.8f position:ccp(0,-widescreenBarHeight)]];
    [lowerBar runAction:[CCMoveBy actionWithDuration:0.8f position:ccp(0,widescreenBarHeight)]];
}

-(void) dismissWidescreenBars
{
    const float widescreenBarHeight = 70;
    
    [upperBar runAction:[CCMoveBy actionWithDuration:0.8f position:ccp(0,widescreenBarHeight)]];
    [lowerBar runAction:[CCMoveBy actionWithDuration:0.8f position:ccp(0,-widescreenBarHeight)]];
}

-(void) exit
{
    //[self dismissWidescreenBars];
    
    //Stop animating
    [enemyParent stopAction:repeatingWalkingAnimationAction];
}

-(void) update: (float) deltaTime
{
    //If we reached the target...
    if( enemyParent.position.x >= playerRef.position.x - 55)
    {
        //Let's switch to the state where we are getting attacked by Johnson
        [enemyParent changeEnemyState:
         [enemyParent.AI_StateFactory createWoundedJunaState_AttackedByJohnson]];
    }
    else
    {
        //If we are on a frame where Juna is not flinching...
        if ( NO == CGPointEqualToPoint( enemyParent.EnemySprite.displayFrame.rect.origin, flinchFrameID))
        {
            //Move forward
            CGPoint newPosition = enemyParent.position;
            
            newPosition.x += enemyParent.MovementSpeed * deltaTime;
            
            [enemyParent setPosition:newPosition];
        }
    }
}
@end
