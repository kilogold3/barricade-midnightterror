//
//  TravelLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "ActivityBase.h"

@class CCControlButton;

@interface TravelLayer : ActivityBase
{
    CCMenu* travelNodeMenu;
    CCControlButton* returnButton;
}

-(void) QuitTravel: (id) sender;
-(void) NewProgressionSelected: (id) sender;

-(void) ResourceGatheringTravelPromptEnabled_EventHandler: (NSNotification*) notification;
-(void) ResourceGatheringTravelPromptDisabled_EventHandler: (NSNotification*) notification;

@end
