//
//  BarricadeRepairPiece.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "CCControlButton.h"

@class GB2Contact;

@interface BarricadeRepairFragment : CCControl
{
@private
    //the offset amount away from the player's finger
    CGPoint touchPositionOffset;
    
@protected
    //Fragment image
    CCSprite* fragmentSprite;
    
    //Original location where the fragment is located
    CGPoint startLocation;
    
    //physics body for collision detection
    b2Body* physicsBody;
    
    //Collision counters
    int blueprintCollisionCounter;

@public
    
    UITouch * _curTouch;
    
}

@property (nonatomic,readwrite) CGPoint StartLocation;


-(void) beginContactWithBarricadeRepairBlueprint: (GB2Contact*)contact;
-(void) endContactWithBarricadeRepairBlueprint: (GB2Contact*)contact;
-(void) configureFragmentWithTagID: (int) tagID;
-(void) resetFragmentToOriginalState;

@end
