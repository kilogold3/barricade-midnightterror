//
//  GameplayPauseLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplayScenePauseLayer.h"
#import "NotificationCenterEventList.h"
#import "AppWidescreener.h"

//DEBUG---
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "ResourceGatheringScene.h"
#import "Aim.h"
#import "CCControlButton.h"
#import "CCBReader.h"
#import "DaysLeftTransitionScene.h"


@interface GameplayScenePauseLayer (PrivateMethods)
// declare private methods here
@end

@implementation GameplayScenePauseLayer


-(void) didLoadFromCCB
{
    aimRef = nil;
    NSValue* aimRefValue = [NSValue valueWithPointer:&aimRef];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_AIM_TARGET object:aimRefValue];
    
    
    switch (aimRef.AimMode)
    {
        case AIM_MODE_ABSOLUTE:
            [btnToggleAim setTitle:@"Aim: Absolute"forState:CCControlStateNormal];
            break;
        case AIM_MODE_RELATIVE:
            [btnToggleAim setTitle:@"Aim: Relative"forState:CCControlStateNormal];
            break;
        case AIM_MODE_OFFSET:
            [btnToggleAim setTitle:@"Aim: Offset"forState:CCControlStateNormal];
            break;
        default:
            break;
    }
    
    REGISTER_EVENT(self,
                   @selector(uiDismissQuitPrompt_EventHandler:),
                   EventList::UI_DISMISS_QUIT_PROMPT);
}

-(void) cleanup
{
    UNREGISTER_EVENT(self, EventList::UI_DISMISS_QUIT_PROMPT);
}

-(void) uiDismissQuitPrompt_EventHandler: (NSNotification*) notification
{
    [self SetButtonsToEnabled:YES];
}

-(void) resumeGameCallback
{
    [self removeFromParentAndCleanup:YES];

    SEND_EVENT(EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED, nil);
}

-(void) quitGameCallback
{
    //The quit button was pressed...
    
    //Let's disable all current buttons.
    [self SetButtonsToEnabled:NO];
    
    //let's make sure to prompt the player.
    CCNode* quitPrompt = [CCBReader nodeGraphFromFile:@"QuitToMainMenuConfirmationScreen.ccbi"];
    [AppWidescreener centralizeSceneOnScreen:quitPrompt];
    [self addChild:quitPrompt];
}

-(void) SetButtonsToEnabled: (BOOL) isEnabled
{
    for (CCControlButton* button in self.children)
    {
        //if the current item is actually a button...
        if( [button isKindOfClass:[CCControlButton class]] )
        {
            //shut it off
            static_cast<CCControlButton*>(button).enabled = isEnabled;
        }
    }
}

-(void) ToggleAimMode
{
    switch (aimRef.AimMode)
    {
        case AIM_MODE_RELATIVE:
            aimRef.AimMode = AIM_MODE_ABSOLUTE;
            [btnToggleAim setTitle:@"Aim: Absolute"forState:CCControlStateNormal];
            break;
        case AIM_MODE_ABSOLUTE:
            aimRef.AimMode = AIM_MODE_OFFSET;
            [btnToggleAim setTitle:@"Aim: Offset"forState:CCControlStateNormal];
            break;
         
        case AIM_MODE_OFFSET:
            aimRef.AimMode = AIM_MODE_RELATIVE;
            [btnToggleAim setTitle:@"Aim: Relative"forState:CCControlStateNormal];
            break;
            
        default:
            break;
    }
}

-(void) toBarricadeRepairCallback
{
    //Save game data
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::DATA_SAVE_BARRICADE_HEALTH
                                                        object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::DATA_SAVE_PLAYER
                                                        object:nil];
    
    SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    
    if( [[resourceGatheringProperties valueForKey:GSP::CURRENT_PROGRESSION] intValue] >= 1 )
    {
    const int currentOverallDay = [[resourceGatheringProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
    [resourceGatheringProperties setValue:[NSNumber numberWithInt:currentOverallDay+1]
                                   forKey:GSP::CURRENT_OVERALL_DAY];
    }

    
    //Create a transition scene to display how many days are left.
    CCScene* nextScene = [CCScene node];
    CCNode* daysLeftScene = [CCBReader nodeGraphFromFile:@"DaysLeftTransitionScene.ccbi"];
    static_cast<DaysLeftTransitionScene*>(daysLeftScene).TransitioningScene = [ResourceGatheringScene class];
    
    [nextScene addChild:daysLeftScene];
    
    CCTransitionScene* fadeTransition = [CCTransitionFade transitionWithDuration:0.8f scene: nextScene];
    [[CCDirector sharedDirector] replaceScene:fadeTransition];
}

@end
