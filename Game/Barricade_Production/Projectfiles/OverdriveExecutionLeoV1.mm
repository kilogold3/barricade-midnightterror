//
//  OverdriveExecutionYunaV1.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OverdriveExecutionLeoV1.h"
#import "PlayerShadowClone.h"
#import "NotificationCenterEventList.h"

@implementation OverdriveExecutionLeoV1
-(void) executeOverdrive
{
    [[PlayerShadowClone node] setPosition:ccp(-15,50)];
    [[PlayerShadowClone node] setPosition:ccp(15,-50)];
        
    [self resetOverdrive];
    
    //Finish the overdrive
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_FINISH_EXECUTION
                                                        object:nil];
}
-(void) resetOverdrive
{
    //Remove the execution so it can be added again later.
    //The execution is persistent and will rely on a 'reset'
    //feature to manage itself.
    [self.parent removeChild:self cleanup:YES];
    CCLOG(@"LEO Overdrive Execution Complete!");
}
@end
