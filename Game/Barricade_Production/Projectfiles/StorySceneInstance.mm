//
//  StorySceneInstance.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/12/13.
//
//

#import "StorySceneInstance.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "DialogueScene.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"
#import "DaysLeftTransitionScene.h"
#import "AppWidescreener.h"

@implementation StorySceneInstance
@synthesize NextSceneName=nextSceneName;

-(id) init
{
	if ((self = [super init]))
	{
		CCLOG(@"%@ init", NSStringFromClass([self class]));
    }
    
    return self;
}

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    animationManagerRef = self.userObject;
    animationManagerRef.delegate = self;
    totalTimelineSequencesForScene = animationManagerRef.sequences.count;
    currentTimelineSequence = 0;
    [self scheduleUpdate];
}

-(void) cleanup
{
    [super cleanup];
    
    // Clear out delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    CCLOG(@"Completed StoryScene Sequence %@", name);
}

-(void) runNextSequence
{
    ++currentTimelineSequence;
    
    //If we have reached the end of all the timelines for this scene...
    if( currentTimelineSequence >= totalTimelineSequencesForScene )
    {
        //We have reached the timeline count limit. There is nowhere to advance.
        //Let's disable the input.
        [self unscheduleUpdate];
        
        
        //We must check if there are any other scenes we will be transitioning to
        //before considering that the cutscene has concluded.
        if( [self.NextSceneName isEqualToString:@"END"] )
        {
            SaveObjectPropertyList* gameProgressProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
            const short currentOverallDay = [[gameProgressProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
            [KKConfig selectKeyPath:@"GameSettings"];
            const short oleanderCityDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
            const short currentProgression = [[[[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS] objectForKey:GSP::CURRENT_PROGRESSION] intValue];
            
            
            //If we are NOT at the final cutscene [AND] we are NOT executing a bad ending...
            if( currentProgression < GSPV::FINAL_PROGRESSION and currentOverallDay <= oleanderCityDaysLimit )
            {
                //Let's go to the dialogue scene.
                //However, if we happen to be in the Vejigante Cutscene (02A), we'll want to transition
                //using a "Days Left" transition to indicate to the player that a day has gone by.
                const Byte VEJIGANTE_CUTSCENE_PROGRESSION = 3;
                if( VEJIGANTE_CUTSCENE_PROGRESSION == currentProgression )
                {
                    //Setup transitioning effect
                    CCNode* daysLeftScene = [CCBReader nodeGraphFromFile:@"DaysLeftTransitionScene.ccbi"];
                    static_cast<DaysLeftTransitionScene*>(daysLeftScene).TransitioningScene = [DialogueScene class];
                    CCScene* cocos2dSceneInstance = [CCScene node];
                    [cocos2dSceneInstance addChild:daysLeftScene];
                    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.8f
                                                                                                  scene: cocos2dSceneInstance]];
                }
                else
                {
                    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration: 0.7f
                                                                                                  scene: [DialogueScene node]
                                                                                              withColor: ccBLACK]];
                }
            }
            else
            {
                //Let's go see the game stats
                CCScene* statsScene = [CCBReader sceneWithNodeGraphFromFile:[NSString stringWithFormat:@"StatsScene%@.ccbi",
                                                                             [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""]];
                
                                
                [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration: 0.7f
                                                                                              scene: statsScene
                                                                                          withColor: ccBLACK]];
                
                //At this point, the game is over. The player will eventually be taken back to the title screen
                //where they can load the game from the save data. If we load as-is, the game will load into the
                //last saved state, which is at progression 06A (the progression value used to load the final cutscene).
                //By doing this, the game will crash attempting to load a GP_06.ccbi, which does not exist since the fight with
                //MechaJuna takes place in progression 05A. To address this, we will reset the progression value (incremented by
                //WoundedJuna's death) to the last playable gameplay scene (05A) by undoing the previous scene's increment.
                [gameProgressProperties setValue:[NSNumber numberWithInt: currentProgression-1]
                                          forKey:GSP::CURRENT_PROGRESSION];
            }
        }
        //At this point, we understand that we have finished the current StorySceneInstance, but the
        //cutscene as a whole has not yet concluded. The value provided in NextSceneName should have
        //the filename of the next CCBI to be loaded which continues the cutscene. It's time to transition
        //into this next scene.
        else
        {
            CCLOG(@"Loading StorySceneInstance: %@",self.NextSceneName);
            [[SimpleAudioEngine sharedEngine] playEffect:@"CS_SFX_PageFlip.wav"];
            
            NSString* const nextSceneString = [self.NextSceneName stringByReplacingOccurrencesOfString: @".ccbi"
                                                                                            withString: ([AppWidescreener isWidescreenEnabled] ? @"__Wide.ccbi" : @"")];
            
            CCScene* const nextScene = [CCBReader sceneWithNodeGraphFromFile:nextSceneString];
            
            CCTransitionScene* transitionScene = [NSClassFromString(sceneTransitionClassType) transitionWithDuration: 1.0f
                                                                                                               scene: nextScene];
            
            [[CCDirector sharedDirector] replaceScene:transitionScene];
        }
    }
    else
    {
        NSString* sequenceName = [NSString stringWithFormat:@"SS%02d",currentTimelineSequence];
        [animationManagerRef runAnimationsForSequenceNamed:sequenceName];
        CCLOG(@"Starting Sequence %@", animationManagerRef.runningSequenceName);
    }
}

// scheduled update method
-(void) update:(ccTime)delta
{
    if(
       [[KKInput sharedInput] isAnyTouchOnNode:self
                                    touchPhase:KKTouchPhaseBegan] )
    {

        [self runNextSequence];
    }
}
@end
