//
//  IEnemyState.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EnemyBase;

@protocol IEnemyState <NSObject>
-(id) initWithEnemyParent: (EnemyBase*) enemyParent;
-(void) enter;
-(void) exit;
-(void) update: (float) deltaTime;
@end

typedef NSObject<IEnemyState> EnemyState;