//
//  AI_DecisionNode.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/18/13.
//
//

#import "AI_TreeNode.h"

typedef BOOL (^ DecisionBlock)(void);

@interface AI_DecisionNode : NSObject<AI_TreeNodeProtocol>
{
    AI_TreeNode* trueNode;
    AI_TreeNode* falseNode;
    
    DecisionBlock decisionBlock;
}
-(id) initWithTrueNode: (AI_TreeNode*) trueNodeIn
             FalseNode: (AI_TreeNode*) falseNodeIn
         DecisionBlock: (DecisionBlock) decisionBlockIn;

-(void) doAction;
@end
