//
//  Pair.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/20/13.
//
//

#import "Pair.h"

@implementation Pair
@synthesize Object1=object1;
@synthesize Object2=object2;

-(id) initWithObject1: (id) object1In
           andObject2: (id) object2In
{
    self = [super init];
    
    if( nil != self)
    {
        object1 = object1In;
        [object1 retain];
        
        object2 = object2In;
        [object2 retain];
    }
    
    return self;
}

-(void) dealloc
{
    [object1 release];
    [object2 release];
    [super dealloc];
}
@end
