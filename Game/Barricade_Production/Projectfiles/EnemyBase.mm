//
//  EnemyBase.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyBase.h"
#import "Box2dPhysicsWorld.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "EnemyStateFactory.h"
#import "NotificationCenterEventList.h"
#import "Player.h"
#import "Barricade.h"
#import "GameOverScene.h"
#import "EnemyStateFactory.h"
#import "GMath.h"
#import "CCBReader.h"
#import "GameOverSceneTransitioner.h"

#import "EnemyMechaJuna.h"

@implementation EnemyBase
@synthesize EnemyPhysicsBody=enemyPhysicsBody;
@synthesize CurrentHealth=currentHealth;
@synthesize MaxHealth=maxHealth;
@synthesize AI_StateFactory=enemyStateFactory;
@synthesize AttackDamage=attackDamage;
@synthesize OverdriveReward=overdriveReward;
@synthesize PlayerRef=playerRef;
@synthesize BarricadeRef=barricadeRef;
@synthesize TargetedCount=targetedCount;
@synthesize AllowHealthDisplay=allowHealthDisplay;
@synthesize CurrentEnemyState=currentState;
@synthesize NextEnemyState=nextState;
@synthesize WeaponBoostReward=weaponBoostReward;
@synthesize MovementSpeed=movementSpeed;
@synthesize DisplayHealthMasterOverride;
@synthesize EnemySprite=enemySprite;

-(void) setTargetedCount:(Byte)TargetedCount
{
    targetedCount = TargetedCount;
    
    if( targetedCount < 0 )
    {
        targetedCount = 0;
    }
    
    
    displayHealth = (targetedCount > 0);
}

-(void) setCurrentHealth:(short)CurrentHealthIn
{
    currentHealth = CurrentHealthIn;
    
    if( currentHealth > maxHealth )
        currentHealth = maxHealth;
    
    if( currentHealth <= 0 )
    {
        currentHealth = 0;
    }
}

-(void) killEnemy
{
    NSAssert(false, @"This method must be overriden by a child class.");
    NSLog(@"ERROR: Non-implemented method call: setCurrentHealth");
}

-(void) OverdriveJeromeDamageEnemies_EventHandler: (NSNotification*) notification
{
    //If the enemy is alive (we don't want to kill the enemy twice)...
    if( currentHealth > 0 )
    {
        //Make sure the enemy is targeted to die from the attack.
        //Check the list of targeted enemies to find an entry.
        NSArray* targetedEnemies = [notification object];
        if( [targetedEnemies indexOfObject:self] != NSNotFound)
        {
            [KKConfig selectKeyPath:@"OverdriveSettings"];
            const float minDamage = [KKConfig floatForKey:@"JeromeOverdriveDamageMin"];
            const float maxDamage = [KKConfig floatForKey:@"JeromeOverdriveDamageMax"];
            self.CurrentHealth = self.CurrentHealth - gRangeRand(minDamage, maxDamage);
            
            if( self.CurrentHealth <= 0 )
            {
                [self killEnemy];
            }
        }
    }
}

-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    BOOL isOverdriveHit = [[notification object] boolValue];
    
    //If overdrive is NOT a HIT
    if( NO == isOverdriveHit )
    {
        //The overdrive is a MISS.
        //Let's reactivate and operate as normal
        [self scheduleUpdate];
    }
    else
    {
        //do nothing. Leave everything as is.
        //Updating should remain disabled until the
        //end of the OverdriveExecution.
    }
}

-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification
{
    //Stop updating the enemy 
    [self unscheduleUpdate]; 
}

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification
{
    //continue updating the enemy (this keeps updating the other
    //states involved with the enemy.)
    [self scheduleUpdate];
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    //Stop updating the enemy
    [self unscheduleUpdate];
}

-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
}

-(void) DebugPrintEnemyHealth_EventHandler: (NSNotification*) notification
{
}

-(void) loadBasicAttributes: (NSString*) enemySettingsTableName
{
    if( [KKConfig selectKeyPath:enemySettingsTableName] )
    {
        maxHealth = gRangeRand( [KKConfig floatForKey:@"MinStartHealth"], [KKConfig floatForKey:@"MaxStartHealth"] );
        movementSpeed = gRangeRand( [KKConfig floatForKey:@"MinMovementSpeed"], [KKConfig floatForKey:@"MaxMovementSpeed"] );
        overdriveReward = [KKConfig floatForKey:@"OverdriveReward"];
        weaponBoostReward = [KKConfig floatForKey:@"WeaponBoostReward"];
        attackDamage = [KKConfig intForKey:@"AttackDamage"];
        currentHealth = maxHealth;
    }
    else
    {
        NSAssert(false, @"Did [NOT] find config.lua keypath");
    }
}


-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        self.DisplayHealthMasterOverride = NO;
        nextState = nil;
        displayHealth = NO;
        allowHealthDisplay = YES;
        contactWithPlayerCounter = 0;
        targetedCount = 0;
        
        //Register Event Listeners
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(DebugPrintEnemyHealth_EventHandler:)
                                                     name:EventList::DEBUG_PRINT_ENEMY_HEALTH
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveJeromeDamageEnemies_EventHandler:)
                                                     name:EventList::OVERDRIVE_JEROME_DAMAGE_ENEMIES
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveExecutionFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationStart_EventHandler:)
                                                     name:EventList::OVERDRIVE_START_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsResumeGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                   object:nil];
        
        //Acquire player reference
        playerRef = nil;       
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET 
                                                            object:[NSValue valueWithPointer:&playerRef]];
        NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
        
        //Acquire barricade reference
        barricadeRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET 
                                                            object:[NSValue valueWithPointer:&barricadeRef]];
        NSAssert(nil != barricadeRef,@"Barricade reference is nil. No barricade found.");
                
        //init data
        enemyStateFactory = [[EnemyStateFactory alloc] initForEnemy:self];
        
        b2BodyDef bodyDef;
        bodyDef.position = b2Vec2( 0,0);
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        enemyPhysicsBody = [GB2Engine sharedInstance].world->CreateBody(&bodyDef);
                
        [self scheduleUpdate];
    }
    
    return self;
}

-(void) cleanup
{
    [enemyStateFactory release];
    
    //Unregister event listeners.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::DEBUG_PRINT_ENEMY_HEALTH
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_JEROME_DAMAGE_ENEMIES
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                  object:nil];
    
    //Clean up states, if necessary
    if( nextState != nil )
    {
        [nextState release];
    }
    
    if( currentState != nil )
    {
        [currentState exit];
        [currentState release];
    }
    
    //Destroy physics data
    
    //Set the body's user data to NULL. This is because the contact listener detaches contacts.
    //Once it does that, it attempts to call the "endContact" method for each body's user data.
    //With a NULL value, objective-c will send a message to NULL which has no
    //effect, as opposed to sending the message to some invalid instance address, which will crash.
    //In this particular case, we don't need "endContact" for anything, so we are able to do this.
    enemyPhysicsBody->SetUserData(NULL);
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:enemyPhysicsBody];
    enemyPhysicsBody = NULL;
    
    [super cleanup];
}
-(void) dealloc
{
    CCLOG(@"Dealloc: EnemyBase");



    [super dealloc];
}

-(void) changeEnemyState: (EnemyState*) newState
{
    //Make sure we are not trying to change to a state we are already in.
    if( [currentState isKindOfClass:newState])
    {
        CCLOG(@"WARNING: Enemy %@ tried to change states to the same state it is currently in. Ignoring.",self);
        return;
    }
    
    //If we already have a state we will be transitioning to, we will discard the first
    //state, and use this incoming one as the new state to transition to. 
    if( nil != nextState )
    {
        CCLOG(@"WARNING: Enemy %@ tried to change states more than once per frame.\n"
              "Discarding the first and using the second.\n"
              "First: %@ \nSecond: %@",
              self,
              NSStringFromClass([nextState class]),
              NSStringFromClass([newState class]));
        
        [nextState release];
    }
    
    nextState = newState;
    [nextState retain];
}

-(void) instantChangeEnemyState: (EnemyState*) newState
{    
    if( currentState != nil )
    {
        [currentState exit];
        [currentState release];
    }
    
    currentState = newState;
    [currentState retain];
    
    [currentState enter];
}

-(void) update: (ccTime) deltaTime
{
    //Update the current state
    if( nil != currentState )
    {
        [currentState update:deltaTime];
    }
    
    //Apply the next state, if available
    if( nil != nextState )
    {
        [self instantChangeEnemyState: nextState];
        [nextState release];
        nextState = nil;
    }
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    NSAssert(false, @"This method must be overriden by a child class.");
    NSLog(@"ERROR: Non-implemented method call: reactToWeaponDamage");
}

-(void) reactToTrapDamage: (TrapBase*) detonatingTrap
{
    NSAssert(false, @"This method must be overriden by a child class.");
    NSLog(@"ERROR: Non-implemented method call: reactToTrapDamage");
}

-(void) beginContactWithPlayer: (GB2Contact*)contact
{
    ++contactWithPlayerCounter;
    if( 1 == contactWithPlayerCounter )
    {
        //We've collided with the player
        [[CCDirector sharedDirector] replaceScene: [GameOverSceneTransitioner node] ];
    }
    
}
-(void) endContactWithPlayer: (GB2Contact*)contact
{
    --contactWithPlayerCounter;
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    //Set the CCNode's position (world & local should match)...
    [super setPosition:positionIn];
    
    CGPoint physicsCoordinates = positionIn;
    
    if( nil != self.parent )
    {
        physicsCoordinates = [self.parent convertToWorldSpace:positionIn];
    }
    
    enemyPhysicsBody->SetTransform( b2Vec2FromCGPoint(physicsCoordinates), 0);
}

-(CGPoint) position
{
    
    
    CGPoint positionValue = CGPointFromb2Vec2( enemyPhysicsBody->GetTransform().p );
    
    //If we are in a hierarchy
    if( nil != self.parent )
    {
        //convert to node-coords
        positionValue = [self.parent convertToNodeSpace: positionValue];
    }
    
    return  positionValue;
}

-(void) draw
{
    //Early outs.
    //We don't want to do anything
    //with dead bodies
    if( currentHealth <= 0 )
        return;
    
    //If we turn off health display detection with the override,
    //we should be able to see the enemy's health regardless of whether
    //it is being aimed at or not.
    //
    //If DisplayHealthMasterOverride is enabled, the visibility control
    //for the enemy health is simply based on "allowHealthDisplay"
    if( NO == self.DisplayHealthMasterOverride)
    {
        if( NO == displayHealth )
            return;
    }
    
    if( NO == allowHealthDisplay )
        return;

    
    const int BAR_WIDTH = 30;
    const int BAR_HEIGHT = 2;
    const int BAR_OFFSET_Y = self.EnemySprite.contentSize.height;
    
    const int BAR_BOUNDS_LEFT = -BAR_WIDTH/2;
    const int BAR_BOUNDS_RIGHT = BAR_WIDTH/2;
    
    //Draw background fill
    ccDrawSolidRect(
                    ccp(
                        BAR_BOUNDS_LEFT,
                        BAR_OFFSET_Y),
                    ccp(
                        BAR_BOUNDS_RIGHT,
                        BAR_OFFSET_Y + BAR_HEIGHT),
                    ccc4FFromccc3B(ccRED)
                    );
    
    //Draw foreground fill
    ccDrawSolidRect(
                    ccp(
                        BAR_BOUNDS_LEFT,
                        BAR_OFFSET_Y ),
                    ccp(
                        BAR_BOUNDS_LEFT + (BAR_WIDTH * (float)currentHealth/(float)maxHealth),
                        BAR_OFFSET_Y + BAR_HEIGHT ),
                    ccc4FFromccc3B(ccGREEN)
                    );
}

@end
