//
//  MiniMapModule.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface MiniMapModule : CCSprite
{    
    //Specified from CocosBuilder
    float minimapLayerPositionX;
    float minimapLayerPositionY;
}

@end
