//
//  EnemyFactory.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EnemyBase;
@class EnemyStateFactory;

@interface EnemyFactory : NSObject
{
}
-(id) initWithDependencies;
-(EnemyBase*) createEnemyFromStringType: (NSString*) enemyType;
-(EnemyBase*) createWalker;
-(EnemyBase*) createVejigante;

@end
