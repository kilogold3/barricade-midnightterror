/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "AppDelegate.h"
#import "GameSaveData.h"
#import "SimpleAudioEngine.h"

@implementation AppDelegate

-(void) initializationComplete
{
#ifdef KK_ARC_ENABLED
	CCLOG(@"ARC is enabled");
#else
	CCLOG(@"ARC is either not available or not enabled");
#endif
    
    //Instantiate game data singleton
    //(It will internally register all event listeners)
    [GameSaveData sharedGameSaveData];
    CCLOG(@"GameSaveData initialized.");

    //Initialize the audio engine
    [SimpleAudioEngine sharedEngine];
    CCLOG(@"GameAudioData initialized.");

    //Set default clear color
    glClearColor(0, 0, 0, 1);
    
    //Seed the random for random number generation
    srand(time(nil));
    
    
    
    "1000"
    1000

}

-(id) alternateView
{
	return nil;
}

@end
