//
//  WeaponMineGun.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "WeaponBase.h"

@interface WeaponMineGun : WeaponBase
{
    float aimMaxArc;
    float aimMinArc;
}

@end
