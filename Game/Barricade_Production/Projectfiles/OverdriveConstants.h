//
//  OverdriveConstants.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/20/13.
//
//

#ifndef Barricade_Production_OverdrivesEnumList_h
#define Barricade_Production_OverdrivesEnumList_h

namespace OverdriveConstants
{
    
    
    //How many overdrives we have in the game.
    //An array holding all these overdrives is
    //stored in the Player class.
    enum OVERDRIVE_ARRAY_INDICES {
        OVERDRIVE_ID_JEROME_V1,
        OVERDRIVE_ID_JEROME_V2,
        OVERDRIVE_ID_JEROME_V3,
        OVERDRIVE_ID_YUNA_V1,
        OVERDRIVE_ID_YUNA_V2,
        OVERDRIVE_ID_YUNA_V3,
        OVERDRIVE_ID_LEO_V1,
        OVERDRIVE_ID_LEO_V2,
        OVERDRIVE_ID_LEO_V3,
        OVERDRIVE_ID_JONNY_V1,
        OVERDRIVE_ID_JONNY_V2,
        OVERDRIVE_ID_JONNY_V3,
        OVERDRIVE_ID_MAX_COUNT
    };
    
    //The different characters that perform the overdrives:

    enum OVERDRIVE_CHARACTERS {
        OVERDRIVE_CHARACTERS_JEROME,
        OVERDRIVE_CHARACTERS_LEO,
        OVERDRIVE_CHARACTERS_YUNA,
        OVERDRIVE_CHARACTERS_JONNY,
        OVERDRIVE_CHARACTERS_MAX_COUNT
    };
    
    //This array represents the same data as the enum above (OVERDRIVE_ARRAY_INDICES), but at iterable items.
    //Each enum value refers to the starting sector of a character's overdrives list.
    
    //Treat this array as a lookup table to find a specific character's overdrive.
    //example:
    //          OVERDRIVE_ARRAY_CHARACTER_OFFSETS[OVERDRIVE_CHARACTERS_JEROME] == OVERDRIVE_ID_JEROME_V1
    static const OVERDRIVE_ARRAY_INDICES OVERDRIVE_ARRAY_CHARACTER_OFFSETS[OVERDRIVE_CHARACTERS_MAX_COUNT] = {
        OVERDRIVE_ID_JEROME_V1,
        OVERDRIVE_ID_LEO_V1,
        OVERDRIVE_ID_YUNA_V1,
        OVERDRIVE_ID_JONNY_V1,
    };
}
#endif
