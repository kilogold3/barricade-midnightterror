//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WoundedJunaState_Dead.h"
#import "EnemyJuna.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "StoryScene.h"
#import "TouchControlLayer.h"
#import "NotificationCenterEventList.h"
#import "GameSaveDataPropertyList.h"
#import "GameSaveData.h"

@implementation WoundedJunaState_Dead

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyJuna class]],@"Wrong enemy parent type");
        enemyParent = (EnemyJuna*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    [enemyParent.EnemySprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"EvilJunaDeath0.png"]];
    
    //Load animation
    const short animationFrameCount = 4;
    const float animationDelay = 0.7f;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"EvilJunaDeath%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    
    //Run the CCSequence
    [enemyParent.EnemySprite runAction:  [CCSequence actions:
                                          [CCAnimate actionWithAnimation:dyingAnimation],
                                          [CCCallFunc actionWithTarget:self selector:@selector(transitionToCutscene)],
                                          nil]];
    
    //Acquire GameplayScene's TouchControlLayer and deactivate
    //the controls. The reason we are doing this here is because
    //when we execute Johnson's overdrive (in the previous state),
    //the controls get reactivated automatically, so we have to force them off again.
    TouchControlLayer* touchControlLayerRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER
                                                        object: [NSValue valueWithPointer:&touchControlLayerRef]];
    NSAssert(touchControlLayerRef != nil, @"Failed to acquire TouchControlLayer");
    [touchControlLayerRef deactivateSelfAndAllComponents];
}

-(void) exit
{
    //Explicitly remove self from the hierarchy
    //[enemyParent removeFromParentAndCleanup:YES];
}

-(void) update: (float) deltaTime
{
}

-(void) transitionToCutscene
{
    //since we are done with the vejigante, we won't be going into some resource gathering
    //to use the traveling backpack and so on. Instead, we will simply advance the progression
    //right here. This will take us to the final game cutscene.
    SaveObjectPropertyList* resourceGatheringSceneProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentProgression = [[resourceGatheringSceneProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
    [resourceGatheringSceneProperties setValue:[NSNumber numberWithInt:currentProgression+1]
                                        forKey: GSP::CURRENT_PROGRESSION];
    

    
    
    //Do the actual transition
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.5f scene:[StoryScene node]]];
}
@end
