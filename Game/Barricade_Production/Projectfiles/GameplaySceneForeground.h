//
//  GameplaySceneForeground.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface GameplaySceneForeground : CCLayer
{
@protected
@private
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;

@end
