//
//  WeaponPistol.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/11/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "WeaponPistol.h"
#import "NotificationCenterEventList.h"
#import "GB2Engine.h"
#import "Player.h"
#import "RayCastCallbackLineOfFire.h"
#import "LineOfFireEventArgs.h"
#import "EnemyFieldInstance.h"
#import "GameAudioList.h"
#import "SimpleAudioEngine.h"
#import "PlayerArms.h"
#import "PlayerState_Alive.h"


#import "GameplayLayer.h"
#import "Aim.h"

@implementation WeaponPistol

-(id) initWithDependencies: (Player*) gamePlayerRefIn
{
    self = [super initWithDependencies: gamePlayerRefIn];
    
    if( nil != self )
    {
        weaponBarrelOffsetMagnitude = 35.0f;

        rearArmSpriteID = @"newPistolBackArm.png";
        frontArmSpriteID = @"pistolfrontArm.png";
        weaponPanelInactiveSpriteID = @"WeaponSelect/GunNormal.png";
        weaponPanelActiveSpriteID = @"WeaponSelect/GunSelected.png";
        hudDataDisplayWeaponSpriteID = @"NEWHUDPistolAmmo2.png";
        weaponSpriteAnchor = ccp(0.91,0.7);
        
        //Set basic attributes
        weaponType = ePISTOL;
        
        if( [KKConfig selectKeyPath:@"WeaponSettings"] )
        {
            damage = [KKConfig intForKey:@"PistolDamage"];
        }
        else
        {
            NSAssert(false, @"Did [NOT] find config.lua keypath");
        }
        
        //Load weapon fire sound
        [[SimpleAudioEngine sharedEngine] preloadEffect:GAL::SND_WEAPON_PISTOL_FIRE];
    }
    
    return self;
}

-(void) dealloc
{
    [[SimpleAudioEngine sharedEngine] unloadEffect:GAL::SND_WEAPON_PISTOL_FIRE];
    [super dealloc];
}

-(void) pullTrigger: (NSNotification*) notification
{
    if( YES == printDebugData )
    {
        CCLOG(@"FIRE!");
    }
    
    //Send event that actually fires the weapon.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_WEAPON_FIRED
                                                        object:self];
}

-(void) releaseTrigger: (NSNotification*) notification
{
}

-(void) fireWeaponFromOrigin: (CGPoint) originPosition
{
    //Play weapon fire sound
    [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_WEAPON_PISTOL_FIRE];
    
    /////////////////////////////////////////////////////////////////////////////
    //Normally, we'd consume 1 round of ammo here.
    //We would use the properties and override the 'setCurrentAmmo'
    //with an empty body. Since each weapon is handling it's own firing
    //mechanism, we can avoid all the hastle and simply not modify the
    //ammo in any way. HAX BABY!!
    /////////////////////////////////////////////////////////////////////////////    
    
    //Project the target position all the way to the left bound
    //of the screen to simulate the bullet firing past the reticle.
    //We use the line equation to give us the projected point where
    //it intersects with the left screen bounds.
    CGPoint targetPosition = [self generateScreenLeftBoundProjectedPointFromLineOfFire:originPosition];
    
    //Perform raycast
    RayCastCallbackLineOfFire raycast;
    [GB2Engine sharedInstance].world->RayCast(&raycast,
                                              b2Vec2FromCGPoint(targetPosition),
                                              b2Vec2FromCGPoint(originPosition)
                                              );
    raycast.ApplyRaycastReaction( self );
    
    //Generate rotation and start-position data
    CGPoint originalBulletPosition = CGPointZero;
    float armRotation = -1;
    [self GetWeaponBarrelPositionBasedOnTarget:targetPosition
                                     andOrigin:originPosition
                         withBarrelPositionOut:&originalBulletPosition
                            withArmRotationOut:&armRotation];
        
    //Determine projectile destination
    CGPoint bulletPositionDestination;
    if( nil != raycast.getFrontmostEnemyFieldInstanceTarget() )
    {
        bulletPositionDestination = raycast.getIntersectionPoint();
    }
    else
    {
        bulletPositionDestination = targetPosition;
    }
    
    /////////////////////////////////////////////////////////////
    //Display muzzle flash effect on bullet's initial location
    //(Might want to optimize this a bit)
    /////////////////////////////////////////////////////////////
    {
        //Grab player arms reference
        NSAssert([gamePlayerRef.CurrentPlayerState class] == [PlayerState_Alive class],
                 @"Compatible player state for weapon initialization");
        const PlayerState_Alive* playerAliveState = (PlayerState_Alive*)gamePlayerRef.CurrentPlayerState;
        PlayerArms* playerArmsRef = playerAliveState.PlayerArmsRef;
        
        //Generate the position equivalent for PlayerArms coordinate system.
        const CGPoint muzzleFlashPositionPlayerArmsLocalCoords = [playerArmsRef convertToNodeSpace:originalBulletPosition];
        
        CCSprite* muzzleFlashSprite = [CCSprite spriteWithSpriteFrameName:@"ballisticMuzzleFlash.png"];
        muzzleFlashSprite.scale = 1.6f;
        muzzleFlashSprite.anchorPoint = ccp(0.6f,0.35f); //just a tiny tweak so it looks right.
        muzzleFlashSprite.position = muzzleFlashPositionPlayerArmsLocalCoords;
        muzzleFlashSprite.rotation = CC_RADIANS_TO_DEGREES(armRotation);
        [muzzleFlashSprite runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.08f]
                                                       two:[CCRemoveFromParentAction action]]];
        [playerArmsRef addChild:muzzleFlashSprite];
    }
    
    //Before continuing, we need to bring back to the bullet position & destination to node spaces.
    //The node space used here is the gameplayLayerRef because we want it to be relative to the
    //playing field, which is what the gameplayLeyerRef represents.
    originalBulletPosition = [gameplayLayerRef convertToNodeSpace:originalBulletPosition];
    bulletPositionDestination = [gameplayLayerRef convertToNodeSpace:bulletPositionDestination];
    
    //Add a projectile sprite
    CCSprite* const bulletLine = [CCSprite spriteWithSpriteFrameName:@"bulletLine.png"];
    [bulletLine setPosition: originalBulletPosition];
    [bulletLine setScale:2.0f];
    [bulletLine setRotation: CC_RADIANS_TO_DEGREES(armRotation) ];
    [gameplayLayerRef addChild:bulletLine];
    
    //Move projectile sprite to destination
    CCMoveTo* moveBulletAction = [CCMoveTo actionWithDuration:0.08f position: bulletPositionDestination ];
    CCCallFuncN* callFuncAction = [CCCallFuncN actionWithTarget:self selector:@selector(disposeFiredBullet:)];
    CCSequence* bulletActionSequence = [CCSequence actionOne:moveBulletAction
                                                         two:callFuncAction];
    [bulletLine runAction:bulletActionSequence];
}


-(void) disposeFiredBullet: (id) sender
{
    //remove the bullet from the scene/layer
    [gameplayLayerRef removeChild:sender cleanup:YES];
}

@end
