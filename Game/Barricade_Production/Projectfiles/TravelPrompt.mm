//
//  TravelPrompt.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TravelPrompt.h"
#import "CCControlButton.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "NotificationCenterEventList.h"
#import "TravelNodeButton.h"
#import "StoryScene.h"
#import "AppWidescreener.h"

@interface TravelPrompt (PrivateMethods)
// declare private methods here
@end

@implementation TravelPrompt
@synthesize DestinationTravelNodeButton=destinationTravelNodeButton;

-(void) setDestinationTravelNodeButton:(TravelNodeButton*) DestinationTravelNodeButton
{
    destinationTravelNodeButton = DestinationTravelNodeButton;
    
    const GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    
    const SaveObjectPropertyList* resourceGatheringPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    
    const short currentTravelSupplies =
    [[resourceGatheringPropertyList objectForKey:GSP::TRAVEL_SUPPLIES_COUNT] shortValue];
    
    //Configure label
    [travelingDestinationNameLabel setString:self.DestinationTravelNodeButton.ProgressionName];
    
    //Configure buttons
    //If we don't have enough supplies...
    if( currentTravelSupplies < destinationTravelNodeButton.SuppliesRequired )
    {
        //Configure Message
        [travelingDestinationLabel setString:
         [NSString stringWithFormat:
          @"You are traveling to ..."
          @"\n\n\n\n\n"
          @"You have %i supplies",
          currentTravelSupplies]];
        
        //Centralize the back button
        [backButton setPosition:ccp([CCDirector sharedDirector].screenCenter.x - [AppWidescreener getWidescreenLetterboxOffset].x,
                                    backButton.position.y)];
        
        //eliminate the go button
        [self removeChild:goButton cleanup:YES];
        
        //show the player what's going on
        [travelingDeniedLabel setString:
         [NSString stringWithFormat:
          @"You need %i supplies to travel",self.DestinationTravelNodeButton.SuppliesRequired]];
    }
    //Otherwise, if we DO have enough supplies
    else
    {
        [travelingDestinationLabel setString:
         [NSString stringWithFormat:
          @"You are traveling to ..."
          @"\n\n\n\n"
          @"Required supplies: %i\n"
          @"Current supplies: %i",
          self.DestinationTravelNodeButton.SuppliesRequired,
          currentTravelSupplies]];
        
        [self removeChild:travelingDeniedLabel cleanup:YES];
    }
}

-(void) buttonBack
{
    [self clearPrompt];
}

-(void) buttonGo
{
    //Remove the prompt
    [self clearPrompt];
    
    //Read supplies count from Save Data
    const GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    
    const SaveObjectPropertyList* resourceGatheringPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    
    const SaveObjectPropertyList* gameProgressPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::GAME_PROGRESS];
    
    const SaveObjectPropertyList* storyPathPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::STORY_SCENE];
    
    const short currentTravelSupplies =
    [[resourceGatheringPropertyList objectForKey:GSP::TRAVEL_SUPPLIES_COUNT] shortValue];
    
    //Set the new amount of supplies
    [resourceGatheringPropertyList setValue: [NSNumber numberWithShort:(currentTravelSupplies - destinationTravelNodeButton.SuppliesRequired)]
                                     forKey: GSP::TRAVEL_SUPPLIES_COUNT];
    
    //Set the new progresion values
    CCLOG(@"Setting new progression: %i%@", destinationTravelNodeButton.ProgressionValue,destinationTravelNodeButton.StoryPathValue);
    
    [gameProgressPropertyList setValue: [NSNumber numberWithShort:destinationTravelNodeButton.ProgressionValue]
                                     forKey: GSP::CURRENT_PROGRESSION];
    
    [storyPathPropertyList setValue: destinationTravelNodeButton.StoryPathValue
                                     forKey: GSP::CURRENT_STORY_PATH];
    
    //Go to the a cutscene
    [[CCDirector sharedDirector] replaceScene: [StoryScene node]];
}

-(void) clearPrompt
{
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_DISABLED
                                                        object:self];
    [self removeFromParentAndCleanup:YES];

}

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		//[self scheduleUpdate];
	}
	return self;
}


-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

// scheduled update method
-(void) update:(ccTime)delta
{
}

@end
