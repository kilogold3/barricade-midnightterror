//
//  LockableItem.h
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "ActivityLauncher.h"

extern const SignedByte UNINITIALIZED_TAP_COUNT;

@interface LockableItem : ActivityLauncher
{
    NSString* openItemSpriteID;     //sprite represented in the OpenItemActivity
    NSString* lockedItemSpriteID;   //sprite represented on screen when locked
    NSString* unlockedItemSpriteID; //sprite represented on screen when unlocked
    BOOL isLocked;                  //lock indicator
    SignedByte tapsToUnlock;        //How many taps are needed to release the lock
}
@property (nonatomic, readonly) NSString* LockedItemSpriteID;
@property (nonatomic, readonly) NSString* UnlockedItemSpriteID;
@property (nonatomic, readonly) NSString* OpenItemSpriteID;
@property (nonatomic, readwrite) BOOL IsLocked;
@property (nonatomic, readonly) SignedByte TapsToUnlock;

-(void) Interact;
@end
