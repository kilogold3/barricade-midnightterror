//
//  RunnerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RunnerState_AttackBarricade.h"
#import "EnemyRunner.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "NotificationCenterEventList.h"
#import "GB2Engine.h"
#import "Barricade.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"
#import "GameAudioList.h"
#import "SimpleAudioEngine.h"

@implementation RunnerState_AttackBarricade

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyRunner class]],@"Wrong enemy parent type");
        enemyParent = enemyParentIn;
        
        //Acquire barricade reference
        barricadeRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET
                                                            object:[NSValue valueWithPointer:&barricadeRef]];
        NSAssert(nil != barricadeRef,@"Barricade reference is nil. No barricade found.");
    }
    
    return self;
}

-(void) enter
{
    //You can't attack if you're dead!
    NSAssert(enemyParent.CurrentHealth > 0, @"A dead enemy has entered attack state.");
    
    [[SimpleAudioEngine sharedEngine] preloadEffect:GAL::SND_ENEMY_WALKER_ATTACK];
    
    readyToAttack = YES;
    
    //Set the enemy sprite to the attack frame
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyRunnerAttack4.png"];
    
    //use the bottom-left point ofthe spriteframe as the attack frame identifier
    attackFrameID = enemyParent.EnemySprite.textureRect.origin;
    
    //Load animation
    const short animationFrameCount = 6;
    float animationDelay = 0.05f;
    
    CCAnimation* attackingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyRunnerAttack%d.png" 
                                                                     numFrames:animationFrameCount
                                                                         delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:attackingAnimation];
    repeatingAttackingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody 
                                           forShapeName:@"enemyRunnerAttackShape"]; 
    //Set anchor point 
    [enemyParent.EnemySprite setAnchorPoint: 
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyRunnerAttackShape"]
     ];
    
    [enemyParent addChild:enemyParent.EnemySprite];
    [enemyParent.EnemySprite runAction:repeatingAttackingAnimationAction];
    
}

-(void) exit
{
    [enemyParent.EnemySprite stopAction:repeatingAttackingAnimationAction];
    [enemyParent removeChild:enemyParent.EnemySprite cleanup:YES];
    [[GB2Engine sharedInstance] addBodyToPostWorldStepCompleteFixtureRemovalList:enemyParent.EnemyPhysicsBody];
}

-(void) update: (float) deltaTime
{
    //Verify if we are allowed to process damage.
    //We avoid causing damage twice on the same frame.
    if ( YES == CGPointEqualToPoint( enemyParent.EnemySprite.displayFrame.rect.origin, attackFrameID) && YES == readyToAttack ) 
    {
        NSNumber* attackDamage = [NSNumber numberWithInt: enemyParent.AttackDamage ];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_ENEMY_STRIKE_BARRICADE 
                                                            object:attackDamage];
        
        [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_ENEMY_WALKER_ATTACK];
        
        readyToAttack = NO;
    }
    else if ( NO == CGPointEqualToPoint( enemyParent.EnemySprite.displayFrame.rect.origin, attackFrameID) )
    {
        readyToAttack = YES;
    }
}

-(void) endContactWithBarricadeSegment: (GB2Contact*)contact
{
    //We only check if the barricade is still alive when transitioning to
    //barricade approach state. We don't check if we should change into
    //player approach state because we would probably be hijacking the
    //control from the EnemyRunner class method: "barricadeDestroyedEventHandler".
    //Let EnemyRunner class take care of when to switch to player approach.
    //We will just worry about going back to approach barricade if we're too far.
    
    if( barricadeRef.IsDefending )
    {
        //Set the state to approach barricade
        //NOTE: This will crash if we are calling this due to EnemyBase deallocation
        [enemyParent changeEnemyState: [enemyParent.AI_StateFactory createRunnerState_ApproachBarricade]];
    }
    else
    {
        [enemyParent changeEnemyState: [enemyParent.AI_StateFactory createRunnerState_ApproachPlayer]];
    }
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    [enemyParent runAction:[CCMoveBy actionWithDuration:0 position:ccp(-firingWeapon.Damage, 0)]];
}

@end