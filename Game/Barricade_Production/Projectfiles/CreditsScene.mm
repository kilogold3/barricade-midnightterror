//
//  CreditsScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "CreditsScene.h"
#import "TitleScene.h"
#import "CCBReader.h"
#import "CreditsSceneChupacabras.h"
#import "GMath.h"
#import "SimpleAudioEngine.h"
#import "AppWidescreener.h"

@implementation CreditsScene

-(void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    [self scheduleUpdate];
    [self schedule:@selector(spawnChupacabra) interval: 1];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Credits_BGM.mp3"];
    
    [AppWidescreener centralizeSceneOnScreen:self];
}

-(void) update:(ccTime)delta
{
    if(
       [[KKInput sharedInput] isAnyTouchOnNode:self
                                    touchPhase:KKTouchPhaseEnded] )
    {
        [self unscheduleUpdate];
        
        
        [[CCDirector sharedDirector] replaceScene:[TitleScene node]];
    }
}

-(void) spawnChupacabra
{
    [self unschedule: _cmd];
    
    NSString* chupacabraTypeName;
    
    const Byte EnemyType = (Byte)gFloatRand(0, 3.5f);
    switch (EnemyType)
    {
        case 0:
            chupacabraTypeName = @"Flier";
            break;
        case 1:
            chupacabraTypeName = @"Walker";
            break;
        case 2:
            chupacabraTypeName = @"Runner";
            break;
        case 3:
            chupacabraTypeName = @"Tank";
            break;
        default:
            chupacabraTypeName = @"Walker";
            break;
    }
    
    CCNode* newChupacabra = [[[CreditsSceneChupacabras alloc] initWithEnemyTypeName:chupacabraTypeName] autorelease];
    [self addChild:newChupacabra];
    
    //If we're running on iPhone 5, we should undo the offset on the chupacabras position since they are
    //relative to the parent, who if currently offset.
    if( YES == [AppWidescreener isWidescreenEnabled] )
    {
        newChupacabra.position = ccpSub(newChupacabra.position, [AppWidescreener getWidescreenLetterboxOffset]);
    }
    
    [self schedule:_cmd interval: gFloatRand(0.5, 2)];
}
@end
