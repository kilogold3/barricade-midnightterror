//
//  MiniMapLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "MiniMapLayer.h"
#import "NotificationCenterEventList.h"
#import "GameSaveData.h"
#import "WeaponTypes.h"
#import "Pair.h"
#import "GameSaveDataPropertyList.h"
#import "SimpleAudioEngine.h"
#import "CCBReader.h"

@interface MiniMapLayer (PrivateMethods)
// declare private methods here
@end

@implementation MiniMapLayer

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		[[SimpleAudioEngine sharedEngine] preloadEffect:@"MiniMapAccess.wav"];
        
		// uncomment if you want the update method to be executed every frame
		[self scheduleUpdate];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];
    [[SimpleAudioEngine sharedEngine] playEffect:@"MiniMapAccess.wav"];

    //We want to disable the timer to give the player time to read, and
    //pause the level as well.
    SEND_EVENT(EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE, [NSNumber numberWithBool:NO]);
}

-(void) onExit
{
    [super onExit];
    SEND_EVENT(EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE, [NSNumber numberWithBool:YES]);
}

-(void) toPreviousScreen
{
    //clean the touches
    KKTouch* touch;
    KKInput* touchInput = [KKInput sharedInput];
    CCARRAY_FOREACH(touchInput.touches, touch)
    {
        [touch invalidate];
    }
    
    SEND_EVENT(EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY, nil);

    [self removeFromParentAndCleanup:YES];
    [[SimpleAudioEngine sharedEngine] playEffect:@"MiniMapAccess.wav"];
}

-(void) toMainMenu
{
    navigationMenu.enabled = NO;
    //Add the node on top of EVERYTHING
    [self addChild:[CCBReader nodeGraphFromFile:@"QuitToMainMenuConfirmationScreen.ccbi"] z:1];
}

-(void) didLoadFromCCB
{
    //Grab all of the required save data
    GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    NSDictionary* gameprogressPropertyList = [gameSaveData getSaveObjectProperties:GSC::GAME_PROGRESS];
    
    //Load the minimap module
    const NSInteger currentProgression = [[gameprogressPropertyList objectForKey:GSP::CURRENT_PROGRESSION] intValue];
    NSString* miniMapModuleFilename = [NSString stringWithFormat:@"RG_MiniMap_%02iA.ccbi",currentProgression];
    CCNode* miniMapModule = [CCBReader nodeGraphFromFile:miniMapModuleFilename];
    miniMapModule.position = ccp(270,179);
    [self addChild:miniMapModule];
    
    //Since the minimap is loaded, let's bring the buttons to the top.
    [navigationMenu reorderChild:navigationMenu z:1];
    
    REGISTER_EVENT(self, @selector(uiDismissQuitPrompt_EventHandler:), EventList::UI_DISMISS_QUIT_PROMPT);
}

-(void) cleanup
{
    [super cleanup];
    UNREGISTER_EVENT(self, EventList::UI_DISMISS_QUIT_PROMPT);
}

-(void) uiDismissQuitPrompt_EventHandler: (NSNotification*) notification
{
    navigationMenu.enabled = YES;
}
@end
