//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TankState_Death.h"
#import "EnemyTank.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "GMath.h"
#import "GameAudioList.h"
#import "SimpleAudioEngine.h"

@implementation TankState_Death

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyTank class]],@"Wrong enemy parent type");
        enemyParent = enemyParentIn;
    }
    
    return self;
}

-(void) dealloc
{
    [animateAction release];
    [super dealloc];
}

-(void) enter
{    
    [self setDeathAnimationA:enemyParent];

    CCSequence* animationSequence = [CCSequence actionOne:animateAction
                                                      two:[CCTintTo actionWithDuration:1.0f
                                                                                   red:100
                                                                                 green:128
                                                                                  blue:100]
                                     ];
    
    [enemyParent.EnemySprite runAction:animationSequence];
    [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_ENEMY_TANK_DEATH];
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}

-(void) setDeathAnimationA: (EnemyBase*) enemyParentIn
{
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyTankDeath00.png"];
    
    //Load animation
    const short animationFrameCount = 8;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyTankDeath%02d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyTankWalkShape"]
     ];
    
    [enemyParentIn addChild:enemyParent.EnemySprite];
}
@end
