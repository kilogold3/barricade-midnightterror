//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyVejigante;

@interface VejiganteState_ApproachPlayer : NSObject<IEnemyState>
{
    EnemyVejigante* enemyParentRef;
    CCSprite* enemySprite;

}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

@end
