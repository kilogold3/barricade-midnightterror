//
//  CreditsScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CreditsScene : CCLayer
{
    CCNode* creditsListNode;
    CCSprite* ktgLogoBackground;
}

@end
