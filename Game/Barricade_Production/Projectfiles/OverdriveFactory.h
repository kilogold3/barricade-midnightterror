//
//  OverdriveFactory.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import <Foundation/Foundation.h>

@class Overdrive;

@interface OverdriveFactory : NSObject

-(Overdrive*) createJeromeOverdriveV1_HIT;
-(Overdrive*) createJeromeOverdriveV1_MISS;

-(Overdrive*) createYunaOverdriveV1_HIT;
-(Overdrive*) createYunaOverdriveV1_MISS;

-(Overdrive*) createJonnyOverdriveV1_HIT;
-(Overdrive*) createJonnyOverdriveV1_MISS;

-(Overdrive*) createLeoOverdriveV1_HIT;
-(Overdrive*) createLeoOverdriveV1_MISS;

-(Overdrive*) createJeromeOverdriveAnimationNoExecution;
-(Overdrive*) createJohnsonOverdriveAnimationNoExecution;


@end
