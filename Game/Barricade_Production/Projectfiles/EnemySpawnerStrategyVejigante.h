//
//  EnemySpawnerStrategyDefault.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import "EnemySpawnerStrategyBase.h"

@interface EnemySpawnerStrategyVejigante : EnemySpawnerStrategyBase
{
    /*
     This flag is designed for the sole purpose of avoiding overdrives from reactivating this spawner.
     The player will try to execute overdrives during the vejigante encounter. Due to system structure,
     the spawningEnabled flag will be altered. We will use this stubbornlock to make sure that regardless
     of what the spawningEnabled flag is, we will remain disabled.
     */
    BOOL stubbornLockEnabled;
    Byte spawnCountBeforeVejigante;
}

-(id) initWithEnemyField: (CCLayer*) enemyFieldIn;

@end
