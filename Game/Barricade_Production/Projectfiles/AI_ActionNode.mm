//
//  AI_ActionNode.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/20/13.
//
//

#import "AI_ActionNode.h"

@implementation AI_ActionNode

-(id) initWithActionBlock: (ActionBlock) actionBlockIn
{
    self = [super init];
    
    if( nil != self )
    {
        actionBlock = [actionBlockIn copy]; //increases retain count
    }
    
    return self;
}

-(void) doAction
{
    actionBlock();
}

-(void) dealloc
{
    [actionBlock release];
    [super dealloc];
}

-(void) cleanup
{
    
}


@end
