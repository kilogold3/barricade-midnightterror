//
//  EnemyWalker.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyChupacabraBase.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "Barricade.h"
#import "TrapBase.h"
#import "GMath.h"

@implementation EnemyChupacabraBase

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        isKilledbyYunaOverdrive = NO;
        
        REGISTER_EVENT(self,
                       @selector(barricadeDestroyedEventHandler:),
                       EventList::GAMEPLAY_BARRICADE_DESTROYED)
        
        REGISTER_EVENT(self,
                       @selector(OverdriveYunaReinforceBarricadeStart_EventHandler:),
                       EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START)
        
        REGISTER_EVENT(self,
                       @selector(OverdriveYunaReinforceBarricadeEnd_EventHandler:),
                       EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_END)
        
        REGISTER_EVENT(self,
                       @selector(OverdriveYunaKillEnemiesInBarricadeArea_EventHandler:),
                       EventList::OVERDRIVE_YUNA_KILL_ENEMIES_IN_BARRICADE_AREA);
        
        REGISTER_EVENT(self,
                       @selector(OverdriveYunaClearBarricadeArea_EventHandler:),
                       EventList::OVERDRIVE_YUNA_CLEAR_BARRICADE_AREA);
    }
    
    return self;
}

-(void) cleanup
{
    [super cleanup];
    
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START)
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_END)
    UNREGISTER_EVENT(self, EventList::GAMEPLAY_BARRICADE_DESTROYED)
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_YUNA_KILL_ENEMIES_IN_BARRICADE_AREA)
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_YUNA_CLEAR_BARRICADE_AREA)
    
}


-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    //Only if we're alive do we want to react to weapon damage...
    if( self.CurrentHealth > 0 )
    {
        self.CurrentHealth = (self.CurrentHealth - firingWeapon.Damage);
        
        //If this was the final blow...
        if ( self.CurrentHealth <= 0 )
        {
            //Let's die
            [self killEnemy];
        }
        else
        {
            //If the current enemy AI state supports it,
            //make AI state react to weapon damage
            SEL stateSelector = @selector(reactToWeaponDamage:);
            
            if( [currentState respondsToSelector: stateSelector] )
            {
                [currentState performSelector:stateSelector withObject:firingWeapon];
            }
        }
    }
}

-(void) reactToTrapDamage: (TrapBase*) detonatingTrap
{
    currentHealth -= detonatingTrap.TrapDamage;
    
    if ( currentHealth <= 0 )
    {        
        [self killEnemy];
    }
}

-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact
{
    ++contactWithBarricadeSegmentCounter;
    
    if( 1 == contactWithBarricadeSegmentCounter )
    {
        SEL contactSelector = @selector(beginContactWithBarricadeSegment:);
        
        if( [currentState respondsToSelector: contactSelector] )
        {
            [currentState performSelector:contactSelector withObject:contact];
        }
    }
}

-(void) endContactWithBarricadeSegment: (GB2Contact*)contact
{
    --contactWithBarricadeSegmentCounter;
    
    if( 0 == contactWithBarricadeSegmentCounter )
    {
        SEL contactSelector = @selector(endContactWithBarricadeSegment:);
        
        if( [currentState respondsToSelector: contactSelector] )
        {
            [currentState performSelector:contactSelector withObject:contact];
        }
    }
}

-(void) barricadeDestroyedEventHandler: (NSNotification*) notification
{
    NSAssert(false, @"Must be called by the child class");

}
-(void) OverdriveYunaReinforceBarricadeStart_EventHandler: (NSNotification*) notification
{
    NSAssert(false, @"Must be called by the child class");
}

-(void) OverdriveYunaReinforceBarricadeEnd_EventHandler: (NSNotification*) notification
{
    NSAssert(false, @"Must be called by the child class");
}

-(void) OverdriveYunaClearBarricadeArea_EventHandler: (NSNotification*) notification
{
    //If the enemy is within the bounds of the barricade...
    if( isKilledbyYunaOverdrive )
    {
        //(Enemy should be dead at this point)
        NSAssert(currentHealth == 0, @"Enemy is not dead");
        
        //Remove from scene
        [self removeFromParentAndCleanup:YES];
    }
}

-(void) OverdriveYunaKillEnemiesInBarricadeArea_EventHandler: (NSNotification*) notification
{
    //The bounds of the barricade are defined by it's position and a spacing buffer
    //The barricade is not a square, so it doesn't have a tpyical width/height.
    //However, the barricade's anchor point is on the very front of the barricade,
    //so we can use this to our advantage.
    //We can use it's position as the left-most side, and use position+spacing_buffer
    //for the right-most side.
    
    //ON SECOND THOUGHT....
    //Let's just kill anyone past the barricade.
    
    //If the enemy is past the bounds of the barricade...
    //if( currentHealth > 0 && self.position.x > barricadeRef.position.x )
    if( currentHealth > 0 && contactWithBarricadeSegmentCounter > 0)
    {
        //We need to schedule the update so the death animations play.
        //This is because the "update" method is in charge of actually
        //transitioning the state. Otherwise, the state never transition,
        //it only gets queued to transition.
        
        //Normally, we'd have a problem here, because at the end of an
        //overdrive, all enemy updates are re-scheduled (rescheduling
        //twice causes an assertion by Cocos2D).
        //In this specific case, it's okay, because Yuna's overdrive will
        //remove the enemy objects anyways, so by the time the overdrive is
        //over, we only have the other enemies away from the barricade to worry
        //about.
        [self scheduleUpdate];
        
        //KILL EM!
        isKilledbyYunaOverdrive = YES;
        currentHealth = 0;
        [self killEnemy];
    }
}
@end
