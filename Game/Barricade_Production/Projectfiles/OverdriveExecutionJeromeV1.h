//
//  OverdriveExecutionJonathanLittleV1.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import <Foundation/Foundation.h>
#import "OverdriveExecutionProtocol.h"

@class CCShake;
@class GameplayLayer;
@class Aim;
@class EnemyField;

@interface OverdriveExecutionJeromeV1 : CCNode<OverdriveExecutionProtocol>
{
    GameplayLayer* gameplayLayerRef;
    CCAction* repeatingShakeScreenAction;
    CCSprite* explosionSprite;
    CCSequence* trapDetonationSequence;
    CCAnimation* explodeAnimation;
    short curDetonationCount;
    short maxDetonationCount;
    
    float currentAimTime; //How much time the aim phase will last
    CCLayerColor* blackLayer; //The darkening layer for the aim phase.
    Aim* aimRef;
    EnemyField* enemyFieldRef;
    NSMutableArray* targetedEnemies;
    float reticleRadius; //radius of the overdrive reticle (NOT the default reticle)

}
-(void) executeOverdrive;
-(void) resetOverdrive;
@end
