//
//  EnemyVejigante.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/30/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyJuna.h"
#import "GMath.h"
#import "EnemyStateFactory.h"
#import "NotificationCenterEventList.h"
#import "CCBReader.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"


@interface EnemyJuna (PrivateMethods)
// declare private methods here
@end

@implementation EnemyJuna

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
        enemySprite = [CCSprite spriteWithSpriteFrameName:@"EvilJunaRun0.png"];
        [self addChild:enemySprite];
        
        //We don't really use health for this...
        currentHealth = maxHealth = 0;
        movementSpeed = 145.0f;
        allowHealthDisplay = NO;
        
        [self instantChangeEnemyState: [enemyStateFactory createRampageJunaState_ApproachBarricade]];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	[self removeChild:enemySprite cleanup:YES];
    
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    
    [self phasingDisplace:ccp(self.position.x, gRangeRand(70,150))];
}

-(void) phasingDisplace: (CGPoint) position
{
    const float FADE_TIME = 0.2f;
    
    CCFadeOut* fadeEnemyOut = [CCFadeOut actionWithDuration:FADE_TIME];
    CCFadeIn* fadeEnemyIn = [CCFadeIn actionWithDuration:FADE_TIME];
    CCCallBlock* moveEnemy = [CCCallBlock actionWithBlock:^{
        [self setPosition:position];
    }];
    
    CCSequence* actionSequence = [CCSequence actions:fadeEnemyOut, moveEnemy, fadeEnemyIn, nil];
    
    
    [enemySprite runAction:actionSequence];
}

@end
