//
//  StorySceneInstance.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/12/13.
//
//

#import "kobold2d.h"
#import "CCBAnimationManager.h"

@interface StorySceneInstance : CCLayer <CCBAnimationManagerDelegate>
{
    NSString* nextSceneName;
    NSString* sceneTransitionClassType;
    
    //Count for how many sequences we have in our scene.
    //This way, we know how many scenes to advance before
    //transitioning to the next game-scene.
    SignedByte totalTimelineSequencesForScene;
    SignedByte currentTimelineSequence;
    
    CCBAnimationManager* animationManagerRef;
}
@property (readonly) NSString* NextSceneName;

-(void) runNextSequence;

@end

