//
//  OverdriveStatusBar.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface WeaponBoostCooldownBar : CCNode
{    
    //Other variables
    CCSprite* barSprite;
    float percentageFillTarget; //How full the bar should be
}
@property (nonatomic, readwrite) float CurrentBrightness;

//Event Handlers
-(void) UpdateUI_EventHandler: (NSNotification*) notification;

@end
