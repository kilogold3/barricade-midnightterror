//
//  RunnerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyBase;

@interface RunnerState_Death : NSObject<IEnemyState>
{
    CCAnimate* animateAction;
    EnemyBase* enemyParent;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

@end
