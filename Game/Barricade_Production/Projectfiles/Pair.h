//
//  Pair.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/20/13.
//
//

#import <Foundation/Foundation.h>

@interface Pair : NSObject
{
    id object1;
    id object2;
}

@property (nonatomic,retain) id Object1;
@property (nonatomic,retain) id Object2;

-(id) initWithObject1: (id) object1
           andObject2: (id) object2;
@end
