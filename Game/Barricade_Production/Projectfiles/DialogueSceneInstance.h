//
//  DialogueScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCBAnimationManager.h"

@interface DialogueSceneInstance : CCLayer <CCBAnimationManagerDelegate>
{
    //Play/Pause control
    BOOL isSequencePaused;
    
    //Count for how many sequences we have in our scene.
    //This way, we know how many scenes to advance before
    //transitioning to the next game-scene.
    SignedByte totalTimelineSequencesForScene;
    SignedByte currentTimelineSequence;
    
    CCBAnimationManager* animationManagerRef;
}
@property (nonatomic,readonly) BOOL IsSequencePaused;

-(void) resumeSequence;

@end
