//
//  SaveData.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/23/12.
//
//

#import "GameSaveData.h"
#import "WeaponTypes.h"
#import "Player.h"
#import "Barricade.h"
#import "NotificationCenterEventList.h"
#import "Pair.h"
#import "ResourceGatheringScene.h"
#import "StoryScene.h"
#import "GameSaveDataPropertyList.h"

static NSString* const SAVE_FILE_OBJECT_KEY = @"SaveData";

@implementation GameSaveData

//Singleton
static GameSaveData* gameSaveDataInstance = nil;
+ (GameSaveData *)sharedGameSaveData
{
	if (!gameSaveDataInstance) {
			gameSaveDataInstance = [[self alloc] init];
	}
    
	return gameSaveDataInstance;
}

-(BOOL) isSaveFileAvailable
{
    NSUserDefaults* loadBuffer = [NSUserDefaults standardUserDefaults];
    
    if( [loadBuffer objectForKey:SAVE_FILE_OBJECT_KEY] )
    {
        return YES;
    }
    
    return NO;
}


//Class methods
-(id) init
{
    self = [super init];

    if( nil != self)
    {
        //listen to events
        [self RegisterListeners];
        
        //If save file exists...
        if( [self isSaveFileAvailable] )
        {
            //initialize all data with the save file.
            NSUserDefaults* loadBuffer = [NSUserDefaults standardUserDefaults];
            saveObjectDictionary = [loadBuffer objectForKey:SAVE_FILE_OBJECT_KEY];
        }
        else
        {
            //We're starting a new game.
            //Initialize default data for the game...
            [self SetDefaultGameSaveData];
        }
    }
    
    return self;
}

-(void) dealloc
{
    [self WriteToFile];
    [self UnregisterListeners];
    [saveObjectDictionary release];
    gameSaveDataInstance = nil;
    [super dealloc];
}

-(void) WriteToFile
{
    //Before we write to file, we need to update our current scene value
    //so we know where to load from next time. We could update this value every time we
    //change scene, or we could just handle that here. Of course, by only handling this here,
    //that means the value of the current scene is unreliable until this method is excecuted.
    //Considering we never really use that variable outside of the loading procedure, this should
    //not affect anyone.
    //We MUST make sure that we are only saving from only two locations:
    //--- Resource Gathering Scene
    //--- Story Scene
    //--- Gameplay Scene 
    //We should NEVER allow saving from any other location. For this reason we will do some Asserts here.
    NSString* const currentSceneClassName = NSStringFromClass( [ [ CCDirector sharedDirector ].runningScene class ]  );
    NSString* const assertDescription = [NSString stringWithFormat: @"Saving from the wrong scene: %@", currentSceneClassName];
    CCLOG(@"Now saving from %@", currentSceneClassName);
    NSAssert( [currentSceneClassName isEqualToString: GSPV::CURRENT_RUNNING_SCENE__RESOURCEGATHERINGSCENE] or
              [currentSceneClassName isEqualToString: GSPV::CURRENT_RUNNING_SCENE__GAMEPLAYSCENE] or
              [currentSceneClassName isEqualToString: GSPV::CURRENT_RUNNING_SCENE__STORYSCENE], assertDescription );
    
    //We have asserted that we are in a valid scene to allow for saving.
    //Let's update the current scene value before proceeding
    [[self getSaveObjectProperties:GSC::GAME_PROGRESS] setValue:currentSceneClassName
                                                         forKey:GSP::CURRENT_RUNNING_SCENE];
    
    //If we made it this far, it is safe to save
    NSUserDefaults* saveBuffer = [NSUserDefaults standardUserDefaults];
    
    [saveBuffer setObject:saveObjectDictionary
                   forKey:SAVE_FILE_OBJECT_KEY];
    
    [saveBuffer synchronize];
}

-(void) ResetSaveDataToDefaults
{
    [saveObjectDictionary release];
    saveObjectDictionary = nil;
    
    [self SetDefaultGameSaveData];
}


-(void) RegisterListeners
{
}

-(void) UnregisterListeners
{
}

-(void) SetDefaultGameSaveData
{      
    //Resource gathering room data
    //Key: Room ID filename (ex: RG_A00.ccbi)
    //Value: Array of InteractiveRoomObject tag values (from CCNode)
    /****************************************************************
     This is to store info on all the 'used' objects in a room.
     The way this is utilized is depicted below:
     
     [resourceGatheringRoomData : dictionary]
                |
                |----[key="room1" <> value=array]
                |       |
                |       |---[CCNode.tag : int]
                |       |---[CCNode.tag : int]
                |
                |----[key="room2" <> value=array]
                |       |
                |       |---[CCNode.tag : int]
                |       |---[CCNode.tag : int]
    ****************************************************************/
    NSMutableDictionary* resourceGatheringRoomData = [NSMutableDictionary dictionary];
    NSMutableDictionary* enemiesKilledData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              [NSNumber numberWithInt:0],@"EnemyWalker",
                                              [NSNumber numberWithInt:0],@"EnemyRunner",
                                              [NSNumber numberWithInt:0],@"EnemyFlier",
                                              [NSNumber numberWithInt:0],@"EnemyTank",
                                              nil];
    
    //Generate the list of all savable objects
    saveObjectDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                            ////////////////////////////////
                            //Set the player properties
                            ////////////////////////////////
                            [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             enemiesKilledData,GSP::ENEMIES_KILLED, ///PROPERTY1
                                                                    ///PROPERTY2
                                                                    ///PROPERTY3
                                                                    ///PROPERTY4
                             nil],
                            GSC::PLAYER,
                            
                            
                            ////////////////////////////////
                            //Set the barricade properties
                            ////////////////////////////////
                            [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithInt:100],GSP::BARRICADE_HEALTH,    ///PROPERTY1
                                                                                    ///PROPERTY2
                                                                                    ///PROPERTY3
                                                                                    ///PROPERTY4
                             nil],
                            GSC::BARRICADE,
                            
                            ///////////////////////////////////////////////
                            //Set the game progress properties
                            ///////////////////////////////////////////////
                            [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             GSPV::CURRENT_RUNNING_SCENE__STORYSCENE, GSP::CURRENT_RUNNING_SCENE,   ///PROPERTY1
                             [NSNumber numberWithInt:0],              GSP::CURRENT_PROGRESSION,     ///PROPERTY2
                             [NSNumber numberWithInt:0],              GSP::CURRENT_PROGRESSION_DAY, ///PROPERTY3
                             [NSNumber numberWithInt:0],              GSP::CURRENT_OVERALL_DAY,     ///PROPERTY4
                             nil],
                            GSC::GAME_PROGRESS,
                            
                            ///////////////////////////////////////////////
                            //Set the story scene properties
                            ///////////////////////////////////////////////
                            [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             @"A", GSP::CURRENT_STORY_PATH,
                             nil],
                            GSC::STORY_SCENE,
                            
                            
                            ///////////////////////////////////////////////
                            //Set the resource gathering room properties
                            ///////////////////////////////////////////////
                            [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             resourceGatheringRoomData,     GSP::USED_ROOM_INTERACTIVE_OBJECTS,
                             [NSNumber numberWithInt:0],    GSP::TRAVEL_SUPPLIES_COUNT,
                             [NSNumber numberWithBool:NO],  GSP::TUTORIAL_COMPLETED,
                             [NSNumber numberWithBool:NO],  GSP::TUTORIAL_TO_BE_EXECUTED,
                             nil],
                            GSC::RESOURCE_GATHERING_SCENE,
                            
                            ////////////////////
                            //End of dictionary
                            ////////////////////
                            nil];

    [saveObjectDictionary retain];
}

-(SaveObjectPropertyList*) getSaveObjectProperties: (NSString*) gameSaveCategory;
{
    return [saveObjectDictionary objectForKey:gameSaveCategory];
}


@end


