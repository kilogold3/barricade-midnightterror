//
//  ResourceGatheringTransitionScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//
//
//
//  Class usage:
//
//  It is IMPERATIVE to assign a value to TransitioningScene,
//  once it is loaded.
//


#import "cocos2d.h"
#import "CCBAnimationManager.h"
@interface DaysLeftTransitionScene : CCLayer<CCBAnimationManagerDelegate>
{
    CCLabelTTF* daysLeftLabel;
}
@property (readwrite, nonatomic) Class TransitioningScene;

@end
