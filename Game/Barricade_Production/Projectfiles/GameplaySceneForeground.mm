//
//  GameplaySceneForeground.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplaySceneForeground.h"
#import "NotificationCenterEventList.h"
#import "CCBReader.h"
#import "AppWidescreener.h"

@interface GameplaySceneForeground (PrivateMethods)
// declare private methods here
@end

@implementation GameplaySceneForeground

-(id) init
{
	self = [super init];
	if (self)
	{       
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                   object:nil];
	}
	return self;
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    
    CCNode* pauseMenu = [CCBReader nodeGraphFromFile:[NSString stringWithFormat:@"GameplayPauseMenu%@.ccbi",
                                                      [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""]];
    [self addChild: pauseMenu];
}


@end
