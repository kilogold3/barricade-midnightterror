//
//  Player.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"
#import "Box2D.h"
#import "PlayerStateFactory.h"
#import "NotificationCenterEventList.h"
#import "EnemyBase.h"
#import "GB2Engine.h"
#import "OverdriveFactory.h"
#import "Pair.h"
#import "Overdrive.h"
#import "Pair.h"
#import "OverdriveConstants.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "PlayerArms.h"
#import "SimpleAudioEngine.h"

//Weapon imports
#import "WeaponTypes.h"
#import "WeaponPistol.h"
#import "WeaponRifle.h"
#import "WeaponMineGun.h"
#import "WeaponRocketLauncher.h"

@interface Player (Private)
-(void) LoadWeaponsArray;
@end

@implementation Player
@synthesize PhysicsBody=physicsBody;
@synthesize CurrentPlayerState=currentPlayerState;
@synthesize WeaponsArray=weaponsArray;
@synthesize OverdrivesArray=overdrivesArray;
@synthesize CurrentOverdrivePercentage=currentOverdrivePercentage;
@synthesize CurrentWeaponBoostPercentage=currentWeaponBoostPercentage;

//Property overrides
-(void) setCurrentOverdrivePercentage:(float)CurrentOverdrivePercentageIn
{
    //Modify the value
    currentOverdrivePercentage = CurrentOverdrivePercentageIn;
    
    if( currentOverdrivePercentage > 100 )
        currentOverdrivePercentage = 100;
    
    else if( currentOverdrivePercentage < 0 )
        currentOverdrivePercentage = 0;
    
    //Update the OverdriveStatusBar
    SEND_EVENT(EventList::UI_UPDATE_OVERDRIVE_STATUS_BAR,
               [NSNumber numberWithFloat:currentOverdrivePercentage]);
}

-(void) setCurrentWeaponBoostPercentage:(float)CurrentWeaponBoostPercentageIn
{
    currentWeaponBoostPercentage = CurrentWeaponBoostPercentageIn;
    
    if( currentWeaponBoostPercentage > 100)
        currentWeaponBoostPercentage = 100;
    else if (currentWeaponBoostPercentage < 0)
        currentWeaponBoostPercentage = 0;
    
    SEND_EVENT(EventList::UI_UPDATE_WEAPONBOOST_STATUS_BAR,
               [NSNumber numberWithFloat:currentWeaponBoostPercentage]);
}

//Event Handlers
-(void) OverdriveJohnsonEndAimPhase_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
}

-(void) OverdriveJohnsonStartAimPhase_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
}

-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    BOOL isOverdriveHit = [[notification object] boolValue];
    
    //If overdrive is NOT a HIT
    if( NO == isOverdriveHit )
    {
        //The overdrive is a MISS.
        //Let's reactivate and operate as normal
        [self scheduleUpdate];
    }
    else
    {
        //do nothing. Leave everything as is.
        //Updating should remain disabled until the
        //end of the OverdriveExecution.
    }
}

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
}

-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
}

-(void) EnemyDead_EventHandler: (NSNotification*) notification
{
    EnemyBase* deadEnemy = [notification object];
    self.CurrentOverdrivePercentage = self.CurrentOverdrivePercentage + deadEnemy.OverdriveReward;
    self.CurrentWeaponBoostPercentage = self.CurrentWeaponBoostPercentage + deadEnemy.WeaponBoostReward;
    
    //Add the enemy to the kill stats in the save file
    SaveObjectPropertyList* playerSaveProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::PLAYER];
    const NSMutableDictionary* enemyKillsDictionary = [playerSaveProperties objectForKey:GSP::ENEMIES_KILLED];
    const NSString* deadEnemyClassName = NSStringFromClass([deadEnemy class]);
    const NSNumber* currentEnemyKillsCount = [enemyKillsDictionary objectForKey: deadEnemyClassName ];
    [enemyKillsDictionary setObject: [NSNumber numberWithInt: currentEnemyKillsCount.intValue + 1]
                             forKey: deadEnemyClassName];
}

-(void) SetOverdrive_EventHandler: (NSNotification*) notification
{
    [self setCurrentOverdrivePercentage: [[notification object] floatValue] ];
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
}

-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
}

-(void) ControlSwapButtonPressed_EventHandler: (NSNotification*) notification
{
    if( 100 ==currentWeaponBoostPercentage )
    {
        //Play activation sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Button.wav"];

        [self activateWeaponBoost];
    }
    else
    {
        //Play inaccessible sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_MechaJuna_Impenetrable.wav"];

    }
}

-(void) activateWeaponBoost
{
    isWeaponBoostActive = YES;
    currentWeaponBoostTimer = maxWeaponBoostTimer;
}


-(id) init
{
    self = [super init];
    
    if( self != nil)
    {
        //Basic variables
        self.CurrentOverdrivePercentage = 0;
        currentWeaponBoostPercentage = 0;
        currentWeaponBoostTimer = maxWeaponBoostTimer = 3;

        //Register Event Listeners
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveJohnsonEndAimPhase_EventHandler:)
                                                     name:EventList::OVERDRIVE_JOHNSON_END_AIM_PHASE
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveJohnsonStartAimPhase_EventHandler:)
                                                     name:EventList::OVERDRIVE_JOHNSON_START_AIM_PHASE
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(acquirePlayerEventHandler:)
                                                     name:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveExecutionFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationStart_EventHandler:)
                                                     name:EventList::OVERDRIVE_START_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(EnemyDead_EventHandler:)
                                                     name:EventList::GAMEPLAY_ENEMY_DEAD
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(SetOverdrive_EventHandler:)
                                                     name:EventList::GAMEPLAY_SET_OVERDRIVE
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsResumeGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                   object:nil];
        REGISTER_EVENT(self,
                       @selector(ControlSwapButtonPressed_EventHandler:),
                       EventList::CONTROLS_SWAP_BUTTON_PRESSED)
        
        
        //Instantiate physics body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        physicsBody = [GB2Engine sharedInstance].world->CreateBody( &bodyDef );
        
        //Redundant call, but used to invoke the underlying mechanics of synchronizing
        //the physics body from Box2D with the player object from Cocos2D.
        //self.position = _position;
                
                
        [self scheduleUpdate];
    }
    return self;
}

-(void) onEnter
{
    [super onEnter];
    
    //During the init phase, we have (0,0) position. Cocos builder set up in the following order:
    //1) init
    //2) set position
    //3) add child to hierarchy
    //Because of this, we don't have reliable data to snap the player into place during init.
    //When onEnter is called, we have a position which we can use to snap/sync the physics body
    //to the game object. However, since we are overriding the position getter/accessor to provide
    //physics body position (at this point, uninitialized properly), we don't have accurate data.
    //Because of this, we directly access the cocos2D position to use as a reference. Conceptually,
    //we are setting our position to our current position (a redundant action), but now we understand
    //why we are doing it.
    self.position = _position;
    
    //Load weapon data from save file
    [self LoadFromSaveData];
    
    //Load Weapons
    [self LoadWeaponsArray];

    //Load overdrives
    [self LoadOverdrivesArray];
    
    //Load player state factory
    playerStateFactory = [[[PlayerStateFactory alloc] initWithDependencies:self] autorelease];
    
    /*
     NOTE:
     We moved the state transition to onEnter because during the init phase, there is no position set.
     Default CCNode position will be (0,0). If we're starting with the "Alive" state, we need to make sure
     we have specified the position the player will actually start at because the "Alive" state uses the
     players current position (default [0,0]) to define the waypoint location (where the player will walk
     towards).
     By moving this into onEnter phase, we have a chance to specify the position before the Player goes
     into a CCNode hierarchy.
     */
    
    //Set state to alive
    [self changeState:[playerStateFactory createPlayerState_Alive]];
}

-(void) LoadFromSaveData
{
}

-(void) changeState: (PlayerState*) newState
{
    if( newState != nil )
    {
        if( nil != currentPlayerState )
        {
            [currentPlayerState Exit];  
            [currentPlayerState release];
        }
        
        currentPlayerState = newState;
        [currentPlayerState retain];
        [currentPlayerState Enter];
    }
}

-(void) cleanup
{
    //Unregister event listeners
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_JOHNSON_END_AIM_PHASE
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_JOHNSON_START_AIM_PHASE
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_ENEMY_DEAD
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_SET_OVERDRIVE
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                  object:nil];
    
    UNREGISTER_EVENT(self, EventList::CONTROLS_SWAP_BUTTON_PRESSED)
    
    //Remove running state
    if( currentPlayerState != nil )
    {
        [currentPlayerState Exit];
        [currentPlayerState release];
    }
    
    //Remove weapons
    [static_cast<NSMutableArray*>(weaponsArray) removeAllObjects];
    [weaponsArray release];
    
    //Remove overdrives
    for (Pair* curPair in overdrivesArray)
    {
        [curPair release];
    }
    [overdrivesArray release];
        
    [super cleanup];
}
-(void) dealloc
{
    CCLOG(@"Dealloc: Player");
    //cleanup physics
    physicsBody->SetUserData(NULL);
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:physicsBody];

    [super dealloc];
}

-(void) update: (ccTime) delta
{    
    //Update the player's state
    [currentPlayerState Update:delta];
    
    //If we are boosting...
    if( YES == isWeaponBoostActive )
    {
        //First we tick the timer and make sure we didn't run out of boost-time
        
        currentWeaponBoostTimer -= delta;
        if( currentWeaponBoostTimer <= 0 )
        {
            //disable weapon boosting
            isWeaponBoostActive = NO;
            
            //Reset the timer
            currentWeaponBoostTimer = maxWeaponBoostTimer;
            
            //Eliminate any accumulated boost percentage
            self.CurrentWeaponBoostPercentage = 0;
            
            //switch back to the original weapon (Pistol)
            SEND_EVENT(EventList::CONTROLS_WEAPON_SELECTED, [NSNumber numberWithInt:0])
            
            //Let's update the weapon boost signals to deactivate
            SEND_EVENT(EventList::UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL, [NSNumber numberWithInt:0])
            
            //Lets bail
            return;

        }
        
        
        //Check if we are able to boost the weapon
        if ( self.CurrentWeaponBoostPercentage >= 100 )
        {
            //Clear the boost percentage
            self.CurrentWeaponBoostPercentage = 0;
            
            //Notify the arms to switch to the next weapon.
            //Find out which weapon we currently have, and grab the next one.
            //In order to do this, we'll ping the arms for the current weapon.
            //Then, we'll cross-reference with the weapon array.
            PlayerArms* playerArmsRef = nil;
            SEND_EVENT(EventList::COMPONENT_ACQUIRE_PLAYER_ARMS_TARGET, [NSValue valueWithPointer:&playerArmsRef])
            NSAssert( playerArmsRef != nil,@"PlayerArms not found");
            
            const WeaponBase* currentWeaponRef = playerArmsRef.CurrentWeapon;
            for (NSUInteger curWeaponIndex = 0; curWeaponIndex < self.WeaponsArray.count; ++curWeaponIndex)
            {
                const WeaponBase* iteratingWeaponRef = [self.WeaponsArray objectAtIndex:curWeaponIndex];
                if( currentWeaponRef.WeaponType == iteratingWeaponRef.WeaponType )
                {
                    //We found a match!
                    //Is our current level below the maximum level limit?
                    //(in this case, this is dictated by the player's weapon array)
                    if( curWeaponIndex < self.WeaponsArray.count - 1)
                    {
                        //Let's level up our weapon, by switching to that weapon
                        SEND_EVENT(EventList::CONTROLS_WEAPON_SELECTED, [NSNumber numberWithInt:curWeaponIndex+1])
                    }
                    
                    //Let's update the weapon boost signals to be active or inactive based on our weapon boost level.
                    //Each weapon boost signal has a tag value that is associated to the weapon boost level. By sending the
                    //current weapon level as a parameter, each signal can react to the event in a unique way. For our purposes,
                    //when the signal receives this event, it will check it's own tag to see if it should be enabled or disabled.
                    SEND_EVENT(EventList::UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL, [NSNumber numberWithInt:curWeaponIndex+1])
                    
                    //We now reset the timer for the weapon boost
                    currentWeaponBoostTimer = maxWeaponBoostTimer;
                }
            }
        }
    }
    
    //After all the data has been processed, let's update the weapon boost cooldown bar on the HUD.
    //We have to send a percentile value...
    SEND_EVENT(EventList::UI_UPDATE_WEAPONBOOST_COOLDOWN_BAR,
               [NSNumber numberWithFloat: (100.0f * (currentWeaponBoostTimer/maxWeaponBoostTimer))])
}

-(void) acquirePlayerEventHandler: (NSNotification*) notification
{
    NSValue* enemyRefPointer = [notification object];
    Player** targetPlayerRef = (Player**)[enemyRefPointer pointerValue];
    (*targetPlayerRef) = self;
}

-(Pair*) getCurrentOverdrivePairForCharacter: (OverdriveConstants::OVERDRIVE_CHARACTERS) character
{
    //Get the current overdrive level for that character
    NSNumber* characterKey = [NSNumber numberWithInteger:character];
    NSNumber* characterLevel = [overdriveLevelsMap objectForKey: characterKey];
    
    //Get the overdrive array offset (character offset + character level)
    return [overdrivesArray objectAtIndex: ( OverdriveConstants::OVERDRIVE_ARRAY_CHARACTER_OFFSETS[character] + characterLevel.intValue ) ];
}

-(void) LoadOverdriveLevels
{
    overdriveLevelsMap = [NSDictionary dictionaryWithObjectsAndKeys:
                          (NSUInteger)OverdriveConstants::OVERDRIVE_CHARACTERS_JEROME, 0,
                                      OverdriveConstants::OVERDRIVE_CHARACTERS_LEO, 0,
                                      OverdriveConstants::OVERDRIVE_CHARACTERS_YUNA, 0,
                                      OverdriveConstants::OVERDRIVE_CHARACTERS_JONNY, 0
                          , nil];
}

-(void) LoadOverdrivesArray
{   
    //Create a factory to instantiate all overdrives
    OverdriveFactory* overdriveFactory = [[OverdriveFactory alloc] init];
    
    //Instantiate/Add ALL the overdrives available in the game
    overdrivesArray = [NSArray arrayWithObjects:
                       [[Pair alloc] initWithObject1:[overdriveFactory createJeromeOverdriveV1_HIT] andObject2:[overdriveFactory createJeromeOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createJeromeOverdriveV1_HIT] andObject2:[overdriveFactory createJeromeOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createJeromeOverdriveV1_HIT] andObject2:[overdriveFactory createJeromeOverdriveV1_MISS]],
                       
                       [[Pair alloc] initWithObject1:[overdriveFactory createYunaOverdriveV1_HIT] andObject2:[overdriveFactory createYunaOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createYunaOverdriveV1_HIT] andObject2:[overdriveFactory createYunaOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createYunaOverdriveV1_HIT] andObject2:[overdriveFactory createYunaOverdriveV1_MISS]],
                       
                       [[Pair alloc] initWithObject1:[overdriveFactory createLeoOverdriveV1_HIT] andObject2:[overdriveFactory createLeoOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createLeoOverdriveV1_HIT] andObject2:[overdriveFactory createLeoOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createLeoOverdriveV1_HIT] andObject2:[overdriveFactory createLeoOverdriveV1_MISS]],
                       
                       [[Pair alloc] initWithObject1:[overdriveFactory createJonnyOverdriveV1_HIT] andObject2:[overdriveFactory createJonnyOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createJonnyOverdriveV1_HIT] andObject2:[overdriveFactory createJonnyOverdriveV1_MISS]],
                       [[Pair alloc] initWithObject1:[overdriveFactory createJonnyOverdriveV1_HIT] andObject2:[overdriveFactory createJonnyOverdriveV1_MISS]],
                       nil];
    
    //We need to retain, because Foundation gives us an array with autorelease
    [overdrivesArray retain];
        
    //Dispose of the factory; we are done with it.
    [overdriveFactory release];
}

-(void) LoadWeaponsArray
{
    //We'll make this array mutable, but no one should know this!
    //This is our little secret. We need this in order to automate the loading process.
    //To everyone else in the program, this will be an 'NSArray*'
    //No one should be modifying this list but us.
    //We will only have 3 usable weapons types in the game.
    weaponsArray = [NSMutableArray arrayWithCapacity: 3 ];
    
    //We need to retain, because Foundation gives us an array with autorelease
    [weaponsArray retain];
    
    //We don't invoke the selector directly because
    //weaponsArray is declared as 'NSArray' (not mutable).
    [weaponsArray performSelector: @selector(addObject:)
                       withObject: [[[WeaponPistol alloc] initWithDependencies:self] autorelease]];
    
    [weaponsArray performSelector: @selector(addObject:)
                       withObject: [[[WeaponRifle alloc] initWithDependencies:self] autorelease]];
    
    [weaponsArray performSelector: @selector(addObject:)
                       withObject: [[[WeaponRocketLauncher alloc] initWithDependencies:self] autorelease]];
    
}
-(void) saveAmmoSaveData
{
    
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    //Set the CCNode's position (world & local should match)...
    [super setPosition:positionIn];
    
    CGPoint physicsCoordinates = positionIn;
    
    if( nil != self.parent )
    {
        physicsCoordinates = [self.parent convertToWorldSpace:positionIn];
    }
    
    physicsBody->SetTransform( b2Vec2FromCGPoint(physicsCoordinates), 0);
}

-(CGPoint) position
{
    CGPoint positionValue = CGPointFromb2Vec2( physicsBody->GetTransform().p );
    
    //If we are in a hierarchy
    if( nil != self.parent )
    {
        //convert to node-coords
        positionValue = [self.parent convertToNodeSpace: positionValue];
    }
    
    return  positionValue;
}

@end
