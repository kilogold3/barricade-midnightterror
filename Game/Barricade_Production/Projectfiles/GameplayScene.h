//
//  GameplayScene.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2World;
class GB2WorldContactListener;
class GLESDebugDraw;
@class LevelTimer;

@interface GameplayScene : CCScene 
{
    //Engine Components
    b2World* world;
	GLESDebugDraw* debugDraw;
    
    //Level Components
    LevelTimer* levelTimer;
    
    //Scene Layers
    CCLayer* screenBackgroundLayer;
    CCLayer* gameplayLayer;
    CCLayer* hudLayer;
    CCLayer* screenForegroundLayer;
    CCLayer* touchControlsLayer;

}

-(void) GameplayFinalEnemyDefeated_EventHandler: (NSNotification*) notification;
-(void) acquireGameplaySceneScreenForegroundLayer_EventHandler: (NSNotification*) notification;

-(void) EndGameplayLevel;

@end
