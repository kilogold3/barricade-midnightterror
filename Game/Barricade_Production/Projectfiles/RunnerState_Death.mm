//
//  RunnerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RunnerState_Death.h"
#import "EnemyRunner.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "GMath.h"
#import "GameAudioList.h"
#import "SimpleAudioEngine.h"

@implementation RunnerState_Death

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyRunner class]],@"Wrong enemy parent type");
        enemyParent = enemyParentIn;
    }

    return self;
}

-(void) dealloc
{
    [animateAction release];
    [super dealloc];
}

-(void) enter
{
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyRunnerDeathD_0.png"];
    
    //Load animation
    const short animationFrameCount = 6;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyRunnerDeathD_%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyRunnerWalkShape"]
     ];
    
    [enemyParent addChild:enemyParent.EnemySprite];
    
    
    //compose the animaton
    CCSequence* animationSequence = [CCSequence actionOne:animateAction
                                                      two:[CCTintTo actionWithDuration:1.0f
                                                                                   red:100
                                                                                 green:100
                                                                                  blue:100]
                                     ];
    
    [enemyParent.EnemySprite runAction:animationSequence];
    [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_ENEMY_RUNNER_DEATH];
}

-(void) exit
{
    [enemyParent.EnemySprite removeFromParentAndCleanup:YES];
}

-(void) update: (float) deltaTime
{
}

@end
