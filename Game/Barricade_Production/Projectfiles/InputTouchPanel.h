//
//  InputTouchPanel.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InputTouchPanel : NSObject
{
    CGRect touchRect;
    KKTouch* acquiredTouch;
}
@property (readonly) KKTouch* AcquiredTouch;

-(id) initWithRect: (CGRect) touchRectIn;

//Exceptions in this case means to avoid processing those touches in particular.
-(KKTouch*) processTouchesWithException: (KKTouch*) TouchException
                         relativeToNode: (CCNode*) node;

@end
