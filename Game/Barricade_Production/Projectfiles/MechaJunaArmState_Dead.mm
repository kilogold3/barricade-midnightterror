//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaArmState_Dead.h"
#import "EnemyMechaJuna.h"
#import "GB2Engine.h"


@implementation MechaJunaArmState_Dead

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaArm class]],@"Wrong enemy parent type");

        armRef = static_cast<EnemyMechaJunaArm*>(enemyParentIn);
    }
    
    return self;
}

-(void) enter
{
    //Change the sprite to the exploded one
    [armRef.ArmSprite setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"JunaTankMissileBlown.png"]];
    [armRef removeChild:armRef.ArmLightSprite cleanup:YES];
    
    //Remove the fixtures for body for the arm.
    //It's dead. No longer needed.
    //Remove the fixtures from the body.
    //We don't destroy the body per se... EnemyBase does that.
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = armRef.EnemyPhysicsBody->GetFixtureList();
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    [[GB2Engine sharedInstance] addBodyToPostWorldStepSelectiveFixtureRemovalList:armRef.EnemyPhysicsBody
                                                                        :fixtureValuesArray];
    armRef.EnemyPhysicsBody->SetUserData(NULL);
    
}

-(void) exit
{
    
}

-(void) update: (float) deltaTime
{    
    if(NO == CGPointEqualToPoint(armRef.position, armRef.OriginalPosition))
    {
        //Perform movement
        [self stepToLocation: armRef.OriginalPosition
               withDeltaTime: deltaTime];
        
        //If near origin AND not locked into position,
        if( 1.0f > ccpDistance( armRef.position, armRef.OriginalPosition ) )
        {
            //snap to origin
            [armRef setPosition:armRef.OriginalPosition];
        }
    }
}

-(void) stepToLocation: (CGPoint) location
         withDeltaTime: (float) deltaTime
{
    //Grab destination position
    CGPoint destPos = location;
    
    //Grab self position
    CGPoint selfPosition = armRef.position;
    
    //Form a line from enemy to player
    CGPoint direction = ccpSub(destPos, selfPosition);
    
    //Normalize for direction
    CGPoint normailzedDirection = ccpNormalize(direction);
    
    //create velocity
    CGPoint velocity = {    normailzedDirection.x * armRef.ArmMovementSpeed * deltaTime ,
                            normailzedDirection.y * armRef.ArmMovementSpeed * deltaTime };
    
    //apply velocity to position
    CGPoint newPosition = { armRef.position.x + velocity.x ,
                            armRef.position.y + velocity.y };
    
    //Set new position
    [armRef setPosition:newPosition];
}


@end
