//
//  Aim.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum AIM_MODE {
    AIM_MODE_RELATIVE,
    AIM_MODE_ABSOLUTE,
    AIM_MODE_OFFSET,
};

@class Player;

@interface Aim : CCSprite 
{
    AIM_MODE aimMode;
    CGSize screenSize; //To avoid querying it every frame.
    Player* playerRef;
}
@property (nonatomic,readwrite) AIM_MODE AimMode;
-(id) init;

@end
