//
//  EnemySpawnerStrategyDefault.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import "EnemySpawnerStrategyDefault.h"
#import "EnemyFactory.h"
#import "EnemyBase.h"
#import "Pair.h"
#import "GMath.h"
#import "EnemyFlier.h"

@implementation EnemySpawnerStrategyDefault


-(id) initWithEnemyField: (CCLayer*) enemyFieldIn;
{
    self = [super initWithEnemyField: enemyFieldIn];
    
    if( nil != self)
    {        

    }
    
    return self;
}

-(void) spawnEnemyAtPosition: (CGPoint) position
{
    /*********************************************************************************************************
     Calculate probability for type spawning. I don't really know how this works, I 
     assume this all has to do with statistics math. The information below was taken
     from: http://forums.bukkit.org/threads/help-with-fake-random.85467/
     
     ...
     
     You need to store the relative probabilities for each elements somewhere, like a hashmap<Integer, Integer> 
     where the 1st integer is the index of the item in the array and the second integer is a relative probability 
     (for six strings you could use 12, 12, 12, 12, 12, 40)
     
     Your code needs to add up all the probabilities (100 in this case, but could be anything), generate a 
     random number random().nextInt(total) to get something from 0 to 99.
     Then loop through the integer/integer hashmap, and keep a running counter of the total probability up to 
     that item.
     So at index 1, you're checking if the rnd # you produced is < 12. At index 2, check if its < 24. At index 3, 
     check if its < 36. At each step through the array, add the new relative probability of that index to the 
     subtotal you're at, and check if the random number was less than the running subtotal. By the time you get 
     to the last one, its if rnd# < 100 (or whatever the total is) which will succeed even if the rnd# was 99. 
     So index 5 is < 60, index 6 is < 100, so thats 40% chance.
     
     With this method you can easily change relative probabilities however you want, and you don't have to choose 
     numbers that add up to 100, so the relative probabilities can be 3, 3, 3, 3, 3, 10 and its the same as the 
     above example. Or 1, 1, 1, 1, 1, 2 if you want the last one to be twice as likely. Or whatever you desire.
     
     Of course, if you ONLY need to change the probability of one item, there are simpler ways to do it, 
     but this way the probabilities can be changed for any/all items with ease.
     ==================================================================================
     List<String> fruit = Arrays.asList({"Apple","Banana","Grape","Orange"});
     
     HashMap<String,Integer> chance = new HashMap<String,Integer>();
     
     for (String name : fruit) {chance.put(name, name.length());
     
     int chanceTotal = 0;
     
     for (String name : chance.keySet())
     {
        chanceTotal += chance.get(name);
     }
     
     int choice = new Random().nextInt(chanceTotal), subtotal = 0;
    
     for (String name : chance.keySet())
     {
        subtotal += chance.get(name);
        if (choice < subtotal) return name + " is the best fruit!";
     }
     ==================================================================================
     In this example the fruits are weighted according to the length of their name; i.e. banana and orange 
     have weight 6 and apple and grape have weight 5 (and thus are less likely). You can either store the 
     string, as I did here, in the hashmap or an index to the position in the arraylist, or whatever you 
     want that lets you get back to what you are trying to select from.
     *********************************************************************************************************/
    
    int totalSum = 0;
    
    for (Pair* curPair in spawnTypesAndProbabilities)
    {
        totalSum += [curPair.Object2 intValue];
    }
    
    int choice = gRangeRand(0, totalSum);
    
    int subTotal = 0;
    
    for (Pair* curPair in spawnTypesAndProbabilities)
    {
        subTotal += [curPair.Object2 intValue];
        if( choice < subTotal )
        {
            /******************************************
             Sorting enemy positions requires consideration of enemy types.
             We have flying enemy types that would look odd if mixed with grounded
             enemy types because they could appear behind a grounded enemy type.
             We will store children within the enemy field based on their Y-coordinates,
             but within their respective layers.
             The layers here will be abstractly represented by value ranges.
             [-320 to 0] Grounded Units Layer
             [0 to 320] Aerial Units Layer.
             We use 320 in this example as the magic number because it is the screen height.
             So we really want:
             
             GroundedLayerID = 0
             AerialLayerID = 320
             
             Formula:
             Z = LayerID - positionY
             *****************************************/
            const int GROUNDED_LAYER_ID = 0;
            const int AERIAL_LAYER_ID = [CCDirector sharedDirector].screenSize.height;
            
            EnemyBase* newEnemy = [enemyFactory createEnemyFromStringType:curPair.Object1];
            
            float zValue = 0;
            if ([newEnemy isKindOfClass:[EnemyFlier class]])
            {
                zValue = AERIAL_LAYER_ID - position.y;
            }
            else
            {
                zValue = GROUNDED_LAYER_ID - position.y;
            }
            
            [enemyField addChild:newEnemy z:zValue];
            [newEnemy setPosition: position];

            break;
        }
    }
    

}

@end
