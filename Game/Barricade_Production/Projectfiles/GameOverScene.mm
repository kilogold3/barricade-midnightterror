//
//  GameOverScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverScene.h"
#import "kobold2d.h"
#import "GameplayScene.h"
#import "TitleScene.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"
#import "CCControlButton.h"
#import "NotificationCenterEventList.h"
#import "AppWidescreener.h"
#import "CCBReader.h"

@implementation GameOverScene

-(void) didLoadFromCCB
{
    buttonContinue.enabled = NO;
    buttonQuit.enabled = NO;
}

-(void) onExit
{
    [self removeAllChildrenWithCleanup:YES];
    UNREGISTER_EVENT(self, EventList::UI_DISMISS_QUIT_PROMPT);
}

-(void) onEnter
{
    [super onEnter];
    
    REGISTER_EVENT(self, @selector(uiDismissQuitPrompt_EventHandler:), EventList::UI_DISMISS_QUIT_PROMPT)
    
    
    //Implement a little flare here by adding a red flash
    CCLayerColor* redFlash = [CCLayerColor layerWithColor:ccc4(255, 0, 0, 255)];
    [redFlash setPosition:ccp(0,0)];
    [redFlash setSize:[CCDirector sharedDirector].screenSize];
    CCCallFuncN* callFunc = [CCCallFuncN actionWithTarget:self selector:@selector(cleanupRedFlash:)];
    CCFadeOut* fadeOut = [CCFadeOut actionWithDuration:1.2f];
    CCSequence* redFlashSequence = [CCSequence actionOne:fadeOut
                                                     two:callFunc ];
    
    [redFlash runAction:redFlashSequence];
    [self addChild:redFlash];
}

-(void) dealloc
{
    CCLOG(@"Deallocating: Game Over Scene");
    [super dealloc];
}

-(void) cleanupRedFlash: (id) sender
{
    //eliminate the read flash
    [self removeChild:sender cleanup:YES];
    
    //enable the buttons
    buttonContinue.enabled = YES;
    buttonQuit.enabled = YES;
}

-(void) menuItemSelected_Coninue
{
    [self loadNextScene:[GameplayScene node]];
}

-(void) menuItemSelected_Exit
{
    buttonContinue.enabled = buttonQuit.enabled = NO;
    CCNode* confirmationScreen = [CCBReader sceneWithNodeGraphFromFile:@"QuitToMainMenuConfirmationScreen.ccbi"];
    [AppWidescreener centralizeSceneOnScreen:confirmationScreen];
    [self addChild: confirmationScreen];
}

-(void) loadNextScene: (CCScene*) nextScene
{
    CCLOG(@"RESET!!!!");

    //Reset any loose buffers and whatnot.
    //This is actually kind of hacky as it is used to
    //clear out any stuck buffers that shouldn't be getting
    //stuck.
    [SimpleAudioEngine end];
    
    [[CCDirector sharedDirector] replaceScene: nextScene ];
}

-(void) uiDismissQuitPrompt_EventHandler: (NSNotification*) notification
{
    buttonContinue.enabled = buttonQuit.enabled = YES;
}

@end
