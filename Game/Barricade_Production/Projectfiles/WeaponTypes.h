//
//  WeaponTypes.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/24/12.
//
//

#ifndef Barricade_Production_WeaponTypes_h
#define Barricade_Production_WeaponTypes_h

enum WEAPON_TYPES
{
    eINVALID = 0,
    ePISTOL = 1,
    eRIFLE = 2,
    eMINE_GUN = 3,
    eROCKET_LAUNCHER = 4,
    eMAX_WEAPON_TYPES
};

#endif
