//
//  GameplayBackgroundLayer_00.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/1/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplayDynamicTimeBackgroundBase.h"

@interface GameplayBackgroundLayer_01A : GameplayDynamicTimeBackgroundBase
{
@protected
@private
    CCParticleSystemQuad* particleEffect_Stars;
}
@end
