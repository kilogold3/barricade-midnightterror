//
//  TravelTree.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/19/13.
//
//

#ifndef __Barricade_Production__TravelTree__
#define __Barricade_Production__TravelTree__

#import "kobold2d.h"
#import <vector>

@class TravelNodeButton;

class GameTreeNode
{
private:
    
    //Members
    TravelNodeButton* nodeData;
    std::vector<GameTreeNode*> nodeChildren;
    GameTreeNode* nodeParent;

    //Methods
    bool compareFunc( GameTreeNode* newNode );
    
    
public:
    //Ctor
    GameTreeNode( TravelNodeButton* travelNodeButtonIn );
    
    GameTreeNode* FindTreeNodeFromData( TravelNodeButton* travelNode );
    
    void DeleteChildren();
    
    bool AddNode( GameTreeNode* newNode );
    
    //Getters
    GameTreeNode* const GetParent(void) {return nodeParent;}
    std::vector<GameTreeNode*>& GetChildren(void) { return nodeChildren;}
    const TravelNodeButton* const GetNodeData(void) { return nodeData; }
};


#endif /* defined(__Barricade_Production__TravelTree__) */
