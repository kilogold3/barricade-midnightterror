//
//  ResourceGatheringScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/3/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ResourceGatheringScene.h"
#import "GB2Engine.h"
#import "CCBReader.h"
#import "NotificationCenterEventList.h"
#import "GameplayScene.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "Pickup.h"
#import "CCControlButton.h"
#import "ActivityBase.h"
#import "Door.h"
#import "ItemContainer.h"
#import "SearchTimerClock.h"
#import "SimpleAudioEngine.h"
#import "DialogueScene.h"
#import "AppWidescreener.h"

enum CHILD_Z_VALUES {
    CHILD_Z_ORDER_ROOM,
    CHILD_Z_ORDER_MINIMAP_BUTTON,
    CHILD_Z_ORDER_ACTIVITY,
    CHILD_Z_ORDER_TIMER,
};

@interface ResourceGatheringScene (PrivateMethods)
// declare private methods here
-(NSMutableArray*) getListOfUsedInteractiveObjectsForCurrentRoom;
@end

@implementation ResourceGatheringScene


-(id) init
{
	self = [super init];
	if (self)
	{
        roomLayerChildTag = 1337; //arbitrary value just for identification purposes.
        [self scheduleUpdate];        
	}
	return self;
}

-(void) onEnter
{
    [super onEnter];
    
	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resourceGatheringTraverseDoor_EventHandler:)
                                                 name:EventList::RESOURCE_GATHERING_TRAVERSE_DOOR
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resourceGatheringLaunchActivity_EventHandler:)
                                                 name:EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resourceGatheringPickupAcquired_EventHandler:)
                                                 name:EventList::RESOURCE_GATHERING_PICKUP_ACQUIRED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(componentAcquireResourceGatheringCurrentRoom_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_CURRENT_ROOM
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resourceGatheringSuspendActivity_EventHandler:)
                                                 name:EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(componentAcquireResourceGatheringMinimapButton_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_MINIMAP_BUTTON
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(componentAcquireResourceGatheringScene_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_GAME_PROGRESS
                                               object:nil];
    
    //We'll recycle the function call. It's all the same behavior.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resourceGatheringPickupAcquired_EventHandler:)
                                                 name:EventList::RESOURCE_GATHERING_DOOR_UNLOCKED
                                               object:nil];
    
    
    
}

-(void) onEnterTransitionDidFinish
{
    /**************************************************************************
     We want to make sure the previous scene is deallocated before allocating
     this one. That is because scene transitions are executed in the following
     order:
     1. scene: OtherScene
     
     2. init: <OtherScene = 066B2130 | Tag = -1>
     
     3. onEnter: <OtherScene = 066B2130 | Tag = -1>
     ￼
     4. [[[[Transition is running here]]]
     
     5. onExit: <FirstScene = 0668DF40 | Tag = -1>
     
     6. onEnterTransitionDidFinish: <OtherScene = 066B2130 | Tag = -1>
     
     7. dealloc: <FirstScene = 0668DF40 | Tag = -1>
     
     Note that the FirstScene dealloc method is called last. This means that during
     onEnterTransitionDidFinish, the previous scene is still in memory. If you want
     to allocate memory-intensive nodes at this point, you’ll have to schedule a
     selector to wait at least one frame before doing the memory allocations, to be
     certain that the previous scene’s memory is released. You can schedule a method
     that is guaranteed to be called the next frame by omitting the interval parameter
     when scheduling the method:
     
     [self schedule:@selector(waitOneFrame:)];
     
     The method that will be called then needs to unschedule itself by calling unschedule
     with the hidden_cmd parameter:
     
     -(void) waitOneFrame:(ccTime)delta
     {
     [self unschedule:_cmd];
     // delayed code goes here ...
     }
     **************************************************************************/
    [self scheduleOnce:@selector(loadSceneAfterOneFrame:)
                 delay:0];
    
    [super onEnterTransitionDidFinish];    
}

-(void) cleanup
{

    //Retianed string during init and traverse doors need to be cleaned up
    [currentRoomIdString release];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_TRAVERSE_DOOR
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_PICKUP_ACQUIRED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_CURRENT_ROOM
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_DOOR_UNLOCKED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_MINIMAP_BUTTON
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAME_PROGRESS
                                                  object:nil];
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"ResourceGatheringCommon.plist"];
    
    [super cleanup];

}

-(void)loadSceneAfterOneFrame: (ccTime) delta
{
    //Reset any loose buffers and whatnot.
    //This is actually kind of hacky as it is used to
    //clear out any stuck buffers that shouldn't be getting
    //stuck.
    [SimpleAudioEngine end];
    
    //Let's make sure we have already completed the tutorial and know what we have to do in the game.
    //If we have not. We will unterrupt the loading procedure and redirect to a dialogue scene.
    SaveObjectPropertyList* resourceGatheringSaveProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    BOOL tutorialCompleted = [[resourceGatheringSaveProperties valueForKey:GSP::TUTORIAL_COMPLETED] boolValue];
    if( NO == tutorialCompleted )
    {
        //Trigger the tutorial scene (let's make sure it hasn't been triggered before)
        BOOL tutorialToBeExecuted = [[resourceGatheringSaveProperties valueForKey:GSP::TUTORIAL_TO_BE_EXECUTED] boolValue];
        NSAssert(NO == tutorialToBeExecuted, @"tutorialToBeExecuted has unexpected bool value");
        [resourceGatheringSaveProperties setValue:[NSNumber numberWithBool:YES]
                                           forKey:GSP::TUTORIAL_TO_BE_EXECUTED];
        
        //Now with the trigger active, let's go to the Dialogue scene
        [[CCDirector sharedDirector] replaceScene:[DialogueScene node]];
        
        //We don't want to execute the remainder of this loading procedure since we won't be going
        //to ResourceGathering after all.
        return;
    }
    
    //Before anything, let's write out our save file from here.
    [[GameSaveData sharedGameSaveData] WriteToFile];
    
    //Load required spritesheets
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ResourceGatheringCommon.plist"];
    
    
    //Instantiate minimap button.
    //(We do this here because some nodes within the scene request
    // the instance of the button during init, so we have to have
    // it instatiated prior to that)
    CCMenuItemSprite* miniMapMenuItem =
    [CCMenuItemSprite itemWithNormalSprite: [CCSprite spriteWithSpriteFrameName:@"MiniMap_Icon.png"]
                            selectedSprite: [CCSprite spriteWithSpriteFrameName:@"MiniMap_Icon.png"]
                                    target:self
                                  selector:@selector(resourceGatheringMiniMapButton_Callback:)];
    [miniMapMenuItem setPosition:ccp( [CCDirector sharedDirector].screenSize.width - 32,292)];
    miniMapButtonMenu = [CCMenu menuWithItems:miniMapMenuItem, nil];
    [miniMapButtonMenu setPosition:CGPointZero];
    
    //Set up the minimap's activity as part of the minimap button initialization.
    //We need to add this to some hierarchy in order to have it call the cleanup
    //method (we could just call it manually). So we're adding this to the minimap
    //menu item for the lame reason that they're both related.
    dummyMiniMapActivityLauncher = [ActivityLauncher node];
    dummyMiniMapActivityLauncher.ActivityToLaunch = @"RG_MiniMapLayer.ccbi";
    [miniMapMenuItem addChild:dummyMiniMapActivityLauncher];
    
    //Assign the default room
    SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentProgression = [[resourceGatheringProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
    
    SaveObjectPropertyList* storySceneProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::STORY_SCENE];
    const NSString* currentStoryPath = [storySceneProperties objectForKey:GSP::CURRENT_STORY_PATH];
    
    currentRoomIdString = [NSString stringWithFormat:@"RG_%02i%@00.ccbi",
                           currentProgression,
                           currentStoryPath];
    [currentRoomIdString retain];
    
    //Load the starting point of the navigation map
    CCNode* roomLayer = [CCBReader nodeGraphFromFile:
                         [currentRoomIdString stringByReplacingOccurrencesOfString: @".ccbi"
                                                                        withString: ([AppWidescreener isWidescreenEnabled] ? @"__Wide.ccbi" : @"")]];
    
    //Set a tag to know how to find the object later for adding/removing
    [roomLayer setTag: roomLayerChildTag];
    
    //Z order must be set so that the room is all the way in the back
    [self addChild:roomLayer z:CHILD_Z_ORDER_ROOM];
    
    //Instantiate the clock timer.
    timerClock = static_cast<SearchTimerClock*>( [CCBReader nodeGraphFromFile:@"ResourceGatheringClockTimer.ccbi"] );
    [timerClock setPosition:ccp(0,320)];
    [self addChild:timerClock z:CHILD_Z_ORDER_TIMER];
    
    //Add the minimap button to the screen
    [self addChild:miniMapButtonMenu z:CHILD_Z_ORDER_MINIMAP_BUTTON];
    
    //Make sure there are no duplicate tag values
    [self scanForDuplicateNodeTagsInRoom: roomLayer];
    
    //Eliminate all 'used' pickups
    //Unlock all 'used' doors
    [self updateRoomItemStateFromSaveData:roomLayer];
    
    //Play music
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"ResourceGatheringBGM.wav" loop:YES];
}

-(void) resourceGatheringTraverseDoor_EventHandler: (NSNotification*) notification
{
    //Remove the current room
    [self removeChildByTag:roomLayerChildTag cleanup:YES];
    
    //Get new room filename (use appropriate if widescreen)
    //We'll release any previously retained string
    //and retain a new one now.
    [currentRoomIdString release];
    currentRoomIdString = [notification object];
    [currentRoomIdString retain];

    CCLOG(@"Accessing Room: %@",currentRoomIdString);
    
    //Load new room
    CCNode* roomLayer = [CCBReader nodeGraphFromFile:
                         [currentRoomIdString stringByReplacingOccurrencesOfString: @".ccbi"
                                                                        withString: ([AppWidescreener isWidescreenEnabled] ? @"__Wide.ccbi" : @"")]];
    [roomLayer setTag: roomLayerChildTag];
    
    //Make sure there are no duplicate tag values
    [self scanForDuplicateNodeTagsInRoom: roomLayer];
    
    //Eliminate all 'used' pickups
    //Unlock all 'used' doors
    [self updateRoomItemStateFromSaveData:roomLayer];

    //Now that the room is set up, let's add it to the game.
    [self addChild:roomLayer z:CHILD_Z_ORDER_ROOM];
}

-(void) scanForDuplicateNodeTagsInRoom: (CCNode*) roomLayer
{
    //Start by gathering all the items in the room
    NSMutableArray* itemsTagValues = [NSMutableArray array];
    CCNode* interactiveObject = nil;
    CCARRAY_FOREACH(roomLayer.children, interactiveObject)
    {
        //If the child is a child-class of 'ResourceGatheringInteractiveObject'
        if( [[interactiveObject class] isSubclassOfClass:[ResourceGatheringInteractiveObject class]] )
        {
            //Add it to the list of pickups
            [itemsTagValues addObject: [NSNumber numberWithInt:interactiveObject.tag]];
        }
    }
    
    //Only truly check for duplicates if we have more than one item
    if( itemsTagValues.count > 1)
    {
        //Sorts the list from highest to lowest.
        //Source: http://stackoverflow.com/questions/3205792/how-do-i-sort-an-nsmutable-array-with-nsnumbers-in-it
        NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        [itemsTagValues sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
        
        //Cycle through the list and find duplicates
        for( NSUInteger curTagIndex = 0; curTagIndex < itemsTagValues.count - 1; curTagIndex++ )
        {
            if ([[itemsTagValues objectAtIndex:curTagIndex] intValue] ==
                [[itemsTagValues objectAtIndex:curTagIndex + 1] intValue] )
            {
                //We found a duplicate, let's notify
                UIAlertView *alert = [[[UIAlertView alloc] init] autorelease];
                [alert setTitle:@"Warning"];
                [alert setMessage:@"There are items with duplicate tags in the scene."];
                [alert addButtonWithTitle:@"OK"];
                [alert show];
                
                break;
            }
        }
    }
}

-(void) updateRoomItemStateFromSaveData: (CCNode*) roomLayer
{
    //find all the 'used' child object tags for that specific room
    NSMutableArray* roomUsedInteractiveObjectList = [self getListOfUsedInteractiveObjectsForCurrentRoom];
    
    //Go through each item
    for (NSNumber* childNodeTag in roomUsedInteractiveObjectList)
    {
        CCNode* childNode = [roomLayer getChildByTag:childNodeTag.intValue];
        
        //If it's a Pickup..
        if( [childNode isKindOfClass:[Pickup class]] )
        {
            //Remove it from the room.
            
            //NOTE: if 2 objects have the same tag, only the first one
            //      found will be removed.
            [roomLayer removeChild:childNode cleanup:YES];
        }
        //Otherwise, if it's a Door....
        else if( [childNode isKindOfClass:[Door class]] )
        {
            Door* door = static_cast<Door*>(childNode);
            [door setIsLocked:NO];
        }
        //Otherwise if it's an ItemContainer...
        else if ( [childNode isKindOfClass:[ItemContainer class]] )
        {
            ItemContainer* itemContainer = static_cast<ItemContainer*>(childNode);
            [itemContainer getAssociatedPickupItemInstanceReference].IsReadyForPickup = YES;
            [roomLayer removeChild:childNode cleanup:YES];
        }
    }
}

-(void) resourceGatheringLaunchActivity_EventHandler: (NSNotification*) notification
{
    //Grab the activity launcher posting the activity
    ActivityLauncher* activityLauncher = [notification object];
    
    //Grab the CCBI filename that holds the activity
    NSString* ccbiActivityLayer = activityLauncher.ActivityToLaunch;
    
    //Load in the activity layer via CCBI reading
    ActivityBase* activityLayer = static_cast<ActivityBase*>( [CCBReader nodeGraphFromFile:ccbiActivityLayer] );
    
    //Set the original launcher to the activity for referencing
    //specific data about the launch of the activity itself.
    activityLayer.OriginalActivityLauncher = activityLauncher;

    //Add the layer activity
    [self addChild:activityLayer z:CHILD_Z_ORDER_ACTIVITY];
    
    //Disable the map button
    miniMapButtonMenu.enabled = NO;
}

-(void) resourceGatheringSuspendActivity_EventHandler: (NSNotification*) notification
{
    //Re-enable the map button
    miniMapButtonMenu.enabled = YES;
}

-(void) resourceGatheringPickupAcquired_EventHandler: (NSNotification*) notification
{
    NSMutableArray* roomUsedInteractiveObjectList = [self getListOfUsedInteractiveObjectsForCurrentRoom];
    
    //Grab the incoming CCNode reference via event args
    CCNode* pickupInstance = [notification object];
    
    //Add the current pickup (in this case, self) to that list.
    //We consider the item used and will no longer be available
    //next time we access the room
    [roomUsedInteractiveObjectList addObject: [NSNumber numberWithInt:pickupInstance.tag] ];
}

-(NSMutableArray*) getListOfUsedInteractiveObjectsForCurrentRoom
{
    //Grab the game save data
    GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    
    //Get a hold of the resource gathering save data property list.
    NSDictionary* ResourceGatheringDataPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    
    //Get the map that contains room data within the property list.
    NSMutableDictionary* usedRoomInteractiveObjectsMap =
    [ResourceGatheringDataPropertyList objectForKey: GSP::USED_ROOM_INTERACTIVE_OBJECTS];
    
    //return the list of 'used' interactive object tags that pertain to the current room.
    NSMutableArray* usedRoomInteractiveObjectsList =
    [usedRoomInteractiveObjectsMap objectForKey: currentRoomIdString];
    
    //If we did not find an object...
    if( nil == usedRoomInteractiveObjectsList )
    {
        //Let's create one and add it to the dictionary
        usedRoomInteractiveObjectsList = [NSMutableArray array];
        [usedRoomInteractiveObjectsMap setObject:usedRoomInteractiveObjectsList
                                          forKey:currentRoomIdString];
    }
    
    //return the new array
    return usedRoomInteractiveObjectsList;
}

-(void) resourceGatheringMiniMapButton_Callback: (id) sender
{
    //launch the mini map activity with the dummy activity launcher
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                                        object:dummyMiniMapActivityLauncher];
}

-(void) componentAcquireResourceGatheringCurrentRoom_EventHandler: (NSNotification*) notification
{
    NSAssert(nil != currentRoomIdString, @"currentRoomIdString is not initialized");
    
    NSMutableString* requestorRefPointer = [notification object];
    [requestorRefPointer setString:currentRoomIdString];
}

-(void) componentAcquireResourceGatheringMinimapButton_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    CCMenuItem** requestorRef = (CCMenuItem**)[requestorRefPointer pointerValue];
    (*requestorRef) = [miniMapButtonMenu.children objectAtIndex:0];
}

-(void) componentAcquireResourceGatheringScene_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    ResourceGatheringScene** requestorRef = (ResourceGatheringScene**)[requestorRefPointer pointerValue];
    (*requestorRef) = self;
}


@end
