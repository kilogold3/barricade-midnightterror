//
//  DialogueScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "DialogueSceneInstance.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "CCBReader.h"
#import "GameplayScene.h"
#import "StoryScene.h"
#import "SimpleAudioEngine.h"
#import "ResourceGatheringScene.h"

@implementation DialogueSceneInstance
@synthesize IsSequencePaused=isSequencePaused;

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    animationManagerRef = self.userObject;
    animationManagerRef.delegate = self;
    totalTimelineSequencesForScene = animationManagerRef.sequences.count;
    currentTimelineSequence = 0;
    isSequencePaused = YES;
}

-(void) onEnter
{
    [super onEnter];
    [animationManagerRef runAnimationsForSequenceNamed:@"DL00"];
    [[KKAdBanner sharedAdBanner] loadBanner];
}

-(void) cleanup
{
    [super cleanup];
    
    // Clear out delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    CCLOG(@"Completed Sequence %@", name);
}

-(void) resumeSequence
{
    ++currentTimelineSequence;

    //If we have advanced all the available timelines...
    if( currentTimelineSequence >= totalTimelineSequencesForScene )
    {
        SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
        SaveObjectPropertyList* gameProgressProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
        const int currentOverallDay = [[gameProgressProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
        [KKConfig selectKeyPath:@"GameSettings"];
        const int oleanderCityDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];

        //If we're at the end of days limit...
        //Let's transition to the sad ending cutscene
        if( currentOverallDay > oleanderCityDaysLimit )
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.7f
                                                                                          scene:[StoryScene node]
                                                                                      withColor:ccBLACK]];
        }
        //If we are executing the tutorial dialogue...
        //Let's transition into resource gathering
        else if( YES == [[resourceGatheringProperties valueForKey:GSP::TUTORIAL_TO_BE_EXECUTED] boolValue])
        {
            //Set the tutorial triggers to never launch the tutorial again
            [resourceGatheringProperties setValue:[NSNumber numberWithBool:NO]
                                           forKey:GSP::TUTORIAL_TO_BE_EXECUTED];
            [resourceGatheringProperties setValue:[NSNumber numberWithBool:YES]
                                           forKey:GSP::TUTORIAL_COMPLETED];
            
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.7f
                                                                                          scene:[ResourceGatheringScene node]
                                                                                      withColor:ccBLACK]];
        }
        //Otherwise, let's continue onto the actual game.
        else
        {
            [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.7f
                                                                                          scene:[GameplayScene node]
                                                                                      withColor:ccBLACK]];
        }
    }
    else
    {
        NSString* sequenceName = [NSString stringWithFormat:@"DL%02d",currentTimelineSequence];
        [animationManagerRef runAnimationsForSequenceNamed:sequenceName];
        CCLOG(@"Starting Sequence %@", animationManagerRef.runningSequenceName);
    }
}
@end
