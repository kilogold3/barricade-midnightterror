//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WoundedJunaState_DeathShock.h"
#import "EnemyJuna.h"

@implementation WoundedJunaState_DeathShock

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyJuna class]],@"Wrong enemy parent type");
        enemyParent = (EnemyJuna*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{    
    
}

-(void) exit
{

}

-(void) update: (float) deltaTime
{
}
@end
