//
//  TrapBase.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TrapBase.h"
#import "GB2Engine.h"

@interface TrapBase (PrivateMethods)
// declare private methods here
@end

@implementation TrapBase
@synthesize TrapDamage=trapDamage;
@synthesize HasDetonated=hasDetonated;

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
        hasDetonated = NO;
        
        //Instantiate body
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        
        trapPhysicsBody = [GB2Engine sharedInstance].world->CreateBody(&bodyDef);
        
        
        
		// uncomment if you want the update method to be executed every frame
		//[self scheduleUpdate];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) dealloc
{
    //Remove the physics body in the next possible frame
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:trapPhysicsBody];
    
    //Set the body's user data to NULL. This is because the contact listener detaches contacts.
    //Once it does that, it attempts to call the "endContact" method for each body's user data.
    //Since we are deallocating right here, the instance trapPhysicsBody's user data points to
    //will be invalid. With a NULL value, objective-c will send a message to NULL which has no
    //effect, as opposed to sending the message to some invalid instance address.
    trapPhysicsBody->SetUserData(NULL);
    
	[super dealloc];
}

// scheduled update method
-(void) update:(ccTime)delta
{
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    trapPhysicsBody->SetTransform( b2Vec2FromCGPoint(positionIn), 0);
    const b2Transform& transform = trapPhysicsBody->GetTransform();
    [super setPosition:CGPointFromb2Vec2(transform.p)];
    
}

-(CGPoint) position
{
    return CGPointFromb2Vec2( trapPhysicsBody->GetTransform().p ) ;
}

@end
