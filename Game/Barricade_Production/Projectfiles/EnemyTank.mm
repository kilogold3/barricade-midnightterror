//
//  EnemyTank.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyTank.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "Barricade.h"
#import "TrapBase.h"
#import "GMath.h"
#import "TankState_ApproachBarricade.h"
#import "TankState_ApproachPlayer.h"
#import "TankState_AttackBarricade.h"

@implementation EnemyTank

-(void) killEnemy
{
    //Notify death of enemy
    SEND_EVENT(EventList::GAMEPLAY_ENEMY_DEAD, self)
    
    //Switch to dead state
    [self changeEnemyState: [enemyStateFactory createTankState_Death]];
}

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        //Order matters here, because AI states use this class' attributes
        //to set behavior. Set the basic attributes before dealing with states.
        
        //Set basic attributes
        [self loadBasicAttributes:@"EnemyTankSettings"];
        contactWithBarricadeSegmentCounter = 0;
        
        
        //set initial state
        if( YES == barricadeRef.IsDefending )
            [self instantChangeEnemyState: [enemyStateFactory createTankState_ApproachBarricade]];
        else
            [self instantChangeEnemyState: [enemyStateFactory createTankState_ApproachPlayer]];
    }
    
    return self;
}

-(void) OverdriveYunaReinforceBarricadeStart_EventHandler: (NSNotification*) notification
{
    //At this point, we have already destroyed all enemies touching the barricade.
    //Only the enemies behind the barricade, and the ones in front remain.
    if( self.CurrentHealth > 0 )
    {
        if( YES == [self.CurrentEnemyState isKindOfClass:[TankState_AttackBarricade class]])
        {
            //Don't do anything. We are already at the barricade.
            //Just keep attacking.
            return;
        }
        else if( YES == [self.CurrentEnemyState isKindOfClass:[TankState_ApproachBarricade class]])
        {
            //Don't do anything. We continue walking towards the barricade.
            return;
        }
        else if( YES == [self.CurrentEnemyState isKindOfClass:[TankState_ApproachPlayer class]] )
        {
            //If we are past the barricade, we will want to continue chasing the player, but if we are
            //behind the barricade, we will want to switch to seeking the barricade.
            
            //To check if we are past the barricade, we need to make sure that the enemy is
            //beyond half the size of the barricade sprite, because just checking barricade's X-position,
            //will result in enemies chasing the player on the lower part of the screen due to the barricade
            //shape being slanted.
            if(self.position.x > self.BarricadeRef.position.x )
            {
                //We are past the barricade, and we are already chasing the player.
                //Don't do anything different. Chase the player.
                return;
            }
            else
            {
                //If we are behind the barricade, we will want to approach the barricade since
                //the player is no longer accessible because of the BarricadeReinforcer.
                [self changeEnemyState:[enemyStateFactory createTankState_ApproachBarricade]];
            }
        }
    }
}

-(void) OverdriveYunaReinforceBarricadeEnd_EventHandler: (NSNotification*) notification
{
    //It is impossible for any enemy to be in approach player state. There should only be two
    //states present: ApproachBarricade & AttackBarricade.
    NSAssert( NO == [self.CurrentEnemyState isKindOfClass:[TankState_ApproachPlayer class]],
             @"EnemyTank should not be in ApproachPlayer state.");
    
    //Let's switch our state.
    //We might already be in the state that we choose to switch to,
    //but that should not be a problem since the state machine should
    //be ignoring state changes to the same state.
    if( self.CurrentHealth > 0 )
    {
        //The reinforcement is down, but is the barricade still up?
        //Let's check if the barricade is NOT defending.
        if( NO == self.BarricadeRef.IsDefending )
        {
            [self changeEnemyState:[enemyStateFactory createTankState_ApproachPlayer]];
        }
        //If the barricade is up and running, and we are on the left side of it...
        else if (self.position.x < self.BarricadeRef.position.x)
        {
            [self changeEnemyState:[enemyStateFactory createTankState_ApproachBarricade]];
        }
    }
}

-(void) barricadeDestroyedEventHandler: (NSNotification*) notification
{
    if( currentHealth > 0 )
    {
        [self changeEnemyState: [enemyStateFactory createTankState_ApproachPlayer]];
    }
}

@end
