//
//  OverdriveExecutionYunaV1.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverdriveExecutionProtocol.h"

@class DebugDrawLineOfFire;
@class Aim;
@class PlayerArms;
@class Player;
@class EnemyField;

@interface OverdriveExecutionJonnyV1 : CCNode<OverdriveExecutionProtocol>
{
    float currentAimTime;
    CGPoint lineOfFireDestination;
    Aim* aimRef;
    PlayerArms* playerArmsRef;
    Player* playerRef;
    EnemyField* enemyFieldRef;
}
-(void) executeOverdrive;
-(void) resetOverdrive;
@end
