//
//  TouchControlLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TouchControlLayer.h"
#import "InputTouchPanel.h"
#import "NotificationCenterEventList.h"
#import "SneakyHoldEventButton.h"
#import "SneakyButtonSkinnedBase.h"
#import "CCRadioMenu.h"
#import "Player.h"
#import "WeaponBase.h"
#import "Overdrive.h"
#import "OverdriveStatusBar.h"
#import "OverdriveConstants.h"
#import "Pair.h"
#import "GMath.h"
#import "Aim.h"
#import "WeaponBase.h"
#import "HUD_DataDisplayStatsAmmo.h"
#import "SimpleAudioEngine.h"
#import "CCAnimation+SequenceLoader.h"
#import "Barricade.h"

#define HIDDEN_POS ( ccp([CCDirector sharedDirector].screenCenter.x,40) )
#define VISIBLE_POS ( ccp([CCDirector sharedDirector].screenCenter.x,90) )
#define TRANSITION_DURATION (0.1f)

const uint8 CCB_FIRE_BUTTON_TAG = 1;
const uint8 CCB_BOOST_BUTTON_TAG = 2;
const uint8 CCB_OVERDRIVE_BUTTON_TAG = 3;
const uint8 BUTTON_EXPLODE_TAG = 4;
const uint8 OVERDRIVE_PANEL_ACTION_TAG = 5;

@interface TouchControlLayer (Private)

//Private helper method for calculating whether
//an overdrive should HIT or MISS. This is to
//be used for every overdrive button callback.
-(BOOL) calculateOverdriveHitMiss;

//Callback method for unselecting any selected
//overdrive buttons within the selected overdrive
//panel
-(void) deselectAllOverdriveButtonsInOverdrivePanel;

//Private helper method to launch overdrives
-(void) launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) character;

@end

@implementation TouchControlLayer
-(void) launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) character
{
    //Call to bring the panel down automatically
    //NOTE: the code inside this function instructs that we will be deactivating the data-display
    //      panel message. We will be calling OVERDRIVE_START_ANIMATION a few lines below from here
    //      This call will reactivate the data-display message with the proper message. This also means
    //      that OVERDRIVE_FINISH_ANIMATION will also put away the data-display message. The code for
    //      this behavior will be found in the respective event handlers.
    [self overdriveButtonPressed_EventHandler:nil];
    
    //Grab the overdrive pair
    Pair* selectedOverdriveHitMissPair =
    [playerRef getCurrentOverdrivePairForCharacter: character];
    
    //Determine if it's a hit or a miss
    Overdrive* selectedOverdrive;
    if( YES == [self calculateOverdriveHitMiss] )
    {
        //HIT
        selectedOverdrive = static_cast<Overdrive*>( selectedOverdriveHitMissPair.Object1 );
    }
    else
    {
        //MISS
        selectedOverdrive = static_cast<Overdrive*>( selectedOverdriveHitMissPair.Object2 );
    }
    
    //send an event to the gameplay foreground
    //to play the overdrive animation.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_START_ANIMATION
                                                        object: selectedOverdrive];
    
    //send an event to clear the accumulated overdrive
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_SET_OVERDRIVE
                                                        object: [NSNumber numberWithInt:0]];
}

-(void) deselectAllOverdriveButtonsInOverdrivePanel
{
    [overdrivePanel setSelectedItem_:nil];
}

-(BOOL) calculateOverdriveHitMiss
{
    //Generate the random chance value
    int random = gRangeRand(0, 100);

    //If the random value is within the overdrive value
    //this indicates that we were successful. The higher
    //the overdrive percentage, the higher the odds.
    //"random" will always be less than or equal to 100,
    //therefore it is always guaranteed that it will HIT.
    if( random <= playerRef.CurrentOverdrivePercentage )
    {
        return YES;
    }
    
    return NO;
}

#pragma mark Event Handlers
-(void) ControlsRestrictOverdrive_EventHandler: (NSNotification*) notification
{
    //Disable the buttons
    overdriveSkinnedButton.disabledSprite = [CCSprite spriteWithSpriteFrameName:@"Overdrive_Damaged.png"];
    isOverdriveButtonRestricted = YES;
    overdriveSkinnedButton.button.status = NO;
    weaponBoostSkinnedButton.disabledSprite = [CCSprite spriteWithSpriteFrameName:@"Swap_Damaged.png"];
    isWeaponBoostButtonRestricted = YES;
    weaponBoostSkinnedButton.button.status = NO;
    
    //If the overdrive panel is up, let's put it away.
    if( YES == overdrivePanel.touchEnabled )
    {
        overdrivePanel.touchEnabled = NO;
        CCMoveTo* moveAction = [CCMoveTo actionWithDuration:TRANSITION_DURATION
                                                   position: HIDDEN_POS];
        
        CCCallBlock* finalizeSequence = [CCCallBlock actionWithBlock:^{
            //Make sure we have no other overdrive selected for next try
            [self deselectAllOverdriveButtonsInOverdrivePanel];
            //Just to make sure the panel arrived to its destination
            //(in case it got interrupted)
            overdrivePanel.position = HIDDEN_POS;
        }];

        
        //Instead of immediately deselecting the button, we will delay it until
        //the button is out of view.
        CCSequence* actionSequence = [CCSequence actionOne:moveAction
                                                       two:finalizeSequence];
        actionSequence.tag = OVERDRIVE_PANEL_ACTION_TAG;
        [overdrivePanel runAction:actionSequence];
    }
    
    
    //Play an explosion effect for both buttons.
    //(duplicate code ensues here, separated by scopes.)
    //////////////
    //OVERDRIVE
    {
        //Load explosion sprite
        CCSprite* explosionSprite = [CCSprite spriteWithSpriteFrameName:@"mineSprite_Idle_b.png"];
        [explosionSprite setPosition:CGPointZero];
        //(set z value positive here so it appears in front of the button)
        [overdriveSkinnedButton addChild:explosionSprite z:0 tag:BUTTON_EXPLODE_TAG];
        
        //Load explosion animation sequence
        const short animationFrameCount = 7;
        const float animationDelay = 0.1f;
        
        CCAnimation* explodeAnimation = [CCAnimation animationWithSpriteSequence:@"mineSprite_Explode_b%d.png"
                                                                       numFrames:animationFrameCount
                                                                           delay:animationDelay];
        CCAnimate* explodeAnimationAction = [CCAnimate actionWithAnimation:explodeAnimation];
        
        CCCallBlock* finalizeBlock = [CCCallBlock actionWithBlock:^{
            [overdriveSkinnedButton removeChildByTag:BUTTON_EXPLODE_TAG cleanup:YES];
        }];
        
        
        [explosionSprite runAction:[CCSequence actions:explodeAnimationAction,
                                    [CCDelayTime actionWithDuration:animationDelay * animationFrameCount],
                                    finalizeBlock,
                                    nil]];
    }
    /////////////////
    //WEAPON BOOST
    {
        //Load explosion sprite
        CCSprite* explosionSprite = [CCSprite spriteWithSpriteFrameName:@"mineSprite_Idle_b.png"];
        [explosionSprite setPosition:CGPointZero];
        //(set z value positive here so it appears in front of the button)
        [weaponBoostSkinnedButton addChild:explosionSprite z:0 tag:BUTTON_EXPLODE_TAG];
        
        
        //Load explosion animation sequence
        const short animationFrameCount = 7;
        const float animationDelay = 0.1f;
        
        CCAnimation* explodeAnimation = [CCAnimation animationWithSpriteSequence:@"mineSprite_Explode_b%d.png"
                                                                       numFrames:animationFrameCount
                                                                           delay:animationDelay];
        CCAnimate* explodeAnimationAction = [CCAnimate actionWithAnimation:explodeAnimation];
        
        CCCallBlock* finalizeBlock = [CCCallBlock actionWithBlock:^{
            [weaponBoostSkinnedButton removeChildByTag:BUTTON_EXPLODE_TAG cleanup:YES];
        }];
        
        
        [explosionSprite runAction:[CCSequence actions:explodeAnimationAction,
                                                       [CCDelayTime actionWithDuration:animationDelay * animationFrameCount],
                                                        finalizeBlock,
                                    nil]];
    }

}

-(void) OverdriveJeromeEndAimPhase_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
    aimRef.visible = NO;
    aimRef.displayFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"Reticle.png"];
}

-(void) OverdriveJeromeStartAimPhase_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
    aimRef.visible = YES;
    aimRef.displayFrame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"OD_Jerome_DetonationReticle.png"];

}

-(void) OverdriveJohnsonEndAimPhase_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
    aimRef.visible = NO;
}

-(void) OverdriveJohnsonStartAimPhase_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
    aimRef.visible = YES;
}

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification
{
    [self activateSelfAndAllComponents];
}

-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification
{
    [self deactivateSelfAndAllComponents];
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self deactivateSelfAndAllComponents]; 
}

-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self activateSelfAndAllComponents];
}

-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    BOOL isOverdriveHit = [[notification object] boolValue];
    
    //If overdrive is NOT a HIT
    if( NO == isOverdriveHit )
    {
        //The overdrive is a MISS.
        //Let's reactivate and operate as normal
        [self activateSelfAndAllComponents];
    }
    else
    {
        //do nothing. Leave everything as is.
        //The control layer should remain disabled until the
        //end of the OverdriveExecution.
    }
}


-(void) overdriveButtonPressed_EventHandler: (NSNotification*) notification
{
    static int countTest = 0;
    
    //Handle overdrive panel logic now.
    //If we are still performing an action...
    CCAction* possibleRunningAction = [overdrivePanel getActionByTag:OVERDRIVE_PANEL_ACTION_TAG];
    if( nil != possibleRunningAction && NO == [possibleRunningAction isDone] )
    {
        //Exit. We don't want to interrupt.
        return;
    }
    //If we are hidden...
    else if( NO == overdrivePanel.touchEnabled )
    {
        if(++countTest > 1)
        {
            CCLOG(@"Error");
        }
        overdrivePanel.touchEnabled = YES;
        CCMoveTo* moveAction = [CCMoveTo actionWithDuration:TRANSITION_DURATION
                                                   position: VISIBLE_POS];
        moveAction.tag = OVERDRIVE_PANEL_ACTION_TAG;
        [overdrivePanel runAction:moveAction];
    }
    //Otherwise, we are visible...
    else
    {
        countTest--;
        overdrivePanel.touchEnabled = NO;
        CCMoveTo* moveAction = [CCMoveTo actionWithDuration:TRANSITION_DURATION
                                                   position: HIDDEN_POS];
        CCCallFunc* deselectPanelButtonsCallback = [CCCallFunc actionWithTarget:self
                                                                       selector:@selector(deselectAllOverdriveButtonsInOverdrivePanel)];
        
        //Instead of immediately deselecting the button, we will delay it until
        //the button is out of view.
        CCSequence* actionSequence = [CCSequence actionOne:moveAction
                                                       two:deselectPanelButtonsCallback];
        actionSequence.tag = OVERDRIVE_PANEL_ACTION_TAG;
        [overdrivePanel runAction:actionSequence];
    }
    
    //Play the button press sound
    [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Button.wav"];
}

#pragma mark Menu Button Callbacks
-(void) weaponSwitchHelper: (Byte) weaponIndexIn
{
    //Set the weapon
    NSNumber* weaponIndex = [NSNumber numberWithInteger:weaponIndexIn];
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::CONTROLS_WEAPON_SELECTED
                                                        object: weaponIndex];
    
    
    //update the weapon icon based on the current weapon
    //to do this we must first get the weapon from the player.
    //We must always do this on runtime because the player's
    //current weapon is always changing.
    WeaponBase* weaponRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON
                                                        object:[NSValue valueWithPointer:&weaponRef]];
    NSAssert(nil != weaponRef,@"Weapon reference is nil. No weapon found. (Is Player Dead?)");
    [dataDisplayStatsAmmoSprite setDataForWeapon:weaponRef];
}

- (void)overdriveButton0Tapped:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Panel.wav"];
    [self launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) 0 ];
}

- (void)overdriveButton1Tapped:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Panel.wav"];
    [self launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) 1 ];
}

- (void)overdriveButton2Tapped:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Panel.wav"];
    [self launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) 2 ];
}

- (void)overdriveButton3Tapped:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"HUD_Select_Panel.wav"];
    [self launchCharacterOverdrive: (OverdriveConstants::OVERDRIVE_CHARACTERS) 3 ];
}

#pragma mark Instance Methods
-(id) init
{
    self = [super init];
    
    if( self != nil)
    {
        //Listen for events
        REGISTER_EVENT(self, @selector(OverdriveJeromeStartAimPhase_EventHandler:), EventList::OVERDRIVE_JEROME_START_AIM_PHASE);
        REGISTER_EVENT(self, @selector(OverdriveJeromeEndAimPhase_EventHandler:), EventList::OVERDRIVE_JEROME_END_AIM_PHASE);
        REGISTER_EVENT(self, @selector(ControlsRestrictOverdrive_EventHandler:), EventList::CONTROLS_RESTRICT_CONTROLS);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveJohnsonStartAimPhase_EventHandler:)
                                                     name:EventList::OVERDRIVE_JOHNSON_START_AIM_PHASE
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveJohnsonEndAimPhase_EventHandler:)
                                                     name:EventList::OVERDRIVE_JOHNSON_END_AIM_PHASE
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(overdriveButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_OVERDRIVE_BUTTON_PRESSED
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsResumeGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationStart_EventHandler:)
                                                     name:EventList::OVERDRIVE_START_ANIMATION
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveExecutionFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ComponentAcquireGameplaySceneTouchControlLayer_EventHandler:)
                                                     name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gameplayBarricadeHealthChanged_EventHandler:)
                                                     name:EventList::GAMEPLAY_BARRICADE_HEALTH_CHANGED
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gameplayWeaponOutOfAmmo_EventHandler:)
                                                     name:EventList::GAMEPLAY_WEAPON_OUT_OF_AMMO
                                                   object:nil];
        
    }
    return self;
}

-(void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    isWeaponBoostButtonRestricted = NO;
    isOverdriveButtonRestricted = NO;
    
    CGSize windowSize = [CCDirector sharedDirector].winSize;
    
    
    //Debug Layer
    [KKConfig selectKeyPath:@"KKStartupConfig"];
    const BOOL debugDrawEnabled = [KKConfig boolForKey:@"EnableControlTouchPanelDebugDraw"];

    //Movement Panel Setup (assume local coordinate origin)
    //We are using the barricade as reference of where to place the touch panel.
    //Since the barricade has it's origin on the top/left corner of the sprite, we can use
    //the following formula:
    //barricade-x + barricade-width
    //However, because the barricade sprite has a lot of transparent buffer space (I assume it's
    //due to the gradient glow effect), we will only be using half the width.
    Barricade* barricadeRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET, [NSValue valueWithPointer:&barricadeRef]);
    NSAssert(nil != barricadeRef, @"Barricade reference not found.");
    
    CGRect movementPanelRect = CGRectMake( barricadeRef.position.x + (barricadeRef.BarricadeSprite.contentSize.width/2),
                                          windowSize.height/5,
                                          windowSize.width/2,
                                          windowSize.height );
    movementTouchPanel = [[InputTouchPanel alloc] initWithRect:movementPanelRect];
    
    if( YES == debugDrawEnabled )
    {
        CCSprite* magic = [CCSprite spriteWithFile:@"MagicPixel.png"];
        magic.anchorPoint = CGPointZero;
        magic.position = movementPanelRect.origin;
        magic.scaleX = movementPanelRect.size.width;
        magic.scaleY = movementPanelRect.size.height;
        magic.color = ccBLUE;
        magic.opacity = 100;
        [self addChild:magic];
    }
    
    
    //Aim Panel Setup (assume local coordinate origin)
    CGRect aimPanelRect = CGRectMake( -100,
                                     0,
                                     windowSize.width/1.2f,
                                     windowSize.height );
    aimTouchPanel = [[InputTouchPanel alloc] initWithRect:aimPanelRect];
    
    if( YES == debugDrawEnabled )
    {
        CCSprite* magic = [CCSprite spriteWithFile:@"MagicPixel.png"];
        magic.anchorPoint = CGPointZero;
        magic.position = aimPanelRect.origin;
        magic.scaleX = aimPanelRect.size.width;
        magic.scaleY = aimPanelRect.size.height;
        magic.opacity = 100;
        [self addChild:magic];
    }
    
    //Acquire temporary player & aim references
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                        object:[NSValue valueWithPointer:&playerRef]];
    NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
    
    aimRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_AIM_TARGET
                                                        object:[NSValue valueWithPointer:&aimRef]];
    NSAssert(nil != aimRef,@"Aim reference is nil. No aim found.");

    ///////////////////////////////////////////////////////////////////////
    //Make buttons:
    //Remember, each button has a dummy CCSprite made in CocosBuilder for
    //positional data reference. Get the data, remove the dummy, and add
    //the actual button in place of the dummy.
    ///////////////////////////////////////////////////////////////////////    
    //Fire Button
    fireButton = [[[SneakyHoldEventButton alloc] initWithRect:CGRectZero] autorelease]; ;
    fireButton.ButtonPressEventID = EventList::CONTROLS_FIRE_BUTTON_PRESSED;
    fireButton.ButtonReleaseEventID = EventList::CONTROLS_FIRE_BUTTON_RELEASED;
    SneakyButtonSkinnedBase* skinFireButton = [SneakyButtonSkinnedBase skinnedButton];
    skinFireButton.defaultSprite = [CCSprite spriteWithSpriteFrameName:@"Fire_UP.png"];
    skinFireButton.pressSprite = [CCSprite spriteWithSpriteFrameName:@"Fire_DOWN.png"];
    skinFireButton.button = fireButton;
    [skinFireButton setPosition: [self getChildByTag:CCB_FIRE_BUTTON_TAG].position];
    [self removeChildByTag:CCB_FIRE_BUTTON_TAG];
    [self addChild:skinFireButton z:0 tag:CCB_FIRE_BUTTON_TAG];
    
    /////////////////////////////////////////////////////
    //Weapon Switch Button
    SneakyHoldEventButton* weaponBoostButton = [[[SneakyHoldEventButton alloc] initWithRect:CGRectZero] autorelease]; ;
    weaponBoostButton.ButtonPressEventID = EventList::CONTROLS_SWAP_BUTTON_PRESSED;
    weaponBoostButton.ButtonReleaseEventID = EventList::CONTROLS_SWAP_BUTTON_RELEASED;
    weaponBoostSkinnedButton = [SneakyButtonSkinnedBase skinnedButton];
    weaponBoostSkinnedButton.defaultSprite = [CCSprite spriteWithSpriteFrameName:@"Swap_UP.png"];
    weaponBoostSkinnedButton.pressSprite = [CCSprite spriteWithSpriteFrameName:@"Swap_DOWN.png"];
    weaponBoostSkinnedButton.button = weaponBoostButton;
    [weaponBoostSkinnedButton setPosition: [self getChildByTag:CCB_BOOST_BUTTON_TAG].position];
    [self removeChildByTag:CCB_BOOST_BUTTON_TAG];
    [self addChild:weaponBoostSkinnedButton z:0 tag:CCB_BOOST_BUTTON_TAG];
    
    /////////////////////////////////////////////////////
    //Overdrive Button
    SneakyHoldEventButton* overdriveButton = [[[SneakyHoldEventButton alloc] initWithRect:CGRectZero] autorelease]; ;
    overdriveButton.ButtonPressEventID = EventList::CONTROLS_OVERDRIVE_BUTTON_PRESSED;
    overdriveButton.ButtonReleaseEventID = EventList::CONTROLS_OVERDRIVE_BUTTON_RELEASED;
    overdriveSkinnedButton = [SneakyButtonSkinnedBase skinnedButton];
    overdriveSkinnedButton.defaultSprite = [CCSprite spriteWithSpriteFrameName:@"Overdrive_UP.png"];
    overdriveSkinnedButton.pressSprite = [CCSprite spriteWithSpriteFrameName:@"Overdrive_DOWN.png"];
    overdriveSkinnedButton.button = overdriveButton;
    [overdriveSkinnedButton setPosition: [self getChildByTag:CCB_OVERDRIVE_BUTTON_TAG].position];
    [self removeChildByTag:CCB_OVERDRIVE_BUTTON_TAG];
    [self addChild:overdriveSkinnedButton z:0 tag:CCB_OVERDRIVE_BUTTON_TAG];
    
    /////////////////////////////////////////////////////
    //Pause Button
    pauseButton = [[[SneakyHoldEventButton alloc] initWithRect:CGRectZero] autorelease];
    pauseButton.ButtonPressEventID = EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED;
    pauseButton.ButtonReleaseEventID = EventList::CONTROLS_PAUSEGAME_BUTTON_RELEASED;
    SneakyButtonSkinnedBase* skinPauseButton = [SneakyButtonSkinnedBase skinnedButton];
    skinPauseButton.position = ccp([CCDirector sharedDirector].screenSize.width - 20,300);
    skinPauseButton.defaultSprite = [CCSprite spriteWithSpriteFrameName:@"PauseButton.png"];
    skinPauseButton.pressSprite = [CCSprite spriteWithSpriteFrameName:@"PauseButton.png"];
    skinPauseButton.button = pauseButton;
    [self addChild:skinPauseButton];
    
    
    ////////////////////////////
    //Add overdrive panel
    ////////////////////////////
    NSMutableArray* overdriveMenuItemsArray = [NSMutableArray arrayWithCapacity: OverdriveConstants::OVERDRIVE_CHARACTERS_MAX_COUNT ];
    
    //For every overdrive...
    for ( NSUInteger curOverdriveIndex = 0; curOverdriveIndex < OverdriveConstants::OVERDRIVE_CHARACTERS_MAX_COUNT; ++curOverdriveIndex )
    {
        //Generate the selector-to-be-called
        NSString* curSelectorString = [NSString stringWithFormat:@"overdriveButton%iTapped:", curOverdriveIndex];
        
        //Instantiate the button:
        //Here we grab the pair from the array and extract Object1, which represents the overdrive
        //that HITS. Since we're only extracting the filenames for the button sprites, it is
        //irrelevant if we use Object1/HIT or Object2/MISS, since they both have the same data for
        //this particular use.
        Pair* overdriveHitMissPair = [playerRef.OverdrivesArray objectAtIndex:  OverdriveConstants::OVERDRIVE_ARRAY_CHARACTER_OFFSETS[curOverdriveIndex] ];
        
        Overdrive* curOverdrive = static_cast<Overdrive*>(overdriveHitMissPair.Object1);
        
        CCMenuItem* menuItem = [CCMenuItemSprite itemWithNormalSprite: [CCSprite spriteWithSpriteFrameName:curOverdrive.OverdrivePanelIconInactiveFilename]
                                                       selectedSprite: [CCSprite spriteWithSpriteFrameName:curOverdrive.OverdrivePanelIconActiveFilename]
                                                               target: self
                                                             selector: NSSelectorFromString(curSelectorString)];
        //Add to the list of buttons
        [overdriveMenuItemsArray addObject:menuItem];
    }
    
    //Choose which button is enabled by default.
    CCMenuItem* overdriveDefaultEnabledItem = [overdriveMenuItemsArray objectAtIndex:0];
    
    overdrivePanel = [CCRadioMenu menuWithArray: overdriveMenuItemsArray ];
    overdrivePanel.position = HIDDEN_POS;
    [overdrivePanel alignItemsHorizontallyWithPadding: 0];
    [overdrivePanel setSelectedItem_: overdriveDefaultEnabledItem];
    overdrivePanel.touchEnabled = NO;
    [self addChild:overdrivePanel z: -self.children.count];
    
    //Schedule for updates
    [self scheduleUpdate];


}

-(void) cleanup
{
    //detatch listeners
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_JEROME_START_AIM_PHASE);
    UNREGISTER_EVENT(self, EventList::OVERDRIVE_JEROME_END_AIM_PHASE);
    UNREGISTER_EVENT(self, EventList::CONTROLS_RESTRICT_CONTROLS);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_JOHNSON_END_AIM_PHASE
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_JOHNSON_START_AIM_PHASE
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_OVERDRIVE_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_BARRICADE_HEALTH_CHANGED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_WEAPON_OUT_OF_AMMO
                                                  object:nil];
    [super cleanup];
}
-(void) dealloc
{
    [aimTouchPanel release];
    [movementTouchPanel release];
    [self removeAllChildrenWithCleanup:YES];
    [super dealloc];
}

-(void) update: (ccTime) delta
{
    /*************************************************************************************************
     Here we will be calling 'processTouchesWithException: relativeToNode:'. The idea of using
     'relativeToNode: self' is because we want to search within this layer.
     The nuance here is that the movementTouchPanel is a hardcoded rect, therefore it's treated as
     screen space coordinates. The trick being done here is that by treating to the touch's position
     as a relative position to the node, it's as if the hardcoded coordinates now correspond to said
     node.
     Example:
     rect origin:           (100,100)
     rect size:             (100,100)
     self position:         (10,0)
     screen touch location: (105,120)
     
     Initially, the touch should be in the rect, however, that's because we are not taking offsets of 
     CCNode hierarchies into consideration...
     
     Due to hierarchy, the actual screen location of the rect origin is (100,100) + (10,0) = (110,100).
     By that understanding, screen touch (105,120) is outside the rect, kind of to the left.
     This what the 'relativeToNode: self' is accomplishing here.
     *************************************************************************************************/
    
    //Process touches for the player movement
    [movementTouchPanel processTouchesWithException: aimTouchPanel.AcquiredTouch
                                     relativeToNode: self];
    
    //If we have a touch...
    if ( nil != movementTouchPanel.AcquiredTouch )
    {
        //If the touch is not the same as last frame...
        if( NO == CGPointEqualToPoint( previousMovementDestination, movementTouchPanel.AcquiredTouch.location ) )
        {
            //Set the new "previous position"
            previousMovementDestination = movementTouchPanel.AcquiredTouch.location;
            
            //Send new movement destination event
            [[NSNotificationCenter defaultCenter] postNotificationName:EventList::PLAYER_NEW_MOVEMENT_DESTINATION
                                                                object: [NSValue valueWithCGPoint:movementTouchPanel.AcquiredTouch.location]];
        }
    }
    
    //Process touches for the aim
    [aimTouchPanel processTouchesWithException: movementTouchPanel.AcquiredTouch
                                relativeToNode: self];
    
    //If we have a touch...
    if ( nil != aimTouchPanel.AcquiredTouch )
    {
        //If the touch is not the same as last frame...
        if( NO == CGPointEqualToPoint( previousAimDestination, aimTouchPanel.AcquiredTouch.location ) )
        {
            //Set the new "previous position"
            previousAimDestination = aimTouchPanel.AcquiredTouch.location;
            
            //Send new movement destination event
            //CCLOG(@"Class Type: %@", aimTouchPanel.AcquiredTouch);

            
            [[NSNotificationCenter defaultCenter] postNotificationName:EventList::CONTROLS_NEW_AIM_DESTINATION
                                                                object: aimTouchPanel.AcquiredTouch];
        }
    }
}

-(void) activateSelfAndAllComponents
{
    [self scheduleUpdate]; //For aiming and moving panels
    fireButton.IsTouchEnabled = YES;
    pauseButton.IsTouchEnabled = YES;
    aimRef.visible = YES;
    overdriveSkinnedButton.button.status = (isOverdriveButtonRestricted ? NO : YES);
    weaponBoostSkinnedButton.button.status = (isWeaponBoostButtonRestricted ? NO : YES);
    overdrivePanel.enabled = YES;
}

-(void) deactivateSelfAndAllComponents
{
    [self unscheduleUpdate]; //For aiming and moving panels
    fireButton.IsTouchEnabled = NO;
    pauseButton.IsTouchEnabled = NO;
    aimRef.visible = NO;
    overdriveSkinnedButton.button.status = NO;
    weaponBoostSkinnedButton.button.status = NO;
    overdrivePanel.enabled = NO;
}

-(void) ComponentAcquireGameplaySceneTouchControlLayer_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    TouchControlLayer** requestorRef = (TouchControlLayer**)[requestorRefPointer pointerValue];
    (*requestorRef) = self;
}

-(void) gameplayBarricadeHealthChanged_EventHandler: (NSNotification*) notification
{
    NSNumber* barricadeCurrentHealth = [notification object];
    [healthAmountLabel setString: [barricadeCurrentHealth stringValue] ];
}

-(void) gameplayWeaponOutOfAmmo_EventHandler: (NSNotification*) notification
{
    //Set the weapon
    NSNumber* weaponIndex = [NSNumber numberWithInteger:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::CONTROLS_WEAPON_SELECTED
                                                        object: weaponIndex];
    
    
    //update the weapon icon based on the current weapon
    //to do this we must first get the weapon from the player.
    //We must always do this on runtime because the player's
    //current weapon is always changing.
    WeaponBase* weaponRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON
                                                        object:[NSValue valueWithPointer:&weaponRef]];
    NSAssert(nil != weaponRef,@"Weapon reference is nil. No weapon found. (Is Player Dead?)");
    [dataDisplayStatsAmmoSprite setDataForWeapon:weaponRef];
    
    
    //Set the weapon panel to the correctly selected weapon
    //(I just copied and pasted from the setup code. I don't exactly know what I'm doing here)
    CCMenuItem* pistolSlot = [weaponSwitchPanel.children objectAtIndex:0];
    [weaponSwitchPanel setSelectedItem_: pistolSlot];
    [pistolSlot selected];
    
    //Play the out of ammo sound
    [[SimpleAudioEngine sharedEngine]playEffect:@"OutOfAmmo.wav"];
}


@end
