//
//  SearchTimerClockTransition.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "SearchTimerClockTransition.h"
#import "SearchTimerClock.h"
#import "NotificationCenterEventList.h"
#import "GameplayScene.h"
#import "SimpleAudioEngine.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "AppWidescreener.h"

@implementation SearchTimerClockTransition

-(void) didLoadFromCCB
{
    //Initialize local variables
    [backdropLayerColor setOpacity:0];
    
    //Acquire the SearchTimerClock reference
    searchTimerClockRef = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_SEARCHTIMERCLOCK
                                                        object: [NSValue valueWithPointer:&searchTimerClockRef]];
    NSAssert(searchTimerClockRef != nil, @"Failed to acquire SearchTimerClock");
    
    //configure CCActions to: scale, delay, and transition to gameplay scene.
    const float scaleTime = 0.85f;
    CCFiniteTimeAction* scaleToCenterAction;
    if( YES == [AppWidescreener isWidescreenEnabled] )
    {
       scaleToCenterAction = [CCSpawn actionOne:[CCScaleTo actionWithDuration:scaleTime scale:1.0f]
                                            two:[CCMoveBy actionWithDuration:scaleTime position:
                                                 [AppWidescreener getWidescreenLetterboxOffset]]];
    }
    else
    {
        scaleToCenterAction = [CCScaleTo actionWithDuration:scaleTime scale:1.0f];      
    }
    
    CCDelayTime* delayAction = [CCDelayTime actionWithDuration:3.5f];
    CCCallBlock* blockAction = [CCCallBlock actionWithBlock:^{
        //Night has fallen, therefore, a day has passed. Let's update our save data to reflect that.
        SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
        int currentProgressionDay = [[resourceGatheringProperties objectForKey:GSP::CURRENT_PROGRESSION_DAY] intValue];
        [resourceGatheringProperties setValue:[NSNumber numberWithInt:currentProgressionDay+1]
                                       forKey:GSP::CURRENT_PROGRESSION_DAY];
        
        //transition to gameplay scene
        [[CCDirector sharedDirector] replaceScene:[GameplayScene node]];
    }];
    
    //run actions
    [searchTimerClockRef runAction: [CCSequence actions:
                                     scaleToCenterAction,
                                     delayAction,
                                     blockAction, nil]];
    [backdropLayerColor runAction: [CCFadeTo actionWithDuration:scaleTime opacity:210]];
    [[SimpleAudioEngine sharedEngine] playEffect:@"RG_Clock_TransitionToGameplay.wav"];
}

@end
