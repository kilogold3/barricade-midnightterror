//
//  OverdriveExecutionYunaV1.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OverdriveExecutionJonnyV1.h"
#import "NotificationCenterEventList.h"
#import "DebugDrawLineOfFire.h"
#import "EnemyField.h"
#import "CCBReader.h"
#import "WeaponBase.h"


//Hacks --  All these includes are here because we just tossed all
//          the funtionality into this file. There is probably a better way
//          to do this
#import "PlayerState_Alive.h"
#import "Player.h"
#import "PlayerArms.h"
#import "Aim.h"
#import "GB2Engine.h"
#import "RayCastCallbackLineOfFire.h"
#import "EnemyBase.h"
#import "SimpleAudioEngine.h"

const CGPoint LINE_OF_FIRE_DESTINATION_UNINITIALIZED = { -1,-1};

@implementation OverdriveExecutionJonnyV1
-(void) executeOverdrive
{
    const float MAX_AIM_TIME = 4.0f;
    [self scheduleUpdate];
    
    //Initialize the line of fire destination to invalid
    lineOfFireDestination = LINE_OF_FIRE_DESTINATION_UNINITIALIZED;
    
    //Set the timer
    currentAimTime = MAX_AIM_TIME;
    
    //Acquire the gameplay background layer to darken
    CCLayer* gameplayLayerBackground = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND
                                                        object:[NSValue valueWithPointer:&gameplayLayerBackground]];
    NSAssert(nil != gameplayLayerBackground,@"Gameplay layer background reference is nil. No gameplay layer background found.");
    
    //set up the actual black layer w/ all the actions
    CCLayerColor* blackLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 255)
                                                      width:[CCDirector sharedDirector].screenSize.width
                                                     height:[CCDirector sharedDirector].screenSize.height];
    
    CCFadeOut* fadeOutEffect = [CCFadeOut actionWithDuration:MAX_AIM_TIME];
    
    CCCallFunc* finalizeOverdriveCallback = [CCCallFunc actionWithTarget:self
                                                                selector:@selector(trajectBulletEffect)];
    
    [blackLayer runAction: [CCSequence actionOne:fadeOutEffect
                                             two:finalizeOverdriveCallback]];
    
    //Play the lock-on sound. the sound length is intended to be as long as the FadeOut time
    [[SimpleAudioEngine sharedEngine] playEffect:@"OD_SFX_JohnsonLockOn.wav"];
    
    
    //Add the layer into the engine.
    [gameplayLayerBackground addChild:blackLayer];
    
    
    //Start the aiming phase.
    //as well as allow leo to move.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JOHNSON_START_AIM_PHASE
                                                        object:nil];
    
    //Grab hold of all the enemies on screen
    enemyFieldRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD, [NSValue valueWithPointer:&enemyFieldRef]);
    NSAssert(nil != enemyFieldRef,@"Reference is nil. No EnemyField instance found.");
    
    //Override all enemy health display behaviors
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {
        //Only mess with the alive enemies. The dead ones should not play any part in this.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.DisplayHealthMasterOverride = YES;
            curEnemy.AllowHealthDisplay = NO;
        }
    }
    
    //This should enable the line of fire...
    playerRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_PLAYER_TARGET,[NSValue valueWithPointer:&playerRef]);
    NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
    NSAssert([playerRef.CurrentPlayerState class] == [PlayerState_Alive class],
             @"Compatible player state for weapon initialization");
    
    PlayerState_Alive* playerAliveState = (PlayerState_Alive*)playerRef.CurrentPlayerState;
    playerArmsRef = playerAliveState.PlayerArmsRef;
    
    aimRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_AIM_TARGET, [NSValue valueWithPointer:&aimRef]);
    NSAssert(nil != aimRef,@"Aim reference is nil. No aim found.");
    
    aimRef.visible = NO;
}

-(void) resetOverdrive
{
    aimRef.visible = YES;
    
    [self unscheduleUpdate];
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {
        //Only mess with the alive enemies. The dead ones have performed color modulation
        //previously, and we don't want to accidentally screw that up.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.DisplayHealthMasterOverride = NO;
            curEnemy.AllowHealthDisplay = YES;
            curEnemy.EnemySprite.color = ccWHITE;
        }
    }
}

-(void) trajectBulletEffect
{
    //Let's release control of Leo
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JOHNSON_END_AIM_PHASE
                                                        object:nil];
    
    //Instantiate the particle effect
    CCNode* bulletTracer = [CCBReader nodeGraphFromFile:@"FX_BulletFlare.ccbi"];
    [self addChild:bulletTracer];
    
    //Set it's original position. First, bring all coords to local space.
    //To do this, first we want to bring the coordinates to world space (screen coords).
    //From here we then want to bring them specifically to this node's space (because
    //it is aligned with the same coordinate space at the player's).
    CGPoint playerArmsPosition = [playerRef convertToWorldSpace:playerArmsRef.position];
    playerArmsPosition = [self convertToNodeSpace:playerArmsPosition];
    [bulletTracer setPosition:playerArmsPosition];
    
    //Move the particle effect down the line    
    //queue the call to the overdrive execution finalization
    const float MOVEMENT_TIME = 0.4f;

    [bulletTracer runAction: [CCSequence actions:[CCMoveTo actionWithDuration:MOVEMENT_TIME position:lineOfFireDestination],
                              [CCDelayTime actionWithDuration:MOVEMENT_TIME],
                              [CCCallFunc actionWithTarget:self selector:@selector(finalizeOverdriveExecution)],
                              nil]];
    
    //Play the sound effect
    [[SimpleAudioEngine sharedEngine] playEffect:@"OD_SFX_Sniper.wav"];
}

-(void) finalizeOverdriveExecution
{
    //Remove the particle effect
    [self removeAllChildrenWithCleanup:YES];
    
    //eliminate any enemies in the line of fire
    [self eliminateEnemiesWithinLineOfFire];
    
    //Reset the overdrive
    [self resetOverdrive];
    
    //clean-up/remove the overdrive execution instance (self)
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_FINISH_EXECUTION
                                                        object:nil];
    
    [self removeFromParentAndCleanup:YES];
    
    CCLOG(@"JOHNSON Overdrive Execution Complete!");
}

-(void) update:(ccTime)delta
{
    //Every frame, we will disable every enemy's health, and
    //re-enable only the targeted ones.
    for (EnemyBase* curEnemy in enemyFieldRef.children)
    {
        //Only mess with the alive enemies. The dead ones have performed color modulation
        //previously, and we don't want to accidentally screw that up.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.AllowHealthDisplay = NO;
            curEnemy.EnemySprite.color = ccWHITE;
        }
    }
    
    NSArray* enemiesInLineOfFire = [self getEnemiesWithinLineOfFire];
    for (EnemyBase* curEnemy in enemiesInLineOfFire )
    {
        //Only mess with the alive enemies. The dead ones have performed color modulation
        //previously, and we don't want to accidentally screw that up.
        if( curEnemy.CurrentHealth > 0 )
        {
            curEnemy.AllowHealthDisplay = YES;
            curEnemy.EnemySprite.color = ccRED;
        }
    }
}

-(void) eliminateEnemiesWithinLineOfFire
{
    NSArray* intersectedEnemies = [self getEnemiesWithinLineOfFire];
    
    for (EnemyBase* curEnemy in intersectedEnemies)
    {
        [curEnemy setCurrentHealth:0];
        [curEnemy killEnemy];
    }
}

-(NSArray*) getEnemiesWithinLineOfFire
{
    const CGPoint aimPosition = [aimRef.parent convertToWorldSpace:aimRef.position];
    const CGPoint originPosition = [playerRef convertToWorldSpace:playerArmsRef.position];
    
    //Project the target position all the way to the left bound
    //of the screen to simulate the bullet firing past the reticle.
    //We use the line equation to give us the projected point where
    //it intersects with the left screen bounds.
    
    //Calculate slope
    //
    //  (y2 - y1)
    //  ----------
    //  (x2 - x1)
    const float m = (aimPosition.y - originPosition.y) / (aimPosition.x - originPosition.x);
    
    //Find Y-intercept
    // y = mx + b [OR] b = y-mx
    const float b = originPosition.y - (m * originPosition.x);
    
    //acquire the projected point on the screens left bound:
    //(0,0) to (0, screen_height).
    const CGPoint targetPosition = ccp(0,b);
    
    //Perform raycast
    RayCastCallbackLineOfFire raycast;
    [GB2Engine sharedInstance].world->RayCast(&raycast,
                                              b2Vec2FromCGPoint(targetPosition),
                                              b2Vec2FromCGPoint(originPosition)
                                              );
    
    //Save the targetPosition for when we draw the line of fire.
    //This line is sort of out of place, but we save having to recalculate
    //the targetPosition again.
    //NOTE:
    //This position is in world coordinates (due to raycast, which only operates
    //in world space by Box2D design), so we'll want to bring it to this node's
    //local coordinates if we want to use it inside of Cocos2D.
    //Since we'll only be using this in the draw method using Cocos2D mechanics, let's
    //go ahead and make that modification right now.
    lineOfFireDestination = targetPosition;
    lineOfFireDestination = [self convertToNodeSpace:lineOfFireDestination];
    
    //Return the list of enemis within the line of fire
    return raycast.getIntersectedEnemiesList();
}

-(void) draw
{
    [super draw];
    
    //Only draw if the destination line has been initialized
    if( NO == CGPointEqualToPoint(lineOfFireDestination, LINE_OF_FIRE_DESTINATION_UNINITIALIZED) &&
        [playerRef.CurrentPlayerState class] ==  [PlayerState_Alive class] )
    {
        //Bring all coords to local space.
        //To do this, first we want to bring the coordinates to world space (screen coords).
        //From here we then want to bring them specifically to this node's space (because
        //it is aligned with the same coordinate space at the player's).
        CGPoint playerArmsPosition = [playerRef convertToWorldSpace:playerArmsRef.position];
        playerArmsPosition = [self convertToNodeSpace:playerArmsPosition];
        
        //Obtain the barrel position
        CGPoint playerWeaponBarrelPosition = CGPointZero;
        float dummyRotationBuffer = -1;
        [playerArmsRef.CurrentWeapon GetWeaponBarrelPositionBasedOnTarget:lineOfFireDestination
                                                                andOrigin:playerArmsPosition
                                                    withBarrelPositionOut:&playerWeaponBarrelPosition
                                                       withArmRotationOut:&dummyRotationBuffer];
        
        //Draw line
        ccDrawColor4F(1, 0, 0, 1);
        glLineWidth(1.5f);
        ccDrawLine(playerArmsPosition, lineOfFireDestination);
    }
    
}
@end
