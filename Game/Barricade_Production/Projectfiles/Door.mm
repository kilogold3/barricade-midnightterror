//
//  Door.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "Door.h"
#import "CCBReader.h"
#import "NotificationCenterEventList.h"
#import "SimpleAudioEngine.h"

@interface Door (PrivateMethods)
// declare private methods here
@end

@implementation Door

-(void) setIsLocked:(BOOL)IsLocked
{
    [super setIsLocked:IsLocked];
    
    if( isLocked )
    {
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:lockedItemSpriteID]];
    }
    else
    {
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:unlockedItemSpriteID]];
    }
}

-(void) Interact
{
    //if the door is locked....
    if( self.IsLocked )
    {
        //Let's launch the unlocking activity
        [self LaunchActivity];
    }
    //otherwise...
    else
    {
        //Let's just walk through the door.
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::RESOURCE_GATHERING_TRAVERSE_DOOR
                                                        object:destinationCCBI];
        
        //Play the walking-through-the-door sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"Through_Doors.wav"];
    }
}

@end
