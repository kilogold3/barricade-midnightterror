//
//  ActivityLauncher.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/13/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

/*******************************************************************************
 *  This class is used in the resource gathering and represents
 *  any mini-game / activity that can be performed during resource gathering.
 *  Example: Barricade Repair or Traveling.
 *
 *  Each activity is actually a CCLayer. That is because the activity will be
 *  encapsulated within the CCLayer itself. This activity-CCLayer will be added
 *  to the scene on top of the current room layer, but underneath the timer.
 *******************************************************************************/
#import "kobold2d.h"
#import "ResourceGatheringInteractiveObject.h"

@interface ActivityLauncher : ResourceGatheringInteractiveObject
{
    //Should be the class name of the activity-CCLayer
    NSString* activityToLaunch;
}

//We have to use a retain property, because the CCBReader loads strings
//using NSFoundation API which returns objects aith autorelease by default.
@property (nonatomic, retain) NSString* ActivityToLaunch;

//Launches the activity
-(void) Interact;

//Actual implementation of the activity launching.
//This is done so that child classes can call this
//behavior without having to walk the parent hierarchy
//in order to invoke the appropriate "Interact" method.
//In objective-c, doing this is sort of hacky. This
//would be no problem in C++.
-(void) LaunchActivity;

@end
