//
//  GameAudioList.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/29/13.
//
//

#ifndef Barricade_Production_GameAudioList_h
#define Barricade_Production_GameAudioList_h

//GAL stands for Game Audio List
namespace GAL
{
    static NSString* const BGM_GAMEPLAY_COMBAT = @"M.wav";
    static NSString* const SND_GAMEPLAY_PERCUSSION = @"P.wav";
    
    static NSString* const SND_WEAPON_PISTOL_FIRE = @"sndWeaponPistolFire2.wav";
    static NSString* const SND_WEAPON_RIFLE_FIRE = @"sndWeaponPistolFire.wav";
    static NSString* const SND_ENEMY_WALKER_ATTACK = @"sndEnemyWalkerAttack.wav";
    static NSString* const SND_ENEMY_WALKER_DEATH = @"sndEnemyWalkerDeath.wav";
    static NSString* const SND_ENEMY_FLIER_DEATH = @"GP_SFX_Chupacabra_FlyerDeath.wav";
    static NSString* const SND_ENEMY_RUNNER_DEATH = @"GP_SFX_Chupacabra_RunnerDeath.wav";
    static NSString* const SND_ENEMY_TANK_DEATH = @"GP_SFX_Chupacabra_TankDeath.wav";
};

#endif
