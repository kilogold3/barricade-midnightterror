//
//  TravelLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TravelLayer.h"
#import "NotificationCenterEventList.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "TravelNodeButton.h"
#import "TravelTree.h"
#import "TravelPrompt.h"
#import "CCBReader.h"
#import "CCControlButton.h"
#import <vector>

@interface TravelLayer (PrivateMethods)
// declare private methods here
@end

@implementation TravelLayer

-(void) ResourceGatheringTravelPromptEnabled_EventHandler: (NSNotification*) notification
{
    //Present Travel Prompt
    TravelPrompt* roomLayer = static_cast<TravelPrompt*>([CCBReader nodeGraphFromFile:@"TravelPrompt.ccbi"]);
    roomLayer.DestinationTravelNodeButton = [notification object];
    
    [self addChild:roomLayer];
    
    //Disable buttons on this layer
    travelNodeMenu.enabled = NO;
}

-(void) ResourceGatheringTravelPromptDisabled_EventHandler: (NSNotification*) notification
{
    //enable the buttons on this layer
    travelNodeMenu.enabled = YES;
}

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		//[self scheduleUpdate];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    REGISTER_EVENT(self,
                   @selector(ResourceGatheringTravelPromptEnabled_EventHandler:),
                   EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_ENABLED)
    
    REGISTER_EVENT(self,
                   @selector(ResourceGatheringTravelPromptDisabled_EventHandler:),
                   EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_DISABLED)
    
    SEND_EVENT(EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE, [NSNumber numberWithBool:NO]);

}

-(void) onExit
{
    SEND_EVENT(EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE, [NSNumber numberWithBool:YES]);
}

-(void) cleanup
{
	[super cleanup];
    
    //So I figured this was optional, but for some reason, if I don't include the line below,
    //we the travel menu will not deallocate and hijack touches for the areas where the nodes are.
    [self removeChild:travelNodeMenu cleanup:YES];

    UNREGISTER_EVENT(self, EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_ENABLED)
    UNREGISTER_EVENT(self, EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_DISABLED)
}


-(void) QuitTravel: (id) sender
{
    SEND_EVENT(EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY, nil);

    [self removeFromParentAndCleanup:YES];
}

-(void) NewProgressionSelected: (id) sender
{
    SEND_EVENT(EventList::RESOURCE_GATHERING_TRAVEL_PROMPT_ENABLED, sender);
}

-(void) didLoadFromCCB
{
    //Let's grab the current game's progression
    const GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    
    const SaveObjectPropertyList* resourceGatheringPropertyList =
    [gameSaveData getSaveObjectProperties:GSC::GAME_PROGRESS];
    
    const SaveObjectPropertyList* storyScenePropertyList =
    [gameSaveData getSaveObjectProperties:GSC::STORY_SCENE];
    
    const short currentGameProgression =
    [[resourceGatheringPropertyList objectForKey: GSP::CURRENT_PROGRESSION] shortValue];
    
    NSString* const currentStoryPath =
    [storyScenePropertyList objectForKey:GSP::CURRENT_STORY_PATH];
    
    CCLOG(@"Loading Game Progression: %i",currentGameProgression);
        
    //Create tree node root
    GameTreeNode* nodeTree = new GameTreeNode( static_cast<TravelNodeButton*>([travelNodeMenu.children objectAtIndex:0]) );
    
    //Cycle through all the progression nodes creating the tree
    //(start at the second one. We have the first)
    for (NSUInteger curProgressionNodeIndex = 1;
         curProgressionNodeIndex < travelNodeMenu.children.count;
         ++curProgressionNodeIndex)
    {
        //Let's make sure we are an actual progression, and not some other button
        CCNode* currentChildNode =
        [travelNodeMenu.children objectAtIndex:curProgressionNodeIndex];
        
        if( [currentChildNode isKindOfClass: [TravelNodeButton class]])
        {
            nodeTree->AddNode( new GameTreeNode( static_cast<TravelNodeButton*>(currentChildNode) ) );
        }
    }
    
    //Look for our current location in the tree
    TravelNodeButton* currentLocation = nil;
    for (CCNode* curMenuItem in travelNodeMenu.children)
    {
        //Check if we are working with a TravelNodeButton before messing with anything
        if( [curMenuItem isKindOfClass: [TravelNodeButton class]] )
        {
            TravelNodeButton* curTravelNodeButton = static_cast<TravelNodeButton*>(curMenuItem);
            if( curTravelNodeButton.ProgressionValue == currentGameProgression and
               [curTravelNodeButton.StoryPathValue isEqualToString:currentStoryPath])
            {
                currentLocation = curTravelNodeButton;
            }
        }
    }
    NSAssert(currentLocation != nil, @"Current location not found");
    
    GameTreeNode* gameTreeNodeCurrentLocation = nodeTree->FindTreeNodeFromData(currentLocation);
    
    
    //store current, and children references in a list of at least 2 slots
    NSMutableArray* availableTravelNodes = [NSMutableArray arrayWithCapacity:2];
    
    [availableTravelNodes addObject:gameTreeNodeCurrentLocation->GetNodeData()];
    
    for( size_t curChildNode = 0; curChildNode < gameTreeNodeCurrentLocation->GetChildren().size(); ++curChildNode )
    {
        [availableTravelNodes addObject:gameTreeNodeCurrentLocation->GetChildren()[curChildNode]->GetNodeData()];
    }
    
    
    //iterate through the nodeMenu disabling all children not matching the list
    for (TravelNodeButton* curMenuItem in travelNodeMenu.children)
    {
        //Check if we are working with a TravelNodeButton before messing with anything
        if( [curMenuItem isKindOfClass: [TravelNodeButton class]] )
        {
            BOOL foundMatch = NO;
            for (TravelNodeButton* curTravelNode in availableTravelNodes)
            {
                if(curMenuItem == curTravelNode)
                {
                    foundMatch = YES;
                    break;
                }
            }
            
            if(NO == foundMatch)
            {
                curMenuItem.isEnabled = NO;
            }
        }
    }
    
    //remove all disabled travel nodes (We won't use this anymore, but let's keep it here for reference)
    //    for( NSUInteger curChildNodeIndex = 0; curChildNodeIndex < travelNodeMenu.children.count; ++curChildNodeIndex )
    //    {
    //        TravelNodeButton* curNode = [travelNodeMenu.children objectAtIndex:curChildNodeIndex];
    //        if( NO == curNode.isEnabled )
    //        {
    //
    //            [travelNodeMenu removeChild:curNode cleanup:YES];
    //            curChildNodeIndex = -1; //This is to allow the loop to start over at 0.
    //                                    //(Causes underflow)
    //        }
    //    }

    
    //Mark all non-accessible nodes
    for (CCNode* curTravelNode in travelNodeMenu.children)
    {
        //Check if we are working with a TravelNodeButton before messing with anything
        if( [curTravelNode isKindOfClass: [TravelNodeButton class]] )
        {
            [static_cast<TravelNodeButton*>(curTravelNode).disabledImage setColor:ccRED];
        }
    }
    
    //Mark all traveled nodes
    GameTreeNode* iteratingTreeNode = gameTreeNodeCurrentLocation;
    while (nil != iteratingTreeNode)
    {
        [iteratingTreeNode->GetNodeData().disabledImage setColor:ccBLUE];
        iteratingTreeNode = iteratingTreeNode->GetParent();
    }
    
    //finally, we disable the current location now. We don't want to re-enter our location.
    currentLocation.isEnabled = NO;
    
    //let's cleanup the allocated memory on the heap
    nodeTree->DeleteChildren();
    delete nodeTree;
    
 }

@end
