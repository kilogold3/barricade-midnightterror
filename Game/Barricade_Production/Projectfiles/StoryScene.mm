//
//  StoryScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "StoryScene.h"
#import "CCBReader.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"

#import "CCGLView.h"
#import "KKStartupConfig.h"
#import "AppWidescreener.h"

const short STORY_SCENE_INSTANCE_TAG = 1;
const float SCENE_SKIP_TRIGGER_TIME = 3.0f;

@implementation StoryScene

-(void) onEnterTransitionDidFinish
{
    /**************************************************************************
     We want to make sure the previous scene is deallocated before allocating
     this one. That is because scene transitions are executed in the following
     order:
     1. scene: OtherScene
     
     2. init: <OtherScene = 066B2130 | Tag = -1>
     
     3. onEnter: <OtherScene = 066B2130 | Tag = -1>
     ￼
     4. [[[[Transition is running here]]]
     
     5. onExit: <FirstScene = 0668DF40 | Tag = -1>
     
     6. onEnterTransitionDidFinish: <OtherScene = 066B2130 | Tag = -1>
     
     7. dealloc: <FirstScene = 0668DF40 | Tag = -1>
     
     Note that the FirstScene dealloc method is called last. This means that during
     onEnterTransitionDidFinish, the previous scene is still in memory. If you want
     to allocate memory-intensive nodes at this point, you’ll have to schedule a
     selector to wait at least one frame before doing the memory allocations, to be
     certain that the previous scene’s memory is released. You can schedule a method
     that is guaranteed to be called the next frame by omitting the interval parameter
     when scheduling the method:
     
     [self schedule:@selector(waitOneFrame:)];
     
     The method that will be called then needs to unschedule itself by calling unschedule
     with the hidden_cmd parameter:
     
     -(void) waitOneFrame:(ccTime)delta
     {
     [self unschedule:_cmd];
     // delayed code goes here ...
     }
     **************************************************************************/
    [self scheduleOnce:@selector(loadSceneAfterOneFrame)
                 delay:0];
    
    [super onEnterTransitionDidFinish];
}

-(void) loadSceneAfterOneFrame
{   
    //Step1: Get the Story Chapter
    SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentOverallDay = [[resourceGatheringProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
    [KKConfig selectKeyPath:@"GameSettings"];
    const int oleanderCityDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
    
    NSString* CCB_StorySceneFilename = nil;
    //If we are past the game's days limit...
    if( currentOverallDay > oleanderCityDaysLimit )
    {
        //We will choose to transition to the bad ending
        CCB_StorySceneFilename = [NSString stringWithFormat:@"SS_BE_A_Main%@.ccbi",
                                  [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    }
    //Otherwise, we are still in time to reach the good ending
    else
    {
        //Generate the corresponding cutscene filename to load.
        int currentlyLoadedStoryChapter = [[resourceGatheringProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
        CCB_StorySceneFilename = [NSString stringWithFormat:@"SS_%02iA_A_Main%@.ccbi",
                                  currentlyLoadedStoryChapter,
                                  [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
        
        //IMPORTANT:
        //Let's write out our save file from here only if...
        //1) We are before the final cutscene (progression 6)
        //2) We are not loading the final cutscene (already assumed if we are here)
        //
        //The major goal here is to avoid having the player end up in the ending cutscene
        //if they decide to continue after completing the game.
        if( currentlyLoadedStoryChapter < GSPV::FINAL_PROGRESSION )
        {
            [[GameSaveData sharedGameSaveData] WriteToFile];
        }
            
    }
    
    //Step2: Replace the current scene with StorySceneInstance corresponding to game data
    CCLOG(@"Loading StorySceneInstance: %@",CCB_StorySceneFilename);
    [[CCDirector sharedDirector] replaceScene:[CCBReader sceneWithNodeGraphFromFile:CCB_StorySceneFilename]];
    
    
}
@end
