//
//  TestScene.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "ActivityBase.h"

@class BarricadeRepairBlueprint;
@class CCControlButton;

@interface BarricadeRepairLayer : ActivityBase
{
    BarricadeRepairBlueprint* barricadeRepairBlueprint; //contains all the fragments on the blueprint
    
    CCLayer* barricadeFragmentsLayer; //contains all the fragments on the table
    
    CCControlButton* buttonToGameplay;
    CCControlButton* buttonToHelp;
    CCLabelTTF* instructionsLabel;
    CCNode* barricadeRepairedPromptNode;
}

-(void) QuitBarricadeRepair: (id) sender;

-(void) BarricadeRepairBlueprintLoaded_EventHandler: (NSNotification*) notification;

-(void) BarricadeRepairFullyRepaired_EventHandler:(NSNotification*)notification;

@end
