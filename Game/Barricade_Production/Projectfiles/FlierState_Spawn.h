//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@interface FlierState_Spawn : NSObject<IEnemyState>
{
    EnemyBase* enemyParent;
    float defaultHoverHeight; //height at which the enemy is registered.
    BOOL hasInitiallyAcquiredHoverHeight;
    SignedByte movementDirection;
    SignedByte hoverDirection;
}
@property (nonatomic,readwrite) float HoverHeight;

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

@end
