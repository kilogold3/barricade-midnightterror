//
//  AutosavePromptScene.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/1/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "CCBAnimationManager.h"

@interface AutosavePromptScene : CCLayer <CCBAnimationManagerDelegate>
{
    //Count for how many sequences we have in our scene.
    //This way, we know how many scenes to advance before
    //transitioning to the next game-scene.
    SignedByte totalTimelineSequencesForScene;
    SignedByte currentTimelineSequence;
    
    CCBAnimationManager* animationManagerRef;

}

@end
