//
//  GameAudioData.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/29/13.
//
//

#import <Foundation/Foundation.h>
#import "CDAudioManager.h"

static const int MAX_SOUND_IDS = 30; //We only support 30 different SFX

@interface GameAudioData : NSObject
{
    //CocosDenshion audio manager weak reference
    CDAudioManager* audioManager;
    
    //Sound Id timestamps
    NSDate* soundIdSlots [MAX_SOUND_IDS];
    
    //Debug
    BOOL printDebugData;
    
}
@property (readonly) CDAudioManager* cdAudioManager;

+ (GameAudioData *)sharedGameAudioData;
-(ALuint) preloadAndGenerateNewSoundIdWithFile: (NSString*) filename;
-(void) unloadSoundId: (ALuint) soundId;
-(CDSoundSource*) getSoundSourceFromID: (ALuint) soundId;
-(void) playSound:(ALuint) soundId;
-(void) playSound:(ALuint) soundId
            pitch:(float) pitch
              pan:(float) pan
             gain:(float) gain
             loop:(BOOL) loop;
@end