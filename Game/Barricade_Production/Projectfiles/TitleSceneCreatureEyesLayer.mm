//
//  TitleSceneCreatureEyesLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TitleSceneCreatureEyesLayer.h"
#import "GMath.h"

@implementation TitleSceneCreatureEyes
@synthesize OpenEyeDelay;
@synthesize BlinkSpeed;

-(id) init
{
    NSAssert(NO, @"Wrong initializer");
    return nil;
}
-(id) initWithFile:(NSString *)filename
{
    self = [super initWithFile:filename];
    
    if( self != nil )
    {
        //Call configuration at least once.
        //It will call itself every other time.
        [self configureEye];
    }
    return self;
}

-(id) initWithSpriteFrameName:(NSString *)spriteFrameName
{
    self = [super initWithSpriteFrameName:spriteFrameName];
    
    if( self != nil )
    {
        //Call configuration at least once.
        //It will call itself every other time.
        [self configureEye];
    }
    return self;
}

-(void) configureEye
{
    //Stop any previous actions.
    [self stopAllActions];
    
    //Set default values
    self.scaleY = 0; //closed eyes
    
    //Set random parameters
    self.position =  [self generateUniquePosition];
    
    self.OpenEyeDelay = gFloatRand(0.25f, 1.0f);
    
    self.BlinkSpeed = gFloatRand(0.05f, 0.15f);
    
    self.IntendedScale = gFloatRand(0.15f, 0.5f);
    
    self.scale = self.IntendedScale;
    
    self.color = ccRED;

    //Execute the action
    [self runAction: [CCSequence actions:
                      //1) Open eyes
                      [CCActionTween actionWithDuration:self.BlinkSpeed
                                                    key:@"scaleY"
                                                   from:0.0f
                                                     to:self.IntendedScale],
                      
                      //2) Stare
                      [CCDelayTime actionWithDuration:self.OpenEyeDelay],
                      
                      //3) Close eyes
                      [CCActionTween actionWithDuration:self.BlinkSpeed
                                                    key:@"scaleY"
                                                   from:self.IntendedScale
                                                     to:0.0f],
                      
                      //4) Brief pause
                      [CCDelayTime actionWithDuration:gFloatRand(0.5f, 2.0f)],
                      
                      //5) Reconfigure
                      [CCCallFunc actionWithTarget:self selector:@selector(configureEye)],
                      
                      nil]
     ];
}

//Generate a point that does not overlap with another pair of eyes
-(CGPoint) generateUniquePosition
{
    BOOL foundDuplicate;
    CGPoint newPostion;
    
    do {
        //Set loop condition flag.
        foundDuplicate = NO;
        
        //Generate a random position at least once.
        newPostion = [self generateRandomScreenPosition];
        
        //Search for any overlapping position
        for (TitleSceneCreatureEyes* eyesPair in self.parent.children)
        {
            //We check distance against the largest dimension: width.
            const float newEyeDistance = ccpLength(ccpSub(newPostion, eyesPair.position));
            if(  newEyeDistance < eyesPair.displayFrame.rect.size.width )
            {
                //Found an overlapping position.
                //Let's restart and generate a new position.
                foundDuplicate = YES;
                break;
            }
        }
    }
    while (YES == foundDuplicate);
    
    
    //Let's return with our unique position.
    return newPostion;
}

-(CGPoint) generateRandomScreenPosition
{
    const CGSize screenSize = [CCDirector sharedDirector].screenSize;
    return ccp( (int)gFloatRand(0, screenSize.width) ,
                (int)gFloatRand(0, screenSize.height) );
}
@end


const Byte MAX_EYE_COUNT = 12;
@implementation TitleSceneCreatureEyesLayer

-(void) startEyes
{
    creatureEyeCount = 0;
    
    [self schedule:@selector(spawnEyes) interval:0];
}

-(void) spawnEyes
{
    [self unschedule:@selector(spawnEyes)];
    
    if( creatureEyeCount < MAX_EYE_COUNT )
    {        
        TitleSceneCreatureEyes* newEyes =
        [[[TitleSceneCreatureEyes alloc] initWithSpriteFrameName:@"ChupaEyes.png"] autorelease];
        [self addChild:newEyes];
        
        ++creatureEyeCount;
        
        //[self schedule:@selector(spawnEyes) interval: gFloatRand(0.3f, 1.0f)];
        [self schedule:@selector(spawnEyes) interval: 0];
    }
}

@end
