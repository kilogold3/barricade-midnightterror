//
//  ParticlesRain.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@class CDSoundSource;

@interface ParticlesRain : CCParticleSystemQuad
{
    CDSoundSource* soundSourceRain;
}

@end
