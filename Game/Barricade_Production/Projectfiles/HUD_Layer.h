//
//  HUD_Layer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

/**********************************************************
 * The HUD layer is designed to manage information data
 * such as combos (maybe), and enemy boss life bars and such
 **********************************************************/

@interface HUD_Layer : CCLayer 
{
}

@end