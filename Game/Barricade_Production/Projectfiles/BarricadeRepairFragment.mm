//
//  BarricadeRepairPiece.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "BarricadeRepairFragment.h"
#import "NotificationCenterEventList.h"
#import "CCBReader.h"
#import "GB2Engine.h"

@interface BarricadeRepairFragment (PrivateMethods)
-(BOOL) processFragmentAttachment; //Returns YES if the fragement was attached
@end

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

@implementation BarricadeRepairFragment
@synthesize StartLocation=startLocation;

-(void) setStartLocation:(CGPoint)StartLocation
{
    startLocation = StartLocation;
    [self setPosition:startLocation];
}

-(BOOL) processFragmentAttachment
{
    //If we are colliding with the blueprint
    if( blueprintCollisionCounter > 0 )
    {
        //enable fragment on blueprint
        /*
         A goodway to do this is via TAGS. The same tag can be shared for both
         drag/drop-fragment and blueprint-fragment. We can send this tag to the blueprint
         and the blueprint can activate that sprite.
         */
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::BARRICADE_REPAIR_FRAGMENT_REPAIR
                                                            object: self];
        
        return YES;
    }
    
    return NO;
}


-(void) configureFragmentWithTagID: (int) tagID
{   
    //Make sure we don't call this function more than once.
    NSAssert( nil == [self getChildByTag:1989], @"Attempting to reinitialize a ball fragment");
    
    self.tag = tagID;

    NSString* fragmentSpriteName = [NSString stringWithFormat:@"PuzzleBall01_piece%02d.png",tagID];
    
    fragmentSprite = [CCSprite spriteWithSpriteFrameName:fragmentSpriteName];
    [fragmentSprite setScale:0.5];
    
    //specify arbitrary tag number. Just picked a random one.
    [self addChild:fragmentSprite z:0 tag:1989];
}

-(id) init
{
    self = [super init];
	if (self)
	{
        [self setAnchorPoint:ccp(0.5,0.5)];
        
        touchPositionOffset = ccp(0,40);
        
        b2World* world = [GB2Engine sharedInstance].world;
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        bodyDef.position = b2Vec2(0,0);
        physicsBody = world->CreateBody( &bodyDef );
        
        b2CircleShape circleShape;
        circleShape.m_p.Set(0, 0); //position, relative to body position
        circleShape.m_radius = 2; //radius
        
        b2FixtureDef myFixtureDef;
        myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
        myFixtureDef.isSensor = true;
        physicsBody->CreateFixture(&myFixtureDef); //add a fixture to the body
        
        blueprintCollisionCounter = 0;
        
        
	}
	return self;
}

-(void) cleanup
{
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:physicsBody];
    [super cleanup];
}

-(void) didLoadFromCCB
{    
    //We will lock down our start location to where this node is
    //located withing cocosbuilder
    startLocation = self.position;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    /*******************Stolen (possibly modified) from CCButton [START]********************/
    if (![self isTouchInside:touch]
        || ![self isEnabled]
        || ![self visible]
        || ![self hasVisibleParents])
    {
		return NO;
	}
    
    _state              = CCControlStateHighlighted;
    self.highlighted    = YES;
    _curTouch = touch;
    [self sendActionsForControlEvents:CCControlEventTouchDown];
    /*******************Stolen (possibly modified) from CCButton [END]***********************/
    
    [self setPosition:ccpAdd(self.position, touchPositionOffset)];
    [fragmentSprite setScale:1.0];
    
	return YES;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    /*******************Stolen (possibly modified) from CCButton [START]********************/
    if (![self isEnabled]
        || [self isSelected])
    {
        if ([self isHighlighted])
        {
            [self setHighlighted:NO];
        }
        return;
    }
    
    BOOL isTouchMoveInside = [self isTouchInside:touch];
    if (isTouchMoveInside && ![self isHighlighted])
    {
        _state = CCControlStateHighlighted;
        
        [self setHighlighted:YES];
        
        [self sendActionsForControlEvents:CCControlEventTouchDragEnter];
    } else if (isTouchMoveInside && [self isHighlighted])
    {
        [self sendActionsForControlEvents:CCControlEventTouchDragInside];
    } else if (!isTouchMoveInside && [self isHighlighted])
    {
        _state = CCControlStateNormal;
        
        [self setHighlighted:NO];
        
        [self sendActionsForControlEvents:CCControlEventTouchDragExit];
    } else if (!isTouchMoveInside && ![self isHighlighted])
    {
        [self sendActionsForControlEvents:CCControlEventTouchDragOutside];
    }
    /*******************Stolen (possibly modified) from CCButton [END]***********************/
    
    //move towards touch
    CGPoint touchLocation   = [_curTouch locationInView:[_curTouch view]];                      // Get the touch position
    touchLocation           = [[CCDirector sharedDirector] convertToGL:touchLocation];  // Convert the position to GL space
    touchLocation           = [[self parent] convertToNodeSpace:touchLocation];         // Convert to the node space of this class
    [self setPosition: ccpAdd(touchLocation, touchPositionOffset)];
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    /*******************Stolen (possibly modified) from CCButton [START]********************/
    _state              = CCControlStateNormal;
    self.highlighted    = NO;
    
    if ([self isTouchInside:touch])
    {
        [self sendActionsForControlEvents:CCControlEventTouchUpInside];
    } else
    {
        [self sendActionsForControlEvents:CCControlEventTouchUpOutside];
    }
    
    _curTouch = nil;
    /*******************Stolen (possibly modified) from CCButton [END]***********************/
    
    [self processFragmentAttachment];
    [self resetFragmentToOriginalState];

}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
    /*******************Stolen (possibly modified) from CCButton [START]********************/
    _state              = CCControlStateNormal;
    self.highlighted    = NO;
    
    [self sendActionsForControlEvents:CCControlEventTouchCancel];
    
    _curTouch = nil;
    /*******************Stolen (possibly modified) from CCButton [END]***********************/
    
    [self setPosition:startLocation];
}

- (BOOL)isTouchInside:(UITouch *)touch
{
    CGPoint touchLocation   = [touch locationInView:[touch view]];                      // Get the touch position
    touchLocation           = [[CCDirector sharedDirector] convertToGL:touchLocation];  // Convert the position to GL space
    touchLocation           = [self convertToNodeSpace:touchLocation];                  // Convert to the node space of this class
    
    CGRect inflatedBoundingBox = fragmentSprite.boundingBox;                            //inflate the bounding box so it's easier to grab?
    
    return CGRectContainsPoint(inflatedBoundingBox,touchLocation);
}

-(void) draw
{
    [super draw];
    
    [KKConfig selectKeyPath:@"KKStartupConfig"];
    if( [KKConfig boolForKey:@"EnableCollisionDebugDraw"] )
    {
        CGRect boundingBox = fragmentSprite.boundingBox;
        
        ccDrawRect( boundingBox.origin,
                   ccp( boundingBox.origin.x + boundingBox.size.width ,
                       boundingBox.origin.y + boundingBox.size.height)
                   );
    }
}


//This algorithm is designed for inflating only.
//No defalting.
-(CGRect) inflateRect: (CGRect) originalRect
{
    const float inflateFactor = 0.5f;
    
    float offsetOriginX = originalRect.origin.x - originalRect.size.width * inflateFactor;
    float offsetOriginY = originalRect.origin.y - originalRect.size.height * inflateFactor;
    
    return CGRectMake(
                      //Here we set the offsets that will manipulate lower left corner.
                      offsetOriginX,
                      offsetOriginY,
                      
                      //Here we compensate for what we offset, then inflate.
                      (originalRect.size.width + fabsf(offsetOriginX)) + originalRect.size.width * inflateFactor,
                      (originalRect.size.height + fabsf(offsetOriginY)) + originalRect.size.height * inflateFactor
                      
                      );
}

-(void) beginContactWithBarricadeRepairBlueprint: (GB2Contact*)contact
{
    blueprintCollisionCounter++;
}
-(void) endContactWithBarricadeRepairBlueprint: (GB2Contact*)contact
{
    blueprintCollisionCounter--;
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    physicsBody->SetTransform( b2Vec2FromCGPoint(positionIn), 0);
    const b2Transform& transform = physicsBody->GetTransform();
    [super setPosition:CGPointFromb2Vec2(transform.p)];
}

-(CGPoint) position
{
    return CGPointFromb2Vec2( physicsBody->GetTransform().p ) ;
}

-(void) resetFragmentToOriginalState
{
    [self setPosition:startLocation];
    [fragmentSprite setScale:0.5];
}

@end
