//
//  ActivityBase.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/10/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "ActivityLauncher.h"

@interface ActivityBase : CCLayer
{
    //The interactive object that launched the activity
    //we are in now.
    ActivityLauncher* originalActivityLauncher;
}
@property (nonatomic,retain) ActivityLauncher* OriginalActivityLauncher;

//Stops the activity for being interactible. Designed as a
//one-time use. Should only really be invoked when transitioning
//out of ResourceGatheringScene
-(void) FreezeActivity_EventHandler:(NSNotification*) notification;
@end
