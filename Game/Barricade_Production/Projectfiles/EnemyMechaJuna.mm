//
//  EnemyMechaJuna.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyMechaJuna.h"
#import "AI_DecisionNode.h"
#import "AI_ActionNode.h"
#import "Player.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "GameOverScene.h"
#import "GMath.h"
#import "Barricade.h"
#import "CCBReader.h"
#import "GameOverSceneTransitioner.h"
#import "SimpleAudioEngine.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation EnemyMechaJuna
@synthesize LeftArm=leftArm;
@synthesize RightArm=rightArm;
@synthesize CannonWeapon=cannonWeapon;
@synthesize CurrentStateID=currentStateID;

-(void) onEnter
{
    [super onEnter];
    
    //After the level is fully loaded, let's restrict controls.
    [self scheduleOnce:@selector(restrictPlayercontrols) delay:0.15f];
}

-(void) restrictPlayercontrols
{
    //knock out player's special controls
    SEND_EVENT(EventList::CONTROLS_RESTRICT_CONTROLS, nil);
    
    //destroy the barricade
    Barricade* barricadeRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET, [NSValue valueWithPointer:&barricadeRef]);
    NSAssert(nil != barricadeRef, @"Barricade reference not found.");
    barricadeRef.CurrentHealth = 0;
    
}

-(id) init
{
	self = [super init];
	if (nil != self)
	{
        [self scheduleUpdate];
        [self constructBehaviorTree];
        currentStateID = eIDLE;
        
        
        //Acquire player reference
        playerRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                            object:[NSValue valueWithPointer:&playerRef]];
        NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayMechaJunaLaserFireEnd_EventHandler:)
                                                     name:EventList::GAMEPLAY_MECHA_JUNA_LASER_FIRE_END
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayMechaJunaDefeated_EventHandler:)
                                                     name:EventList::GAMEPLAY_MECHA_JUNA_DEFEATED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayMechaJunaArmDestroyed_EventHandler:)
                                                     name:EventList::GAMEPLAY_MECHA_JUNA_ARM_DESTROYED
                                                   object:nil];
        
        thinkingTimeMax = 3.0f;
        thinkingTimeMin = 1.0f;
        thinkingTimeCurrent = thinkingTimeMax;
        cannonRotationSpeed = 20;
        
    }
	return self;
}

-(void) GameplayMechaJunaArmDestroyed_EventHandler: (NSNotification*) notification
{
    if( leftArm.CurrentHealth <= 0 and rightArm.CurrentHealth <= 0 )
    {
        cannonWeapon.IsVulnerable = YES;
    }
}


-(void) GameplayMechaJunaDefeated_EventHandler: (NSNotification*) notification
{
    [self unscheduleUpdate];
}

-(void) GameplayMechaJunaLaserFireEnd_EventHandler: (NSNotification*) notification
{
    //The purpose here is that if the laser is required to be fired again right after it is done,
    //it won't be able to do it because EnemyMechaJuna believes we are still firing the laser.
    //By signaling to EnemyMechaJuna that we are done with the laser, EnemyMechaJuna will flag itself
    //as being idle. At that point, EnemyMechaJuna is free to select a new state, which in this case,
    //selects the fire cannon again.
    if( eFIRE_CANNON == currentStateID )
    {
        currentStateID = eIDLE;
    }
}

-(void) cleanup
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_MECHA_JUNA_LASER_FIRE_END
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_MECHA_JUNA_DEFEATED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_MECHA_JUNA_ARM_DESTROYED
                                                  object:nil];
    
    [self removeChild:leftArm cleanup:YES];
    [self removeChild:rightArm cleanup:YES];
    
    [self destructBehaviorTree];
    
    [super cleanup];
}

-(void) destructBehaviorTree
{
    [rootNode cleanup];
    [rootNode release];
}

-(void) constructBehaviorTree
{
    //////////////////////////////////
    // ACTION NODES
    //////////////////////////////////
    AI_ActionNode* punchWithLeftArm = [[[AI_ActionNode alloc] initWithActionBlock:^{
        
        if( currentStateID != ePUNCH_LEFT )
        {
            CCLOG(@"Changing state: ePUNCH_LEFT");
            currentStateID = ePUNCH_LEFT;
            [leftArm changeEnemyState:[leftArm.AI_StateFactory createMechaJunaArmState_Attack]];

            [cannonWeapon changeEnemyState:[cannonWeapon.AI_StateFactory createMechaJunaCannonState_Idle]];
            [rightArm changeEnemyState:[rightArm.AI_StateFactory createMechaJunaArmState_Idle]];
        }
        
    }] autorelease];
    
    AI_ActionNode* punchWithRightArm = [[[AI_ActionNode alloc] initWithActionBlock:^{
        
        if( currentStateID != ePUNCH_RIGHT )
        {
            CCLOG(@"Changing state: ePUNCH_RIGHT");
            currentStateID = ePUNCH_RIGHT;
            [rightArm changeEnemyState:[rightArm.AI_StateFactory createMechaJunaArmState_Attack]];

            [cannonWeapon changeEnemyState:[cannonWeapon.AI_StateFactory createMechaJunaCannonState_Idle]];
            [leftArm changeEnemyState:[leftArm.AI_StateFactory createMechaJunaArmState_Idle]];
        }
        
    }] autorelease];
    
    
    AI_ActionNode* fireCannon = [[[AI_ActionNode alloc] initWithActionBlock:^{
        if( currentStateID != eFIRE_CANNON )
        {
            CCLOG(@"Changing state: eFIRE_CANNON");
            currentStateID = eFIRE_CANNON;
            [cannonWeapon changeEnemyState:[cannonWeapon.AI_StateFactory createMechaJunaCannonState_Fire]];
            
            [rightArm changeEnemyState:[rightArm.AI_StateFactory createMechaJunaArmState_Idle]];
            [leftArm changeEnemyState:[leftArm.AI_StateFactory createMechaJunaArmState_Idle]];
        }
    }] autorelease];
    
    AI_ActionNode* selfDestruct = [[[AI_ActionNode alloc] initWithActionBlock:^{
    }] autorelease];
    
    AI_ActionNode* errorAssertion = [[[AI_ActionNode alloc] initWithActionBlock:^{
    }] autorelease];

    //////////////////////////////////
    // DECISION NODES
    //////////////////////////////////
    AI_DecisionNode* leftArmDestroyed = [AI_DecisionNode alloc];
    AI_DecisionNode* bothArmsDestroyed = [AI_DecisionNode alloc];
    AI_DecisionNode* rightArmDestroyed = [AI_DecisionNode alloc];
    AI_DecisionNode* cannonDestroyed = [AI_DecisionNode alloc];
    AI_DecisionNode* leoPos3 = [AI_DecisionNode alloc];
    AI_DecisionNode* leoPos2 = [AI_DecisionNode alloc];
    AI_DecisionNode* leoPos1 = [AI_DecisionNode alloc];
                                         
    
    [[bothArmsDestroyed initWithTrueNode:cannonDestroyed
                               FalseNode:leoPos1
                           DecisionBlock:^{
                               BOOL decisionCondition = ( self.LeftArm.CurrentHealth <= 0 && self.RightArm.CurrentHealth <=0 );
                                                              
                               return decisionCondition;
                           }] autorelease];
    
    [[leftArmDestroyed initWithTrueNode:fireCannon
                              FalseNode:punchWithLeftArm
                          DecisionBlock:^{
                              BOOL decisionCondition = ( self.LeftArm.CurrentHealth <= 0 );
                              //If we're firing the cannon, set the coordinates
                              if( YES == decisionCondition )
                              {
                                  self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK1].start;
                                  self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].end;
                              }
                                                            
                              return decisionCondition;
                          }] autorelease];
    
    [[rightArmDestroyed initWithTrueNode:fireCannon
                               FalseNode:punchWithRightArm
                           DecisionBlock:^{
                               BOOL decisionCondition = ( self.RightArm.CurrentHealth <= 0 );
                               //If we're firing the cannon, set the coordinates
                               if( YES == decisionCondition )
                               {
                                   self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK3].end;
                                   self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].start;
                               }
                               
                               return decisionCondition;
                           }] autorelease];
    
    [[cannonDestroyed initWithTrueNode:selfDestruct
                             FalseNode:fireCannon
                         DecisionBlock:^{
                             
                             BOOL decisionCondition = ( self.CannonWeapon.CurrentHealth <= 0 );

                             //If we're firing the cannon, set the coordinates
                             if( NO == decisionCondition )
                             {
                                 //Depending on where the player is, Mecha Juna will sweep from the outside directions
                                 if( playerRef.position.y >= [self.CannonWeapon getTargetBlock:eBLOCK2].start.y )
                                 {
                                     self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK1].start;
                                     self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].end;
                                 }
                                 else if( playerRef.position.y <= [self.CannonWeapon getTargetBlock:eBLOCK2].end.y )
                                 {
                                     self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK3].end;
                                     self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].start;
                                 }
                                 else
                                 {
                                     //Here we assume we have the player within Block2.
                                     //To keep things insteresting, we'll alternate whether the beam sweeps
                                     //upwards or downwards.
                                     
                                     if( CCRANDOM_0_1() > 0.5f )
                                     {
                                         self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK2].start;
                                         self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].end;
                                     }
                                     else
                                     {
                                         self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK2].end;
                                         self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].start;
                                     }
                                 }
                             }
                             return decisionCondition;
                         }] autorelease];
    
    [[leoPos3 initWithTrueNode:rightArmDestroyed
                     FalseNode:errorAssertion
                 DecisionBlock:^{
                     
                     BOOL decisionCondition =
                     ( playerRef.position.y > [cannonWeapon getTargetBlock:eBLOCK3].end.y and
                      playerRef.position.y <= [cannonWeapon getTargetBlock:eBLOCK3].start.y );
                    
                     return decisionCondition;
                     
                 }] autorelease];
    
    [[leoPos2 initWithTrueNode:fireCannon
                     FalseNode:leoPos3
                 DecisionBlock:^{
                     
                     BOOL decisionCondition =
                     ( playerRef.position.y > [cannonWeapon getTargetBlock:eBLOCK2].end.y and
                      playerRef.position.y <= [cannonWeapon getTargetBlock:eBLOCK2].start.y );
                     //If we're firing the cannon, set the coordinates
                     if( YES == decisionCondition )
                     {
                         self.CannonWeapon.CannonAimPointStart = [self.CannonWeapon getTargetBlock:eBLOCK2].end;
                         self.CannonWeapon.CannonAimPointEnd = [self.CannonWeapon getTargetBlock:eBLOCK2].start;
                     }
                                          
                     return decisionCondition;
                     
                 }] autorelease];
    
    [[leoPos1 initWithTrueNode:leftArmDestroyed
                     FalseNode:leoPos2
                 DecisionBlock:^{
                     
                     BOOL decisionCondition =
                     ( playerRef.position.y > [cannonWeapon getTargetBlock:eBLOCK1].end.y and
                      playerRef.position.y <= [cannonWeapon getTargetBlock:eBLOCK1].start.y );
                                          
                     return decisionCondition;
                     
                 }] autorelease];
    
    rootNode = bothArmsDestroyed;
    [rootNode retain];
}

-(void) update:(ccTime)delta
{
    thinkingTimeCurrent -= delta;
    
    if(thinkingTimeCurrent <= 0)
    {
        thinkingTimeCurrent = gFloatRand(thinkingTimeMin, thinkingTimeMax);
        
        [rootNode doAction];
    }
}

-(void) killEnemy
{
    
    [self unscheduleUpdate];
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation EnemyMechaJunaCannonBeam
@synthesize IsHarmful=isHarmful;
@synthesize BeamSprite=beamSprite;

-(void) setIsHarmful:(BOOL)IsHarmful
{
    //set the value
    isHarmful= IsHarmful;
    
    //Then check if the player is caught in the beam, the moment we become harmful
    if( YES == isHarmful and 1 == contactWithPlayerCounter )
    {
        //We've collided with the player
        [[CCDirector sharedDirector] replaceScene: [GameOverSceneTransitioner node] ];
    }
}

-(void) syncPhysicsBodyRotationWithParentRotation
{
    beamPhysicsBody->SetTransform(beamPhysicsBody->GetPosition(), CC_DEGREES_TO_RADIANS(-self.parent.rotation));
}

-(id) init
{
	self = [super init];
	if (nil != self)
	{
        //We shouldn't be able to hurt the player on start
        isHarmful = NO;
        contactWithPlayerCounter = 0;
        
        //Create the physics body the laser beam will use.
        b2BodyDef bodyDef;
        bodyDef.position = b2Vec2(100,100);
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        beamPhysicsBody = [GB2Engine sharedInstance].world->CreateBody(&bodyDef);
    }
    return self;
}

-(void) cleanup
{
    //cleanup the body
    beamPhysicsBody->SetUserData(NULL);
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:beamPhysicsBody];
    
    [self removeChild:beamSprite cleanup:YES];
    
    [super cleanup];
}

-(void) beginContactWithPlayer: (GB2Contact*)contact
{
    ++contactWithPlayerCounter;
    
    if( YES == isHarmful and 1 == contactWithPlayerCounter )
    {
        //We've collided with the player
        [[CCDirector sharedDirector] replaceScene: [GameOverSceneTransitioner node] ];
    }
    
}
-(void) endContactWithPlayer: (GB2Contact*)contact
{
    --contactWithPlayerCounter;
}

-(void) onEnter
{
    //physics fixture
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:beamPhysicsBody
                                           forShapeName:@"JunaTankCannonLaserBeam"];
    
    //set the sprite
    beamSprite = [CCSprite spriteWithSpriteFrameName:@"JunaTankCannonLaserBeam.png"];
    [beamSprite setAnchorPoint: [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"JunaTankCannonLaserBeam"]];
    [beamSprite setScaleY:0];
    beamSprite.position = ccp(130,0);
    [self addChild:beamSprite];
    
    
    //scale the sprite to the same length as the physics body.
    //We do this by grabbing the right-most vertex.
    b2PolygonShape* polygonShape = static_cast<b2PolygonShape*>(beamPhysicsBody->GetFixtureList()->GetShape());
    //NOTE: WE DON'T REALLY KNOW WHICH VERTEX IT IS.
    //      WE SHOULD FIND IT EVERY TIME INSTEAD!
    const b2Vec2& rightmostVertex = polygonShape->GetVertex(1);
    [beamSprite setScaleX:rightmostVertex.x * PTM_RATIO];
    
    //set the position of the physics body where the sprite is
    CGPoint worldCoordinates = [self.parent convertToWorldSpace:_position];
    beamPhysicsBody->SetTransform(b2Vec2FromCGPoint(worldCoordinates), 0);
    
    [super onEnter];
}
@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation EnemyMechaJunaCannon
@synthesize CannonAimPointEnd=cannonAimPointEnd;
@synthesize CannonAimPointStart=cannonAimPointStart;
@synthesize CannonTargetBlockHeight=cannonTargetBlockHeight;
@synthesize CannonBeam=cannonBeam;
@synthesize CannonLightSprite=cannonLightSprite;
@synthesize CannonSprite=cannonSprite;
@synthesize CannonRotationSpeed=cannonRotationSpeed;
@synthesize IsVulnerable=isVulnerable;

-(void) setRotation:(float)rotation
{
    //Apply the rotation
    [super setRotation:rotation];
    
    //Apply the rotation to the beam's physics body
    [cannonBeam syncPhysicsBodyRotationWithParentRotation];
    
    //Apply physics rotation to the cannon's physics body
    enemyPhysicsBody->SetTransform(enemyPhysicsBody->GetPosition(), CC_DEGREES_TO_RADIANS(-self.rotation));
}

-(void) killEnemy
{
    //Notify the system that Mecha Juna has been defeated.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_MECHA_JUNA_DEFEATED
                                                        object:nil];
    
    //Change state to MechaJuna being dead (this removed her fixtures)
    [self changeEnemyState: [self.AI_StateFactory createMechaJunaCannonState_Dead]];
    
    //Since we're dead, we won't be using the cannon beam anymore.
    //Let's go ahead and destroy it right here: remove cannon beam from the hierarchy.
    [self removeChild:cannonBeam cleanup:YES];
    cannonBeam = nil;
    
}

-(CannonTargetBlock) getTargetBlock: (CANNON_TARGET_BLOCK) targetBlockIn
{
    
    //Generate the start point
    CGPoint start = ccp(cannonRootTargetBlockStart.x,
                        cannonRootTargetBlockStart.y - cannonTargetBlockHeight * (int)targetBlockIn
                        );
    
    CGPoint end = ccp(start.x,
                      start.y - cannonTargetBlockHeight
                      );
    
    CannonTargetBlock targetBlock = {
        start,
        end
    };
    
    return targetBlock;
}

-(id) init
{
	self = [super init];
	if (nil != self)
	{
        //physics fixture
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyPhysicsBody
                                               forShapeName:@"JunaTankCannonBody"];
        
        cannonTargetBlockHeight = 54;
        cannonRootTargetBlockStart = ccp([CCDirector sharedDirector].winSize.width, 235);
        
        allowHealthDisplay = NO;
        isVulnerable = NO;
        
        currentHealth = maxHealth = 200;
        
        cannonRotationSpeed = 10;
        
        cannonAimPointEnd = cannonAimPointStart = CGPointZero;
        
        cannonBeam = [EnemyMechaJunaCannonBeam node];
        [self addChild:cannonBeam];

    }
    return self;
}

-(void) update:(ccTime)delta
{
    [super update:delta];
}

-(void) cleanup
{
    //Remove the fixtures for body for the cannon.
    //It's cleaned up. No longer needed.
    //Remove the fixtures from the body.
    //We don't destroy the body per se... EnemyBase does that.
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = enemyPhysicsBody->GetFixtureList();
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    [[GB2Engine sharedInstance] addBodyToPostWorldStepSelectiveFixtureRemovalList:enemyPhysicsBody
                                                                                 :fixtureValuesArray];
    [super cleanup];
}

-(void) didLoadFromCCB
{
    //Use the anchor point specified in Physics Editor in order to alin the fixture.
    [cannonSprite setAnchorPoint:[[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"JunaTankCannonBody"]];
    [cannonLightSprite setAnchorPoint:cannonSprite.anchorPoint];    
}

-(void) onEnter
{
    [super onEnter];
    
    //We need to add this redundant call because
    //during the loading process 'setPosition' is called
    //when the self has no parent. Thus, does not allow for
    //the physics data to properly align.
    //HACK:
    //We can't pass "self.position" here. Calling the property invokes an override
    //that returns the position of the physics volume. This will not work because
    //the physics volume during init is positioned with local coordinates into world-space
    //(it thinks the passed-in coordinates are world-coordinates, because self has no parent).
    //If we invoke a "self.position" at this point (we now have a parent), it will attempt
    //to transform the position (which is considered to be already in local coordinates) into
    //local coordinates a second time, thus, distorting the position.
    [self setPosition:_position];
    
    //Set the default state
    [self instantChangeEnemyState:[self.AI_StateFactory createMechaJunaCannonState_Idle]];
}


-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    if( YES == isVulnerable )
    {
        //modify health
        [self setCurrentHealth: self.CurrentHealth - firingWeapon.Damage];
        
        //If we ran out of health,
        if( 0 == self.CurrentHealth )
        {
            //let's change the state...
            [self killEnemy];
        }
        //Otherwise...
        else
        {
            //Tint the cannon
            [cannonSprite setColor:ccRED];
            [cannonSprite runAction:[CCTintTo actionWithDuration:0.2f red:255 green:255 blue:255]];
            
            //Tint the cannon light
            float currentHealthScalar = (float)currentHealth / (float)maxHealth;
            
            GLubyte scaledBrightness = (GLubyte)(255.0f * currentHealthScalar);
            [cannonLightSprite setColor:ccc3( 255, scaledBrightness, scaledBrightness )];
            
            //Play a sound effect to indicate damage
            [[SimpleAudioEngine sharedEngine] playEffect:@"sndEnemyWalkerAttack.wav"];
        }
    }
    else
    {
        //Play a sound or something to show that the enemy is unaffected.
        [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_MechaJuna_Impenetrable.wav"];
    }
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation EnemyMechaJunaArm
@synthesize OriginalPosition=originalPosition;
@synthesize ArmAttackRange=armAttackRange;
@synthesize DestinationPosition=destinationPosition;
@synthesize ArmMovementSpeed=armMovementSpeed;
@synthesize ArmSprite=armSprite;
@synthesize ArmLightSprite=armLightSprite;

-(void) didLoadFromCCB
{
    //Use the anchor point specified in Physics Editor in order to alin the fixture.
    [armSprite setAnchorPoint:[[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"JunaTankMissileBody"]];
    [armLightSprite setAnchorPoint:armSprite.anchorPoint];
}

-(id) init
{
	self = [super init];
	if (nil != self)
	{
        //physics fixture
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyPhysicsBody
                                               forShapeName:@"JunaTankMissileBody"];
        
        
        armAttackRange = 235;
        armMovementSpeed = 150;
        currentHealth = maxHealth = 150;
        allowHealthDisplay = NO;
    }
	return self;
}

-(void) setOriginalPosition:(CGPoint)OriginalPositionIn
{
    //Set new origin
    originalPosition = OriginalPositionIn;
    
    //re-adjust new destination
    destinationPosition = ccp(originalPosition.x + self.ArmAttackRange,
                              originalPosition.y);
}

//QUESTION:
//Shouldn't we be moving the arms every time the main body moves?
-(void) onEnter
{
    [super onEnter];
    
    //We need to add this redundant call because
    //during the loading process 'setPosition' is called
    //when the self has no parent. Thus, does not allow for
    //the physics data to properly align.
    //HACK:
    //We can't pass "self.position" here. Calling the property invokes an override
    //that returns the position of the physics volume. This will not work because
    //the physics volume during init is positioned with local coordinates into world-space
    //(it thinks the passed-in coordinates are world-coordinates, because self has no parent).
    //If we invoke a "self.position" at this point (we now have a parent), it will attempt
    //to transform the position (which is considered to be already in local coordinates) into
    //local coordinates a second time, thus, distorting the position.
    [self setPosition:_position];
    
    //We are loaded and ready to go!
    //Let's make this our original position, since it's
    //only called once in our lifetime.
    //(The property override adjusts the destinationPosition too!)
    self.OriginalPosition = self.position;

    //Set the default state
    [self instantChangeEnemyState:[enemyStateFactory createMechaJunaArmState_Idle]];

}


-(void) killEnemy
{
    [self changeEnemyState: [self.AI_StateFactory createMechaJunaArmState_Dead]];
    
    //Notify the death to the system
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_MECHA_JUNA_ARM_DESTROYED
                                                        object:nil];
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    //modify health
    [self setCurrentHealth: self.CurrentHealth - firingWeapon.Damage];
    
    //If we ran out of health, 
    if( 0 == self.CurrentHealth )
    {
        //let's change the state...
        [self killEnemy];
    }
    //Otherwise...
    else
    {
        //Play a sound effect to indicate damage
        [[SimpleAudioEngine sharedEngine] playEffect:@"sndEnemyWalkerAttack.wav"];
        
        //Tint the arm
        [armSprite setColor:ccRED];
        [armSprite runAction:[CCTintTo actionWithDuration:0.2f red:255 green:255 blue:255]];
        
        //Tint the arm light
        float currentHealthScalar = (float)currentHealth / (float)maxHealth;
        
        [armLightSprite setColor:ccc3(
                                      255,
                                      (GLubyte)(255.0f * currentHealthScalar),
                                      (GLubyte)(255.0f * currentHealthScalar)
                                      )];
    }
}


@end
