//
//  OpenItemTapObject.h
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface OpenItemTapObject : CCSprite
{
    SignedByte currentTapCount;
    float originalScale;
    float tappedScale;
}
@property (nonatomic,readwrite) SignedByte CurrentTapCount;

-(id)initWithSpriteFrameName:(NSString *)spriteFrameName;
-(void) QuitOpenItemActivity;
-(void) reactToTouch;
@end
