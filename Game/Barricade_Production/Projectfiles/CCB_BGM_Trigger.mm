//
//  CCBSoundTrigger.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "CCB_BGM_Trigger.h"
#import "SimpleAudioEngine.h"

@implementation CCB_BGM_Trigger

-(void) didLoadFromCCB
{

}

-(void) setVisible:(BOOL)visible
{
    [super setVisible:visible];
    
    if( self.visible )
    {
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:soundFilename
                                                         loop:isLooping];
    }
}

-(void) setPosition:(CGPoint)position
{
    [super setPosition:position];

    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void) setRotation:(float)rotation
{
    NSAssert( rotation >= 0 and rotation <= 100, @"Rotation/Volume out of bounds");
    
    [super setRotation:rotation];
    
    [SimpleAudioEngine sharedEngine].backgroundMusicVolume = rotation/100.0f;
}

@end