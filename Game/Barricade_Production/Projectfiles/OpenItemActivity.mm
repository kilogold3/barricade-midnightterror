//
//  OpenItemActivity.m
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OpenItemActivity.h"
#import "OpenItemTapObject.h"
#import "LockableItem.h"
#import "AppWidescreener.h"

@implementation OpenItemActivity

//Override the setter method to be able to add/remove the new OpenItem
//that will be tapped on to open
-(void)setOriginalActivityLauncher:(ActivityLauncher *)OriginalActivityLauncherIn
{
    [super setOriginalActivityLauncher:OriginalActivityLauncherIn];
    
    //Remove any active OpenItem
    if( objectToOpen != nil )
    {
        [self removeChild: objectToOpen];
    }
    
    //Let's create a new OpenItem with the sprite frame name
    //provided by the original activity launcher we just got from the setter.
    //If we are running this activity, it is safe to assume that the original
    //activity launcher is of type 'LockedItem'.
    NSAssert([self.OriginalActivityLauncher isKindOfClass:[LockableItem class]],
             @"OriginalActivityLauncher is NOT of type LockedItem");
    
    LockableItem* lockedItemActiviyLauncher = static_cast<LockableItem*>(self.OriginalActivityLauncher);
    
    NSString* openItemSpriteID = lockedItemActiviyLauncher.OpenItemSpriteID;
    
    objectToOpen = [[[OpenItemTapObject alloc] initWithSpriteFrameName:openItemSpriteID] autorelease];
    objectToOpen.CurrentTapCount = lockedItemActiviyLauncher.TapsToUnlock;
    
    //Center the object on the screen
    const CGSize SCREEN_SIZE = [AppWidescreener getSceneContentSize];
    objectToOpen.position = ccp(SCREEN_SIZE.width/2,SCREEN_SIZE.height/2);
    [self addChild:objectToOpen];
}

-(void) FreezeActivity_EventHandler:(NSNotification*) notification
{
    //This is kind of hacky, we should probably not be so specific here as
    //to how we disable the objectToOpen.
    //Still, it works like this just fine.
    [objectToOpen unscheduleUpdate];
}

@end
