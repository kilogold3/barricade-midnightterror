//
//  ItemContainer.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "LockableItem.h"

@class Pickup;

@interface ItemContainer : LockableItem
{
    int associatedPickupTagID;
}

-(Pickup*) getAssociatedPickupItemInstanceReference;

@end
