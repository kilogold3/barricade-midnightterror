//
//  TravelPrompt.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@class CCControlButton;
@class TravelNodeButton;

@interface TravelPrompt : CCLayer
{
    TravelNodeButton* destinationTravelNodeButton;
    
    CCLabelTTF* travelingDestinationLabel;
    CCLabelTTF* travelingDestinationNameLabel;
    CCLabelTTF* travelingDeniedLabel;
    CCControlButton* backButton;
    CCControlButton* goButton;
    CCLayerColor* backdropLayer;
}

@property (nonatomic, assign) TravelNodeButton* DestinationTravelNodeButton;

@end
