//
//  OverdriveAnimationJeromeV1.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/14/13.
//
//

#import "OverdriveSequence.h"
#import "NotificationCenterEventList.h"
#import "AppWidescreener.h"

@implementation OverdriveSequence
@synthesize OverdriveExecution=overdriveExecution;

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = self;
    
    //Default overdrive to nil explicitly
    overdriveExecution = nil;
    
    //If we're on the widescreen, let's just nudge a bit to the center.
    if( [AppWidescreener isWidescreenEnabled] )
    {
        self.position = ccp( 44 ,self.position.y);
    }
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    //if we have an overdrive (a.k.a, OverdriveExecutionObject
    //is not nil), then we process the execution.
    BOOL isOverdriveHit = (nil != overdriveExecution);
    
    if( isOverdriveHit )
    {
        //Add overdrive execution to parent in order to
        //have the execution update on it's own.
        //the overdriveExecution will remove itself
        //from it's parent when it is done.
        [self.parent addChild:overdriveExecution];
        [overdriveExecution executeOverdrive];
    }
    
    //Remove CCLayer that runs the overdrive animation.
    //If is done animating. We don't need it anymore.
    [self removeFromParentAndCleanup:YES];
    
    //Send an event notifying that the animation has finished
    NSNumber* isOverdriveHitEventArg = [NSNumber numberWithBool:isOverdriveHit];

    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_FINISH_ANIMATION
                                                        object:isOverdriveHitEventArg];
}

-(void) cleanup
{
    //Unassign the delegate method to remove the retain caused
    //by CCBAnimationManager's "delegate" property.
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
    
    [super cleanup];
    
}
-(void) dealloc
{
    CCLOG(@"OverdriveSequence dealloc");
    [super dealloc];
}


@end
