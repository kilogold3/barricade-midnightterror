//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VejiganteState_ApproachPlayer.h"
#import "EnemyVejigante.h"
#import "Player.h"
#import "GB2ShapeCache.h"
#import "StoryScene.h"
#import "ToBeContinuedScene.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"
#import "StoryScene.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"

@implementation VejiganteState_ApproachPlayer

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyVejigante class]],@"Wrong enemy parent type");
        enemyParentRef = (EnemyVejigante*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    //Let's load the image, but inverted this time because we are expecting
    //the Vejigante to come in from the right.

    
    //Flip the image horizontally. (Enemy should be facing Leo at this point)
    [enemyParentRef.VejiganteSprite setScaleX:-1.0f];

    CCDelayTime* delayAction = [CCDelayTime actionWithDuration:1.5f];
    
    CCMoveTo* moveAction = [CCMoveTo actionWithDuration:0.6f
                                               position:enemyParentRef.PlayerRef.position];
    
    CCCallBlock* callBlockAction = [CCCallBlock actionWithBlock:^{
        
        //Play the vejigante screaming noise 
        [[SimpleAudioEngine sharedEngine] playEffect:@"VejiganteScream.wav"];
        
        //since we are done with the vejigante, we won't be going into some resource gathering
        //to use the traveling backpack and so on. Instead, we will simply advance the progression
        //right here. This will take us to the cutscene where we are on our way to Zaffre City.
        SaveObjectPropertyList* resourceGatheringSceneProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
        const int currentProgression = [[resourceGatheringSceneProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
        [resourceGatheringSceneProperties setValue:[NSNumber numberWithInt:currentProgression+1]
                                            forKey: GSP::CURRENT_PROGRESSION];
        
        //load the story scene
        [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1.15f
                                                                                      scene:[StoryScene node]
                                                                                  withColor:ccBLACK]];
    }];
    CCSpawn* moveAndTransitionActions = [CCSpawn actionOne:moveAction two:callBlockAction];

    [enemyParentRef runAction:[CCSequence actions: delayAction, moveAndTransitionActions, nil]];

}

-(void) exit
{
}

-(void) update: (float) delta
{
}
@end
