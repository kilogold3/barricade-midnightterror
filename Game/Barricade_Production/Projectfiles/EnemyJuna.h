//
//  EnemyVejigante.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/30/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyBase.h"

@interface EnemyJuna : EnemyBase
{
}

//This method is designed to have the Vejigante fade out and
//appear somewhere else in a ghostlike manner.
-(void) phasingDisplace: (CGPoint) position;

//NOTE:
//      When implementing the better design, we simply need to have
//      EnemyRampageJuna forward the "reactToWeaponDamage" message to
//      the current AI state.
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;

@end
