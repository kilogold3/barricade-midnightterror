//
//  BarricadeReinforcer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface BarricadeReinforcer : CCNode
{
    CCSprite* reinforcerSprite;
    BOOL isGamePaused;
    BOOL isOverdrivePlaying;
    BOOL isConfigured;
    BOOL isTransitioningOut;
    float currentTimer;
}

-(void) restartReinforcement;

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;

@end
