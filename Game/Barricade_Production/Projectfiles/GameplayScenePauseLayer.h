//
//  GameplayPauseLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@class CCControlButton;
@class Aim;

@interface GameplayScenePauseLayer : CCLayer
{
    CCControlButton* btnToggleAim;
    Aim* aimRef;
}

-(void) resumeGameCallback;

@end
