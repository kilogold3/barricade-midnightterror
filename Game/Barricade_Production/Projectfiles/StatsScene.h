//
//  StatsScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCBAnimationManager.h"

@interface StatsScene : CCLayer <CCBAnimationManagerDelegate>
{
    CCNode* enemyActorsLayer;
    CCLabelTTF* screenTitleString;
    CCBAnimationManager* animationManagerRef;
    uint8 totalTimelineSequencesForScene;
    uint8 currentTimelineSequence;
    uint16 currentTallyStringValue; //This is just a temp buffer.
}

@end
