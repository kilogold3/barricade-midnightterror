//
//  WeaponRocketLauncher.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "WeaponRocketLauncher.h"
#import "WeaponRocketLauncherProjectile.h"
#import "NotificationCenterEventList.h"
#import "SimpleAudioEngine.h"
#import "PlayerState_Alive.h"
#import "PlayerArms.h"
#import "Player.h"

@implementation WeaponRocketLauncher

-(id) initWithDependencies: (Player*) gamePlayerRefIn
{
    self = [super initWithDependencies: gamePlayerRefIn];
    
    if( nil != self )
    {
        weaponBarrelOffsetMagnitude = 25.0f;

        rearArmSpriteID = @"newRocketLauncherBackArm.png";
        frontArmSpriteID = @"newRocketLauncherFrontArm.png";
        weaponPanelInactiveSpriteID = @"WeaponSelect/GunNormal.png";
        weaponPanelActiveSpriteID = @"WeaponSelect/GunSelected.png";
        hudDataDisplayWeaponSpriteID = @"NEWHUDPistolAmmo2.png";
        weaponSpriteAnchor = ccp(0.65,0.56);

        if( [KKConfig selectKeyPath:@"WeaponSettings"] )
        {
            damage = [KKConfig intForKey:@"RocketDamage"];
        }
        
        //Set basic attributes
        weaponType = eROCKET_LAUNCHER;
    }
    
    return self;
}

-(void) pullTrigger:(NSNotification *)notification
{
    //Send event that actually fires the weapon.
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_WEAPON_FIRED
                                                        object:self];
}

-(void) releaseTrigger:(NSNotification *)notification
{
    
}

-(void) fireWeaponFromOrigin: (CGPoint) originPosition
{
    //Play weapon fire sound
    [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_RocketLaunch.wav"];
    
    //Instantiate a projectile
    WeaponRocketLauncherProjectile* newProjectile = [[[WeaponRocketLauncherProjectile alloc] initWithDependencies:self] autorelease];

    //Start moving projectile to destination
    const CGPoint targetPosition = [self generateScreenLeftBoundProjectedPointFromLineOfFire:originPosition];
    newProjectile.ProjectileSpeed = 260;
    newProjectile.ProjectileDirection = ccpNormalize( ccpSub( targetPosition, originPosition ) );
    
    //Rotate the projectile so it's facing the right way    
    //Generate rotation and start-position data
    CGPoint originalProjectilePosition = CGPointZero;
    float armRotation = -1;
    [self GetWeaponBarrelPositionBasedOnTarget:targetPosition
                                     andOrigin:originPosition
                         withBarrelPositionOut:&originalProjectilePosition
                            withArmRotationOut:&armRotation];
    
    [newProjectile setPosition: originalProjectilePosition];
    [newProjectile setRotation: CC_RADIANS_TO_DEGREES(armRotation) ];
    
    //Put the newly prepared projectile in play
    [gameplayLayerRef addChild:newProjectile];
    
    
    /////////////////////////////////////////////////////////////
    //Display muzzle flash effect on bullet's initial location
    //(Might want to optimize this a bit)
    /////////////////////////////////////////////////////////////
    {
        //Grab player arms reference
        NSAssert([gamePlayerRef.CurrentPlayerState class] == [PlayerState_Alive class],
                 @"Compatible player state for weapon initialization");
        const PlayerState_Alive* playerAliveState = (PlayerState_Alive*)gamePlayerRef.CurrentPlayerState;
        PlayerArms* playerArmsRef = playerAliveState.PlayerArmsRef;
        
        //Generate the position equivalent for PlayerArms coordinate system.
        const CGPoint muzzleFlashPositionPlayerArmsLocalCoords = [playerArmsRef convertToNodeSpace:newProjectile.position];
        
        CCSprite* muzzleFlashSprite = [CCSprite spriteWithSpriteFrameName:@"ballisticMuzzleFlash.png"];
        muzzleFlashSprite.scale = 1.6f;
        muzzleFlashSprite.anchorPoint = ccp(0.6f,0.35f); //just a tiny tweak so it looks right.
        muzzleFlashSprite.position = muzzleFlashPositionPlayerArmsLocalCoords;
        muzzleFlashSprite.rotation = newProjectile.rotation;
        [muzzleFlashSprite runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.08f]
                                                       two:[CCRemoveFromParentAction action]]];
        [playerArmsRef addChild:muzzleFlashSprite];
    }
}
@end
