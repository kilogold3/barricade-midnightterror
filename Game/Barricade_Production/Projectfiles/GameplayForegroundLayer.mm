//
//  GameplayForegroundLayer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameplayForegroundLayer.h"
#import "CCBReader.h"
#import "NotificationCenterEventList.h"
#import "Overdrive.h"
#import "OverdriveSequence.h"
#import "GameplayScenePauseLayer.h"

@implementation GameplayForegroundLayer

-(id) init
{
    self = [super init];
    if( self != nil )
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(eventHandler_playSpecialEffectAnimation:)
                                                     name:EventList::OVERDRIVE_START_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ComponentAcquireGameplayLayerForeground_EventHandler:)
                                                     name:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_FOREGROUND
                                                   object:nil];
    }
    
    return self;
}



-(void) cleanup
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_FOREGROUND
                                                  object:nil];
    [super cleanup];
}

-(void) eventHandler_playSpecialEffectAnimation: (NSNotification*) notification;
{
    //Grab the overdrive (this instance is persistent throughout the game)
    Overdrive* overdrive = [notification object];
    
    //Instantiate an overdrive sequence
    OverdriveSequence* specialEffectAnimationLayer = (OverdriveSequence*)[CCBReader nodeGraphFromFile: overdrive.CocosBuilderAnimationFilename];
        
    //Pass a weak-reference to the Sequence's OverdriveExecution pointer.
    //This pointer will point to the persistent OverdriveExecution stored
    //in the Player class, where all Overdrive objects are stored.
    //OverdriveSequence shall not modify the source data in any way other
    //than through the OverdriveExecution interface.
    specialEffectAnimationLayer.OverdriveExecution = overdrive.OverdriveExecution;
    
    [self addChild:specialEffectAnimationLayer];
}

-(void) ComponentAcquireGameplayLayerForeground_EventHandler:(NSNotification*) notification
{
    NSValue* layerRefPointer = [notification object];
    CCLayer** targetLayerRef = (CCLayer**)[layerRefPointer pointerValue];
    (*targetLayerRef) = self;
}



@end
