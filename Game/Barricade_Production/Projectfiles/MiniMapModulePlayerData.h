//
//  MiniMapModulePlayerData.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 11/9/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MiniMapModulePlayerData : CCNode
{
    CCLabelTTF* suppliesLabel;
    CCLabelTTF* daysLeftLabel;
    CCLabelTTF* daysLeftCountLabel;
    
    CCNode* topDividerNode;
    CCNode* middleDividerNode;
    CCNode* bottomDividerNode;
}

@end
