//
//  WeaponBase.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/11/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "Player.h"
#import "PlayerState_Alive.h"
#import "PlayerArms.h"
#import "GB2Engine.h"
#import "EnemyBase.h"
#import "SimpleAudioEngine.h"
#import "Aim.h"

@implementation WeaponBase
@synthesize RearArmSpriteID=rearArmSpriteID;
@synthesize FrontArmSpriteID=frontArmSpriteID;
@synthesize Damage=damage;
@synthesize WeaponType=weaponType;
@synthesize WeaponPanelActiveSpriteID=weaponPanelActiveSpriteID;
@synthesize WeaponPanelInactiveSpriteID=weaponPanelInactiveSpriteID;
@synthesize HudDataDisplayWeaponSpriteID=hudDataDisplayWeaponSpriteID;
@synthesize WeaponSpriteAnchor=weaponSpriteAnchor;

-(void) GetWeaponBarrelPositionBasedOnTarget: (CGPoint) targetPosition
                                   andOrigin: (CGPoint) originPosition
                       withBarrelPositionOut: (CGPoint*) barrelPositionOut
                          withArmRotationOut: (float*) armRotationOut
{
    //Set the position to a little bit ahead of the shoulder.
    //Then we'll move it a bit over the shoulder, so it looks like it's coming out the barrel.
    //NOTE: This might slightly look like a bullet goes through the enemy if the line of fire
    //      does not cross the enemy. The bullet will fly though regardless.
    const CGPoint OriginToTarget = ccpSub(targetPosition, originPosition);
    float armRotation = ccpAngle(ccp(-1,0), ccpNormalize(OriginToTarget));
    if( targetPosition.y < originPosition.y )
    {
        armRotation = (2*kmPI) - armRotation;
    }
    
    const CGPoint direction = ccpRotateByAngle(ccp(-1,0), ccp(0,0), -armRotation );
    CGPoint originalBulletPosition = ccpAdd( originPosition, ccpMult(direction, weaponBarrelOffsetMagnitude) );
    originalBulletPosition.y += 3;
    
    //Output the return variables
    (*barrelPositionOut) = originalBulletPosition;
    (*armRotationOut) = armRotation;
}

-(id) initWithDependencies: (Player*) gamePlayerRefIn
{
    self = [super init];
    
    if( nil != self )
    {        
        gamePlayerRef = gamePlayerRefIn;
        lastAcquiredTarget = nil;
        weaponBarrelOffsetMagnitude = 0;
        
        //Acquire GameplayScene's GameplayLayer
        gameplayLayerRef = nil;
        NSValue* gameplayLayerRefValue = [NSValue valueWithPointer:&gameplayLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER
                                                            object: gameplayLayerRefValue];
        NSAssert(gameplayLayerRef != nil, @"Failed to acquire GameplayLayer");
        
        if( [KKConfig selectKeyPath:@"KKStartupConfig"] )
        {
            printDebugData = [KKConfig boolForKey:@"EnableWeaponFireDebugOutput"];
        }
        

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateLineOfFire:)
                                                     name:EventList::PLAYER_NEW_AIM_DESTINATION
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveYunaReinforceBarricadeStart_EventHandler:)
                                                     name:EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START
                                                   object:nil];
        
    }
    
    return self;
}

-(void)dealloc
{
    CCLOG(@"Dealloc: %@",self);
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::PLAYER_NEW_AIM_DESTINATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START
                                                  object:nil];
    [super dealloc];
}

-(void) GameplayEnemyDead_EventHandler:(NSNotification*) notification
{
    lastAcquiredTarget = nil;
}


-(void) enter
{
    //Always acquire the Aim reference on Enter to make sure we don't have an invalid reference
    gameAimRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_AIM_TARGET, [NSValue valueWithPointer:&gameAimRef]);
    NSAssert(nil != gameAimRef,@"Aim reference is nil. No aim found.");
    
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_LevelUpWeapon.wav"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pullTrigger:)
                                                 name:EventList::CONTROLS_FIRE_BUTTON_PRESSED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(releaseTrigger:)
                                                 name:EventList::CONTROLS_FIRE_BUTTON_RELEASED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(releaseTrigger:)
                                                 name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(releaseTrigger:)
                                                 name:EventList::OVERDRIVE_START_ANIMATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GameplayWeaponFired_EventHandler:)
                                                 name:EventList::GAMEPLAY_WEAPON_FIRED
                                               object:nil];
    
    REGISTER_EVENT(self,
                   @selector(GameplayEnemyDead_EventHandler:),
                   EventList::GAMEPLAY_ENEMY_DEAD);
}

-(void) exit
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_FIRE_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_FIRE_BUTTON_RELEASED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_WEAPON_FIRED
                                                  object:nil];
    
    UNREGISTER_EVENT(self, EventList::GAMEPLAY_ENEMY_DEAD);
}


-(void) pullTrigger: (NSNotification*) notification
{
    NSAssert(false, @"This method must be overriden by the child class.");
}

-(void) releaseTrigger: (NSNotification*) notification
{
    NSAssert(false, @"This method must be overriden by the child class.");
}

-(void) fireWeaponFromOrigin: (CGPoint) originPosition
{
    NSAssert(false, @"This method must be overriden by the child class.");
}

-(void) GameplayWeaponFired_EventHandler:(NSNotification*) notification
{
    NSAssert([gamePlayerRef.CurrentPlayerState class] == [PlayerState_Alive class],
             @"Compatible player state for weapon initialization");
    
    PlayerState_Alive* playerAliveState = (PlayerState_Alive*)gamePlayerRef.CurrentPlayerState;
    
    PlayerArms* playerArmsRef = playerAliveState.PlayerArmsRef;
    
    CGPoint originPosition = [gamePlayerRef convertToWorldSpace:playerArmsRef.position];
    
    [self fireWeaponFromOrigin:originPosition];
}

-(void) OverdriveYunaReinforceBarricadeStart_EventHandler:(NSNotification*) notification
{
    //This is to avoid having a deallocated enemy be referenced.
    lastAcquiredTarget = nil;
}


-(void) update:(ccTime) delta
{
    //Empty base method designed to be overridden by child classes.
}

-(void) updateLineOfFire: (NSNotification*) notification
{
    //Project the target position all the way to the left bound
    //of the screen to simulate the bullet firing past the reticle.
    //We use the line equation to give us the projected point where
    //it intersects with the left screen bounds.
    
    NSAssert([gamePlayerRef.CurrentPlayerState class] == [PlayerState_Alive class],
             @"Compatible player state for weapon initialization");
    
    PlayerState_Alive* playerAliveState = (PlayerState_Alive*)gamePlayerRef.CurrentPlayerState;
    
    PlayerArms* playerArmsRef = playerAliveState.PlayerArmsRef;
    
    CGPoint originPosition = [gamePlayerRef convertToWorldSpace:playerArmsRef.position];
    
    CGPoint targetPosition = [self generateScreenLeftBoundProjectedPointFromLineOfFire:originPosition];
    
    //perform the raycast
    RayCastCallbackLineOfFire raycast;
    [GB2Engine sharedInstance].world->RayCast(&raycast,
                                              b2Vec2FromCGPoint(targetPosition),
                                              b2Vec2FromCGPoint(originPosition)
                                              );
    
    
    EnemyFieldInstance* raycastTarget = raycast.getFrontmostEnemyFieldInstanceTarget();
    EnemyBase* newTarget = nil;
    
    //If the raycast target is an EnemyBase
    if( nil != raycastTarget && [raycastTarget isKindOfClass:[EnemyBase class]])
    {
        newTarget = (EnemyBase*)raycastTarget;
    }
    
    //If there is no newly acquired target...
    if( nil == newTarget )
    {
        //disable the last acquired target's health display
        if( nil != lastAcquiredTarget)
        {
            //lose a targeting count
            --lastAcquiredTarget.TargetedCount;
        }
        
        //stop referencing the previous enemy as the last acquired target
        lastAcquiredTarget = nil;
    }
    //If the target acquired is different...
    else if( newTarget != lastAcquiredTarget )
    {
        //turn off health indicator for that enemy.
        if( nil != lastAcquiredTarget )
        {
            //lose a targeting count
            --lastAcquiredTarget.TargetedCount;
        }
        
        //turn on health indicator for the new enemy.
        ++newTarget.TargetedCount;
        
        //refer to the new enemy as the last acquired target
        lastAcquiredTarget = newTarget;
    }
}


-(CGPoint) generateScreenLeftBoundProjectedPointFromLineOfFire: (CGPoint) originPosition
{
    //Before we begin any calculations, we want to make sure the Aim Position used in this
    //function is in the proper coodinate space. Since Box2D only has one coordinate space
    //(world coords), we want to make sure the aim position reflects the same coord space.
    const CGPoint worldCoordsAimPosition = [gameAimRef.parent convertToWorldSpace:gameAimRef.position];
    
    //Calculate slope
    //
    //  (y2 - y1)
    //  ----------
    //  (x2 - x1)
    const float m = (worldCoordsAimPosition.y - originPosition.y) / (worldCoordsAimPosition.x - originPosition.x);
    
    //Find Y-intercept
    // y = mx + b [OR] b = y-mx
    const float b = originPosition.y - (m * originPosition.x);
    
    //acquire the projected point on the screens left bound:
    //(0,0) to (0, screen_height).
    return ccp(0,b);

}
@end
