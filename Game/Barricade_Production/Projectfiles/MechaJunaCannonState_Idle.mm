//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaCannonState_Idle.h"
#import "EnemyMechaJuna.h"

@implementation MechaJunaCannonState_Idle

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaCannon class]], @"Enemy state applied to inapropriate object type.");
        
        enemyParentRef = static_cast<EnemyMechaJunaCannon*>(enemyParentIn);
    }
    
    return self;
}


-(void) enter
{
}

-(void) exit
{
    
}

-(void) update: (float) deltaTime
{
    if( fabsf(enemyParentRef.rotation) < 0.2f )
    {
        [enemyParentRef setRotation:0];
    }
    else
    {
        const float rotationDirection = (enemyParentRef.rotation > 0) ? -1 : 1;
        
        [enemyParentRef setRotation: enemyParentRef.rotation + (enemyParentRef.CannonRotationSpeed * deltaTime) * rotationDirection];
    }
}

@end
