//
//  StatsSceneActor.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 11/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "StatsSceneActor.h"


@implementation StatsSceneActor
@synthesize KillCountLabel=killCountLabel;
@synthesize EnemyTypeSaveDataID=enemyTypeSaveDataID;

-(void) didLoadFromCCB
{
    killCountLabel.visible = NO;
}

@end
