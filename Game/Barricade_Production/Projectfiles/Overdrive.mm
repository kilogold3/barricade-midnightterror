//
//  Overdrive.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import "Overdrive.h"

@implementation Overdrive
@synthesize OverdrivePanelIconInactiveFilename=overdrivePanelIconInactiveFilename;
@synthesize OverdrivePanelIconActiveFilename=overdrivePanelIconActiveFilename;
@synthesize OverdriveExecution=overdriveExecution;
@synthesize CocosBuilderAnimationFilename=cocosBuilderAnimationFilename;

-(id) initWithDependencies: (NSString*) CocosBuilderAnimationFilenameIn
                          : (NSString*) OverdrivePanelIconActiveFilenameIn
                          : (NSString*) OverdrivePanelIconInactiveFilenameIn
                          : (OverdriveExecutionObject*) OverdriveExecutionIn
{
    self = [super init];
    
    if( nil != self)
    {
        cocosBuilderAnimationFilename = CocosBuilderAnimationFilenameIn;
        overdrivePanelIconActiveFilename = OverdrivePanelIconActiveFilenameIn;
        overdrivePanelIconInactiveFilename = OverdrivePanelIconInactiveFilenameIn;
        overdriveExecution = OverdriveExecutionIn;
        
        //Grab hold of the instances
        [cocosBuilderAnimationFilename retain];
        [overdrivePanelIconActiveFilename retain];
        [overdrivePanelIconInactiveFilename retain];
        [overdriveExecution retain];
    }
    
    return self;
}


-(void) dealloc
{
    //Let go of the instances
    [cocosBuilderAnimationFilename release];
    [overdrivePanelIconActiveFilename release];
    [overdrivePanelIconInactiveFilename release];
    [overdriveExecution release];
    
    [super dealloc];
}
@end
