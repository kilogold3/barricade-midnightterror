//
//  GameSaveDataPropertyList.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/3/13.
//
//

#ifndef Barricade_Production_GameSaveDataPropertyList_h
#define Barricade_Production_GameSaveDataPropertyList_h

//GSP stands for "Game Save Property"
namespace GSP {

    //PLAYER
    static NSString* const ENEMIES_KILLED = @"ENEMIES_KILLED";

    //BARRICADE
    static NSString* const BARRICADE_HEALTH = @"BARRICADE_HEALTH";
    
    //GAME_PROGRESS
    static NSString* const USED_ROOM_INTERACTIVE_OBJECTS = @"USED_ROOM_INTERACTIVE_OBJECTS";
    static NSString* const TRAVEL_SUPPLIES_COUNT = @"TRAVEL_SUPPLIES_COUNT";
    static NSString* const SURVIVORS_FOUND = @"SURVIVORS_FOUND";
    static NSString* const CURRENT_PROGRESSION = @"CURRENT_PROGRESSION";
    static NSString* const CURRENT_OVERALL_DAY = @"CURRENT_OVERALL_DAY";
    static NSString* const CURRENT_PROGRESSION_DAY = @"CURRENT_PROGRESSION_DAY";
    static NSString* const TUTORIAL_COMPLETED = @"TUTORIAL_COMPLETED";
    static NSString* const TUTORIAL_TO_BE_EXECUTED = @"TUTORIAL_TO_BE_EXECUTED";
    
    //STORY_SCENE
    static NSString* const CURRENT_STORY_PATH = @"CURRENT_STORY_PATH";
    
    //GAME_PROGRESS
    static NSString* const CURRENT_RUNNING_SCENE = @"CURRENT_RUNNING_SCENE";
};

//GSPV stands for "Game Save Property Value"
namespace GSPV {
    
    //GAME_PROGRESS
    static NSString* const CURRENT_RUNNING_SCENE__STORYSCENE = @"StoryScene";
    static NSString* const CURRENT_RUNNING_SCENE__RESOURCEGATHERINGSCENE = @"ResourceGatheringScene";
    static NSString* const CURRENT_RUNNING_SCENE__GAMEPLAYSCENE = @"GameplayScene";
    static const Byte FINAL_PROGRESSION = 6;
};



//GSC stands for "Game Save Category"
namespace GSC {
    static NSString* const PLAYER = @"Player";
    static NSString* const BARRICADE = @"Barricade";
    static NSString* const RESOURCE_GATHERING_SCENE = @"ResourceGatheringScene";
    static NSString* const STORY_SCENE = @"StoryScene";
    static NSString* const GAME_PROGRESS = @"GameProgress";
};



#endif
