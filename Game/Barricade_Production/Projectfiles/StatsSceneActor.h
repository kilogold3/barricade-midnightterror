//
//  StatsSceneActor.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 11/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface StatsSceneActor : CCSprite
{
    CCLabelTTF* killCountLabel;
    NSString* enemyTypeSaveDataID; //used to identify which enemy we are referring to.
}
@property (readonly) CCLabelTTF* KillCountLabel;
@property (readonly) NSString* EnemyTypeSaveDataID;

@end
