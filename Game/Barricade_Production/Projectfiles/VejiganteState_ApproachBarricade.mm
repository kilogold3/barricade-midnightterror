//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VejiganteState_ApproachBarricade.h"
#import "EnemyVejigante.h"
#import "Barricade.h"
#import "GMath.h"
#import "EnemyStateFactory.h"
#import "GB2ShapeCache.h"
#import "NotificationCenterEventList.h"
#import "CCAnimation+SequenceLoader.h"
#import "SimpleAudioEngine.h"
#import "AudioToolbox/AudioToolbox.h" //for rumble



@implementation VejiganteState_ApproachBarricade

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyVejigante class]],@"Wrong enemy parent type");
        enemyParentRef = (EnemyVejigante*) enemyParentIn;
        
        //Acquire GameplayScene's ScreenForegroundLayer
        screenForegroundLayerRef = nil;
        NSValue* screenForegroundLayerRefValue = [NSValue valueWithPointer:&screenForegroundLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER
                                                            object: screenForegroundLayerRefValue];
        NSAssert(screenForegroundLayerRef != nil, @"Failed to acquire ScreenForegroundLayer");
        
        
        hasArrivedAtBarricade = NO;
        faceFlashCount = 0;
    }
    
    return self;
}

-(void) enter
{
    //Change the vejigante sprite frame
    [enemyParentRef.VejiganteSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"VegifloatSprite0.png"]];
    
    //Set anchor point
    [enemyParentRef.VejiganteSprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyVejiganteShape"]
     ];
    
    //Load animation
    const short animationFrameCount = 7;
    float animationDelay = (30 / enemyParentRef.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"VegifloatSprite%d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    [enemyParentRef.VejiganteSprite runAction: [CCRepeatForever actionWithAction:animateAction]];
    
    //Setup flash effect layer
    screenFlashEffectLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)
                                                      width:[CCDirector sharedDirector].screenSize.width
                                                     height:[CCDirector sharedDirector].screenSize.height];
    [screenFlashEffectLayer setPosition:CGPointZero];
    [screenFlashEffectLayer setAnchorPoint:CGPointZero];
    
    CCSprite* faceSprite = [CCSprite spriteWithFile:@"vjigaFace.png"];
    [faceSprite setPosition:ccp([CCDirector sharedDirector].screenSize.width/2,
                                [CCDirector sharedDirector].screenSize.height/2)];
    
    [screenFlashEffectLayer addChild:faceSprite];
    
    
    //We will be adding and removing the flash effect layer without cleaning up,
    //so let's retain it.
    [screenFlashEffectLayer retain];
    
}

-(void) exit
{
    //Let go of the flash effect layer
    [screenFlashEffectLayer cleanup];
    [screenFlashEffectLayer release];
}

-(void) update: (float) delta
{
    //Keep moving forward.
    [enemyParentRef setPosition: ccp(enemyParentRef.position.x + (enemyParentRef.MovementSpeed * delta),
                                     enemyParentRef.position.y)];
    
    
    //If Vejigante is at the barricade, it's time to make him dissapear,
    //and make him appear behind Leo
    if( NO == hasArrivedAtBarricade && enemyParentRef.position.x > enemyParentRef.BarricadeRef.position.x )
    {
        //Let mark the flag so we don't execute this every frame.
        hasArrivedAtBarricade = YES;
        
        //We should also mark the enemy as invulnerable, so he doesn't process
        //his phasingDisplace.
        enemyParentRef.Invulnerable = YES;
        
        //We will stop any running actions here in order to avoid any wierd freaky stuff
        //by executing in the middle of another phasing displacement
        [enemyParentRef.VejiganteSprite stopAllActions];
        
        //reposition at random-Y and at the right of the screen.
        [enemyParentRef phasingDisplace:ccp([CCDirector sharedDirector].screenSize.width + enemyParentRef.VejiganteSprite.displayFrame.rect.size.width,
                                        gRangeRand(90,250))];
        
        //After the phasing is complete, change state to approach Leo
        //We'll wait a little bit and then transition. We'll make a CCSequence
        //that can be added to anything which will have a delay and a state transition call.
        CCSequence* transitionSequence = [CCSequence actions:
                                          [CCDelayTime actionWithDuration:1.0f],
                                          [CCCallBlock actionWithBlock:^
                                           {
                                               [enemyParentRef changeEnemyState:[enemyParentRef.AI_StateFactory createVejiganteState_ApproachPlayer]];
                                           }],
                                          nil];
        [enemyParentRef runAction:transitionSequence];
        
    }
    else
    {
        //logic for face flash, below...
        
        if(
           ( 0 == faceFlashCount && enemyParentRef.position.x > 140 ) ||
           ( 1 == faceFlashCount && enemyParentRef.position.x > 200 ) ||
           ( 2 == faceFlashCount && enemyParentRef.position.x > 255 )
           )
        {
            faceFlashCount++;
            
            [screenForegroundLayerRef addChild:screenFlashEffectLayer];
            
            [[SimpleAudioEngine sharedEngine] playEffect:@"VejiganteScreech.wav"];
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate); //rumble
            
            CCDelayTime* delayAction = [CCDelayTime actionWithDuration:0.1f];
            CCCallBlock* removeAction = [CCCallBlock actionWithBlock:^{
                [screenForegroundLayerRef removeChild:screenFlashEffectLayer cleanup:NO];
            }];
            
            [enemyParentRef runAction:[CCSequence actionOne:delayAction two:removeAction]];
        }
    }
}


@end
