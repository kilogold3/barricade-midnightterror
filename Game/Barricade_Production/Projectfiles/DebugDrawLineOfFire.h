//
//  DebugDrawLineOfFire.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Aim;
@class Player;

@interface DebugDrawLineOfFire : CCLayer 
{
    Aim* gameAimRef;
    Player* gamePlayerRef;
}
-(id) initWithDependencies: (Player*) gamePlayerRefIn
                          : (Aim*) gameAimRefIn;
@end
