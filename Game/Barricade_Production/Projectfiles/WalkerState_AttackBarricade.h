//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class GB2Contact;
@class Barricade;

@interface WalkerState_AttackBarricade : NSObject<IEnemyState>
{
    Barricade* barricadeRef;
    EnemyBase* enemyParent;
    CCAnimate* animateAction;
    CCAction* repeatingAttackingAnimationAction;
    CGPoint attackFrameID;
    BOOL readyToAttack;
    int soundIdAttack;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) endContactWithBarricadeSegment: (GB2Contact*)contact;


@end
