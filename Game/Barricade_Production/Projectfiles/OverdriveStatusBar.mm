//
//  OverdriveStatusBar.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OverdriveStatusBar.h"
#import "NotificationCenterEventList.h"
#import "EnemyBase.h"
#import "Player.h"
#import "GMath.h"
#import "StatusBarColorCycler.h"
#import "SimpleAudioEngine.h"

@interface OverdriveStatusBar (PrivateMethods)
// declare private methods here
@end

@implementation OverdriveStatusBar
@synthesize CurrentBrightness=currentBrightness;

-(id) init
{
	self = [super init];
	if (self)
	{        
		// add init code here (note: self.parent is still nil here!)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(UpdateUI_EventHandler:)
                                                     name:EventList::UI_UPDATE_OVERDRIVE_STATUS_BAR
                                                   object:nil];
        [self scheduleUpdate];
        
    }
	return self;
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::UI_UPDATE_OVERDRIVE_STATUS_BAR
                                                  object:nil];
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) didLoadFromCCB
{
    //Acquire player reference
    Player* playerRef = nil;
    NSValue* playerRefValue = [NSValue valueWithPointer:&playerRef];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                        object:playerRefValue];
    NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
    
    //Save the initial color
    initialColorTone = barSprite.color;
    
    //Set the initial bar value according to player
    [self setOverdriveBarToValue:playerRef.CurrentOverdrivePercentage];
    
    //Initialize the color cycler
    colorCycler = [StatusBarColorCycler node];
    colorCycler.CurrentSaturation = 0.5f;
    colorCycler.MinBrightness = 0.5f;
    colorCycler.MaxBrightness = 0.8f;
    colorCycler.HueTweenDuration = 0.3f;
    colorCycler.IsActive = (playerRef.CurrentOverdrivePercentage >= 100);
    [self addChild:colorCycler];
}

-(void) dealloc
{
	[super dealloc];
}

// scheduled update method
-(void) update:(ccTime)delta
{
    //Generate position for target fill
    const float targetPosition = barSprite.textureRect.size.width * percentageFillTarget;
    const float currentPosition = barSprite.position.x;
    
    //if current position is NOT at target position (or near it we should say)...
    if( abs(targetPosition - currentPosition) > 0.5f )
    {
        //move current position in direction to target
        const char direction = (targetPosition - currentPosition) > 0 ? 1 : -1;
        const float fillRate = 200.0f;
        [barSprite setPosition:ccp(currentPosition + ( ( fillRate * delta ) * direction ), 0 )];
        
        //Check to see if we haven't gone past our target...
        const float postCurrentPosition = barSprite.position.x;
        const float postDistanceFromTargetDirection = (targetPosition - postCurrentPosition) > 0 ? 1 : -1;
        
        //If the post-direction is NOT the same as the initial direction, we have crossed the
        //target position.
        if( postDistanceFromTargetDirection != direction )
        {
            //Lock the position at the intended coordinates. Next frame, we won't be moving
            //beause we are within the deadzone for animating.
            barSprite.position = ccp(targetPosition, barSprite.position.y);
        }
    }
    
    //Manage color mode states
    if( percentageFillTarget >= 1.0f and NO == colorCycler.IsActive )
    {
        colorCycler.IsActive = YES;
    }
    else if( percentageFillTarget < 1.0f and YES == colorCycler.IsActive )
    {
        colorCycler.IsActive = NO;
    }

    //Apply bar color
    barSprite.color = (colorCycler.IsActive ? [colorCycler getCurrentColorRGB] : initialColorTone);

}

-(void) UpdateUI_EventHandler: (NSNotification*) notification
{
    float currentOverdrivePercentage = [[notification object] floatValue];
    [self setOverdriveBarToValue:currentOverdrivePercentage];
}

-(void) setOverdriveBarToValue: (float) percentage
{
    NSAssert(percentage >= 0 and percentage <=100, @"OverdriveBar value out of bounds.");
    
    const float oldPercentageFillTarget = percentageFillTarget;
    percentageFillTarget = percentage / 100;
    
    if( 1 == percentageFillTarget and percentageFillTarget != oldPercentageFillTarget )
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_HudBarFull.wav"];
    }
}

@end
