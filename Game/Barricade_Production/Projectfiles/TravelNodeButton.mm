//
//  TravelNodeButton.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TravelNodeButton.h"
#import "GameSaveData.h"
#import "StoryScene.h"
#import "GameSaveDataPropertyList.h"

@interface TravelNodeButton (PrivateMethods)
// declare private methods here
@end

@implementation TravelNodeButton
@synthesize ProgressionValue=progressionValue;
@synthesize StoryPathValue=storyPathValue;
@synthesize SuppliesRequired=suppliesRequired;
@synthesize ProgressionName=progressionName;

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		//[self scheduleUpdate];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    //Let's grab the current game's progression

}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

// scheduled update method
-(void) update:(ccTime)delta
{
}

-(void) didLoadFromCCB
{

}

@end
