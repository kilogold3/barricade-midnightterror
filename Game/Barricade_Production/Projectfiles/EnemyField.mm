//
//  EnemyField.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyField.h"
#import "EnemyBase.h"
#import "GB2Engine.h"
#import "GMath.h"
#import "ProximityMine.h"
#import "NotificationCenterEventList.h"
#import "EnemySpawnerStrategyDefault.h"
#import "EnemySpawnerStrategyVejigante.h"

@implementation EnemyField
@synthesize SpawningEnabled;

-(BOOL) SpawningEnabled
{
    return enemySpawner.SpawningEnabled;
}

-(void) setSpawningEnabled:(BOOL)SpawningEnabledIn
{
    enemySpawner.SpawningEnabled = SpawningEnabledIn;
}

//Event Handlers
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    BOOL isOverdriveHit = [[notification object] boolValue];
    
    //If overdrive is NOT a HIT
    if( NO == isOverdriveHit )
    {
        //The overdrive is a MISS.
        //Let's reactivate and operate as normal with a special condition explained as follows:
        //When the end of the level is triggered by the sunrise animation, spawning also
        //gets disabled. However, if we launch an overdrive, at the end of the animation/execution
        //we end up re-enabling spawning.
        //To resolve this, we reactivate only prior to the end of the level.
        if( NO == endOfLevelTriggered )
        {
            self.SpawningEnabled = YES;
        }
    }
    else
    {
        //do nothing. Leave everything as is.
        //Spawning should remain disabled until the
        //end of the OverdriveExecution.
    }
}

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification
{
    //When the end of the level is triggered by the sunrise animation, spawning also
    //gets disabled. However, if we launch an overdrive, at the end of the animation/execution
    //we end up re-enabling spawning.
    //To resolve this, we reactivate only prior to the end of the level.
    if( NO == endOfLevelTriggered )
    {
        self.SpawningEnabled = YES;
    }
}

-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification
{
    self.SpawningEnabled = NO;
}

-(void) GameplayLevelTimerFinish_EventHandler: (NSNotification*) notification
{
    self.SpawningEnabled = NO;
    endOfLevelTriggered = YES;
    
    //The level has ended now...
    //Are all enemies dead? Then that means the final enemy has been defeated.
    if( YES == [self areAllEnemiesDead] )
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_FINAL_ENEMY_DEFEATED
                                                            object:nil];
    }
}

-(void) GameplayEnemyDead_EventHandler: (NSNotification*) notification
{
    //We've killed an enemy.
    //Have we ended the level? Then that means the final enemy has been defeated.
    if (NO == self.SpawningEnabled && YES == endOfLevelTriggered && YES == [self areAllEnemiesDead])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_FINAL_ENEMY_DEFEATED
                                                            object:nil];
    }
}

-(BOOL) areAllEnemiesDead
{
    //Check to see if there are any other enemies on the field.
    BOOL areAllEnemiesDeadBoolean = YES;
    for(CCNode* child in self.children)
    {
        //Verify if child is an EnemyBase
        if([child isKindOfClass:[EnemyBase class]])
        {
            //If child/enemy is alive...
            if( static_cast<EnemyBase*>(child).CurrentHealth > 0 )
            {
                areAllEnemiesDeadBoolean = NO;
                break;
            }
        }
    }
    
    return areAllEnemiesDeadBoolean;
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    self.SpawningEnabled = NO;
}
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    if( NO == endOfLevelTriggered )
    {
        self.SpawningEnabled = YES;
    }
}

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {        
        //listen to events
        REGISTER_EVENT(self, @selector(ComponentAcquireGameplayLayerEnemyfield_EventHandler:), EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveExecutionFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationStart_EventHandler:)
                                                     name:EventList::OVERDRIVE_START_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveAnimationFinish_EventHandler:)
                                                     name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayLevelTimerFinish_EventHandler:)
                                                     name:EventList::GAMEPLAY_LEVEL_TIMER_FINISH
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayEnemyDead_EventHandler:)
                                                     name:EventList::GAMEPLAY_ENEMY_DEAD
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(ControlsResumeGameButtonPressed_EventHandler:)
                                                     name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(DebugSpawnEnemy_EventHandler:)
                                                     name:EventList::DEBUG_SPAWN_ENEMY
                                                   object:nil];
        
        [self scheduleUpdate];
    }
    
    return self;
}

-(void) didLoadFromCCB
{
    //setup enemy spawner using the string passed in from CocosBuilder
    enemySpawner = [[NSClassFromString(EnemySpawnerClassName) alloc] initWithEnemyField:self];
    
    //scheduled-update data now that the spawner is created
    self.SpawningEnabled = YES;
    endOfLevelTriggered = NO;
}

-(void) cleanup
{    
    [enemySpawner release];
        
    //Unregister event listeners
    UNREGISTER_EVENT(self, EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD);

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_LEVEL_TIMER_FINISH
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_ENEMY_DEAD
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::DEBUG_SPAWN_ENEMY
                                                  object:nil];

    [self removeAllChildrenWithCleanup:YES];
    [super cleanup];

}

-(void) update: (ccTime) delta
{
    //run the spawner (if there is any). Thanks to objective-c, we don't crash here
    //if there is no spawner, because it's ok to send messages to null objects. (nothing happens)
    [enemySpawner update:delta];
}

-(void) spawnEnemyAtPosition: (CGPoint) position
{
    [enemySpawner spawnEnemyAtPosition:position];
}

-(void) DebugSpawnEnemy_EventHandler: (NSNotification*) notification
{
    [enemySpawner spawnEnemyAtPosition:ccp(50,190)];
}

-(void) ComponentAcquireGameplayLayerEnemyfield_EventHandler: (NSNotification*) notification
{
    EnemyField** enemyFieldRef = (EnemyField**)[[notification object] pointerValue];
    (*enemyFieldRef) = self;
}

@end