//
//  TrapBase.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyFieldInstance.h"

@class GB2Contact;

@interface TrapBase : EnemyFieldInstance
{
    b2Body* trapPhysicsBody;
    CCSprite* trapSprite;
    CCLayer* enemyFieldRef;
    short trapDamage;
    bool hasDetonated;
}
@property (nonatomic, readwrite) CGPoint position; //override CCNode's
@property (readonly) short TrapDamage;
@property (readonly) bool HasDetonated;
@end
