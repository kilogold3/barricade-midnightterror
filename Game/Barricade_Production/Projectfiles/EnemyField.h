//
//  EnemyField.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

class b2World;
@class EnemySpawnerStrategyBase;

@interface EnemyField : CCLayer 
{
@private
    //String of the spawner class to be instantiated. We need this
    //so we can specify what variation of spawner we will be using,
    //during the setup in CocosBuilder. This variable is not meant
    //to be used anywhere else
    NSString* EnemySpawnerClassName;

@protected
    //Reference to the enemy spawner instance
    EnemySpawnerStrategyBase* enemySpawner;
    
    BOOL endOfLevelTriggered;
}
@property (nonatomic, readwrite) BOOL SpawningEnabled;

-(id) init;
-(void) spawnEnemyAtPosition: (CGPoint) position;

//Event Handlers
-(void) ComponentAcquireGameplayLayerEnemyfield_EventHandler: (NSNotification*) notification;
-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
-(void) GameplayLevelTimerFinish_EventHandler: (NSNotification*) notification;
-(void) GameplayEnemyDead_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) DebugSpawnEnemy_EventHandler: (NSNotification*) notification;


@end
