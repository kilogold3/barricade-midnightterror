//
//  ProximityMine.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TrapBase.h"
#import <map>

typedef std::map<b2Body*, int> CollisionCounterLookupTable;

@interface ProximityMine : TrapBase
{
    CollisionCounterLookupTable enemyTargetContactCounters;
    CCSequence* trapDetonationSequence;
    BOOL printDebugData;
}
//Collisions
-(void) beginContactWithEnemyWalker: (GB2Contact*)contact;
-(void) endContactWithEnemyWalker: (GB2Contact*)contact;

@end
