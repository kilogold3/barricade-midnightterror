//
//  ProximityMine.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ProximityMine.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "GB2Contact.h"
#import "EnemyBase.h"
#import "CCAnimation+SequenceLoader.h"

@interface ProximityMine (PrivateMethods)
// declare private methods here
@end

@implementation ProximityMine

-(id) init
{
	self = [super init];
	if (self)
	{
        //Set debug draw flag
        [KKConfig selectKeyPath:@"KKStartupConfig"];
        printDebugData = [KKConfig boolForKey:@"EnableProximityMineDebugOutput"];

        
		// add init code here (note: self.parent is still nil here!)
        trapDamage = 1000;
        
        trapPhysicsBody->SetUserData(self);

        //Load shape
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:trapPhysicsBody
                                               forShapeName:@"mineShapeB"];
        
        //Load sprite
        trapSprite = [CCSprite spriteWithSpriteFrameName:@"mineSprite_Idle_b.png"];
        [self addChild:trapSprite];
        
        //Load detonation animation sequence
        const short animationFrameCount = 7;
        float animationDelay = 0.085f;
        
        CCAnimation* explodeAnimation = [CCAnimation animationWithSpriteSequence:@"mineSprite_Explode_b%d.png"
                                                                       numFrames:animationFrameCount
                                                                           delay:animationDelay];
        
        CCAnimate* explosionAnimateAction = [CCAnimate actionWithAnimation:explodeAnimation];
        CCCallFunc* detonationFinishCallbackAction = [CCCallFunc actionWithTarget:self
                                                                         selector:@selector( trapDetonationFinishCallback: )];
        
        trapDetonationSequence = [CCSequence actions:
                                    explosionAnimateAction,
                                    detonationFinishCallbackAction,
                                  nil];
        [trapDetonationSequence retain];
        
        //Set anchor point
        [trapSprite setAnchorPoint:
         [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"mineShapeB"]
         ];
	}
	return self;
}

-(void) trapDetonationFinishCallback: (id) sender
{
    [self.parent removeChild:self cleanup:YES];
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [trapDetonationSequence release];
    [self removeChild:trapSprite cleanup:YES];

	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) dealloc
{


	[super dealloc];
}

// scheduled update method
-(void) update:(ccTime)delta
{
}

-(void) beginContactWithEnemyWalker: (GB2Contact*)contact
{    
    //Find out if we have a counter stored for this collision
    b2Body* collidingBody = contact.otherFixture->GetBody();
    int collisionCounter;
    
    if( enemyTargetContactCounters.find( collidingBody ) != enemyTargetContactCounters.end() )
        collisionCounter = enemyTargetContactCounters[collidingBody] + 1;
    else
        collisionCounter = 1;
    
    enemyTargetContactCounters[collidingBody] = collisionCounter;
    
    //Found a collision
    if( 1 == collisionCounter && YES == printDebugData)
    {
        CCLOG(@"Enemy: [%@] WITHIN Blast Radius Of Trap: [%@]",
              (EnemyBase*)collidingBody->GetUserData(),
              self
              );
    }
    
    
    //Find out if we have not detonated AND if hit the detection radius
    if( NO == hasDetonated &&
       YES == [@"DetectionRadius" isEqualToString: (NSString*)contact.ownFixture->GetUserData() ] )
    {
        
        if( YES == printDebugData )
        {
            CCLOG(@"Detonating Trap: %@", self);
        }
        
        //Mark trap as detonated (We don't want to detonate twice).
        hasDetonated = YES;
        
        //Damage the enemies within the mine's blast radius
        for(CollisionCounterLookupTable::iterator iterator = enemyTargetContactCounters.begin();
            iterator != enemyTargetContactCounters.end();
            iterator++)
        {
            // iterator->first = key
            // iterator->second = value
            b2Body* enemyPhysicsBody = iterator->first;
            EnemyBase* enemyRef = static_cast<EnemyBase*>( enemyPhysicsBody->GetUserData() );
            
            [enemyRef reactToTrapDamage:self];
        }
        
        //Play detonation sequence
        [trapSprite runAction:trapDetonationSequence];
    }
}

-(void) endContactWithEnemyWalker: (GB2Contact*)contact
{
    //Assumes there has been a previous contact before
    b2Body* collidingBody = contact.otherFixture->GetBody();

    int collisionCounter = enemyTargetContactCounters[collidingBody] - 1;
    enemyTargetContactCounters[collidingBody] = collisionCounter;
    if( 0 == collisionCounter)
    {
        if( YES == printDebugData )
        {
            CCLOG(@"Enemy: [%@] OUTSIDE Blast Radius Of Trap: [%@]",
                  (EnemyBase*)collidingBody->GetUserData(),
                  self
                  );
        }
        
        //Remove from map.
        enemyTargetContactCounters.erase( collidingBody );
    }
}
@end
