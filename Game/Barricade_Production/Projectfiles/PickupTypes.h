//
//  PickupType.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/16/13.
//
//

#ifndef Barricade_Production_PickupTypes_h
#define Barricade_Production_PickupTypes_h

/*************
 We want to use the same weapon type enum values
 for the ammo. This helps in automation
 *************/
enum PICKUP_TYPE {
    PICKUP_AMMO_RIFLE = 0,
    PICKUP_AMMO_SHOTGUN = 1,
    PICKUP_TRAVEL_SUPPLIES = 2,
    PICKUP_DOOR_KEY = 3,
    PICKUP_MAX_TYPES
    };

#endif
