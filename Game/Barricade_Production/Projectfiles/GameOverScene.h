//
//  GameOverScene.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/20/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class CCControlButton;

@interface GameOverScene : CCLayer
{
    CCControlButton* buttonQuit;
    CCControlButton* buttonContinue;
    CCNode* chupacabrasEffectLoop;
}

@end
