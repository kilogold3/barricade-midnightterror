//
//  ResourceGatheringTransitionScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ResourceGatheringTransitionScene.h"
#import "ResourceGatheringScene.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"

@implementation ResourceGatheringTransitionScene

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = self;
    
    
    SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentOverallDay = [[resourceGatheringProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
    
    [KKConfig selectKeyPath:@"GameSettings"];
    const int maxDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
    [daysLeftLabel setString: [NSString stringWithFormat:@"%i Days Left", maxDaysLimit - currentOverallDay] ];
}

-(void) cleanup
{
    [super cleanup];
    
    // Clear out delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3
                                                                                 scene:[ResourceGatheringScene node]]];
}

@end
