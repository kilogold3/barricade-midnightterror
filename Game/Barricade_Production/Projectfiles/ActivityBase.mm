//
//  ActivityBase.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/10/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ActivityBase.h"
#import "NotificationCenterEventList.h"
#import "AppWidescreener.h"

@implementation ActivityBase
@synthesize OriginalActivityLauncher=originalActivityLauncher;

/*****************************************
 *
 *****************************************/
-(id) init
{
    if( self = [super init] )
    {
        REGISTER_EVENT(self,
                       @selector(FreezeActivity_EventHandler:),
                       EventList::RESOURCE_GATHERING_FREEZE_RUNNING_ACTIVITIES)
    }
    
    return self;
}

-(void) cleanup
{
    UNREGISTER_EVENT(self, EventList::RESOURCE_GATHERING_FREEZE_RUNNING_ACTIVITIES)
    [originalActivityLauncher release];

    [super cleanup];
}

-(void) onEnter
{
    [super onEnter];
    [AppWidescreener applyLetterboxToScene:self];
}

-(void) FreezeActivity_EventHandler:(NSNotification*) notification
{
    CCLOG(@"WARNING: Method FreezeActivity is not overriden by %@",self);
}
@end
