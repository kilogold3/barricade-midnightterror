//
//  Player.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "IPlayerState.h"
#import "OverdriveConstants.h"

class b2Body;
class b2World;
@class PlayerStateFactory;
@class WeaponBase;
@class Overdrive;
@class Pair;

@interface Player : CCNode
{
    b2Body* physicsBody;
    PlayerState* currentPlayerState;
    PlayerStateFactory* playerStateFactory;
    NSArray* weaponsArray;
    NSArray* overdrivesArray;
    NSDictionary* overdriveLevelsMap;
    float currentOverdrivePercentage;
    float currentWeaponBoostPercentage;
    float currentWeaponBoostTimer;
    float maxWeaponBoostTimer;
    BOOL isWeaponBoostActive;
}
@property (readonly) b2Body* PhysicsBody;
@property (readonly) PlayerState* CurrentPlayerState;
@property (readonly) NSArray* WeaponsArray;
@property (readonly) NSArray* OverdrivesArray;
@property (nonatomic,readwrite) float CurrentOverdrivePercentage;
@property (nonatomic,readwrite) float CurrentWeaponBoostPercentage;

-(id) init;
-(void) changeState: (PlayerState*) newState;
-(Pair*) getCurrentOverdrivePairForCharacter: (OverdriveConstants::OVERDRIVE_CHARACTERS) character;
-(void) saveAmmoSaveData;
-(void) activateWeaponBoost;

//Event Handlers
-(void) OverdriveJohnsonEndAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveJohnsonStartAimPhase_EventHandler: (NSNotification*) notification;
-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
-(void) EnemyDead_EventHandler: (NSNotification*) notification;
-(void) SetOverdrive_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;
@end
