/*
 MIT License
 
 Copyright (c) 2010 Andreas Loew / www.code-and-web.de
 
 For more information about htis module visit
 http://www.PhysicsEditor.de

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import "Box2D.h"
#import "GB2Contact.h"
#import "GB2Engine.h"
#import "GB2WorldContactListener.h"

// default ptm ratio value
float PTM_RATIO = 32.0f;

@interface GB2Engine (private_selectors)
- (id)init;
- (void)step:(ccTime)dt;
-(void) removeBodiesFromWorld;
-(void) removeFixturesFromBodies;
@end

@implementation GB2Engine

@synthesize world;

+ (GB2Engine*)sharedInstance
{
	static GB2Engine* instance = 0;
	if (!instance)
    {
		instance = [[GB2Engine alloc] init];    
    }
    
	return instance;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        // set default gravity
        b2Vec2 gravity(0,0);
        world = new b2World(gravity);
        world->SetAllowSleeping(false);
        
        // get ptmRatio from GB2ShapeCache
        if(GB2_HIGHRES_PHYSICS_SHAPES)
        {
            PTM_RATIO = [GB2ShapeCache sharedShapeCache].ptmRatio / 2.0f;            
        }
        else
        {
            PTM_RATIO = [GB2ShapeCache sharedShapeCache].ptmRatio;
        }
        
        // set the contact listener
        worldContactListener = new GB2WorldContactListener();
        world->SetContactListener(worldContactListener);    
        
        // schedule update
        [[CCDirector sharedDirector].scheduler scheduleUpdateForTarget:self priority:0 paused:NO];
        
        //Create removal lists
        postWorldStepWorldBodyRemovalList = [[NSMutableArray array] retain];
        postWorldStepBodyFixtureRemovalDictionary = [[NSMutableDictionary dictionary] retain];

    }
    return self;
}

- (void)deleteAllObjects
{
    // iterate all bodies
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()) 
    {        
        GB2Node *o = (GB2Node*)(b->GetUserData());
        if(o)
        {
            // destroy physics object
            [o deleteNow];        
        }
        else
        {
            // destroy body
            world->DestroyBody(b);
        }
    }
}

- (void)deleteWorld 
{    
    // delete all objects
    [self deleteAllObjects];
    
    // delete the world
	delete world;
	world = NULL;
    
    // delete the contact listener
    delete worldContactListener;
    worldContactListener = NULL;
    
    //delete removal lists
    [postWorldStepWorldBodyRemovalList release];
    [postWorldStepBodyFixtureRemovalDictionary release];
}


- (void)update:(ccTime)dt 
{            
	const float timeStep = 0.03f;
	const int32 velocityIterations = 8;
	const int32 positionIterations = 1;
	world->Step(timeStep, velocityIterations, positionIterations);
    
    /***************************************************************
     Now that we stepped over the world, we need to remove
     all the bodies that were meant to be destroyed during that loop.
     We start with the fixtures, then we check the actual bodies.
     This order matters because destroying a body, destroys all it's
     fixtures. So if we try to destroy a fixture, we won't find it.
     ***************************************************************/
    [self removeFixturesFromBodies];
    [self removeBodiesFromWorld];
}

- (void) iterateObjectsWithBlock:(GB2NodeCallBack)callback
{
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()) 
    {        
        // get the object
        callback((GB2Node*)(b->GetUserData()));
    }    
}

-(void) removeBodiesFromWorld
{
    for( NSUInteger curBodyIndex = 0; curBodyIndex < postWorldStepWorldBodyRemovalList.count; ++curBodyIndex )
    {
        //Obtain the b2Body reference container
        NSValue* storedValue = [postWorldStepWorldBodyRemovalList objectAtIndex:curBodyIndex];
        
        //Obtain the b2Body reference from the container
        b2Body* destructingBody = (b2Body*)storedValue.pointerValue;
        
        //Obtain the b2Body's UserData reference for deletion
        NSObject* userData = static_cast<NSObject*>( destructingBody->GetUserData() );

        //Destroy the b2Body
        world->DestroyBody( destructingBody );
        
        //If we have a valid userData AND is an NSObject type...
        if( nil != userData && [userData isKindOfClass: [NSObject class]]  )
        {
            //We'll let it go.
            //If retain count reaches zero, it will be deleted
            [userData release];
        }
    }
    
    [postWorldStepWorldBodyRemovalList removeAllObjects];
}
-(void) removeFixturesFromBodies
{
    NSArray* allKeysArray = [postWorldStepBodyFixtureRemovalDictionary allKeys];
    
    if (0 == allKeysArray.count) {
        return;
    }
    
    //For every body in the dictionary...
    for (NSValue* bodyValueKey in allKeysArray) 
    {
        //Grab the fixtures and body
        NSMutableArray* removableFixtures = [postWorldStepBodyFixtureRemovalDictionary objectForKey: bodyValueKey];
        b2Body* physicsBody = (b2Body*) bodyValueKey.pointerValue;
        
        //For every registered fixture in that body...
        for( NSValue* fixtureValue in removableFixtures ) 
        {            
            //Destroy the fixture
            physicsBody->DestroyFixture( (b2Fixture*)fixtureValue.pointerValue );
        }
        
        //Clear out the mutable array of fixtures
        [removableFixtures removeAllObjects];
        
        //Clear out the current key's dictionary entry
        [postWorldStepBodyFixtureRemovalDictionary removeObjectForKey:bodyValueKey];        
    }
    
    //Should be nothing left at this point...
    //But let's clear the dictionary anyhow.
    [postWorldStepBodyFixtureRemovalDictionary removeAllObjects];
}


- (void)addBodyToPostWorldStepSelectiveFixtureRemovalList: (b2Body*) body
                                                : (NSArray*) newFixtureArray
{    
    //Create the values to store
    NSValue* bodyValue = [NSValue valueWithPointer:body];
    
    //Search to see if there are any previously added bodies
    NSMutableArray* storedFixtureMutableArray =
        [postWorldStepBodyFixtureRemovalDictionary objectForKey: bodyValue];
    
    //If we didn't find anthing...
    if( nil == storedFixtureMutableArray )
    {
        //Add the values to the dictionary
        [postWorldStepBodyFixtureRemovalDictionary setObject: newFixtureArray
                                                      forKey: bodyValue];
    }
    else 
    {
        //Expand the array, but make sure there are no duplicates.
        //If we find duplicate, WE WILL CRASH!!!
        //That's because we will iterate over the same deleted fixture in
        //[removeFixturesFromBodies] and delete an already deleted instance.
        
        BOOL foundDuplicate;
        
        for (NSValue* newFixture in newFixtureArray)
        {
            foundDuplicate = NO;
            for (NSValue* storedFixture in storedFixtureMutableArray)
            {
                //if we found a duplicate match...
                if( newFixture.pointerValue == storedFixture.pointerValue )
                {
                    foundDuplicate = YES;
                    break;
                }
            }
            
            if(NO ==foundDuplicate)
            {
                [storedFixtureMutableArray addObject:newFixture];
            }
        }
    }
}

- (void)addBodyToPostWorldStepCompleteFixtureRemovalList: (b2Body*) body
{
    //Grab all the fixtures
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = body->GetFixtureList();
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    
    //FWD to the selective fixture release "selecting" all the fixtures.
    [self addBodyToPostWorldStepSelectiveFixtureRemovalList:body
                                                           :fixtureValuesArray];
}


- (void)addBodyToPostWorldStepBodyRemovalList: (b2Body*) body
{
    NSObject* userData = static_cast<NSObject*>( body->GetUserData() );
    
    //If we have a valid userData AND is an NSObject type...
    if( nil != userData && [userData isKindOfClass: [NSObject class]]  )
    {
        //We'll grab hold of it
        [userData retain];
    }
    
    [postWorldStepWorldBodyRemovalList addObject: [NSValue valueWithPointer:body] ];
}


@end