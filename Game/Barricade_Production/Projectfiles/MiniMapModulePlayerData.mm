//
//  MiniMapModulePlayerData.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 11/9/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "MiniMapModulePlayerData.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"


@implementation MiniMapModulePlayerData

-(void) didLoadFromCCB
{
    const GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];
    NSDictionary* gameprogressPropertyList = [gameSaveData getSaveObjectProperties:GSC::GAME_PROGRESS];
    NSDictionary* resourceGatheringDataPropertyList = [gameSaveData getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    const NSInteger currentProgression = [[gameprogressPropertyList objectForKey:GSP::CURRENT_PROGRESSION] intValue];

    
    //Set supplies string value with data
    NSNumber* suppliesCount = [resourceGatheringDataPropertyList objectForKey:GSP::TRAVEL_SUPPLIES_COUNT];
    [suppliesLabel setString: [NSString stringWithFormat:@"%i", [suppliesCount intValue]]];
    
    
    //We only set the Days-Left once we are past the first progression.
    //Let's check for that.
    if( currentProgression > 0 )
    {
        //Set Days left string value with data
        const int maxDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
        const int currentOverallDay = [[gameprogressPropertyList objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
        const int daysLeftCount = maxDaysLimit - currentOverallDay;
        [daysLeftCountLabel setString: [NSString stringWithFormat:@"%i", daysLeftCount]];
    }
    //Otherwise, we only keep the supplies data on screen, so we will
    //readjust the line decorations to make it look nice.
    else
    {
        //Make days left data invisible
        daysLeftLabel.visible = NO;
        daysLeftCountLabel.visible = NO;
        middleDividerNode.visible = NO;
        topDividerNode.position = middleDividerNode.position;
    }
}
@end
