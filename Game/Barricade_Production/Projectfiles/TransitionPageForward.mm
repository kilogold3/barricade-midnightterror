//
//  TransitionPageForward.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/27/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TransitionPageForward.h"

@interface TransitionPageForward (PrivateMethods)
// declare private methods here
@end

@implementation TransitionPageForward

+(id) transitionWithDuration:(ccTime) t scene:(CCScene*)s
{
    return [CCTransitionPageTurn transitionWithDuration:t
                                                  scene:s
                                              backwards:NO];
}

@end
