//
//  PlayerArms.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerArms.h"
#import "Player.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"

@implementation PlayerArms
@synthesize CurrentWeapon=currentWeapon;
@synthesize RearArmSprite=rearArmSprite;
@synthesize FrontArmSprite=frontArmSprite;

-(id) initWithDependencies: (Player*) playerRefIn
{
    self = [super init];
    
    if( self != nil)
    {
        playerRef = playerRefIn;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(weaponSelected_EventHandler:)
                                                     name:EventList::CONTROLS_WEAPON_SELECTED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(acquirePlayerArmsEventHandler:)
                                                     name:EventList::COMPONENT_ACQUIRE_PLAYER_ARMS_TARGET
                                                   object:nil];
        
        [self scheduleUpdate];
    }
    
    return self;
}

-(void) onEnter
{
    [super onEnter];
    
    //Load weapon
    NSAssert([playerRef.WeaponsArray count] > 0, @"The player has a valid weapons array");
    [self changeWeapon:[playerRef.WeaponsArray objectAtIndex:0]];
}

-(void) acquirePlayerArmsEventHandler: (NSNotification*) notification
{
    NSValue* armsRefPointer = [notification object];
    PlayerArms** targetPlayerArmsRef = (PlayerArms**)[armsRefPointer pointerValue];
    (*targetPlayerArmsRef) = self;
}

-(void) changeWeapon: (WeaponBase*) newWeapon
{
    //If we have a current weapon...
    if( currentWeapon != nil )
    {
        //Unload the weapon
        [currentWeapon exit];
    }
    
    //Assing the new weapon
    currentWeapon = newWeapon;
    
    //Lock and load!
    [currentWeapon enter];

    //Unload an arm sprite that may be loaded
    if( nil != rearArmSprite )
    {
        [self.parent removeChild: rearArmSprite cleanup: YES];
    }
    
    if( nil != frontArmSprite )
    {
        [self.parent removeChild: frontArmSprite cleanup: YES];
    }
    
    //Load new arms based on the new current weapon.
    //We need to set the rotation data, but we also need to convert positional space
    //to match the right location for the arms, because we are placing it on an arbitrary
    //CCNode hierarchy (in Cocos2D, positions are relative to the parent).
    rearArmSprite = [CCSprite spriteWithSpriteFrameName: currentWeapon.FrontArmSpriteID];
    [rearArmSprite setAnchorPoint: currentWeapon.WeaponSpriteAnchor];
    [self.parent addChild:rearArmSprite z:1];
    
    frontArmSprite = [CCSprite spriteWithSpriteFrameName: currentWeapon.RearArmSpriteID];
    [frontArmSprite setAnchorPoint: currentWeapon.WeaponSpriteAnchor];
    [self.parent addChild:frontArmSprite z:-1];
    
    //Use our rotational reference data to rotate the arms individually
    frontArmSprite.rotation =
    rearArmSprite.rotation =
    self.rotation;
    
    //Use our rotational reference data to reposition the arms individually
    frontArmSprite.position =
    rearArmSprite.position =
    self.position;
}

-(void) setRotationBasedOnPoint: (CGPoint) targetPoint
{
    //Get GameScene coordinates for the arm.
    CGPoint armsSceneCoordinates = [self.parent convertToWorldSpace:self.position];

    CGPoint aimDirection = ccpSub(targetPoint, armsSceneCoordinates);
    aimDirection = ccpNormalize(aimDirection);
    
    float rotationDegrees = CC_RADIANS_TO_DEGREES( ccpAngle( CGPointMake(-1, 0), aimDirection) );

    if (targetPoint.y < armsSceneCoordinates.y ) 
    {
        rotationDegrees = 360 - rotationDegrees;
    }

    //Set out own rotation (we keep this only as reference data)
    [self setRotation:rotationDegrees];
    
    //Use our rotational reference data to rotate the arms individually
    frontArmSprite.rotation =
    rearArmSprite.rotation =
    self.rotation;
    
    //Use our rotational reference data to reposition the arms individually
    frontArmSprite.position =
    rearArmSprite.position =
    self.position;
}

-(void) update: (ccTime) delta
{    
    //self update
    [currentWeapon update:delta];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_WEAPON_SELECTED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_PLAYER_ARMS_TARGET
                                                  object:nil];
    //If we have a current weapon...
    if( currentWeapon != nil )
    {
        //Unload the weapon
        [currentWeapon exit];
    }
    
    [super dealloc];
}

-(void) weaponSelected_EventHandler: (NSNotification*) notification
{
    NSInteger weaponIndex = [[notification object] integerValue];
    
    [self changeWeapon: [playerRef.WeaponsArray objectAtIndex:weaponIndex] ];
}


@end
