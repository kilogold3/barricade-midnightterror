//
//  DialogueScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/9/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
@class DialogueSceneInstance;

@interface DialogueScene : CCScene
{
    DialogueSceneInstance* dialogueSceneInstance;
    CCSprite* advanceSignalSprite;
}

@end
