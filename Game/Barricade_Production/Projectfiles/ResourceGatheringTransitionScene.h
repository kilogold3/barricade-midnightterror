//
//  ResourceGatheringTransitionScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCBAnimationManager.h"
@interface ResourceGatheringTransitionScene : CCLayer<CCBAnimationManagerDelegate>
{
    CCLabelTTF* daysLeftLabel;
}

@end
