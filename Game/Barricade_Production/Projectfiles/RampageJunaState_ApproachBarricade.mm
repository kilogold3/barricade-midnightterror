//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RampageJunaState_ApproachBarricade.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "GB2Contact.h"
#import "EnemyStateFactory.h"
#import "EnemyJuna.h"
#import "Barricade.h"
#import "GMath.h"
#import "NotificationCenterEventList.h"


@implementation RampageJunaState_ApproachBarricade

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyJuna class]],@"Wrong enemy parent type");
        enemyParent = (EnemyJuna*) enemyParentIn;
        
        //Acquire barricade reference
        barricadeRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET
                                                            object:[NSValue valueWithPointer:&barricadeRef]];
        NSAssert(nil != barricadeRef,@"Barricade reference is nil. No barricade found.");
        
   }
    
    return self;
}

-(void) enter
{
    contactWithBarricadeCounter = 0;
 
    //Set the display frame for the walker
    [enemyParent.EnemySprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"EvilJunaRun0.png"]];
    
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    //Load animation
    const short animationFrameCount = 6;
    float animationDelay = (50 / enemyParent.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"EvilJunaRun%d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    repeatingWalkingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody 
                                           forShapeName:@"enemyWalkerWalkShape"]; 
    
    //Animate sprite
    [enemyParent.EnemySprite runAction:repeatingWalkingAnimationAction];
}

-(void) exit
{
    //Remove the fixtures from the body.
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = enemyParent.EnemyPhysicsBody->GetFixtureList();
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    
    [[GB2Engine sharedInstance] addBodyToPostWorldStepSelectiveFixtureRemovalList:enemyParent.EnemyPhysicsBody
                                                                        :fixtureValuesArray];
    
    //Stop animating
    [enemyParent stopAction:repeatingWalkingAnimationAction];
}

-(void) update: (float) deltaTime
{
    //If we reached the target...
    if( enemyParent.position.x >= barricadeRef.position.x)
    {
        //Let's switch to the state where we are getting attacked by Jerome
        [enemyParent changeEnemyState:
         [enemyParent.AI_StateFactory createRampageJunaState_AttackedByJerome]];
    }
    else
    {
        //Move forward
        CGPoint newPosition = enemyParent.position;
        
        newPosition.x += enemyParent.MovementSpeed * deltaTime;
        
        [enemyParent setPosition:newPosition];
    }
}

-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact
{
    ++contactWithBarricadeCounter;
    if( 1 == contactWithBarricadeCounter )
    {
        [enemyParent changeEnemyState: [enemyParent.AI_StateFactory createWalkerState_AttackBarricade] ];
    }
}

-(void) endContactWithBarricadeSegment: (GB2Contact*)contact
{
    --contactWithBarricadeCounter;
}

@end
