//
//  ResourceGatheringInteractiveObject.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface ResourceGatheringInteractiveObject : CCSprite
{
}

-(void) Interact;
-(void) resourceGatheringLaunchActivity_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringSuspendActivity_EventHandler: (NSNotification*) notification;

@end
