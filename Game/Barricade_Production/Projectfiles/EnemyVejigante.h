//
//  EnemyVejigante.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/30/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyBase.h"

@interface EnemyVejigante : EnemyBase
{
    BOOL invulnerable;
    CCSprite* vejiganteSprite;
}
/*******************************************************************
 *Invulnerability in this specific case refers to not performing the 
 *'reactToWeaponDamage' method. phasignDisplace should be indescriminately 
 *called for reacting to weapon damage. However, we will check for
 *invulnerability to determine whether we should react to weapon 
 *damage or not
 *******************************************************************/
@property (nonatomic, readwrite) BOOL Invulnerable;
@property (readonly) CCSprite* VejiganteSprite;

//Collision (See special notes about this in implementation)
-(void) beginContactWithPlayer: (GB2Contact*)contact;
-(void) endContactWithPlayer: (GB2Contact*)contact;

//This method is designed to have the Vejigante fade out and
//appear somewhere else in a ghostlike manner.
-(void) phasingDisplace: (CGPoint) position;


//NOTE:
//      When implementing the better design, we simply need to have
//      EnemyVejigante forward the "reactToWeaponDamage" message to
//      the current AI state.
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;

@end
