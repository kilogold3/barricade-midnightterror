//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaCannonState_Dead.h"
#import "EnemyMechaJuna.h"
#import "GB2Engine.h"
#import "CCShake.h"
#import "EnemyJuna.h"
#import "NotificationCenterEventList.h"

@implementation MechaJunaCannonState_Dead

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaCannon class]], @"Enemy state applied to inapropriate object type.");
        
        enemyParentRef = static_cast<EnemyMechaJunaCannon*>(enemyParentIn);
    }
    
    return self;
}


-(void) enter
{
    //Remove the fixtures for body for the arm.
    //It's dead. No longer needed.
    //Remove the fixtures from the body.
    //We don't destroy the body per se... EnemyBase does that.
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = enemyParentRef.EnemyPhysicsBody->GetFixtureList();
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    [[GB2Engine sharedInstance] addBodyToPostWorldStepSelectiveFixtureRemovalList:enemyParentRef.EnemyPhysicsBody
                                                                        :fixtureValuesArray];
    enemyParentRef.EnemyPhysicsBody->SetUserData(NULL);
    
    
    //Tint the cannon and make it dissapear
    const float actionTime = 2.0f;
    CCSpawn* actionSpawn =
    [CCSpawn actionOne:[CCTintTo actionWithDuration:actionTime red:0 green:0 blue:0]
                   two:[CCFadeOut actionWithDuration:actionTime + 0.5f]];
        
    CCCallFunc* actionWithdrawMechaJuna = [CCCallFunc actionWithTarget:self selector:@selector(withdrawMechaJuna)];
    
    [enemyParentRef.CannonLightSprite runAction: [CCSequence actions:
                                                  actionSpawn,
                                                  actionWithdrawMechaJuna,
                                                  nil]];
}

-(void) exit
{
    
}

-(void) withdrawMechaJuna
{
    const float transitionTime = 2.0f;
    
    //Obtain Mecha Juna reference
    NSAssert([enemyParentRef.parent isKindOfClass:[EnemyMechaJuna class]], @"Enemy state applied to inapropriate object type.");
    EnemyMechaJuna* mechaJuna = static_cast<EnemyMechaJuna*>(enemyParentRef.parent);
    
    CCMoveTo* actionMoveBackwards = [CCMoveTo actionWithDuration:transitionTime position:ccp(-280, mechaJuna.position.y)];
    CCShake* actionShake = [CCShake actionWithDuration:transitionTime amplitude:ccp(0,2)];
    
    CCSpawn* actionSpawnMoveAndShake = [CCSpawn actionOne:actionMoveBackwards two:actionShake];
    
    CCCallBlock* actionRemoveMechaJunaAndSpawnRampageJuna = [CCCallBlock actionWithBlock:^{
        
        //Clear Leo's weapon target.
        SEND_EVENT(EventList::GAMEPLAY_ENEMY_DEAD, enemyParentRef);
        
        //Create RampageJuna and add her to wherever MechaJuna is standing. 
        EnemyJuna* rampageJuna = [EnemyJuna node];
        [rampageJuna setPosition:ccp(-10,mechaJuna.position.y)];
        [mechaJuna.parent addChild: rampageJuna];
        
        //Eliminate MechaJuna
        [mechaJuna removeFromParentAndCleanup:YES];
    }];
    
    [mechaJuna runAction: [CCSequence actionOne:actionSpawnMoveAndShake
                                            two:actionRemoveMechaJunaAndSpawnRampageJuna] ];
}

-(void) update: (float) deltaTime
{
}

@end
