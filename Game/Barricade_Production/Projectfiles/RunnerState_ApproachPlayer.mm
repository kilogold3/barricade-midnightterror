//
//  RunnerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RunnerState_ApproachPlayer.h"
#import "EnemyRunner.h"
#import "Player.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "WeaponBase.h"
#import "Barricade.h"
#import "EnemyStateFactory.h"

@implementation RunnerState_ApproachPlayer

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyRunner class]],@"Wrong enemy parent type");
        enemyParent = (EnemyRunner*)enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyRunnerWalk0.png"];
    
    //Load animation
    const short animationFrameCount = 6;
    float animationDelay = (30 / enemyParent.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyRunnerWalk%d.png" 
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    repeatingWalkingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody 
                                           forShapeName:@"enemyRunnerWalkShape"]; 
    //Set anchor point 
    [enemyParent.EnemySprite setAnchorPoint: 
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyRunnerWalkShape"]
     ];
    
    //Load the sprite
    [enemyParent addChild:enemyParent.EnemySprite];
    
    //Animate sprite
    [enemyParent.EnemySprite runAction:repeatingWalkingAnimationAction];
}

-(void) exit
{
    [enemyParent stopAction:repeatingWalkingAnimationAction];
    [enemyParent removeChild:enemyParent.EnemySprite cleanup:YES];
    [[GB2Engine sharedInstance] addBodyToPostWorldStepCompleteFixtureRemovalList:enemyParent.EnemyPhysicsBody];
}

-(void) update: (float) deltaTime
{
    //Grab player position
    CGPoint playerPos = enemyParent.PlayerRef.position;
    
    //Grab self position
    CGPoint selfPosition = enemyParent.position;
    
    //Form a line from enemy to player
    CGPoint direction = ccpSub(playerPos, selfPosition);
    
    //Normalize for direction
    CGPoint normailzedDirection = ccpNormalize(direction);
    
    //create velocity
    CGPoint velocity = { normailzedDirection.x * enemyParent.MovementSpeed * deltaTime ,
                        normailzedDirection.y * enemyParent.MovementSpeed * deltaTime };
    
    //apply velocity to position
    CGPoint newPosition = { enemyParent.position.x + velocity.x , 
                            enemyParent.position.y + velocity.y };
    
    //Set new position
    [enemyParent setPosition:newPosition];
    
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    [enemyParent runAction:[CCMoveBy actionWithDuration:0 position:ccp(-firingWeapon.Damage, 0)]];
}

-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact
{
    /******************************************************************************************************
     Sometimes, Yuna's overdrive gets called and the BarricadeReinforcer is created.
     During this moment, a check is made to decide whether the enemy should:
     -ApproachPlayer
     -ApproachBarricade
     -AttackBarricade
     
     A light test is done to check whether the enemy is on the left or right side of the barricade:
     if( enemy.position.x > barricade.position.x )
     {
     transition to ApproachPlayer
     }
     
     Because the barricade is slanted, the test has some corner cases which are hard to test for.
     In the event that the enemy is past the top-left corner of the barricade, but behind the
     bottom-left corner (considered to be on the left side of the barricade), the enemy will transition
     into ApproachPlayer even though we can see the enemy being on the left side of the barricade.
     
     To correct this small corner case, we will check if we come in contact with the barricade even though
     we usually don't expect it to...
     ******************************************************************************************************/
    if( enemyParent.BarricadeRef.IsDefending)
    {
        [enemyParent changeEnemyState:[enemyParent.AI_StateFactory createRunnerState_AttackBarricade]];
    }
    
}

@end
