//
//  SneakyHoldEventButton.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SneakyHoldEventButton.h"


@implementation SneakyHoldEventButton
@synthesize ButtonPressEventID=buttonPressEventID;
@synthesize ButtonReleaseEventID=buttonReleaseEventID;
@synthesize IsTouchEnabled=isTouchEnabled;

-(void) setIsTouchEnabled:(BOOL)IsTouchEnabledIn
{
    //Only process the modifier if the
    //new value differs from the old one
    if( isTouchEnabled != IsTouchEnabledIn )
    {
        isTouchEnabled = IsTouchEnabledIn;
        
        if( NO == isTouchEnabled )
        {
            [[CCDirector sharedDirector].touchDispatcher removeDelegate:self];
            
            //Reset button variables avoid leaving the button "pressed"
            //when we disable it.
            value = 0;
            active = NO;
            
        }
        else
        {
            [[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self priority:1 swallowsTouches:YES];
        }
    }
}

- (void) onEnterTransitionDidFinish
{
	[[CCDirector sharedDirector].touchDispatcher addTargetedDelegate:self priority:1 swallowsTouches:YES];
}

- (void) onExit
{
	[[CCDirector sharedDirector].touchDispatcher removeDelegate:self];
}

-(id)initWithRect:(CGRect)rect{
	self = [super init];
	if(self){
		
		bounds = CGRectMake(0, 0, rect.size.width, rect.size.height);
		center = CGPointMake(rect.size.width/2, rect.size.height/2);
		status = 1; //defaults to enabled
		active = NO;
		value = 0;
		isHoldable = YES;
		isToggleable = 0;
		radius = 32.0f;
		rateLimit = 1.0f/120.0f;
        isTouchEnabled = YES;
		
		_position = rect.origin;
	}
	return self;
}

-(void) dealloc
{
    [super dealloc];
}

+(id) button
{
	return [[[SneakyHoldEventButton alloc] initWithRect:CGRectZero] autorelease];
}

+(id) buttonWithRect:(CGRect)rect
{
	return [[[SneakyHoldEventButton alloc] initWithRect:rect] autorelease];
}

-(void)limiter:(float)delta
{
	value = 0;
	[self unschedule: @selector(limiter:)];
	active = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:buttonReleaseEventID 
                                                        object:nil];
}

- (void) setRadius:(float)r
{
	radius = r;
	radiusSq = r*r;
}

#pragma mark Touch Delegate

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (active || !status) return NO;
	
	CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
	location = [self convertToNodeSpace:location];
    //Do a fast rect check before doing a circle hit check:
	if(location.x < -radius || location.x > radius || location.y < -radius || location.y > radius){
		return NO;
	}else{
		float dSq = location.x*location.x + location.y*location.y;
		if(radiusSq > dSq){
			active = YES;
			if (!isHoldable && !isToggleable){
				value = 1;
				[self schedule: @selector(limiter:) interval:rateLimit];
			}
			if (isHoldable) value = 1;
			if (isToggleable) value = !value;
            
            if(1 == value)
            {
                //Execute press
                [[NSNotificationCenter defaultCenter] postNotificationName:buttonPressEventID
                                                                    object:nil];
            }
			return YES;
		}
	}
    return NO;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (!active || !status) return;
	
	CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
	location = [self convertToNodeSpace:location];
    //Do a fast rect check before doing a circle hit check:
	if(location.x < -radius || location.x > radius || location.y < -radius || location.y > radius){
		return;
	}else{
		float dSq = location.x*location.x + location.y*location.y;
		if(radiusSq > dSq){
			if (isHoldable) value = 1;
		}
		else {
			if (isHoldable) value = 0; active = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:buttonReleaseEventID 
                                                                object:nil];
		}
	}
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (!active || !status) return;
	if (isHoldable) value = 0;
	if (isHoldable||isToggleable) active = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:buttonReleaseEventID 
                                                        object:nil];
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self ccTouchEnded:touch withEvent:event];
}

@end
