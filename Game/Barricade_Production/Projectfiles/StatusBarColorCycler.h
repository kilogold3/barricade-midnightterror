//
//  StatusBarColorCycler.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/27/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface StatusBarColorCycler : CCNode
{
    float currentHue;
    float currentSaturation;
    float currentBrightness;
    BOOL isActive;
}
@property (nonatomic, readwrite) BOOL IsActive;
@property (nonatomic, readwrite) float CurrentSaturation;
@property (readonly) float CurrentBrightness;
@property (readonly) float CurrentHue;
@property (nonatomic, readwrite) float MinBrightness;
@property (nonatomic, readwrite) float MaxBrightness;
@property (nonatomic, readwrite) float HueTweenDuration;
@property (nonatomic, readwrite) float BrightnessTweenDuration;
-(ccColor3B) getCurrentColorRGB;
@end