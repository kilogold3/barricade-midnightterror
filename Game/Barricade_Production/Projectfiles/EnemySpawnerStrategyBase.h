//
//  EnemySpawnerStrategyBase.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import <Foundation/Foundation.h>
@class EnemyFactory;

@interface EnemySpawnerStrategyBase : NSObject
{
    EnemyFactory* enemyFactory;
    CCLayer* enemyField;

    BOOL spawningEnabled;
    
    //Spawn timing
    float currentSpawnTimer;
    float maxBaseSpawnTimer;
    float minBaseSpawnTimer;
    
    //Spawn amount
    short minCountPerSpawn;
    short maxCountPerSpawn;
    
    //Spawn modifiers
    float SpawnModifier_WeaponUpgradeLevel2;
    float SpawnModifier_WeaponUpgradeLevel3;
    float SpawnModifier_DaySpent;
    float SpawnModifier_DaySpentLimit;
    
    //Spawn Types
    NSArray* spawnTypesAndProbabilities;
}

@property (nonatomic, readwrite) BOOL SpawningEnabled;
@property (nonatomic, readwrite) float MaxBaseSpawnTimer;
@property (nonatomic, readwrite) float MinBaseSpawnTimer;

-(id) initWithEnemyField: (CCLayer*) enemyFieldIn;
-(void) update: (ccTime) delta;
-(void) spawnEnemyAtPosition: (CGPoint) position;

//This helper method allows child classes to modify the loading procedure
//for spawner properties, providing flexibility in approaches to load data
//on special case situations like the vejigante spawner.
-(void) loadConfigurationFromFile;

@end
