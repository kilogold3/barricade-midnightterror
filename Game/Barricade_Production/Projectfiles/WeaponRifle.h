//
//  WeaponPistol.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/11/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "WeaponBase.h"

@class Player;
class RayCastCallbackLineOfFire;

@interface WeaponRifle : WeaponBase 
{
    float maxWeaponCooldown;
    float currentWeaponCooldown;
    BOOL isTriggerPulled;
}

-(id) initWithDependencies: (Player*) gamePlayerRefIn;
-(void) update:(ccTime) delta;

@end
