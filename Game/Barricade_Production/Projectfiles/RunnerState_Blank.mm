//
//  RunnerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RunnerState_Blank.h"
#import "EnemyRunner.h"

@implementation RunnerState_Blank

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyRunner class]],@"Wrong enemy parent type");
        enemyParentRef = (EnemyRunner*) enemyParentIn;

    }
    
    return self;
}

-(void) enter
{
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}
@end
