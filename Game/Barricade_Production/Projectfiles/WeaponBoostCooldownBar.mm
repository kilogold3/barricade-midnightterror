//
//  OverdriveStatusBar.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "WeaponBoostCooldownBar.h"
#import "NotificationCenterEventList.h"
#import "EnemyBase.h"
#import "Player.h"
#import "ColorSpaceUtilities.h"
#import "GMath.h"

@interface WeaponBoostCooldownBar (PrivateMethods)
// declare private methods here
@end

@implementation WeaponBoostCooldownBar
@synthesize CurrentBrightness=currentBrightness;

-(id) init
{
	self = [super init];
	if (self)
	{        
		// add init code here (note: self.parent is still nil here!)
        REGISTER_EVENT(self,
                       @selector(UpdateUI_EventHandler:),
                       EventList::UI_UPDATE_WEAPONBOOST_COOLDOWN_BAR)
        
        [self scheduleUpdate];
        
    }
	return self;
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    UNREGISTER_EVENT(self, EventList::UI_UPDATE_WEAPONBOOST_COOLDOWN_BAR)

	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) didLoadFromCCB
{
    //Acquire player reference
    Player* playerRef = nil;
    NSValue* playerRefValue = [NSValue valueWithPointer:&playerRef];
    
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_PLAYER_TARGET, playerRefValue)

    NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
    
    //Set the initial bar value according to player
    [self setOverdriveBarToValue:playerRef.CurrentOverdrivePercentage];
}

-(void) dealloc
{
	[super dealloc];
}

// scheduled update method
-(void) update:(ccTime)delta
{
    //Generate position for target fill
    const float targetPosition = barSprite.textureRect.size.width * percentageFillTarget;
    const float currentPosition = barSprite.position.x;
    
    //if current position is NOT at target position (or near it we should say)...
    if( abs(targetPosition - currentPosition) > 0.5f )
    {
        //move current position in direction to target
        const char direction = (targetPosition - currentPosition) > 0 ? 1 : -1;
        const float fillRate = 200.0f;
        [barSprite setPosition:ccp(currentPosition + ( ( fillRate * delta ) * direction ), barSprite.position.y )];
    }
}

-(void) UpdateUI_EventHandler: (NSNotification*) notification
{
    float currentOverdrivePercentage = [[notification object] floatValue];
    [self setOverdriveBarToValue:currentOverdrivePercentage];
}

-(void) setOverdriveBarToValue: (float) percentage
{
    NSAssert(percentage >= 0 and percentage <=100, @"OverdriveBar value out of bounds.");
    
    percentageFillTarget = percentage / 100;
}

@end
