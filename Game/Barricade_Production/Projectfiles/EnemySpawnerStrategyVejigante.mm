//
//  EnemySpawnerStrategyDefault.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import "EnemySpawnerStrategyVejigante.h"
#import "EnemyFactory.h"
#import "EnemyBase.h"

@implementation EnemySpawnerStrategyVejigante


-(id) initWithEnemyField: (CCLayer*) enemyFieldIn;
{
    self = [super initWithEnemyField: enemyFieldIn];
    
    if( nil != self)
    {
        stubbornLockEnabled = NO;
        spawnCountBeforeVejigante = 10;
    }
    
    return self;
}

-(void) spawnEnemyAtPosition: (CGPoint) position
{
    if( NO == stubbornLockEnabled )
    {
        if( spawnCountBeforeVejigante > 0 )
        {
            --spawnCountBeforeVejigante;
            
            EnemyBase* newEnemy = [enemyFactory createWalker];
            [newEnemy setPosition: position];
            [enemyField addChild:newEnemy];
        }
        else
        {
            //Make sure we don't do this more than once.
            NSAssert(YES == spawningEnabled, @"Spawning is already disabled.");
            spawningEnabled = NO;
            stubbornLockEnabled = YES;
            
            EnemyBase* newEnemy = [enemyFactory createVejigante];
            [newEnemy setPosition: position];
            [enemyField addChild:newEnemy];
        }
    }
}

@end
