//
//  PlayerState_Alive.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerState_Alive.h"
#import "Player.h"
#import "NotificationCenterEventList.h"
#import "playerArms.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "WeaponPistol.h"
#import "Aim.h"

const uint16 MAX_PLAYER_Y_POSITION = 215;

@implementation PlayerState_Alive
@synthesize PlayerArmsRef=playerArmsRef;
@synthesize MovementSpeed=movementSpeed;
@synthesize MovementDestination=movementDestination;
@synthesize PlayerBodySprite=playerBodySprite;

-(id) initWithDependencies: (Player*) playerRefIn
{
    self = [super init];
    
    if( self != nil)
    {
        playerRef = playerRefIn;
        isAnimating = NO;
        isAnimationReversed = NO;
        
        movementSpeed = 65;
        movementDestination = playerRef.position;
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(setNewMovementDestination:) 
                                                     name:EventList::PLAYER_NEW_MOVEMENT_DESTINATION 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(processNewAimDestination:) 
                                                     name:EventList::PLAYER_NEW_AIM_DESTINATION 
                                                   object:nil];
        
        //Instantiate the arms
        playerArmsRef = [[PlayerArms alloc] initWithDependencies:playerRef];
        [playerArmsRef setPosition:ccp(1,32)];
        
        //load the sprite sheet into a CCSpriteBatchNode object. If you're adding a new sprite
        //to your scene, and the image exists in this sprite sheet you should add the sprite
        //as a child of the same CCSpriteBatchNode object otherwise you could get an error.
        leoSheet = [CCSpriteBatchNode batchNodeWithFile:@"leoSheet.png"];
        
        //load each frame included in the sprite sheet into an array for use with the CCAnimation object below
        NSMutableArray* leoAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 13; ++i) 
        {
            [leoAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"NewLeo_Walk%02d.png", i]]];
        }
        
        
        //Create the animation from the frame flyAnimFrames array
        float delay = ( [leoAnimFrames count] / (movementSpeed*3.5f) );
        CCAnimation* leoAnim = [CCAnimation animationWithSpriteFrames:leoAnimFrames delay: delay ];
        leoAnim.restoreOriginalFrame = YES;
                
        //create a sprite and set it to be the first image in the sprite sheet
        playerBodySprite = [CCSprite spriteWithSpriteFrameName:@"NewLeo_Stand.png"];
        [playerBodySprite setAnchorPoint:ccp(0.5,0)];
        
        //create a looping action using the animation created above. This just continuosly
        //loops through each frame in the CCAnimation object
        singleAnimationAction = [CCAnimate actionWithAnimation:leoAnim];  
        [singleAnimationAction retain];
        
        leoWalkAction_Forward = [CCRepeatForever actionWithAction: singleAnimationAction];
        [leoWalkAction_Forward retain];
        
        leoWalkAction_Backward = [CCRepeatForever actionWithAction: [singleAnimationAction reverse]];
        [leoWalkAction_Backward retain];
        
        //add the sprite to the CCSpriteBatchNode object
        [leoSheet addChild:playerBodySprite];
  
        //Set anchor point 
        [playerBodySprite setAnchorPoint: 
         [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"leoShape"]
         ];
        
        //add fixtures to player physics body
        //Load shape
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:playerRef.PhysicsBody 
                                               forShapeName:@"leoShape"]; 

    }
    
    return self;
}

-(void) dealloc
{
    //Remove the fixtures from the body.
    NSMutableArray* fixtureValuesArray = [NSMutableArray array];
    b2Fixture* currentFixture = playerRef.PhysicsBody->GetFixtureList();    
    while( currentFixture != nil )
    {
        b2Fixture* releaseFixture = currentFixture;
        
        currentFixture = currentFixture->GetNext();
        
        NSValue* releaseFixtureValue = [NSValue valueWithPointer:releaseFixture];
        [fixtureValuesArray addObject:releaseFixtureValue];
        
    }
    [[GB2Engine sharedInstance] addBodyToPostWorldStepSelectiveFixtureRemovalList:playerRef.PhysicsBody 
                                                                        :fixtureValuesArray];
    
    //Detach event listeners
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:EventList::PLAYER_NEW_MOVEMENT_DESTINATION  
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:EventList::PLAYER_NEW_AIM_DESTINATION  
                                                  object:nil];
    
    //DEALLOC NOTE:
    //singleAnimationAction must be released 
    //AFTER leoWalkAction, because leoWalkAction
    //releases singleAnimationAction internally.
    //If we release singleAnimationAction first,
    //we are releasing an invalid pointer.
    [leoWalkAction_Forward release];
    [leoWalkAction_Backward release];
    [singleAnimationAction release];

    [playerArmsRef cleanup];
    [playerArmsRef release];
    [super dealloc];
}

-(void) Enter
{
    [playerRef addChild: leoSheet];   
    [playerRef addChild: playerArmsRef];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(acquirePlayerCurrentWeapon_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON
                                               object:nil];
     
    
}

-(void) Exit
{
    [playerRef removeChild: playerArmsRef cleanup:NO];
    [playerRef removeChild: leoSheet cleanup:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON
                                                  object:nil];
}

-(void) acquirePlayerCurrentWeapon_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    WeaponBase** requestorRef = (WeaponBase**)[requestorRefPointer pointerValue];
    (*requestorRef) = playerArmsRef.CurrentWeapon;
}

-(void) setNewMovementDestination: (NSNotification*) notification
{
    //NOTE: Event argument contains the point.
    //      THE POINT IS IN SCREEN SPACE!
    NSValue* newMovementDestination = [notification object];
    const CGPoint screenSpaceTouchLocation = [newMovementDestination CGPointValue];
    
    //Here we convert the world coordinate to local space of the node where the player resides.
    movementDestination =  [playerRef.parent convertToNodeSpace:screenSpaceTouchLocation];
    if( movementDestination.y > MAX_PLAYER_Y_POSITION )
    {
        movementDestination.y = MAX_PLAYER_Y_POSITION;
    }
}

-(void) processNewAimDestination: (NSNotification*) notification
{
    const Aim* aimRef = [notification object];
    
    //Within this function we will be dealing in world-space form, so we need to transform
    //the position to the world coordinates before invoking the method.
    [playerArmsRef setRotationBasedOnPoint: [aimRef.parent convertToWorldSpace:aimRef.position] ];
}

-(void) Update: (float) deltaTime
{
    //get direction from current position to destination position
    CGPoint CurrentPositionToDestionation = ccpSub( movementDestination, playerRef.position );
    
    //If we're outside the deadzone
    if( ccpLength(CurrentPositionToDestionation) > 5 )
    {
        ////////////////////////////////
        //Calculate movement offset
        ////////////////////////////////
        CGPoint movementOffset;
        
        //Get direction
        movementOffset = ccpNormalize(CurrentPositionToDestionation);
        
        //Scale based on speed & time
        movementOffset = ccpMult(movementOffset, movementSpeed * deltaTime);
        
        ////////////////////////////////
        //Calculate animation
        ////////////////////////////////
        
        //If we are moving backwards...
        if( movementOffset.x > 0 )
        {
            //If we are not running a reversed animation...
            if( NO == isAnimationReversed )
            {
                //Stop the animation action
                [playerBodySprite stopAction:leoWalkAction_Forward];
                
                //Play the new animation action
                [playerBodySprite runAction:leoWalkAction_Backward];
                
                //Set proper flags
                isAnimationReversed = YES;
                isAnimating = YES;
            }
            //we have a reversed animation
            //if we are not animating...
            else if( NO == isAnimating )
            {
                //animate
                isAnimating = YES;
                [playerBodySprite runAction:leoWalkAction_Backward];
            }
        }
        
        else //we are moving forward
        {
            //If we are running a reversed animation...
            if( YES == isAnimationReversed )
            {                
                //Stop the animation action
                [playerBodySprite stopAction:leoWalkAction_Backward];
                
                //Play the new animation action
                [playerBodySprite runAction:leoWalkAction_Forward];
                
                //Set proper flags
                isAnimationReversed = NO;
                isAnimating = YES;
            }
            //we have a forward animation
            //if we are not animating...
            else if( NO == isAnimating )
            {
                //animate
                isAnimating = YES;
                [playerBodySprite runAction:leoWalkAction_Forward];
            }
        }
        
        ///////////////////////////////
        //Apply movement offset
        ///////////////////////////////
        //Calculate the new position. Make sure the player does not go
        //beyond the Y-Position limit. That would make him look like he's
        //floating in the background.
        movementOffset = ccpAdd(movementOffset, playerRef.position);
        [playerRef setPosition: movementOffset];
        
        //Notify the system, the player has moved
        SEND_EVENT(EventList::GAMEPLAY_PLAYER_MOVED, nil);
    }
    else 
    {
        if( YES == isAnimating )
        {
            if( YES == isAnimationReversed )
            {
                [playerBodySprite stopAction:leoWalkAction_Backward];
            }
            else 
            {
                [playerBodySprite stopAction:leoWalkAction_Forward];
            }
            
            
            [playerBodySprite setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"NewLeo_Stand.png"] ];
            isAnimating = NO;
        }
    }
}

@end
