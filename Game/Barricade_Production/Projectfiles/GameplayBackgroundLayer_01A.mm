//
//  GameplayBackgroundLayer_00.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/1/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplayBackgroundLayer_01A.h"
#import "CCBAnimationManager.h"
#import "CCBSequence.h"

@interface GameplayBackgroundLayer_01A (PrivateMethods)
// declare private methods here
@end

@implementation GameplayBackgroundLayer_01A

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    //If we are responding to the timeline related to
    //the background...
    if( [name isEqualToString:@"Default Timeline"] )
    {
        //Stop glittering the stars
        [particleEffect_Stars stopSystem];
    }
    
    //Manage system events
    [super completedAnimationSequenceNamed:name];
}

@end
