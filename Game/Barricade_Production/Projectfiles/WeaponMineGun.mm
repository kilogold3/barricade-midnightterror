//
//  WeaponMineGun.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "WeaponMineGun.h"
#import "ProximityMine.h"
#import "NotificationCenterEventList.h"
#import "Player.h"

@implementation WeaponMineGun
-(id) initWithDependencies: (Player*) gamePlayerRefIn
{
    self = [super initWithDependencies: gamePlayerRefIn];
    
    if( nil != self )
    {
        aimMaxArc = 45;
        aimMinArc = 20;
        
        rearArmSpriteID = @"pistolBackArm.png";
        frontArmSpriteID = @"pistolFrontArm.png";
        weaponPanelInactiveSpriteID = @"WeaponSelect/GunNormal.png";
        weaponPanelActiveSpriteID = @"WeaponSelect/GunSelected.png";
        hudDataDisplayWeaponSpriteID = @"NEWHUDPistolAmmo2.png";
        weaponSpriteAnchor = ccp(0.91,0.56);
        
        //Set basic attributes
        weaponType = eMINE_GUN;
    }
    
    return self;
}

//-(void) fireWeaponFromOrigin: (CGPoint) originPosition
//{
//    ProximityMine* mine = [ProximityMine node];
//    mine.position = originPosition;
//    
//    ccBezierConfig bzConfig;
//    bzConfig.controlPoint_1 = originPosition;
//    bzConfig.controlPoint_2 = originPosition;
//    bzConfig.endPosition = aimPosition;
//    CCBezierTo* bezierAction = [CCBezierTo actionWithDuration:1.0f
//                                                       bezier:bzConfig];
//    
//    [mine runAction:bezierAction];
//    [gameplayLayerRef addChild:mine];
//}
@end
