//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaCannonState_Fire.h"
#import "EnemyMechaJuna.h"
#import "EnemyStateFactory.h"
#import "NotificationCenterEventList.h"
#import "SimpleAudioEngine.h"

@implementation MechaJunaCannonState_Fire

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaCannon class]], @"Enemy state applied to inapropriate object type.");
        
        enemyParentRef = static_cast<EnemyMechaJunaCannon*>(enemyParentIn);
    }
    
    return self;
}

-(void) enter
{
    isAlignedToStartPosition = NO;
    
    /**********************************************************
     * Let's start by getting the starting rotation direction
     **********************************************************/
    //Get world position of enemyParent
    CGPoint parentWorldSpacePosition = [enemyParentRef.parent convertToWorldSpace:enemyParentRef.position];
    
    //generate a line from the enemy to the target block
    CGPoint enemyToStartTargetLine = ccpSub( enemyParentRef.CannonAimPointStart, parentWorldSpacePosition );
    
    //Get directional vector of the line
    CGPoint enemyToStartTargetLine_Normalized = ccpNormalize(enemyToStartTargetLine);
    
    //Calculate the start rotation
    startRotationDegrees = CC_RADIANS_TO_DEGREES( ccpAngle( enemyToStartTargetLine_Normalized, ccp(1,0) ) );
    
    //Special-Case Handling:
    //We only get the angle but no -/+ signs. We'll have to calculate that manually.
    if(parentWorldSpacePosition.y > enemyParentRef.CannonAimPointStart.y)
    {
        startRotationDegrees = -startRotationDegrees;
    }
    
    
    /**********************************************************
     * Now we need the ending rotation direction
     **********************************************************/
    
    //generate a line from the enemy to the target block
    CGPoint enemyToEndTargetLine = ccpSub( enemyParentRef.CannonAimPointEnd, parentWorldSpacePosition );
    
    //Get directional vector of the line
    CGPoint enemyToEndTargetLine_Normalized = ccpNormalize(enemyToEndTargetLine);

    //Calculate the end rotation
    endRotationDegrees = CC_RADIANS_TO_DEGREES( ccpAngle( enemyToEndTargetLine_Normalized, ccp(1,0) ) );
    
    //Special-Case Handling:
    //We only get the angle but no -/+ signs. We'll have to calculate that manually.
    if(parentWorldSpacePosition.y > enemyParentRef.CannonAimPointEnd.y)
    {
        endRotationDegrees = -endRotationDegrees;
    }
}

-(void) exit
{
    //In case we are removed while we are aligning and/or firing...
    
    //Stop the action to avoid the become harmful flag to be set
    //if it's caught in still transition
    [enemyParentRef.CannonBeam.BeamSprite stopAllActions];
    
    //We are not harmful anymore
    enemyParentRef.CannonBeam.IsHarmful = NO;
    
    //put the laser beam back to it's original size
    CCScaleTo* scaleAction = [CCScaleTo actionWithDuration:0.15f scaleX:enemyParentRef.CannonBeam.BeamSprite.scaleX scaleY:0];
    [enemyParentRef.CannonBeam.BeamSprite runAction:scaleAction];
}

-(void) update: (float) deltaTime
{   
    //Find out which point to rotate to
    float rotationTarget = ( isAlignedToStartPosition ) ? endRotationDegrees : startRotationDegrees;
    
    //Cocos2D rotates CW, but math works CCW. We'll use inverted logic on 'direction' here.
    
    const short RT_CW = 1;
    const short RT_CCW = -1;
    
    //90 degrees [TARGET] - 0 degrees [CURRENT]   is positive, let's rotate up to reach the target - CCW.
    //90 degrees [TARGET] - 100 degrees [CURRENT] is negative, let's rotate down to reach the target - CW
    signed short direction = ( rotationTarget - (-enemyParentRef.rotation) > 0 ) ? RT_CCW : RT_CW;
    
    //Calculate the new roation according to speed and time scaling.
    float newRotation = enemyParentRef.rotation + (enemyParentRef.CannonRotationSpeed * direction) * deltaTime;
    
    //Apply rotation
    [enemyParentRef setRotation: newRotation];
    
    /*********************************
     To explain the wierd check below:
        ( rotationTarget - (-enemyParentRef.rotation) ) * -direction < 0 )
     
     It's hard to explain...
     
     The following is used to see the distance between the current rotation and
     the target rotation. Since Cocos2D rotates in the opposite direction, we negate
     the Cocos2D game object's rotation to invert it into the real-world math rotation
     value. We will call this "D", so it goes like this...
     
        ( D * -direction < 0 )
     
     'direction' is used to know if we're going Cocos2D-CW or Cocos2D-CCW.
     However, since the angle measurements in real-world math are done in inverse,
     we negate 'direction' to get the real-world math rotation direction. We
     will call this "RWMR"
     
        ( D * RWMR < 0 )

     This leaves us with what in essence can be explained in the following...
     
     If the distance between Current & Target rotations is negative, we are past
     the target line. To REALLY know if we are past the target line (and not mistakenly
     behind the line headed in the other way), we present the direction.
     By multiplying the real-world math direction (+1 or -1), we control whether that number
     becomes positive or negative according to the trayectory we're headed to.
     *********************************/
    
    //If we are still aligning, but are past the alignment point...
    if( startRotationDegrees == rotationTarget and
       
       //Here is that weird check...
        ( rotationTarget - (-enemyParentRef.rotation) ) * -direction < 0 )
    {

        //We can consider outselves aligned
        //Only actually useful for next frame.
        isAlignedToStartPosition = YES;
        
        //Since we are aligned, that means we're ready to hurt the player.
        CCCallBlock* playLaserSound = [CCCallBlock actionWithBlock:^{
            [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_MechaJuna_LaserCannon.wav"];
        }];
        CCScaleTo* scaleAction = [CCScaleTo actionWithDuration:0.15f scaleX:enemyParentRef.CannonBeam.BeamSprite.scaleX scaleY:1];
        CCCallBlock* becomeHarmfulBlock = [CCCallBlock actionWithBlock:^{
            [enemyParentRef.CannonBeam setIsHarmful:YES];
        }];
        
        [enemyParentRef.CannonBeam.BeamSprite runAction:[CCSequence actions:
                                                         playLaserSound,
                                                         scaleAction,
                                                         becomeHarmfulBlock, nil]];
    }
    
    //If we are still aligning, but are past the alignment point...
    else if( endRotationDegrees == rotationTarget and
            //Here is that weird check
            ( rotationTarget - (-enemyParentRef.rotation) ) * -direction < 0 )
    {
        //We are not harmful anymore
        enemyParentRef.CannonBeam.IsHarmful = NO;

        //put the laser beam back
        CCScaleTo* scaleAction = [CCScaleTo actionWithDuration:0.15f scaleX:enemyParentRef.CannonBeam.BeamSprite.scaleX scaleY:0];
        [enemyParentRef.CannonBeam.BeamSprite runAction:scaleAction];
        
        //Signal EnemyMechaJuna that the cannon is finished firing
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_MECHA_JUNA_LASER_FIRE_END
                                                            object:nil];
        
        //Go idle
        [enemyParentRef changeEnemyState:[enemyParentRef.AI_StateFactory createMechaJunaCannonState_Idle]];
    }
}

@end
