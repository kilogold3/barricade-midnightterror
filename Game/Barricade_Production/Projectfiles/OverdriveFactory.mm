//
//  OverdriveFactory.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import "OverdriveFactory.h"
#import "Overdrive.h"

//Overdrive Execution Imports
#import "OverdriveExecutionJeromeV1.h"
#import "OverdriveExecutionYunaV1.h"
#import "OverdriveExecutionLeoV1.h"
#import "OverdriveExecutionJonnyV1.h"

@interface OverdriveFactory (Private)
-(Overdrive*) instantiateOverdriveWithDependencies: (NSString*) CocosBuilderAnimationFilenameIn
                                                  : (NSString*) OverdrivePanelIconActiveFilenameIn
                                                  : (NSString*) OverdrivePanelIconInactiveFilenameIn
                                                  : (Class) OverdriveExecutionClassIn;

@end

@implementation OverdriveFactory
-(Overdrive*) instantiateOverdriveWithDependencies: (NSString*) CocosBuilderAnimationFilenameIn
                                                  : (NSString*) OverdrivePanelIconActiveFilenameIn
                                                  : (NSString*) OverdrivePanelIconInactiveFilenameIn
                                                  : (Class) OverdriveExecutionClassIn
{
    //Create an empty shell for overdrive execution
    OverdriveExecutionObject* overdriveExecution = nil;
    
    //If OverdriveExecutionClassIn is a valid overdrive execution...
    if(
       OverdriveExecutionClassIn != nil &&
       [OverdriveExecutionClassIn isSubclassOfClass:[CCNode class]] &&
       [OverdriveExecutionClassIn conformsToProtocol:@protocol(OverdriveExecutionProtocol)])
    {
        //Instantiate the overdrive execution
        overdriveExecution = [[[OverdriveExecutionClassIn alloc] init] autorelease];
    }
    
    //Create overdrive empty shell, and fill it in with the necessary data
    Overdrive* overdrive = [[[Overdrive alloc] initWithDependencies: CocosBuilderAnimationFilenameIn
                                                                   : OverdrivePanelIconActiveFilenameIn
                                                                   : OverdrivePanelIconInactiveFilenameIn
                                                                   : overdriveExecution] autorelease];
    return overdrive;
}

-(Overdrive*) createJeromeOverdriveV1_HIT
{
    
    return [self instantiateOverdriveWithDependencies:@"OD_Jerome_Success.ccbi"
                                                     :@"OverdriveSelect/JeromeOverdriveSelect.png"
                                                     :@"OverdriveSelect/JeromeOverdriveNotSelect.png"
                                                     :[OverdriveExecutionJeromeV1 class]];
}

-(Overdrive*) createJeromeOverdriveV1_MISS
{
    return [self instantiateOverdriveWithDependencies:@"OD_Jerome_Fail.ccbi"
                                                     :@"OverdriveSelect/JeromeOverdriveSelect.png"
                                                     :@"OverdriveSelect/JeromeOverdriveNotSelect.png"
                                                     :nil];
}

-(Overdrive*) createYunaOverdriveV1_HIT
{
    return [self instantiateOverdriveWithDependencies:@"OD_Juna_Success.ccbi"
                                                     :@"OverdriveSelect/YunaOverdriveSelect.png"
                                                     :@"OverdriveSelect/YunaOverdriveNotSelect.png"
                                                     :[OverdriveExecutionYunaV1 class]];
}
-(Overdrive*) createYunaOverdriveV1_MISS
{
    return [self instantiateOverdriveWithDependencies:@"OD_Juna_Fail.ccbi"
                                                     :@"OverdriveSelect/YunaOverdriveSelect.png"
                                                     :@"OverdriveSelect/YunaOverdriveNotSelect.png"
                                                     :nil];
}

-(Overdrive*) createJonnyOverdriveV1_HIT
{
    return [self instantiateOverdriveWithDependencies:@"OD_Johnson_Success.ccbi"
                                                     :@"OverdriveSelect/JonnyOverdriveSelect.png"
                                                     :@"OverdriveSelect/JonnyOverdriveNotSelect.png"
                                                     :[OverdriveExecutionJonnyV1 class]];
}
-(Overdrive*) createJonnyOverdriveV1_MISS
{
    return [self instantiateOverdriveWithDependencies:@"OD_Johnson_Fail.ccbi"
                                                     :@"OverdriveSelect/JonnyOverdriveSelect.png"
                                                     :@"OverdriveSelect/JonnyOverdriveNotSelect.png"
                                                     :nil];
}

-(Overdrive*) createLeoOverdriveV1_HIT
{
    return [self instantiateOverdriveWithDependencies:@"OD_Leo_Success.ccbi"
                                                     :@"OverdriveSelect/LeoOverdriveSelect.png"
                                                     :@"OverdriveSelect/LeoOverdriveNotSelect.png"
                                                     :[OverdriveExecutionLeoV1 class]];
}
-(Overdrive*) createLeoOverdriveV1_MISS
{
    return [self instantiateOverdriveWithDependencies:@"OD_Leo_Fail.ccbi"
                                                     :@"OverdriveSelect/LeoOverdriveSelect.png"
                                                     :@"OverdriveSelect/LeoOverdriveNotSelect.png"
                                                     :nil];
}

-(Overdrive*) createJeromeOverdriveAnimationNoExecution
{
    return [self instantiateOverdriveWithDependencies:@"OD_Jerome_Success.ccbi"
                                                     :nil
                                                     :nil
                                                     :nil];
}
-(Overdrive*) createJohnsonOverdriveAnimationNoExecution
{
    return [self instantiateOverdriveWithDependencies:@"OD_Johnson_Success.ccbi"
                                                     :nil
                                                     :nil
                                                     :nil];
}
@end
