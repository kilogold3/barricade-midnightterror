//
//  GameOverSceneTransitioner.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//


#import "kobold2d.h"


/*******************************************************
 This class is a transitional one to handle the heavy load incurred by loading 
 The actual Game Over Scene. Only the red flash and sound playing should be presented
 during this transition while the GameOver-wave text effect loads and the CCBI for it
 is parsed.
 ********************************************************/
@interface GameOverSceneTransitioner : CCScene
{
}

@end
