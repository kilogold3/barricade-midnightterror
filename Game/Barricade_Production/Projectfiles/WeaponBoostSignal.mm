//
//  WeaponBoostSignal.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 9/15/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "WeaponBoostSignal.h"
#import "NotificationCenterEventList.h"

@implementation WeaponBoostSignal

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        REGISTER_EVENT(self, @selector(uiUpdateWeaponBoostSignalLevel_EventHandler:), EventList::UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL)
    }
    
    return self;
}

-(void) cleanup
{
    UNREGISTER_EVENT(self, EventList::UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL)
}

-(void) didLoadFromCCB
{
    [self stopSystem];
}

-(void) uiUpdateWeaponBoostSignalLevel_EventHandler: (NSNotification*) notification
{
    const int weaponBoostLevel = [[notification object] intValue];
    
    if( weaponBoostLevel >= self.tag)
    {
        if( NO == _active )
        {
            [self resetSystem];
        }
    }
    else
    {
        [self stopSystem];
    }
}

@end
