//
//  BarricadeRepairBlueprint.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface BarricadeRepairBlueprint : CCSprite
{
    //Physics body for collision detection
    b2Body* physicsBody;
    
    //Barricade health
    Byte currentBarricadeHealth;
}

-(void) barricadeRepairFragmentRepair_EventHandler: (NSNotification*) notification;

@end
