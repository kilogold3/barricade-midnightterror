/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */
#import "GameplayLayer.h"
#import "Player.h"
#import "Barricade.h"
#import "EnemyField.h"
#import "Aim.h"
#import "DebugDrawLineOfFire.h"
#import "NotificationCenterEventList.h"
#import "GameplayForegroundLayer.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"
#import "GameAudioList.h"
#import "GameSaveDataPropertyList.h"
#import "GameSaveData.h"

#import "EnemyMechaJuna.h"
#import "EnemyWalker.h"

@implementation GameplayLayer
@synthesize GamePlayer=gamePlayer;
@synthesize GameBarricade=gameBarricade;
@synthesize PercussionSound=percussionSound;
@synthesize GameAim=aim;

-(id) init
{
	if ((self = [super init]))
	{
        //Listen to events
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(acquireGameplayLayerEventHandler:)
                                                     name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayEnemyStrike_EventHandler:)
                                                     name:EventList::GAMEPLAY_ENEMY_STRIKE_BARRICADE
                                                   object:nil];
    }
	return self;
}

-(void) onExit
{
    [[CDAudioManager sharedManager] stopBackgroundMusic];
    [percussionSound stop];
}

-(void) dealloc
{
    CCLOG(@"Dealloc: Gameplay Scene");
    [super dealloc];
}

-(void) GameplayEnemyStrike_EventHandler: (NSNotification*) notification
{
    //Grab attack damage
    const float attackDamage = [[notification object] intValue];
    
    //Set the scalar gain value based on barricade max health
    const float BarricadeMaxHealth = 100.0f;
    float newGain = percussionSound.gain + (attackDamage/BarricadeMaxHealth);
    
    //Apply the new gain
    if(newGain > 1.0f){ newGain = 1.0f; }
    if(percussionSound.gain != 1)
    {
        percussionSound.gain = newGain;
    }
}

-(void) didLoadFromCCB
{
    //Debug Layer
    [KKConfig selectKeyPath:@"KKStartupConfig"];
    BOOL debugDrawEnabled = [KKConfig boolForKey:@"EnableLineOfFireDebugDraw"];
    if( YES == debugDrawEnabled )
    {
        DebugDrawLineOfFire* debugDrawLineOfFire = [[[DebugDrawLineOfFire alloc] initWithDependencies:gamePlayer
                                                                                                     :aim] autorelease];
        [self addChild:debugDrawLineOfFire];
        debugDrawLineOfFire.position = ccpNeg([CCDirector sharedDirector].runningScene.position);
    }
    
    
    //////////////////////////////////////
    //Load background music for the level
    ///////////////////////////////////////
    SimpleAudioEngine* const simpleAudioEngine = [SimpleAudioEngine sharedEngine];
    [simpleAudioEngine preloadBackgroundMusic:GAL::BGM_GAMEPLAY_COMBAT];
    percussionSound = [simpleAudioEngine soundSourceForFile: percussionSoundFilename];
    [percussionSound retain];
    percussionSound.looping = YES;
    
    ///////////////////////////////////////////////////////////////////
    //Grab the barricade health to know how loud to set the percussion
    ///////////////////////////////////////////////////////////////////
    SaveObjectPropertyList* plist = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE];
    float variableHealth = ([[plist objectForKey:GSP::BARRICADE_HEALTH] floatValue]/100.0f);
    percussionSound.gain = 1.0f - variableHealth;
    
    //////////////////////////////////////
    //Play level audio
    //////////////////////////////////////
    [simpleAudioEngine playBackgroundMusic:melodySoundFilename loop:YES];
    
    [percussionSound play];
}

-(void) cleanup
{
    [percussionSound release];
    [[SimpleAudioEngine sharedEngine] unloadEffect:percussionSoundFilename];
    
    //Detach event listeners
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_ENEMY_STRIKE_BARRICADE
                                                  object:nil];
    
    [self removeAllChildrenWithCleanup:YES];
    [super cleanup];
}

-(void) acquireGameplayLayerEventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    GameplayLayer** gameplayLayerRef = (GameplayLayer**)[requestorRefPointer pointerValue];
    (*gameplayLayerRef) = self;
}

@end
