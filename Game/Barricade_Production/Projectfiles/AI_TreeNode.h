//
//  AI_TreeNode.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/18/13.
//
//

#import <Foundation/Foundation.h>

@protocol AI_TreeNodeProtocol <NSObject>
-(void) doAction;
-(void) cleanup;
@end

typedef NSObject<AI_TreeNodeProtocol> AI_TreeNode;
