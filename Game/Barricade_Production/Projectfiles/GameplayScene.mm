/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "GameplayScene.h"

//Engine Components
#import "Box2DDebugLayer.h"
#import "GB2ShapeCache.h"
#import "GB2WorldContactListener.h"
#import "GLES-Render.h"
#import "GB2Engine.h"
#import "CCBReader.h"
#import "CocosDenshion.h"

//Layers
#import "TouchControlLayer.h"
#import "GameplayLayer.h"
#import "HUD_Layer.h"
#import "GameplaySceneForeground.h"

//Data
#import "NotificationCenterEventList.h"
#import "GameSaveDataPropertyList.h"
#import "GameSaveData.h"

//Dependencies
#import "DaysLeftTransitionScene.h"
#import "ResourceGatheringScene.h"
#import "DialogueScene.h"
#import "AppWidescreener.h"

@interface GameplayScene (PrivateMethods)
-(void) enableBox2dDebugDrawing;
@end

@implementation GameplayScene

-(id) init
{
	if ((self = [super init]))
	{
		CCLOG(@"%@ init", NSStringFromClass([self class]));
		[self scheduleUpdate];
    }
    
	return self;
}

-(void) onExit
{
    [super onExit];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_FINAL_ENEMY_DEFEATED
                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER
                                                  object:nil];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"leoSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"leoArms.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"HUDPanel.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"barricadeSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"enemyWalkerSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"vejiganteSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"proximityMine.plist"];
}

-(void) onEnterTransitionDidFinish
{
    /**************************************************************************
     We want to make sure the previous scene is deallocated before allocating 
     this one. That is because scene transitions are executed in the following 
     order:
     1. scene: OtherScene
     
     2. init: <OtherScene = 066B2130 | Tag = -1>
     
     3. onEnter: <OtherScene = 066B2130 | Tag = -1>
     ￼
     4. [[[[Transition is running here]]]
     
     5. onExit: <FirstScene = 0668DF40 | Tag = -1>
     
     6. onEnterTransitionDidFinish: <OtherScene = 066B2130 | Tag = -1> 
     
     7. dealloc: <FirstScene = 0668DF40 | Tag = -1>
     
     Note that the FirstScene dealloc method is called last. This means that during 
     onEnterTransitionDidFinish, the previous scene is still in memory. If you want 
     to allocate memory-intensive nodes at this point, you’ll have to schedule a 
     selector to wait at least one frame before doing the memory allocations, to be 
     certain that the previous scene’s memory is released. You can schedule a method 
     that is guaranteed to be called the next frame by omitting the interval parameter 
     when scheduling the method:
     
     [self schedule:@selector(waitOneFrame:)];
     
     The method that will be called then needs to unschedule itself by calling unschedule
     with the hidden_cmd parameter:
     
     -(void) waitOneFrame:(ccTime)delta 
     {
        [self unschedule:_cmd];
        // delayed code goes here ... 
     }
     **************************************************************************/
    [self scheduleOnce:@selector(loadSceneAfterOneFrame:)
                 delay:0];
    
    [super onEnterTransitionDidFinish];
    
    //Offsetting the scene must occur before any objects are added.
    //This is because objects that do not want to be offset (like box2d debug draw layer)
    //will be added with the scene's negated position in order to counteract the offset,
    //thus leaving the object at the origin.
    //
    //An object negating the position would have the same x/y-components but in negative value.
    //To offset the screen, we do it via the AppWidescreener
    //[AppWidescreener applyLetterboxToScene:self];
    
    //Load all sprite frames
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"leoSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"leoArms.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"HUDPanel.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"barricadeSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"enemyWalkerSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"vejiganteSheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"proximityMine.plist"];
    
    //Load all physics shapes
    [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"shapes.plist"];
    
    // Construct a world object, which will hold and simulate the rigid bodies.
    world = [GB2Engine sharedInstance].world;
    
    //Enable debug drawing
    NSAssert( [KKConfig selectKeyPath:@"KKStartupConfig"], @"Missing config.lua table" );
    
    if( [KKConfig selectKeyPath:@"KKStartupConfig"] )
    {
        NSLog(@"Found config.lua keypath");
    }
    else
    {
        NSLog(@"Did [NOT] find config.lua keypath");
    }
    
    BOOL debugDrawEnabled = [KKConfig boolForKey:@"EnableCollisionDebugDraw"];
    if( YES == debugDrawEnabled )
    {
        NSLog(@"Enabling Debug Draw");
        [self enableBox2dDebugDrawing];
    }
}

-(void)loadSceneAfterOneFrame: (ccTime) delta
{
    //Before anything, let's write out our save file from here.
    [[GameSaveData sharedGameSaveData] WriteToFile];
    
    //////////////////////////////////////
    //Register Event Listeners
    //////////////////////////////////////
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GameplayFinalEnemyDefeated_EventHandler:)
                                                 name:EventList::GAMEPLAY_FINAL_ENEMY_DEFEATED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(acquireGameplaySceneScreenForegroundLayer_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER
                                               object:nil];
    
    //////////////////////////////////
    //Load all the gameplay layers (order matters)
    //////////////////////////////////
    SaveObjectPropertyList* resourceGatheringPropertyList = [[GameSaveData sharedGameSaveData] getSaveObjectProperties: GSC::GAME_PROGRESS];
    const NSNumber* const currentProgression = [resourceGatheringPropertyList objectForKey:GSP::CURRENT_PROGRESSION];
    NSString* const gameplayLayerToLoad = [NSString stringWithFormat:@"GP_%02dA%@.ccbi",
                                           currentProgression.intValue,
                                           [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    CCLOG(@"Loading Gamplay CCBI: %@", gameplayLayerToLoad );
    
    screenForegroundLayer = [GameplaySceneForeground node];
    gameplayLayer = static_cast<GameplayLayer*>([CCBReader nodeGraphFromFile:gameplayLayerToLoad]);
    hudLayer = [HUD_Layer node];
    
    NSString* const touchControlLayerToLoad = [NSString stringWithFormat:@"GameplayTouchControlLayer%@.ccbi",
                                               [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    touchControlsLayer = static_cast<TouchControlLayer*>([CCBReader nodeGraphFromFile:touchControlLayerToLoad]);

    [self addChild:gameplayLayer];
    [self addChild:hudLayer];
    [self addChild:touchControlsLayer];
    [self addChild:screenForegroundLayer];
        
    //////////////////////////////////////
    //Post-Layer-Creation
    //(Lock arms and aim into place)
    //////////////////////////////////////
    const Aim* aimRef = static_cast<GameplayLayer*>(gameplayLayer).GameAim;
    SEND_EVENT(EventList::PLAYER_NEW_AIM_DESTINATION, aimRef);
    
    //For optimization purposes, let's clean up any textures we are not using anymore at this point.
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}


-(void) enableBox2dDebugDrawing
{
	// Using John Wordsworth's Box2DDebugLayer class now
	// The advantage is that it draws the debug information over the normal cocos2d graphics,
	// so you'll still see the textures of each object.
    
	UInt32 debugDrawFlags = 0;
	debugDrawFlags += b2Draw::e_shapeBit;
	debugDrawFlags += b2Draw::e_jointBit;
	//debugDrawFlags += b2Draw::e_aabbBit;
	//debugDrawFlags += b2Draw::e_pairBit;
	//debugDrawFlags += b2Draw::e_centerOfMassBit;
    
    Box2DDebugLayer* debugLayer = [Box2DDebugLayer debugLayerWithWorld:world
                                                            ptmRatio:PTM_RATIO
																	 flags:debugDrawFlags];
    //Reposition to negate the scene position offset
    debugLayer.position = ccp(-self.position.x,-self.position.y);
    [self addChild:debugLayer z:100];
}

-(void) update:(ccTime)delta
{
    //Tick the level timer to count down the time remaining
    //until the level is over.
    [levelTimer tick:delta];
}

-(void) GameplayFinalEnemyDefeated_EventHandler: (NSNotification*) notification
{
    CCDelayTime* delayAction = [CCDelayTime actionWithDuration:4.0f];
    CCCallFunc* endLevelFunctionCallDelegate = [CCCallFunc actionWithTarget:self
                                                                   selector:@selector(EndGameplayLevel)];
    CCSequence* actionSequence = [CCSequence actionOne:delayAction two:endLevelFunctionCallDelegate];
    [self runAction:actionSequence];
}

-(void) EndGameplayLevel
{
    //Let's save out any data here
    SEND_EVENT(EventList::DATA_SAVE_BARRICADE_HEALTH, nil);
    SEND_EVENT(EventList::DATA_SAVE_PLAYER, nil);
    
    
    SaveObjectPropertyList* gameProgressProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    CCTransitionFade* fadeTransition = nil;

    //Now, before we go to the ResourceGathering, we will need to know if we have reached that
    //point in the story where the team finds out about the incoming nuclear strike.
    //If we are at or beyond progression 01A, we should transition with "Days Remaining" effect.
    if( [[gameProgressProperties valueForKey:GSP::CURRENT_PROGRESSION] intValue] >= 1 )
    {
        //Get all the info/data required to operate.
        //+1 on currentOverallDay because we finished this day at this point.
        const int currentOverallDay = [[gameProgressProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue] + 1;
        [gameProgressProperties setValue:[NSNumber numberWithInt:currentOverallDay]
                                  forKey:GSP::CURRENT_OVERALL_DAY];
        [KKConfig selectKeyPath:@"GameSettings"];
        const int oleanderCityDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
        
        //If we are past the days limit, we show the bad ending.
        if( currentOverallDay > oleanderCityDaysLimit )
        {
            fadeTransition = [CCTransitionFade transitionWithDuration:0.8f scene:[DialogueScene node]];
        }
        //Otherwise, just transition into the ResourceGatheringScene with the "Days Remaining" effect.
        else
        {
            //Setup transitioning effect
            CCScene* nextScene = [CCScene node];
            CCNode* daysLeftScene = [CCBReader nodeGraphFromFile:@"DaysLeftTransitionScene.ccbi"];
            static_cast<DaysLeftTransitionScene*>(daysLeftScene).TransitioningScene = [ResourceGatheringScene class];
            
            [nextScene addChild:daysLeftScene];
            
            fadeTransition = [CCTransitionFade transitionWithDuration:0.8f scene: nextScene];
        }
    }
    //Otherwise, we are in the beginning at the Manatee Research Vessel still, so we have no
    //notion of the nuclear strike. Let's transition without the "Days Remaining" effect.
    else
    {
        fadeTransition = [CCTransitionFade transitionWithDuration:0.8f scene: [ResourceGatheringScene node]];
    }
    
    [[CCDirector sharedDirector] replaceScene:fadeTransition];
}


-(void) acquireGameplaySceneScreenForegroundLayer_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    CCLayer** foregroundScreenLayerRef = (CCLayer**)[requestorRefPointer pointerValue];
    (*foregroundScreenLayerRef) = screenForegroundLayer;
}

-(void) cleanup
{
    [self removeAllChildrenWithCleanup:YES];
    [super cleanup];
}

@end
