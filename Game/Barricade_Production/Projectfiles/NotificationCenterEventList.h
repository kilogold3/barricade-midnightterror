//
//  NotificationCenterEventList.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Barricade_Production_NotificationCenterEventList_h
#define Barricade_Production_NotificationCenterEventList_h


#define REGISTER_EVENT(observer, selectorDelegate, eventName) (     \
[[NSNotificationCenter defaultCenter] addObserver:observer          \
                                         selector:selectorDelegate  \
                                             name:eventName         \
                                           object:nil]              );

#define UNREGISTER_EVENT(observer, eventName) (\
[[NSNotificationCenter defaultCenter] removeObserver:observer   \
                                                name:eventName  \
                                              object:nil]       );

#define SEND_EVENT(eventName, eventArgs) (\
[[NSNotificationCenter defaultCenter] postNotificationName: eventName  \
                                                    object: eventArgs] );

namespace EventList
{
    static NSString* const PLAYER_NEW_MOVEMENT_DESTINATION = @"PLAYER_NEW_MOVEMENT_DESTINATION";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Updates the position where the player is aiming at. 
     *
     * OBSERVERS :
     *  Aim - Moves reticle aim to a new location specified by the event args. Player will then aim in that new
     *  PlayerState_Alive - Rotates arms to align with the direction of the aim.
     *  WeaponBase - Updates the line of fire to align with the direction of the aim.
     *
     * SENT FROM :
     *  Aim
     *  GameplayScene
     *
     * EVENT ARGS :
     *  Aim - Reference to the Aim instance. From here we can get position data, and use the ref for coods conversions.
     *
     *********************************************************************************************************************************************/
    static NSString* const PLAYER_NEW_AIM_DESTINATION = @"PLAYER_NEW_AIM_DESTINATION";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Generates a new position for the Aim based on touch input provided
     *
     * OBSERVERS :
     *  Aim - Calculates new position based on aim mode, then sends it via PLAYER_NEW_AIM_DESTINATION event.
     *
     * SENT FROM :
     *  TouchControlLayer
     *
     * EVENT ARGS :
     *  KKTouch - Touch involved with the moving of the Aim.
     *
     *********************************************************************************************************************************************/
    static NSString* const CONTROLS_NEW_AIM_DESTINATION = @"CONTROLS_NEW_AIM_DESTINATION";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Pauses the game and brings up the Pause Menu
     *
     * OBSERVERS :
     *  TouchControlLayer - Disables all controls on the HUD, as well as aiming and player movement
     *  GameplayScenePauseLayer - Loads the CocosBuilder pause menu
     *  EnemyField - Stops spawning enemies
     *  EnemyBase - Stops updating self to freeze the enemy
     *  GameplayForegroundLayer - Stops the sunrise animation
     *  Player - Disables player update
     *  WeaponBase - Releases the trigger from the weapon
     *
     * SENT FROM :
     *  TouchControlLayer
     *
     * EVENT ARGS :
     *  N/A
     *
     *********************************************************************************************************************************************/
    static NSString* const CONTROLS_PAUSEGAME_BUTTON_PRESSED = @"CONTROLS_PAUSEGAME_BUTTON_PRESSED";
    static NSString* const CONTROLS_PAUSEGAME_BUTTON_RELEASED = @"CONTROLS_PAUSEGAME_BUTTON_RELEASED";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Resumes the game and removes the pause menu
     *
     * OBSERVERS :
     *  TouchControlLayer - Re-enables all controls
     *  EnemyField - Resumes spawning enemies
     *  EnemyBase - Resumes updating self
     *  GameplayForegroundLayer - Resumes the sunrise animation
     *  Player - Resumes player update
     *
     * SENT FROM :
     *  GameplayScenePauseLayer
     *
     * EVENT ARGS :
     *  N/A
     *
     *********************************************************************************************************************************************/
    static NSString* const CONTROLS_RESUMEGAME_BUTTON_PRESSED = @"CONTROLS_RESUMEGAME_BUTTON_PRESSED";
    static NSString* const CONTROLS_FIRE_BUTTON_PRESSED = @"CONTROLS_FIRE_BUTTON_PRESSED";
    static NSString* const CONTROLS_FIRE_BUTTON_RELEASED = @"CONTROLS_FIRE_BUTTON_RELEASED";
    static NSString* const CONTROLS_SWAP_BUTTON_PRESSED = @"CONTROLS_SWAP_BUTTON_PRESSED";
    static NSString* const CONTROLS_SWAP_BUTTON_RELEASED = @"CONTROLS_SWAP_BUTTON_RELEASED";
    static NSString* const CONTROLS_OVERDRIVE_BUTTON_PRESSED = @"CONTROLS_OVERDRIVE_BUTTON_PRESSED";
    static NSString* const CONTROLS_OVERDRIVE_BUTTON_RELEASED = @"CONTROLS_OVERDRIVE_BUTTON_RELEASED";
    static NSString* const CONTROLS_WEAPON_SELECTED = @"CONTROLS_WEAPON_SELECTED";
    static NSString* const CONTROLS_RESTRICT_CONTROLS = @"CONTROLS_RESTRICT_CONTROLS";

    static NSString* const GAMEPLAY_WEAPON_OUT_OF_AMMO = @"GAMEPLAY_WEAPON_OUT_OF_AMMO";
    static NSString* const GAMEPLAY_LEVEL_TIMER_FINISH = @"GAMEPLAY_LEVEL_TIMER_FINISH";
    static NSString* const GAMEPLAY_SET_OVERDRIVE = @"GAMEPLAY_SET_OVERDRIVE";
    static NSString* const GAMEPLAY_BARRICADE_DESTROYED = @"GAMEPLAY_BARRICADE_DESTROYED";
    static NSString* const GAMEPLAY_BARRICADE_HEALTH_CHANGED = @"GAMEPLAY_BARRICADE_HEALTH_CHANGED";
    static NSString* const GAMEPLAY_FINAL_ENEMY_DEFEATED = @"GAMEPLAY_FINAL_ENEMY_DEFEATED";
    static NSString* const GAMEPLAY_MECHA_JUNA_LASER_FIRE_END = @"GAMEPLAY_MECHA_JUNA_LASER_FIRE_END";
    static NSString* const GAMEPLAY_MECHA_JUNA_DEFEATED = @"GAMEPLAY_MECHA_JUNA_DEFEATED";
    static NSString* const GAMEPLAY_MECHA_JUNA_ARM_DESTROYED = @"GAMEPLAY_MECHA_JUNA_ARM_DESTROYED";
    static NSString* const GAMEPLAY_PLAYER_MOVED = @"GAMEPLAY_PLAYER_MOVED";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Notifies the HUD about an ammo count change and reflects it
     *  on the weapon data display section
     *
     * OBSERVERS :
     *  HUD_DataDisplayStatsAmmo - updates internal label according to the weapon value
     *
     * SENT FROM :
     *  WeaponBase
     *
     * EVENT ARGS :
     *  WeaponBase - The weapon whose ammo is currently changing
     *
     *********************************************************************************************************************************************/
    static NSString* const GAMEPLAY_WEAPON_AMMO_CHANGED = @"GAMEPLAY_WEAPON_AMMO_CHANGED";

    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Calls WeaponBase's event handler, which executes the "fireWeapon" method, passing in
     *  the Player's origin. PlayerShadowClone will use the provided event argument (which 
     *  is a WeaponBase reference) to also call the "fireWeapon" method, passing it's own origin.
     *
     * OBSERVERS :
     *  PlayerShadowClone - Calls fire on the provided weapon
     *
     * SENT FROM :
     *  
     *
     * EVENT ARGS :
     *  
     *
     *********************************************************************************************************************************************/
    static NSString* const GAMEPLAY_WEAPON_FIRED = @"GAMEPLAY_WEAPON_FIRED";

    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Notify all observers that a specific enemy is now dead. The enemy instance is passed
     *  via event arguments
     *
     * OBSERVERS :
     *  Player - Modifies accumulated overdrive according to the enemy's OverdriveReward
     *
     * SENT FROM :
     *  EnemyWalker
     *
     * EVENT ARGS :
     *  EnemyBase - The now-dead enemy
     *
     *********************************************************************************************************************************************/
    static NSString* const GAMEPLAY_ENEMY_DEAD = @"GAMEPLAY_ENEMY_DEAD";
    
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Pauses gameplay logic. Stops all game world obbejcts. Deactivates controls.
     *
     * OBSERVERS :
     *  
     *
     * SENT FROM :
     * 
     *
     * EVENT ARGS :
     *  
     *
     *********************************************************************************************************************************************/
    static NSString* const GAMEPLAY_PAUSE = @"GAMEPLAY_PAUSE";
    static NSString* const GAMEPLAY_RESUME = @"GAMEPLAY_RESUME";
    
    static NSString* const GAMEPLAY_ENEMY_STRIKE_BARRICADE = @"GAMEPLAY_ENEMY_STRIKE_BARRICADE";
    
    static NSString* const COMPONENT_ACQUIRE_AIM_TARGET = @"COMPONENT_ACQUIRE_AIM_TARGET";
    static NSString* const COMPONENT_ACQUIRE_PLAYER_TARGET = @"COMPONENT_ACQUIRE_PLAYER_TARGET";
    static NSString* const COMPONENT_ACQUIRE_PLAYER_ARMS_TARGET = @"COMPONENT_ACQUIRE_PLAYER_ARMS_TARGET";
    static NSString* const COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON = @"COMPONENT_ACQUIRE_PLAYER_CURRENT_WEAPON";
    static NSString* const COMPONENT_ACQUIRE_BARRICADE_TARGET = @"COMPONENT_ACQUIRE_BARRICADE_TARGET";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER = @"COMPONENT_ACQUIRE_GAMEPLAYSCENE_GAMEPLAYLAYER";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER = @"COMPONENT_ACQUIRE_GAMEPLAYSCENE_TOUCHCONTROLLAYER";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER = @"COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND = @"COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYLAYER_FOREGROUND = @"COMPONENT_ACQUIRE_GAMEPLAYLAYER_FOREGROUND";
    static NSString* const COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD = @"COMPONENT_ACQUIRE_GAMEPLAYLAYER_ENEMYFIELD";
    static NSString* const COMPONENT_ACQUIRE_RESOURCE_GATHERING_CURRENT_ROOM = @"COMPONENT_ACQUIRE_RESOURCE_GATHERING_CURRENT_ROOM";
    static NSString* const COMPONENT_ACQUIRE_RESOURCE_GATHERING_MINIMAP_BUTTON = @"COMPONENT_ACQUIRE_RESOURCE_GATHERING_MINIMAP_BUTTON";
    static NSString* const COMPONENT_ACQUIRE_GAME_PROGRESS = @"COMPONENT_ACQUIRE_GAME_PROGRESS";
    static NSString* const COMPONENT_ACQUIRE_RESOURCE_GATHERING_SEARCHTIMERCLOCK = @"COMPONENT_ACQUIRE_RESOURCE_GATHERING_SEARCHTIMERCLOCK";
    
    static NSString* const DATA_SAVE_PLAYER = @"DATA_SAVE_PLAYER";
    static NSString* const DATA_LOAD_PLAYER = @"DATA_LOAD_PLAYER";
    static NSString* const DATA_SAVE_BARRICADE_HEALTH = @"DATA_SAVE_BARRICADE_HEALTH";
    static NSString* const DATA_LOAD_BARRICADE_HEALTH = @"DATA_LOAD_BARRICADE_HEALTH";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Stops all enemy and player behavior. Executes the cocos builder animation
     *  on the gameplay forground layer.
     *  
     * OBSERVERS :
     *  EnemyField - Stops spawning enemies
     *  EnemyBase - Stops updating self to freeze the enemy
     *  GameplayForegroundLayer - Loads the cocosbuilder file and adds the CCNode to the layer
     *  TouchControlLayer - Disables player and aim controls
     *  Player - Disables player update
     *  
     * SENT FROM :
     *  TouchControlLayer
     *  
     * EVENT ARGS :
     *  Overdrive - reference to the overdrive that will be animated
     *
     *********************************************************************************************************************************************/
    static NSString* const OVERDRIVE_START_ANIMATION = @"OVERDRIVE_START_ANIMATION";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  If the overdrive is a MISS, resume game world simulation.
     *
     * OBSERVERS :
     *  EnemyField - If overdrive HIT, resume spawning enemies
     *  EnemyBase - If overdrive HIT, resumes updating self
     *  TouchControlLayer - If overdrive HIT, resumes player and aim controls
     *  Player - If overdrive HIT, resumes player update
     *
     * SENT FROM :
     *  OverdriveSequence
     *
     * EVENT ARGS :
     *  BOOL - YES if overdrive HIT, otherwise NO
     *
     *********************************************************************************************************************************************/
    static NSString* const OVERDRIVE_FINISH_ANIMATION = @"OVERDRIVE_FINISH_ANIMATION";
    static NSString* const OVERDRIVE_FINISH_EXECUTION = @"OVERDRIVE_FINISH_EXECUTION";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Kills all enemies on screen
     *
     * OBSERVERS :
     *  EnemyBase - kills enemy
     *
     * SENT FROM :
     *  OverdriveExecutionJeromeV1
     *
     * EVENT ARGS :
     *  NSMutableArray - List of all the targeted enemies set to die.
     *
     *********************************************************************************************************************************************/
    static NSString* const OVERDRIVE_JEROME_DAMAGE_ENEMIES = @"OVERDRIVE_JEROME_DAMAGE_ENEMIES";
    static NSString* const OVERDRIVE_YUNA_CLEAR_BARRICADE_AREA = @"OVERDRIVE_YUNA_CLEAR_BARRICADE_AREA";
    static NSString* const OVERDRIVE_YUNA_KILL_ENEMIES_IN_BARRICADE_AREA = @"OVERDRIVE_YUNA_KILL_ENEMIES_IN_BARRICADE_AREA";
    static NSString* const OVERDRIVE_YUNA_REINFORCE_BARRICADE_START = @"OVERDRIVE_YUNA_REINFORCE_BARRICADE_START";
    static NSString* const OVERDRIVE_YUNA_REINFORCE_BARRICADE_END = @"OVERDRIVE_YUNA_REINFORCE_BARRICADE_END";
    static NSString* const OVERDRIVE_JOHNSON_START_AIM_PHASE = @"OVERDRIVE_JOHNSON_START_AIM_PHASE";
    static NSString* const OVERDRIVE_JOHNSON_END_AIM_PHASE = @"OVERDRIVE_JOHNSON_END_AIM_PHASE";
    static NSString* const OVERDRIVE_JEROME_START_AIM_PHASE = @"OVERDRIVE_JEROME_START_AIM_PHASE";
    static NSString* const OVERDRIVE_JEROME_END_AIM_PHASE = @"OVERDRIVE_JEROME_END_AIM_PHASE";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Sets the UI's overdrive bar to the value passed in via event args
     *
     * OBSERVERS :
     *  OverdriveStatusBar - Repositions the bar according to Player's CurrentOverdrive
     *
     * SENT FROM :
     *  Player
     *
     * EVENT ARGS :
     *  NSNumber - Overdrive percentage
     *
     *********************************************************************************************************************************************/
    static NSString* const UI_UPDATE_OVERDRIVE_STATUS_BAR = @"UI_UPDATE_OVERDRIVE_STATUS_BAR";
    static NSString* const UI_UPDATE_WEAPONBOOST_STATUS_BAR = @"UI_UPDATE_WEAPONBOOST_STATUS_BAR";
    static NSString* const UI_UPDATE_WEAPONBOOST_COOLDOWN_BAR = @"UI_UPDATE_WEAPONBOOST_COOLDOWN_BAR";
    static NSString* const UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL = @"UI_UPDATE_WEAPONBOOST_SIGNAL_LEVEL";

    static NSString* const UI_PRESENT_QUIT_PROMPT = @"UI_PRESENT_QUIT_PROMPT";
    static NSString* const UI_DISMISS_QUIT_PROMPT = @"UI_DISMISS_QUIT_PROMPT";

    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Repairs the Barricade ball. The fragment is removed from screen, and the ball now shows the fragment
     *  visible on the ball itself.
     *
     * OBSERVERS :
     *  BarricadeRepairBlueprint - Receives the id of the fragment to enable
     *
     * SENT FROM :
     *  BarricadeRepairFragment
     *
     *********************************************************************************************************************************************/
    static NSString* const BARRICADE_REPAIR_FRAGMENT_REPAIR = @"BARRICADE_REPAIR_FRAGMENT_REPAIR";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Notifies the system that the barricade ball has been loaded. Also includes a list of CCNode tags
     *  identifying the fragment used on the ball. The fragments used on the ball will be removed from the
     *  usable scraps on the table.
     *
     * OBSERVERS :
     *  BarricadeRepairLayer - Receives a list of the fragment ID's that are in use, so it can deactivate
     *                         it from the ones available on the table scraps.
     *
     * SENT FROM :
     *  BarricadeRepairBlueprint
     *
     *********************************************************************************************************************************************/
    static NSString* const BARRICADE_REPAIR_BLUEPRINT_LOADED = @"BARRICADE_REPAIR_BLUEPRINT_LOADED";
    
    
    
    
    
    
    
    static NSString* const BARRICADE_REPAIR_FULLY_REPAIRED = @"BARRICADE_REPAIR_FULLY_REPAIRED";
    
    


    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Notifies the system that we are entering a door in the resource gathering screen. The game will replace the current
     *  room layer with the new one whose filename is being passed via EventArgs 
     *
     * OBSERVERS :
     *  ResourceGatheringScene - Replaces the current room layer with the new one
     *
     * SENT FROM :
     *  Door
     *
     * EVENT ARGS :
     *  NSString* - CocosBuilder filename for the new room to be loaded. 
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_TRAVERSE_DOOR = @"RESOURCE_GATHERING_TRAVERSE_DOOR";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Launches an activity. The specified activity is passed in by event args
     *
     * OBSERVERS :
     *  ResourceGatheringScene - Adds the activity on top of the current room layer
     *  ResourceGatheringInteractiveObject - Deactivates touch input
     *
     * SENT FROM :
     *  ActivityLauncher
     *
     * EVENT ARGS :
     *  ActivityLauncher - The interactive object responsible for launching the activity
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_LAUNCH_ACTIVITY = @"RESOURCE_GATHERING_LAUNCH_ACTIVITY";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Suspends a currently running activity.
     *
     * OBSERVERS :
     *  ResourceGatheringScene - Removes the activity from on top of the current room layer
     *  ResourceGatheringInteractiveObject - Reactivates touch input
     *
     * SENT FROM :
     *  BarricadeRepairLayer
     *
     * EVENT ARGS :
     *  N/A
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_SUSPEND_ACTIVITY = @"RESOURCE_GATHERING_SUSPEND_ACTIVITY";
    
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Adds the acquired pickup to the list of 'used' InteractiveObjects so that it doesn't
     *  reappear next time we enter the room.
     *
     * OBSERVERS :
     *  ResourceGatheringScene - adds object ID to list of 'used' InteractiveObjects
     *
     * SENT FROM :
     *  PickUp
     *
     * EVENT ARGS :
     *  CCNode* - pickup instance
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_PICKUP_ACQUIRED = @"RESOURCE_GATHERING_PICKUP_ACQUIRED";

    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *
     *
     * OBSERVERS :
     *
     *
     * SENT FROM :
     *
     *
     * EVENT ARGS :
     *  
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_DOOR_UNLOCKED = @"RESOURCE_GATHERING_DOOR_UNLOCKED";
    
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Disables all controls from the TravelLayer to allow for TravelPrompt interaction to not 
     *  leak into the TravelLayer
     *
     * OBSERVERS :
     *  TravelLayer - Disables touches TravelNodeButton menu
     *
     * SENT FROM :
     *  TravelLayer
     *
     * EVENT ARGS :
     *  N/A
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_TRAVEL_PROMPT_ENABLED = @"RESOURCE_GATHERING_TRAVEL_PROMPT_ENABLED";
    static NSString* const RESOURCE_GATHERING_TRAVEL_PROMPT_DISABLED = @"RESOURCE_GATHERING_TRAVEL_PROMPT_DISABLED";
    
    /********************************************************************************************************************************************
     * EXPECTED BEHAVIOR :
     *  Sets the enabled/disabled state of the timer. When disabled, the timer will disappear from the screen.
     *
     * OBSERVERS :
     *  SearchTimerClock - Pauses/resumes the update, and enables/disables the sprite.
     *
     * SENT FROM :
     *  
     *
     * EVENT ARGS :
     *  BOOL - YES if enabled, otherwise, NO.
     *
     *********************************************************************************************************************************************/
    static NSString* const RESOURCE_GATHERING_TIMER_CLOCK_ENABLE = @"RESOURCE_GATHERING_TIMER_CLOCK_ENABLE";

    static NSString* const RESOURCE_GATHERING_FREEZE_RUNNING_ACTIVITIES = @"RESOURCE_GATHERING_FREEZE_RUNNING_ACTIVITIES";

    
    //DEBUG EVENTS
    static NSString* const DEBUG_SPAWN_ENEMY = @"DEBUG_SPAWN_ENEMY";
    static NSString* const DEBUG_PRINT_ENEMY_HEALTH = @"DEBUG_PRINT_ENEMY_HEALTH";

    
 }


#endif
