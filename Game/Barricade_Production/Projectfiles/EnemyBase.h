//
//  EnemyBase.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "EnemyFieldInstance.h"
#import "IEnemyState.h"

class b2Body;
@class WeaponBase;
@class EnemyStateFactory;
@class Player;
@class Barricade;
@class GB2Contact;
@class TrapBase;

@interface EnemyBase : EnemyFieldInstance 
{
    //Current enemy state for AI behavior
    EnemyState* currentState;
    
    //We have this to be able to delay the transition of a state
    //until after the current state has finished updating. Quirky things will
    //happen if you change state during an updating state.
    EnemyState* nextState;
    
    EnemyStateFactory* enemyStateFactory;
    CCSprite* enemySprite;
    b2Body* enemyPhysicsBody;
    Player* playerRef;
    Barricade* barricadeRef;
    
    float movementSpeed;
    short currentHealth;
    short maxHealth;
    short attackDamage;
    short overdriveReward; //How much overdrive you get for killing the enemy.
    short weaponBoostReward;
    
    //Collision counters
    short contactWithPlayerCounter;
    
    //Health display
    BOOL displayHealth; //regulator variable to determine whether it is time to display the health.
    BOOL allowHealthDisplay; //whether we want the enemy to display health at all
    Byte targetedCount; //count of how many lines-of-fire are targeting the enemy
}
@property (nonatomic, assign) CCSprite* EnemySprite;
@property (nonatomic, assign) b2Body* EnemyPhysicsBody;
@property (readonly) EnemyStateFactory* AI_StateFactory;
@property (nonatomic, readwrite) short CurrentHealth;
@property (nonatomic, readwrite) Byte TargetedCount;
@property (readonly) short MaxHealth;
@property (readonly) short AttackDamage;
@property (readonly) short OverdriveReward;
@property (readonly) short WeaponBoostReward;
@property (readonly) Player* PlayerRef;
@property (readonly) Barricade* BarricadeRef;
@property (nonatomic, readwrite) BOOL AllowHealthDisplay;
@property (nonatomic, readonly) EnemyState* CurrentEnemyState;
@property (nonatomic, readonly) EnemyState* NextEnemyState;
@property (nonatomic, readwrite) float MovementSpeed;
@property (nonatomic, readwrite) BOOL DisplayHealthMasterOverride;

-(void) changeEnemyState: (EnemyState*) newState;
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;
-(void) reactToTrapDamage: (TrapBase*) detonatingTrap;
-(void) instantChangeEnemyState: (EnemyState*) newState;
-(void) loadBasicAttributes: (NSString*) enemySettingsTableName;

/*********************************************************************************
 * DESCRIPTION :
 *  Kills the enemy instantly by switching it's state to the dead state.
 *  This method is meant to be overridden by a child class. EnemyBase will not
 *  be executing this method since it does not know which death AI state to 
 *  transition to.
 *
 * INPUTS :
 *  N/A
 ********************************************************************************/
 -(void) killEnemy;


//Collisions
-(void) beginContactWithPlayer: (GB2Contact*)contact;
-(void) endContactWithPlayer: (GB2Contact*)contact;


//Event Handlers
-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification;
-(void) OverdriveJeromeDamageEnemies_EventHandler: (NSNotification*) notification;
-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) DebugPrintEnemyHealth_EventHandler: (NSNotification*) notification;



@end
