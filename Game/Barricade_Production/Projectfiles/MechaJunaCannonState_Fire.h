//
//  MechaJunaState_AttackWithRightArm.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"
@class EnemyMechaJunaCannon;

@interface MechaJunaCannonState_Fire : NSObject<IEnemyState>
{
    EnemyMechaJunaCannon* enemyParentRef;
    
    float startRotationDegrees;
    float endRotationDegrees;
    
    //Is the aim aligned to where it will begin to fire from.
    BOOL isAlignedToStartPosition;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) enter;
-(void) exit;
-(void) update: (float) deltaTime;

@end