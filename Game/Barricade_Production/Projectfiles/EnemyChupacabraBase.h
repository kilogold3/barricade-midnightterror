//
//  EnemyWalker.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnemyBase.h"
@class GB2Contact;
@class EnemyStateFactory;

@interface EnemyChupacabraBase : EnemyBase
{
    short contactWithBarricadeSegmentCounter;
    BOOL isKilledbyYunaOverdrive;
}

//Collision
-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact;
-(void) endContactWithBarricadeSegment: (GB2Contact*)contact;

-(void) OverdriveYunaKillEnemiesInBarricadeArea_EventHandler: (NSNotification*) notification;
-(void) OverdriveYunaClearBarricadeArea_EventHandler: (NSNotification*) notification;

@end
