//
//  OpenItemTapObject.mm
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OpenItemTapObject.h"
#import "NotificationCenterEventList.h"
#import "ActivityBase.h"
#import "LockableItem.h"
#import "SimpleAudioEngine.h"

@implementation OpenItemTapObject
@synthesize CurrentTapCount=currentTapCount;

- (id)initWithSpriteFrameName:(NSString *)spriteFrameName
{
    self = [super initWithSpriteFrameName:spriteFrameName];
    if (self != nil)
    {
        //Set the tap amount to an uninitialized value.
        currentTapCount = UNINITIALIZED_TAP_COUNT;
        
        //START HACK:
        //This is for not having high resolution art for containers
        if( [spriteFrameName isEqualToString:@"Tapping_Door.png"] or
           [spriteFrameName isEqualToString:@"Tapping_Hatch.png"] or
           [spriteFrameName isEqualToString:@"TapDoor0.png"] or
           [spriteFrameName isEqualToString:@"TapDoor1.png"] or
           [spriteFrameName isEqualToString:@"TapDoor2.png"] )
        {
            originalScale = 1.0f;
            tappedScale = 0.7f;
        }
        else
        {
            originalScale = 3.5f;
            tappedScale = originalScale - 0.3f;
        }
        
        [self setScale:originalScale];
        //END HACK
        
        
        //We want to schedule the update at least a little bit
        //after the screen is actually showing you what you're
        //going to be clicking. Otherwise, the activity will close
        //practically at the same frame it's loaded.
        //NOTE:
        //We should probably go with a more fool-proof plan here...
        //If we get a drop in the framerate, this will get screwey.
        //Maybe disable all touches until the sprite is showing?
        [self performSelector:@selector(scheduleUpdate)
                   withObject:nil
                   afterDelay:0.05f];
    }
    return self;
}

// scheduled update method
-(void) update:(ccTime)delta
{
    KKInput* touchInput = [KKInput sharedInput];
    
    //If we're touching the screen...
    if( touchInput.anyTouchBeganThisFrame )
    {
        //If we are touching a CCNode...
        if( [touchInput isAnyTouchOnNode:self touchPhase:KKTouchPhaseBegan] )
        {
            //We will invalidate all touches first
            //(avoids processing multiple simultaneous touch event on the same location.
            // also stops the touch from continuing to be executed?)
            KKTouch* touch;
            CCARRAY_FOREACH(touchInput.touches, touch)
            {
                [touch invalidate];
            }
            
            //then we interact
            [self reactToTouch];
        }
        else
        {
            [self QuitOpenItemActivity];
        }
    }
}

-(void) reactToTouch
{
    if( UNINITIALIZED_TAP_COUNT == currentTapCount )
    {
        //We found an uninitialized tap object. Let's notify
        UIAlertView *alert = [[[UIAlertView alloc] init] autorelease];
        [alert setTitle:@"Uninitialized Error"];
        [alert setMessage:@"The current locked item has no tap counts assigned."];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return;
    }
    
    currentTapCount--;
    
    if(currentTapCount <= 0)
    {
        currentTapCount = 0;
        [self QuitOpenItemActivity];
    }
    else
    {
        const float scaleTime = 0.05f;
        
        //Reset scale
        [self stopAllActions];
        [self setScale:originalScale];
        
        //Run re-scale action
        [self runAction:[CCSequence actionOne:[CCScaleTo actionWithDuration:scaleTime
                                                                      scale:tappedScale]
                                          two:[CCScaleTo actionWithDuration:scaleTime
                                                                      scale:originalScale]]];
        
        //Play tap sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"TappingItems.wav"];
    }
}

-(void) QuitOpenItemActivity
{
    //Notifiy the system that we're done with the activity
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY
                                                        object:nil];
    
    //We know that our parent IS the activity layer.
    NSAssert([self.parent isKindOfClass: [ActivityBase class]], @"Parent class is NOT an activity");
    ActivityBase* currentActivity = static_cast<ActivityBase*>(self.parent);
    
    
    //If we've tapped through, we should be unlocking the door
    if( 0 == currentTapCount )
    {
        //We KNOW the original activity launcher is a LockableItem.
        NSAssert([currentActivity.OriginalActivityLauncher isKindOfClass:[LockableItem class]], @"ActivityLauncher is NOT a LockableItem");

        LockableItem* lockableItem = static_cast<LockableItem*>(currentActivity.OriginalActivityLauncher);

        [lockableItem setIsLocked:NO];
        
        //Let's notify the system that this door has been unlocked so it can be saved in
        //the GameSaveData
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::RESOURCE_GATHERING_DOOR_UNLOCKED
                                                            object:currentActivity.OriginalActivityLauncher];
        
        //Let's play the unlocking sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"Unlocked_Crate.wav"];
    }
        
    //Let's remove EVERYTHING by doing it via the activity itself.
    //This will remove everything, thus, resetting the 'currentTapCount'
    [currentActivity removeFromParentAndCleanup:YES];
}


@end
