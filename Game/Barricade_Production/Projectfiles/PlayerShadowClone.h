//
//  PlayerShadowClone.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
@class PlayerArms;
@class Player;
@class EnemyBase;

@interface PlayerShadowClone : CCNode
{
    //instance variables
    CCSprite* playerBodySprite;
    CCSprite* playerArmsSprite;
    PlayerArms* playerArmsRef;
    Player* playerRef;
    
    float lifetimeDuration; //How much time the shadow is there for
    float currentLifetime;  //How much time left until the shadow is gone
    
    //Appearance variables
    Byte shadowCloneOpacity;
    ccColor3B shadowCloneColor;
    
    //Animation variables
    CCSpriteBatchNode* leoSheet;        //Spritesheet in memory
    CCAnimate* singleAnimationAction;   //Animate action for single loop
    CCAction* leoWalkAction_Forward;    //Forever repeating action (uses CCAnimate action)
    CCAction* leoWalkAction_Backward;   //Forever repeating action (uses CCAnimate action)
    BOOL isAnimating;
    BOOL isAnimationReversed;
    
    //Target acquisition
    EnemyBase* lastAcquiredTarget;
    
}
-(void) GameplayWeaponFired_EventHandler:(NSNotification*) notification;
-(void) PlayerNewAimDestination_EventHandler:(NSNotification*) notification;

@end
