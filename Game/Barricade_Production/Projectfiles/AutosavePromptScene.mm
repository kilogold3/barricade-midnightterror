//
//  AutosavePromptScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/1/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "AutosavePromptScene.h"
#import "StoryScene.h"
#import "AppWidescreener.h"

@implementation AutosavePromptScene

-(id) init
{
	self = [super init];
	if (self)
	{
		[self scheduleUpdate];
        [AppWidescreener centralizeSceneOnScreen:self];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

    // Setup a delegate method for the animationManager
    animationManagerRef = self.userObject;
    animationManagerRef.delegate = self;
    totalTimelineSequencesForScene = animationManagerRef.sequences.count;
    currentTimelineSequence = 0;
}

-(void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    //Load the dialogue-advance icon
    CCSprite* advanceSignalSprite = [CCSprite spriteWithFile:@"CursorHand.png"];
    advanceSignalSprite.position = ccp([CCDirector sharedDirector].screenSize.width - [AppWidescreener getWidescreenLetterboxOffset].x,0);
    advanceSignalSprite.anchorPoint = ccp(1.3f,-0.1f);
    advanceSignalSprite.opacity = 0;
    advanceSignalSprite.scale = 0.6f;
    
    const float fadeTime = 1.0f;
    [advanceSignalSprite runAction:[CCRepeatForever actionWithAction:
                                    [CCSequence actionOne:[CCFadeIn actionWithDuration:fadeTime]
                                                      two:[CCFadeOut actionWithDuration:fadeTime]]]];
    
    [self addChild:advanceSignalSprite];
}

// scheduled update method
-(void) update:(ccTime)delta
{
    if( [[KKInput sharedInput] anyTouchEndedThisFrame] )
    {
        ++currentTimelineSequence;
        
        //If there is another sequence available...
        if( currentTimelineSequence < totalTimelineSequencesForScene )
        {
            //load next sequence.
            [animationManagerRef runAnimationsForSequenceNamed:
             [NSString stringWithFormat:@"TL%i",currentTimelineSequence]];
        }
        else
        {
            //there are no more sequences. Let's move right into the story.
            [[CCDirector sharedDirector] replaceScene:[StoryScene node]];
        }
    }
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    CCLOG(@"Completed Sequence %@", name);
}

@end
