//
//  TravelNodeButton.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface TravelNodeButton : CCMenuItemImage
{
    short progressionValue;    //The progression on the map, the button represents
    short suppliesRequired;    //Supplies needed to access the next progression
    NSString* storyPathValue;  //The story path ("A" or "B") the button represents
    NSString* progressionName; //Name of the progression to be used in the travel prompt
}
@property (readonly) short ProgressionValue;
@property (readonly) NSString* StoryPathValue;
@property (readonly) NSString* ProgressionName;
@property (readonly) short SuppliesRequired;

@end
