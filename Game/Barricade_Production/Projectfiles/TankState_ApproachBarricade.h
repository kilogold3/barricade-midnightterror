//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyTank;
@class GB2Contact;

@interface TankState_ApproachBarricade : NSObject<IEnemyState>
{
    CCAction* repeatingWalkingAnimationAction;
    b2Fixture* physicsBodyFixture;
    EnemyTank* enemyParent;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact;
-(void) endContactWithBarricadeSegment: (GB2Contact*)contact;

@end
