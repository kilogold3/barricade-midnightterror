//
//  TitleScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TitleScene.h"
#import "TitleSceneCreatureEyesLayer.h"
#import "CCBReader.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "CCControlButton.h"
#import "SimpleAudioEngine.h"
#import "AppWidescreener.h"
#import "ParticlesRain.h"

@implementation TitleSceneInstance

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = self;
    
    //set children initial states
    menuButtonsLayer.visible = NO;
    touchToStartLabel.visible = NO;
    
    //Animate the wave effect of the "Midnight Terror" logo
    [waves3DEffectLayer runAction:[CCRepeatForever actionWithAction:[CCWaves3D actionWithDuration:20.f
                                                                                size:CGSizeMake(5, 2)
                                                                               waves:8
                                                                            amplitude:19.0f]]];    
}

-(void) cleanup
{
    [super cleanup];
    
    // Clear out delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    //Activate the touch to start label
    touchToStartLabel.opacity = 0;
    touchToStartLabel.visible = YES;
    const float LABEL_FADE_TIME = 0.8f;
    [touchToStartLabel runAction:[CCRepeatForever actionWithAction:
                                  [CCSequence actionOne: [CCFadeIn actionWithDuration: LABEL_FADE_TIME]
                                                    two: [CCFadeOut actionWithDuration: LABEL_FADE_TIME]]]];
    
    //Activate the eyes layer
    [titleSceneCreatureEyesLayer startEyes];
    
    //Allow for processing input
    [self scheduleUpdate];
}

-(void) startGameContinue
{
    SaveObjectPropertyList* gameProgressPropertyList = [[GameSaveData sharedGameSaveData] getSaveObjectProperties: GSC::GAME_PROGRESS];
    
    NSString* const  sceneTypeName = [gameProgressPropertyList objectForKey:GSP::CURRENT_RUNNING_SCENE];
    
    Class sceneType = NSClassFromString(sceneTypeName);
    
    NSAssert([sceneType isSubclassOfClass:[CCScene class]], @"Attempting to load a non-CCScene object type.");
    
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFadeBL transitionWithDuration:1.0f
                                                                                    scene:[sceneType node]]];
}

-(void) startCredits
{
    [[CCDirector sharedDirector] replaceScene: [CCBReader sceneWithNodeGraphFromFile:@"CreditsScene.ccbi"]];
}

-(void) startGameNew
{
    //If we have a previous game session save data...
    if( YES == [[GameSaveData sharedGameSaveData] isSaveFileAvailable])
    {
        //Hide current buttons
        menuButtonsLayer.visible = NO;
        btnContinue.enabled = NO;
        btnNewGame.enabled = NO;
        
        //Bring up the prompt
        saveFilePromptLayer = [CCBReader nodeGraphFromFile:@"TitleSceneSavePrompt.ccbi" owner:self];
        [AppWidescreener centralizeSceneOnScreen:saveFilePromptLayer];
        [self addChild: saveFilePromptLayer];
    }
    //Otherwise, we start a new game like nothing happened.
    else
    {
        [self transitionToNewGame];
    }
}

-(void) update:(ccTime)delta
{
    if(
       [[KKInput sharedInput] isAnyTouchOnNode:self
                                    touchPhase:KKTouchPhaseEnded] )
    {
        if( nil != touchToStartLabel )
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"TS_SFX_TapToStart.wav"];
            [self removeChild:touchToStartLabel cleanup:YES];
            touchToStartLabel = nil;
            
            menuButtonsLayer.visible = YES;
            
            //Get the continue button and disable it if applicable
            [btnContinue setTitleColor:ccGRAY forState:CCControlStateDisabled];
            btnContinue.enabled = [[GameSaveData sharedGameSaveData] isSaveFileAvailable];
            
            //////////////////////////////////////
            //Debug Build info
            //////////////////////////////////////
            CCLabelTTF* label = [CCLabelTTF labelWithString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
                                                   fontName:@"Anime Ace"
                                                   fontSize:10];
            [label setColor:ccc3(222, 222, 255)];
            CGSize screenSize = [CCDirector sharedDirector].screenSize;
            label.position = CGPointMake( screenSize.width - label.contentSize.width , label.contentSize.height);
            label.anchorPoint = ccp(0,0.5);
            [self addChild:label];
        }
    }
}

-(void) transitionToNewGame
{
    [[GameSaveData sharedGameSaveData] ResetSaveDataToDefaults];
    [[SimpleAudioEngine sharedEngine] playEffect:@"TS_SFX_GameStart.wav"];
    
    CCScene* autosavePromptScene = [CCBReader sceneWithNodeGraphFromFile:@"AutosavePromptScene.ccbi"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionProgressInOut transitionWithDuration:2.0f
                                                                                    scene:autosavePromptScene]];
}


-(void) unloadSaveFilePrompt
{
    //Remove the prompt layer
    [saveFilePromptLayer removeFromParentAndCleanup:YES];
    
    //Clear the pointer
    saveFilePromptLayer = nil;
    
    //Show the main buttons
    menuButtonsLayer.visible = YES;
    btnContinue.enabled = YES;
    btnNewGame.enabled = YES;
}

@end


@implementation TitleScene

-(id) init
{
    self = [super init];
    
    if( nil != self)
    {        
        //Load the spritesheet needed for the title scene instance
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"TitleSceneSheet.plist"];
        
        //Load the title scene instance
        titleSceneInstance = static_cast<TitleSceneInstance*>([CCBReader nodeGraphFromFile:
                                                               [NSString stringWithFormat:@"TitleScene%@.ccbi",
                                                                [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""]]);
        [self addChild:titleSceneInstance];
    }
    
    return self;
}

-(void) dealloc
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"TitleSceneSheet.plist"];
    [super dealloc];
}

@end
