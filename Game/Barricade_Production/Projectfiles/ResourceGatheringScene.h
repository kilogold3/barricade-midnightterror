//
//  ResourceGatheringScene.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/3/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
@class ActivityLauncher;
@class SearchTimerClock;

@interface ResourceGatheringScene : CCScene
{
    //Tag value to refer to the room layer
    int roomLayerChildTag;
    
    //Current room
    NSString* currentRoomIdString;
    
    //displays remaining time
    SearchTimerClock* timerClock;
    
    //Menu where the map button is placed
    CCMenu* miniMapButtonMenu;
    
    //Empty Activity launcher for the minimap.
    //Needed due to structure design.
    ActivityLauncher* dummyMiniMapActivityLauncher;
}


-(void) componentAcquireResourceGatheringScene_EventHandler: (NSNotification*) notification;
-(void) componentAcquireResourceGatheringMinimapButton_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringTraverseDoor_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringLaunchActivity_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringPickupAcquired_EventHandler: (NSNotification*) notification;
-(void) componentAcquireResourceGatheringCurrentRoom_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringMiniMapButton_Callback: (id) sender;
@end
