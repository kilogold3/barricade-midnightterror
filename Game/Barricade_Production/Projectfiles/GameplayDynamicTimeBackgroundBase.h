//
//  GameplayBackgroundLayerBase.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/3/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "CCBAnimationManager.h"

@interface GameplayDynamicTimeBackgroundBase : CCLayer<CCBAnimationManagerDelegate>
{
@protected
@private
}
//Method overrides
- (void) didLoadFromCCB;

//Event Handlers
-(void) OverdriveStartAnimation_EventHandler: (NSNotification*) notification;
-(void) OverdriveFinishAnimation_EventHandler: (NSNotification*) notification;
-(void) OverdriveFinishExecution_EventHandler: (NSNotification*) notification;
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification;
-(void) ComponentAcquireGameplayLayerBackground_EventHandler:(NSNotification*) notification;

//Controls
-(void) pauseBackgroundAnimation;
-(void) resumeBackgroundAnimation;
@end
