//
//  Overdrive.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/13/13.
//
//

#import <Foundation/Foundation.h>
#import "OverdriveExecutionProtocol.h"

@interface Overdrive : NSObject
{
    NSString* cocosBuilderAnimationFilename;
    NSString* overdrivePanelIconActiveFilename;
    NSString* overdrivePanelIconInactiveFilename;
    OverdriveExecutionObject* overdriveExecution;
}
@property (readonly) NSString* CocosBuilderAnimationFilename;
@property (readonly) NSString* OverdrivePanelIconActiveFilename;
@property (readonly) NSString* OverdrivePanelIconInactiveFilename;
@property (readonly) OverdriveExecutionObject* OverdriveExecution;

-(id) initWithDependencies: (NSString*) CocosBuilderAnimationFilename
                          : (NSString*) OverdrivePanelIconActiveFilename
                          : (NSString*) OverdrivePanelIconInactiveFilename
                          : (OverdriveExecutionObject*) OverdriveExecution;
@end
