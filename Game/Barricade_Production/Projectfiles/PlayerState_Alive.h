//
//  PlayerState_Alive.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPlayerState.h"

@class Player;
@class PlayerArms;

@interface PlayerState_Alive : NSObject<IPlayerState>
{
    //instance variables
    Player* playerRef;
    PlayerArms* playerArmsRef;
    CCSprite* playerBodySprite;
    
    //Animation variables
    CCSpriteBatchNode* leoSheet;        //Spritesheet in memory
    CCAnimate* singleAnimationAction;   //Animate action for single loop
    CCAction* leoWalkAction_Forward;    //Forever repeating action (uses CCAnimate action)
    CCAction* leoWalkAction_Backward;   //Forever repeating action (uses CCAnimate action)
    BOOL isAnimating;
    BOOL isAnimationReversed;
    
    //Movement variables
    CGFloat movementSpeed;
    CGPoint movementDestination;
}
@property (readonly) CCSprite* PlayerBodySprite;
@property (readonly) PlayerArms* PlayerArmsRef;
@property (readonly) CGFloat MovementSpeed;
@property (nonatomic, readwrite) CGPoint MovementDestination;


-(id) initWithDependencies: (Player*) playerRefIn;

-(void) Enter;
-(void) Exit;
-(void) Update: (float) deltaTime;

@end
