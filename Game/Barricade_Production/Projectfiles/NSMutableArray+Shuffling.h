//
//  NSMutableArray+Shuffling.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/4/13.
//
//

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#include <Cocoa/Cocoa.h>
#endif

// This category enhances NSMutableArray by providing
// methods to randomly shuffle the elements.
@interface NSMutableArray (Shuffling)
- (void)shuffle;
@end