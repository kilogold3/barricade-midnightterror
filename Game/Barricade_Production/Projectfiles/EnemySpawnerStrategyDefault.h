//
//  EnemySpawnerStrategyDefault.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import "EnemySpawnerStrategyBase.h"

@interface EnemySpawnerStrategyDefault : EnemySpawnerStrategyBase

-(id) initWithEnemyField: (CCLayer*) enemyFieldIn;

@end
