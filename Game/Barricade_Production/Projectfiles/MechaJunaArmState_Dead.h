//
//  MechaJunaState_AttackWithRightArm.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"
@class EnemyMechaJunaArm;

@interface MechaJunaArmState_Dead : NSObject<IEnemyState>
{
    EnemyMechaJunaArm* armRef;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) enter;
-(void) exit;
-(void) update: (float) deltaTime;

-(void) stepToLocation: (CGPoint) location
         withDeltaTime: (float) deltaTime;
@end