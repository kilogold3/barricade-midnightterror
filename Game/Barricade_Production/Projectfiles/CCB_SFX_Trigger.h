//
//  CCBSoundTrigger.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
@class CDSoundSource;

@interface CCB_SFX_Trigger
: CCNode
{
    CDSoundSource* soundSource;
    NSString* soundFilename;
    BOOL isLooping;
}

@end
