//
//  SneakyHoldEventButton.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SneakyButton.h"
#import "cocos2d.h"

@interface SneakyHoldEventButton : SneakyButton 
{
    NSString* buttonPressEventID;
    NSString* buttonReleaseEventID;
    BOOL isTouchEnabled;
}
@property (assign, nonatomic) NSString* ButtonPressEventID;
@property (assign, nonatomic) NSString* ButtonReleaseEventID;
@property (readwrite, nonatomic) BOOL IsTouchEnabled;

-(id)initWithRect:(CGRect)rect;

+(id) button;
+(id) buttonWithRect:(CGRect)rect;

@end
