//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaArmState_Idle.h"
#import "EnemyMechaJuna.h"

@implementation MechaJunaArmState_Idle

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaArm class]],@"Wrong enemy parent type");

        armRef = static_cast<EnemyMechaJunaArm*>(enemyParentIn);
    }
    
    return self;
}

-(void) enter
{
}

-(void) exit
{
    
}

-(void) update: (float) deltaTime
{    
    if(NO == CGPointEqualToPoint(armRef.position, armRef.OriginalPosition))
    {
        //Perform movement
        [self stepToLocation: armRef.OriginalPosition
               withDeltaTime: deltaTime];
        
        //If near origin AND not locked into position,
        if( 1.0f > ccpDistance( armRef.position, armRef.OriginalPosition ) )
        {
            //snap to origin
            [armRef setPosition:armRef.OriginalPosition];
        }
    }
}

-(void) stepToLocation: (CGPoint) location
         withDeltaTime: (float) deltaTime
{
    //Grab destination position
    CGPoint destPos = location;
    
    //Grab self position
    CGPoint selfPosition = armRef.position;
    
    //Form a line from enemy to player
    CGPoint direction = ccpSub(destPos, selfPosition);
    
    //Normalize for direction
    CGPoint normailzedDirection = ccpNormalize(direction);
    
    //create velocity
    CGPoint velocity = {    normailzedDirection.x * armRef.ArmMovementSpeed * deltaTime ,
                            normailzedDirection.y * armRef.ArmMovementSpeed * deltaTime };
    
    //apply velocity to position
    CGPoint newPosition = { armRef.position.x + velocity.x ,
                            armRef.position.y + velocity.y };
    
    //Set new position
    [armRef setPosition:newPosition];
}


@end
