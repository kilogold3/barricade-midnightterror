//
//  GameOverSceneTransitioner.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameOverSceneTransitioner.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"
#import "AppWidescreener.h"

@implementation GameOverSceneTransitioner

-(id) init
{
    self = [super init];
    
    if( nil != self)
    {

    }
    
    return self;
}

-(void) onEnter
{
	[super onEnter];
    
    //Implement a little flare here by adding a red flash
    CCLayerColor* redFlash = [CCLayerColor layerWithColor:ccc4(255, 0, 0, 255)];
    [redFlash setPosition:ccp(0,0)];
    [redFlash setSize:[CCDirector sharedDirector].screenSize];
    [self addChild:redFlash];
    
    [[SimpleAudioEngine sharedEngine] playEffect:@"DyingSound.wav"];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate); //rumble
}

-(void) onEnterTransitionDidFinish
{
    /**************************************************************************
     We want to make sure the previous scene is deallocated before allocating
     this one. That is because scene transitions are executed in the following
     order:
     1. scene: OtherScene
     
     2. init: <OtherScene = 066B2130 | Tag = -1>
     
     3. onEnter: <OtherScene = 066B2130 | Tag = -1>
     ￼
     4. [[[[Transition is running here]]]
     
     5. onExit: <FirstScene = 0668DF40 | Tag = -1>
     
     6. onEnterTransitionDidFinish: <OtherScene = 066B2130 | Tag = -1>
     
     7. dealloc: <FirstScene = 0668DF40 | Tag = -1>
     
     Note that the FirstScene dealloc method is called last. This means that during
     onEnterTransitionDidFinish, the previous scene is still in memory. If you want
     to allocate memory-intensive nodes at this point, you’ll have to schedule a
     selector to wait at least one frame before doing the memory allocations, to be
     certain that the previous scene’s memory is released. You can schedule a method
     that is guaranteed to be called the next frame by omitting the interval parameter
     when scheduling the method:
     
     [self schedule:@selector(waitOneFrame:)];
     
     The method that will be called then needs to unschedule itself by calling unschedule
     with the hidden_cmd parameter:
     
     -(void) waitOneFrame:(ccTime)delta
     {
     [self unschedule:_cmd];
     // delayed code goes here ...
     }
     **************************************************************************/
    [self schedule:@selector(loadSceneAfterOneFrame:)];
    
    [super onEnterTransitionDidFinish];
    
}

-(void)loadSceneAfterOneFrame: (ccTime) delta
{
    //Unschedule self. Only designed to run once.
    [self unschedule: _cmd];
    
    if( YES == [AppWidescreener isWidescreenEnabled] )
    {
        [[CCDirector sharedDirector] replaceScene:[CCBReader sceneWithNodeGraphFromFile:@"GameOverScene__Wide.ccbi"]];
    }
    else
    {
        [[CCDirector sharedDirector] replaceScene:[CCBReader sceneWithNodeGraphFromFile:@"GameOverScene.ccbi"]];
    }
}


@end
