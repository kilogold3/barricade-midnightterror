//
//  PlayerShadowClone.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "PlayerShadowClone.h"
#import "PlayerState_Alive.h"
#import "GB2ShapeCache.h"
#import "Player.h"
#import "NotificationCenterEventList.h"
#import "WeaponBase.h"
#import "PlayerArms.h"
#import "EnemyBase.h"
#import "GB2Engine.h"
#import "Aim.h"

@interface PlayerShadowClone (PrivateMethods)
// declare private methods here
@end

@implementation PlayerShadowClone

-(void) GameplayWeaponFired_EventHandler:(NSNotification*) notification
{
    WeaponBase* weapon = [notification object];
    
    CGPoint originPosition = [self convertToWorldSpace:playerArmsSprite.position];
    
    [weapon fireWeaponFromOrigin:originPosition];
    
}

-(id) init
{
	self = [super init];
	if (self)
	{
        shadowCloneOpacity = 150;
        shadowCloneColor = ccBLACK;
        lifetimeDuration = 4.0f;
        currentLifetime = lifetimeDuration;
        
        [self scheduleUpdate];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(GameplayWeaponFired_EventHandler:)
                                                     name:EventList::GAMEPLAY_WEAPON_FIRED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(PlayerNewAimDestination_EventHandler:)
                                                     name:EventList::PLAYER_NEW_AIM_DESTINATION
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(weaponSelected_EventHandler:)
                                                     name:EventList::CONTROLS_WEAPON_SELECTED
                                                   object:nil];
        
        
        playerRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_PLAYER_TARGET
                                                            object:[NSValue valueWithPointer:&playerRef]];
        
        NSAssert(nil != playerRef,@"Player reference is nil. No player found.");
        NSAssert(nil != playerRef.CurrentPlayerState,@"Player state is nil");
        NSAssert([PlayerState_Alive class] == [playerRef.CurrentPlayerState class],@"Wrong player state for shadow clone");
        
        PlayerState_Alive* playerStateRef = static_cast<PlayerState_Alive*>(playerRef.CurrentPlayerState);
        
        //load the sprite sheet into a CCSpriteBatchNode object. If you're adding a new sprite
        //to your scene, and the image exists in this sprite sheet you should add the sprite
        //as a child of the same CCSpriteBatchNode object otherwise you could get an error.
        leoSheet = [CCSpriteBatchNode batchNodeWithFile:@"leoSheet.png"];
        
        //load each frame included in the sprite sheet into an array for use with the CCAnimation object below
        NSMutableArray* leoAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 13; ++i)
        {
            [leoAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"NewLeo_Walk%02d.png", i]]];
        }
        
        
        //Create the animation from the frame flyAnimFrames array
        float delay = ( [leoAnimFrames count] / (playerStateRef.MovementSpeed*3.5f) );
        CCAnimation* leoAnim = [CCAnimation animationWithSpriteFrames:leoAnimFrames delay: delay ];
        leoAnim.restoreOriginalFrame = YES;
        
        //create a sprite and set it to be the first image in the sprite sheet
        playerBodySprite = [CCSprite spriteWithSpriteFrameName:@"NewLeo_Stand.png"];
        [playerBodySprite setAnchorPoint:ccp(0.5,0)];
        
        //create a looping action using the animation created above. This just continuosly
        //loops through each frame in the CCAnimation object
        singleAnimationAction = [CCAnimate actionWithAnimation:leoAnim];
        [singleAnimationAction retain];
        
        leoWalkAction_Forward = [CCRepeatForever actionWithAction: singleAnimationAction];
        [leoWalkAction_Forward retain];
        
        leoWalkAction_Backward = [CCRepeatForever actionWithAction: [singleAnimationAction reverse]];
        [leoWalkAction_Backward retain];
        
        //add the sprite to the CCSpriteBatchNode object
        [leoSheet addChild:playerBodySprite];
        
        //Set anchor point
        [playerBodySprite setAnchorPoint:
         [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"leoShape"]
         ];
        
        //Darken the sprite so it looks like a shadow
        [playerBodySprite setColor:shadowCloneColor];
        [playerBodySprite setOpacity:shadowCloneOpacity];
        [self addChild:leoSheet];
        
        //Add the shadow clone to the player
        [playerRef addChild:self];
        
        //Add the arms
        playerArmsRef = playerStateRef.PlayerArmsRef;
        CCSpriteFrame* armsSpriteFrame = playerArmsRef.FrontArmSprite.displayFrame;
        playerArmsSprite = [CCSprite spriteWithSpriteFrame:armsSpriteFrame];
        [playerArmsSprite setColor:shadowCloneColor];
        [playerArmsSprite setOpacity:shadowCloneOpacity];

        
        [self addChild:playerArmsSprite];
        [playerArmsSprite setPosition:playerArmsRef.position];
        [playerArmsSprite setAnchorPoint: playerArmsRef.CurrentWeapon.WeaponSpriteAnchor];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_WEAPON_FIRED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::PLAYER_NEW_AIM_DESTINATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_WEAPON_SELECTED
                                                  object:nil];
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

// scheduled update method
-(void) update:(ccTime)delta
{
    if( currentLifetime > 0 )
    {
        currentLifetime -= delta;
        return;
    }
    
    if( playerArmsSprite.opacity > 0 ) { playerArmsSprite.opacity -= 1; }
    if( playerBodySprite.opacity > 0 ) { playerBodySprite.opacity -= 1; }
    
    //If we've completely faded out, let's eliminate
    if( 0 == playerBodySprite.opacity && 0 == playerArmsSprite.opacity )
    {
        [self removeFromParentAndCleanup:YES];
    }
}


-(void) PlayerNewAimDestination_EventHandler:(NSNotification*) notification
{
    //////
    //Handle arm rotation
    //////
    Aim* aimRef = [ notification object ];
    const CGPoint aimPosition = aimRef.position;
    
    const CGPoint armsSceneCoordinates = [self convertToWorldSpace:playerArmsSprite.position];
    
    CGPoint aimDirection = ccpSub(aimPosition, armsSceneCoordinates);
    aimDirection = ccpNormalize(aimDirection);
    
    float rotationDegrees = CC_RADIANS_TO_DEGREES( ccpAngle( CGPointMake(-1, 0), aimDirection) );
    
    if (aimPosition.y < armsSceneCoordinates.y )
    {
        rotationDegrees = 360 - rotationDegrees;
    }
    
    [playerArmsSprite setRotation:rotationDegrees];
    
    //////
    //Handle target acquisition
    //////
    
    //Project the target position all the way to the left bound
    //of the screen to simulate the bullet firing past the reticle.
    //We use the line equation to give us the projected point where
    //it intersects with the left screen bounds.
    
    //Calculate slope
    //
    //  (y2 - y1)
    //  ----------
    //  (x2 - x1)
    const float m = (aimPosition.y - armsSceneCoordinates.y) / (aimPosition.x - armsSceneCoordinates.x);
    
    //Find Y-intercept
    // y = mx + b [OR] b = y-mx
    const float b = armsSceneCoordinates.y - (m * armsSceneCoordinates.x);
    
    //acquire the projected point on the screens left bound:
    //(0,0) to (0, screen_height).
    const CGPoint targetPosition = ccp(0,b);
    
    //perform the raycast
    RayCastCallbackLineOfFire raycast;
    [GB2Engine sharedInstance].world->RayCast(&raycast,
                                              b2Vec2FromCGPoint(targetPosition),
                                              b2Vec2FromCGPoint(armsSceneCoordinates)
                                              );
    
    
    EnemyFieldInstance* raycastTarget = raycast.getFrontmostEnemyFieldInstanceTarget();
    EnemyBase* newTarget = nil;
    
    //If the raycast target is an EnemyBase
    if( nil != raycastTarget && [raycastTarget isKindOfClass:[EnemyBase class]])
    {
        newTarget = (EnemyBase*)raycastTarget;
    }
    
    //If there is no newly acquired target...
    if( nil == newTarget )
    {
        //disable the last acquired target's health display
        if( nil != lastAcquiredTarget)
        {
            //lose a targeting count
            --lastAcquiredTarget.TargetedCount;
        }
        
        //stop referencing the previous enemy as the last acquired target
        lastAcquiredTarget = nil;
    }
    //If the target acquired is different...
    else if( newTarget != lastAcquiredTarget )
    {
        //turn off health indicator for that enemy.
        if( nil != lastAcquiredTarget )
        {
            //lose a targeting count
            --lastAcquiredTarget.TargetedCount;
        }
        
        //turn on health indicator for the new enemy.
        ++newTarget.TargetedCount;
        
        //refer to the new enemy as the last acquired target
        lastAcquiredTarget = newTarget;
    }
}

-(void) weaponSelected_EventHandler: (NSNotification*) notification
{
    //Grab the weapon
    NSInteger weaponIndex = [[notification object] integerValue];
    
    WeaponBase* newWeapon = [playerRef.WeaponsArray objectAtIndex:weaponIndex];
    
    //Save position and anchor points
    CGPoint currentAnchorPoint = newWeapon.WeaponSpriteAnchor;
    CGPoint currentPosition = playerArmsSprite.position;
    
    //Eliminate the current sprite
    [self removeChild:playerArmsSprite cleanup:YES];
    
    //reload a new one in it's place
    playerArmsSprite = [CCSprite spriteWithSpriteFrameName:newWeapon.FrontArmSpriteID];
    [self addChild:playerArmsSprite];
    
    //Restore proper positional attributes
    playerArmsSprite.position = currentPosition;
    playerArmsSprite.anchorPoint = currentAnchorPoint;
    playerArmsSprite.color = shadowCloneColor;
    playerArmsSprite.opacity = shadowCloneOpacity;
}

@end
