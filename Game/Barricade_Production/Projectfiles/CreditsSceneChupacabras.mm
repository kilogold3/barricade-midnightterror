//
//  CreditsSceneChupacabras.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "CreditsSceneChupacabras.h"
#import "StatsSceneActor.h"
#import "GMath.h"
#import "CCBReader.h"

const Byte ENEMY_Y_ALIGN = 10;

@interface CreditsSceneChupacabras (PrivateMethods)
// declare private methods here
@end

@implementation CreditsSceneChupacabras

-(id) init
{
    NSAssert(NO, @"Incorrect init method.");
    return nil;
}

-(id) initWithEnemyTypeName: (NSString*) enemyTypeName
{
	self = [super init];
	if (self)
	{
        //Specify an initial direction & position.
        if( gFloatRand(-1, 1) > 0 )
        {
            movementDirection = 1;
            self.position = ccp( 0, ENEMY_Y_ALIGN );
        }
        else
        {
            movementDirection = -1;
            self.position = ccp( [CCDirector sharedDirector].screenSize.width, ENEMY_Y_ALIGN );
        }
        
        //Load chupacabra actor
        NSString* const chupacabrasActorName = [NSString stringWithFormat:@"StatsScene%@Anim.ccbi",enemyTypeName];
        chupacabraActor = [CCBReader nodeGraphFromFile:chupacabrasActorName];
        [self addChild:chupacabraActor];
        
        //Set the sprite facing direction
        chupacabraActor.scaleX = movementDirection;
        
        //Load lua movement settings for enemy
        NSString* const chupacabraActorLuaTable = [NSString stringWithFormat:@"Enemy%@Settings",enemyTypeName];
        
        if( [KKConfig selectKeyPath:chupacabraActorLuaTable] )
        {
            movementSpeed = gRangeRand( [KKConfig floatForKey:@"MinMovementSpeed"], [KKConfig floatForKey:@"MaxMovementSpeed"] );
        }
        else
        {
            NSAssert(false, @"Did [NOT] find config.lua keypath");
        }
        
        //schedule the update for movement
        [self scheduleUpdate];
        
	}
	return self;
}

// scheduled update method
-(void) update:(ccTime)delta
{
    self.position = ccpAdd(self.position, ccp( movementDirection * movementSpeed * delta, 0 ) );
    
    //If we're headed to the left and are past the left screen bound...
    //[OR]
    //If we're headed to the right and are past the right screen bound...
    if( (-1 == movementDirection && self.position.x < 0)  ||
        (1 == movementDirection && self.position.x > [CCDirector sharedDirector].screenSize.width))
    {
        [self removeFromParentAndCleanup:YES];
    }
}

@end
