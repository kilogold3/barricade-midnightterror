//
//  PlayerArms.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class WeaponBase;
@class Player;

@interface PlayerArms : CCNode 
{
    WeaponBase* currentWeapon;
    Player* playerRef;
    
    CCSprite* rearArmSprite;
    CCSprite* frontArmSprite;
}

-(id) initWithDependencies: (Player*) playerRefIn;
-(void) changeWeapon: (WeaponBase*) newWeapon;
-(void) setRotationBasedOnPoint: (CGPoint) targetPoint;
-(void) weaponSelected_EventHandler: (NSNotification*) notification;

@property (readonly) WeaponBase* CurrentWeapon;
@property (readonly) CCSprite* RearArmSprite;
@property (readonly) CCSprite* FrontArmSprite;

@end
