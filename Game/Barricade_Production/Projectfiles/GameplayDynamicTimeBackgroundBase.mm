//
//  GameplayBackgroundLayerBase.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/3/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplayDynamicTimeBackgroundBase.h"
#import "NotificationCenterEventList.h"

@interface GameplayDynamicTimeBackgroundBase (PrivateMethods)
// declare private methods here
@end

@implementation GameplayDynamicTimeBackgroundBase

- (void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = self;
}

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    if( [name isEqualToString:@"Default Timeline"] )
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_LEVEL_TIMER_FINISH
                                                            object:nil];
    }
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OverdriveStartAnimation_EventHandler:)
                                                 name:EventList::OVERDRIVE_START_ANIMATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OverdriveFinishAnimation_EventHandler:)
                                                 name:EventList::OVERDRIVE_FINISH_ANIMATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(OverdriveFinishExecution_EventHandler:)
                                                 name:EventList::OVERDRIVE_FINISH_EXECUTION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ControlsPauseGameButtonPressed_EventHandler:)
                                                 name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ControlsResumeGameButtonPressed_EventHandler:)
                                                 name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ComponentAcquireGameplayLayerBackground_EventHandler:)
                                                 name:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND
                                               object:nil];
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
    // either a sibling or in a different branch of the node hierarchy
    
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_EXECUTION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_START_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_FINISH_ANIMATION
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::COMPONENT_ACQUIRE_GAMEPLAYLAYER_BACKGROUND
                                                  object:nil];
}

// scheduled update method
-(void) update:(ccTime)delta
{
}

-(void) OverdriveStartAnimation_EventHandler: (NSNotification*) notification
{
    [self pauseBackgroundAnimation];
}

-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self pauseBackgroundAnimation];
}

-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    [self resumeBackgroundAnimation];
}

-(void) ComponentAcquireGameplayLayerBackground_EventHandler:(NSNotification*) notification
{
    NSValue* layerRefPointer = [notification object];
    CCLayer** targetLayerRef = (CCLayer**)[layerRefPointer pointerValue];
    (*targetLayerRef) = self;
}

-(void) OverdriveFinishAnimation_EventHandler: (NSNotification*) notification
{
    BOOL isOverdriveHit = [[notification object] boolValue];
    
    //If overdrive is NOT a HIT
    if( NO == isOverdriveHit )
    {
        //The overdrive is a MISS.
        //Let's reactivate and operate as normal
        [self resumeBackgroundAnimation];
    }
    else
    {
        //do nothing. Leave everything as is.
        //Actions should remain disabled until the
        //end of the OverdriveExecution.
    }
}

-(void) OverdriveFinishExecution_EventHandler: (NSNotification*) notification
{
    [self resumeBackgroundAnimation];
}
-(void) pauseBackgroundAnimation
{
    [self pauseSchedulerAndActions];
    [self.children makeObjectsPerformSelector:@selector(pauseSchedulerAndActions)];
}
-(void) resumeBackgroundAnimation
{
    [self resumeSchedulerAndActions];
    [self.children makeObjectsPerformSelector:@selector(resumeSchedulerAndActions)];
}

@end
