//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TankState_Blank.h"
#import "EnemyTank.h"

@implementation TankState_Blank

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyTank class]],@"Wrong enemy parent type");
        enemyParent = (EnemyTank*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}
@end
