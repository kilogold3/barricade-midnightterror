//
//  OverdriveStatusBar.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@class StatusBarColorCycler;

@interface OverdriveStatusBar : CCNode
{
    StatusBarColorCycler* colorCycler;
    
    //Other variables
    CCSprite* barSprite;
    ccColor3B initialColorTone; //The color with which the bar is loaded in.
    float percentageFillTarget; //How full the bar should be
}
@property (nonatomic, readwrite) float CurrentBrightness;

//Event Handlers
-(void) UpdateUI_EventHandler: (NSNotification*) notification;

@end
