//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlierState_Death.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2Engine.h"
#import "EnemyFlier.h"
#import "SimpleAudioEngine.h"
#import "GameAudioList.h"

@implementation FlierState_Death

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyFlier class]],@"Wrong enemy parent type");
        enemyParentRef = (EnemyFlier*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_ENEMY_FLIER_DEATH];

    //Load the sprite
    enemyParentRef.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyFlierDeath0.png"];
    [enemyParentRef addChild:enemyParentRef.EnemySprite];

    //Set anchor point
    [enemyParentRef.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyFlierShape"]
     ];
    
    //Load animation & actions
    const short animationFrameCount = 8;
    float animationDelay = (20 / enemyParentRef.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyFlierDeath%d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    CCCallBlock* cleanupAction = [CCCallBlock actionWithBlock:^{
        //(We want to remove the sprite.)
        [enemyParentRef removeChild:enemyParentRef.EnemySprite cleanup:YES];
    }];
    
    //Execute actions
    [enemyParentRef.EnemySprite runAction:
     [CCSequence
      actionOne:animateAction
      two:cleanupAction
      ]];
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}
@end
