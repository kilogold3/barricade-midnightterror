//
//  EnemySpawnerStrategyBase.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/10/13.
//
//

#import "EnemySpawnerStrategyBase.h"
#import "GMath.h"
#import "EnemyFactory.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "Pair.h"


@implementation EnemySpawnerStrategyBase
@synthesize SpawningEnabled=spawningEnabled;
@synthesize MaxBaseSpawnTimer=maxBaseSpawnTimer;
@synthesize MinBaseSpawnTimer=minBaseSpawnTimer;

-(void) setMaxBaseSpawnTimer:(float)MaxBaseSpawnTimer
{
    
    maxBaseSpawnTimer = MaxBaseSpawnTimer;
    
    if(maxBaseSpawnTimer < SpawnModifier_DaySpentLimit)
    {
        maxBaseSpawnTimer = SpawnModifier_DaySpentLimit;
    }
}

-(void) setMinBaseSpawnTimer:(float)MinBaseSpawnTimer
{
    minBaseSpawnTimer = MinBaseSpawnTimer;
    
    if(minBaseSpawnTimer < SpawnModifier_DaySpentLimit)
    {
        minBaseSpawnTimer = SpawnModifier_DaySpentLimit;
    }
}

-(id) initWithEnemyField: (CCLayer*) enemyFieldIn
{
    self = [super init];
    
    if( nil != self)
    {
        enemyField = enemyFieldIn;
        
        enemyFactory = [[EnemyFactory alloc] initWithDependencies];
        
        [self loadConfigurationFromFile];        
    }
    
    return self;
}

-(void) loadConfigurationFromFile
{
    ///////////////////////////////////////////////////////////
    //First load all the properties
    ///////////////////////////////////////////////////////////
    
    //Obtain all necessary data for processing.
    SaveObjectPropertyList* StorySceneProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentlyLoadedStoryChapter = [[StorySceneProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
    const NSString* configTableName = [NSString stringWithFormat:@"GameplaySceneConfig_%02iA",currentlyLoadedStoryChapter];
    const int currentDay = [[StorySceneProperties objectForKey:GSP::CURRENT_PROGRESSION_DAY] intValue];
    
    //Parse from file
    if( [KKConfig selectKeyPath: configTableName] )
    {
        minBaseSpawnTimer                   = [KKConfig floatForKey:@"SpawnBaseTimerMin"];
        maxBaseSpawnTimer                   = [KKConfig floatForKey:@"SpawnBaseTimerMax"];
        minCountPerSpawn                    = [KKConfig intForKey:@"MinCountPerSpawn"];
        maxCountPerSpawn                    = [KKConfig intForKey:@"MaxCountPerSpawn"];
        SpawnModifier_WeaponUpgradeLevel2   = [KKConfig floatForKey:@"SpawnModifier_WeaponUpgradeLevel2"];
        SpawnModifier_WeaponUpgradeLevel3   = [KKConfig floatForKey:@"SpawnModifier_WeaponUpgradeLevel3"];
        SpawnModifier_DaySpent              = [KKConfig floatForKey:@"SpawnModifier_DaySpent"];
        SpawnModifier_DaySpentLimit         = [KKConfig floatForKey:@"SpawnModifier_DaySpentLimit"];
        
        //Find out which types we'll be using for the current setup
        NSString* keyString = nil;
        if( currentDay < 3 )
            keyString = @"EnemyAppearanceDay1";
        else if( currentDay < 5 )
            keyString = @"EnemyAppearanceDay3";
        else
            keyString = @"EnemyAppearanceDay5";
        
        //Load the raw strings which will be tokenized
        NSString* spawnTypesRaw = [KKConfig stringForKey:[NSString stringWithFormat:@"%@_Types",keyString]];
        NSString* spawnProbabilitiesRaw = [KKConfig stringForKey:[NSString stringWithFormat:@"%@_Probabilities",keyString]];
        
        //Split the raw strings
        NSArray* spawnTypes = [spawnTypesRaw componentsSeparatedByString:@"|"];
        NSArray* spawnProbabilities = [spawnProbabilitiesRaw componentsSeparatedByString:@"|"];
        
        //We have all the data. Let's make an array to store all this information in.
        NSMutableArray* assebledTypeAndProbabilityList = [NSMutableArray arrayWithCapacity:[spawnTypes count]];
        
        for (NSUInteger curType = 0; curType < spawnTypes.count; ++curType)
        {
            Pair* arrayEntry = [[[Pair alloc] initWithObject1: [spawnTypes objectAtIndex:curType]
                                                   andObject2: [spawnProbabilities objectAtIndex:curType] ] autorelease];
            
            [assebledTypeAndProbabilityList addObject:arrayEntry];
        }
        
        //Finally, assign this newly assembled list for use.
        spawnTypesAndProbabilities = assebledTypeAndProbabilityList;
        [spawnTypesAndProbabilities retain];
    }
    
    ///////////////////////////////////////////////////////////
    //Calculate any other property values from parsed data
    ///////////////////////////////////////////////////////////

    //Modify the base timer according to the day (will automatically cap at limit with setter)
    self.MinBaseSpawnTimer = self.MinBaseSpawnTimer + (SpawnModifier_DaySpent * currentDay);
    self.MaxBaseSpawnTimer = self.MaxBaseSpawnTimer + (SpawnModifier_DaySpent * currentDay);
    currentSpawnTimer = gFloatRand(minBaseSpawnTimer, maxBaseSpawnTimer);
}


-(void) dealloc
{
    [enemyFactory release];
    [spawnTypesAndProbabilities release];
    [super dealloc];
}

-(void) update: (ccTime) delta
{
    //If we are allowed to spawn...
    if( YES == spawningEnabled )
    {
        currentSpawnTimer -= delta;
        
        if( currentSpawnTimer <= 0 )
        {
            //Reset timer
            currentSpawnTimer = gFloatRand(minBaseSpawnTimer, maxBaseSpawnTimer);
            
            //Spawn the enemy the correct amount of times
            int spawnCount = gRangeRand(minCountPerSpawn, maxCountPerSpawn);
            for (int curSpawn = 0; curSpawn < spawnCount; ++curSpawn)
            {
                [self spawnEnemyAtPosition: ccp(-45, gRangeRand(70, 200) )];
            }
        }
    }
}


-(void) spawnEnemyAtPosition: (CGPoint) position
{
    NSAssert(NO,@"Must be overridden by child class");
}


@end
