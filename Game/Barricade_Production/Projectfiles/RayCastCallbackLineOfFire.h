//
//  RayCastCallback.h
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#ifndef Barricade_Production_RaycastCallbackLineOfFire_h
#define Barricade_Production_RaycastCallbackLineOfFire_h

#include "Box2D.h"

@class EnemyBase;
@class WeaponBase;
@class EnemyFieldInstance;

class RayCastCallbackLineOfFire : public b2RayCastCallback
{
public:    
    RayCastCallbackLineOfFire();
    

    //Methods
    float32 ReportFixture(	b2Fixture* fixture, 
                            const b2Vec2& point,
                            const b2Vec2& normal, 
                            float32 fraction );
    
    void ApplyRaycastReaction( WeaponBase* firingWeaponRefIn );
    
    //Accessors
    EnemyFieldInstance* getFrontmostEnemyFieldInstanceTarget(void) { return frontmostEnemyFieldInstance; }
    CGPoint getIntersectionPoint(void) { return intersectionPoint; }
    NSArray* getIntersectedEnemiesList(void) { return intersectedEnemies; }
    
private:    
    //Weak reference to the front-most EnemyFieldInstance
    EnemyFieldInstance* frontmostEnemyFieldInstance;
    
    //Intersection point where the line hit the enemy
    CGPoint intersectionPoint;
    
    //Reference list of all intersected enemies
    NSMutableArray* intersectedEnemies;
    
    //Debug data flag
    BOOL printDebugData;
};


#endif
