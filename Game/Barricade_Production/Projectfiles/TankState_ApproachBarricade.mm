//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TankState_ApproachBarricade.h"
#import "GB2Config.h"
#import "GB2Engine.h"
#import "EnemyTank.h"
#import "CCAnimation+SequenceLoader.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"

@implementation TankState_ApproachBarricade
-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyTank class]],@"Wrong enemy parent type");
        enemyParent = (EnemyTank*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyTankWalk00.png"];
    
    //Load animation
    const short animationFrameCount = 8;
    float animationDelay = (20 / enemyParent.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyTankWalk%02d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    repeatingWalkingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody
                                           forShapeName:@"enemyTankWalkShape"];
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyTankWalkShape"]
     ];
    
    //Load the sprite
    [enemyParent addChild:enemyParent.EnemySprite];
    
    //Animate sprite
    [enemyParent.EnemySprite runAction:repeatingWalkingAnimationAction];
}

-(void) exit
{
    [enemyParent stopAction:repeatingWalkingAnimationAction];
    [enemyParent removeChild:enemyParent.EnemySprite cleanup:YES];
    [[GB2Engine sharedInstance] addBodyToPostWorldStepCompleteFixtureRemovalList:enemyParent.EnemyPhysicsBody];
}

-(void) update: (float) deltaTime
{
    //Move forward
    CGPoint newPosition = enemyParent.position;
    
    newPosition.x += enemyParent.MovementSpeed * deltaTime;
    
    [enemyParent setPosition:newPosition];
}

-(void) beginContactWithBarricadeSegment: (GB2Contact*)contact
{
    //We MUST check if we are still alive here...
    //This was a nasty bug to find. Remember we don't transition to another state until the next frame.
    //Because of this, it's not safe to assume that we are in the approach state and we can simply switch
    //to attack state. Making this assumption is deadly because if we are queued for the death state, asking
    //for a attack transition here will leave us with an enemy at 0HP that can attack the barricade, hence
    //the enemy will never die because it's already dead but in a living state.... A ZOMBIE!!! D:
    if( enemyParent.CurrentHealth > 0 )
    {
        [enemyParent changeEnemyState: [enemyParent.AI_StateFactory createTankState_AttackBarricade] ];
    }
}

-(void) endContactWithBarricadeSegment: (GB2Contact*)contact
{
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    [enemyParent runAction:[CCMoveBy actionWithDuration:0 position:ccp(-firingWeapon.Damage, 0)]];
}
@end
