//
//  Box2dPhysicsWorld.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

class b2World;

class Box2dPhysicsWorld
{
public:
    //Instance Control
    static Box2dPhysicsWorld* getInstance(void);
    void PurgeInstance(void);
    
    //Accessors
    inline const b2World* getPhysicsWorld() { return gamePhysicsWorld; }
        
private:
    //Memebers
    static Box2dPhysicsWorld* instance;
    b2World* gamePhysicsWorld;
    
    //Methods
    Box2dPhysicsWorld();
    void init(void);
    void shutdown(void);
};