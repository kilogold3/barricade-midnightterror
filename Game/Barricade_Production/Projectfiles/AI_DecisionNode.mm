//
//  AI_DecisionNode.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/18/13.
//
//

#import "AI_DecisionNode.h"

@implementation AI_DecisionNode
-(void) doAction
{
    if( YES == decisionBlock() )
    {
        [trueNode doAction];
    }
    else
    {
        [falseNode doAction];
    }
}

-(id) initWithTrueNode: (AI_TreeNode*) trueNodeIn
             FalseNode: (AI_TreeNode*) falseNodeIn
         DecisionBlock: (DecisionBlock) decisionBlockIn
{
    self = [super init];
    
    if( nil != self )
    {
        trueNode = trueNodeIn;
        [trueNode retain];
        falseNode = falseNodeIn;
        [falseNode retain];
        
        decisionBlock = [decisionBlockIn copy]; //increases retain count
    }
    
    return self;
}

-(void) dealloc
{
    [super dealloc];
}

-(void) cleanup
{
    [trueNode cleanup];
    [trueNode release];
    
    [falseNode cleanup];
    [falseNode release];
    
    [decisionBlock release];

}

@end
