//
//  RayCastCallback.cpp
//  PhysicsEditor_BarricadeTest
//
//  Created by Kelvin Bonilla on 9/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RayCastCallbackLineOfFire.h"
#import "EnemyBase.h"
#import "EnemyFieldInstance.h"
#import "EnemyWalker.h"
#import "GB2Engine.h"

RayCastCallbackLineOfFire::RayCastCallbackLineOfFire()
{
    frontmostEnemyFieldInstance = nil;
    intersectedEnemies = [NSMutableArray array];
    
    //debug
    if( [KKConfig selectKeyPath:@"KKStartupConfig"] )
    {
        printDebugData = [KKConfig boolForKey:@"EnableEnemyHitDebugOutput"];
    }
}

float32 RayCastCallbackLineOfFire::ReportFixture( b2Fixture* fixture,
                                        const b2Vec2& point,
                                        const b2Vec2& normal, 
                                        float32 fraction )
{
    //Acquire the body.
    b2Body* fixtureBody = fixture->GetBody();
    void* userData = fixtureBody->GetUserData();
    
    if( NULL != userData )
    {
        //If the fixture's body is an enemy field instance...
        if([((id)userData) isKindOfClass:[EnemyFieldInstance class]])
        {
            //If the fixture's body is the enemy's...
            if([((id)userData) isKindOfClass:[EnemyBase class]])
            {
                /***************************************************************************
                Normally we would stop raycasting here by doing a "return 0",
                but we want to poll every item the line has crossed in order to
                identify the closest one to the player. This way, we avoid hitting
                another enemy behind our target.
                
                The is probably an easier way to do this according to reference, except in 
                our case we can't take any shortcuts (returning fraction values), because we 
                need to keep cycling through all the targets to store them in a list.
                Here's the info anyways:
                
                To find only the closest intersection:
                --- return the fraction value from the callback
                --- use the most recent intersection as the result
                
                To find all intersections along the ray:
                --- return 1 from the callback
                --- store the intersections in a list
                
                To simply find if the ray hits anything:
                --- if you get a callback, something was hit (but it may not be the closest)
                --- return 0 from the callback for efficiency
                
                (source: http://www.iforce2d.net/b2dtut/world-querying )
                
                *****************************************************************************/
                
                //Add the enemy to the list
                [intersectedEnemies addObject:(id)userData];
                
                //If we have a field instance...
                if( frontmostEnemyFieldInstance != nil )
                {
                    //Check if new target is closer than the current one.
                    EnemyFieldInstance* newTarget = (EnemyFieldInstance*)userData;
                    
                    if( frontmostEnemyFieldInstance.position.x < newTarget.position.x )
                    {
                        frontmostEnemyFieldInstance = (EnemyFieldInstance*)userData;
                        intersectionPoint = CGPointFromb2Vec2(point);
                    }
                }
                else
                {
                    frontmostEnemyFieldInstance = (EnemyFieldInstance*)userData;
                    intersectionPoint = CGPointFromb2Vec2(point);
                }
            }
        }
//      else
//      {
//          if(YES == printDebugData)
//          {
//              NSString* className = NSStringFromClass( [ ( (id) userData ) class] );
//              CCLOG( @"Found unregistered raycast hit for: %@", className );
//          }
//      }
    }
    else
    {
        CCLOG(@"Raycast UserData reference is NULL. Consider verifying algorithm");
    }
    
    //Keep searching...
    return 1;
    
}

void RayCastCallbackLineOfFire::ApplyRaycastReaction( WeaponBase* firingWeaponRefIn )
{
    if( frontmostEnemyFieldInstance != nil )
    {
        
        //If field instance is an enemy...
        if([frontmostEnemyFieldInstance isKindOfClass:[EnemyBase class]])
        {

            if(YES == printDebugData)
            {
                CCLOG( @"ENEMY HIT!" );
            }
            
            EnemyBase* gameEnemy = ((EnemyBase*)frontmostEnemyFieldInstance);
            [gameEnemy reactToWeaponDamage: firingWeaponRefIn];
        }
        else
        {
            CCLOG( @"Error: Unidentified Raycast Target" );
        }
    }
}
