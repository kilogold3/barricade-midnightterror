//
//  EnemyStateFactory.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyStateFactory.h"

#import "WalkerState_ApproachBarricade.h"
#import "WalkerState_ApproachPlayer.h"
#import "WalkerState_Death.h"
#import "WalkerState_AttackBarricade.h"
#import "WalkerState_Blank.h"

#import "VejiganteState_ApproachBarricade.h"
#import "VejiganteState_ApproachPlayer.h"
#import "VejiganteState_Spawn.h"

#import "MechaJunaArmState_Attack.h"
#import "MechaJunaArmState_Idle.h"
#import "MechaJunaArmState_Dead.h"
#import "MechaJunaCannonState_Fire.h"
#import "MechaJunaCannonState_Idle.h"
#import "MechaJunaCannonState_Dead.h"

#import "RampageJunaState_ApproachBarricade.h"
#import "RampageJunaState_AttackedByJerome.h"

#import "WoundedJunaState_ApproachLeo.h"
#import "WoundedJunaState_AttackedByJohnson.h"
#import "WoundedJunaState_DeathShock.h"
#import "WoundedJunaState_Dead.h"

#import "TankState_ApproachBarricade.h"
#import "TankState_ApproachPlayer.h"
#import "TankState_Death.h"
#import "TankState_AttackBarricade.h"

#import "FlierState_Spawn.h"
#import "FlierState_Death.h"

#import "RunnerState_ApproachBarricade.h"
#import "RunnerState_ApproachPlayer.h"
#import "RunnerState_Death.h"
#import "RunnerState_AttackBarricade.h"
#import "RunnerState_Blank.h"

@implementation EnemyStateFactory

-(id) initForEnemy: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil )
    {
        enemyParent = enemyParentIn;
    }
    return self;
}

-(EnemyState*) createWalkerState_ApproachBarricade
{
    WalkerState_ApproachBarricade* newState = [[[WalkerState_ApproachBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWalkerState_ApproachPlayer
{
    WalkerState_ApproachPlayer* newState = [[[WalkerState_ApproachPlayer alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWalkerState_Death
{
    WalkerState_Death* newState = [[[WalkerState_Death alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWalkerState_AttackBarricade
{
    WalkerState_AttackBarricade* newState = [[[WalkerState_AttackBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWalkerState_Blank
{
    WalkerState_Blank* newState = [[[WalkerState_Blank alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createVejiganteState_ApproachBarricade
{
    VejiganteState_ApproachBarricade* newState = [[[VejiganteState_ApproachBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createVejiganteState_ApproachPlayer
{
    VejiganteState_ApproachPlayer* newState = [[[VejiganteState_ApproachPlayer alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createVejiganteState_Spawn
{
    VejiganteState_Spawn* newState = [[[VejiganteState_Spawn alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaArmState_Attack
{
    MechaJunaArmState_Attack* newState = [[[MechaJunaArmState_Attack alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaArmState_Idle
{
    MechaJunaArmState_Idle* newState = [[[MechaJunaArmState_Idle alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaArmState_Dead
{
    MechaJunaArmState_Dead* newState = [[[MechaJunaArmState_Dead alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaCannonState_Idle
{
    MechaJunaCannonState_Idle* newState = [[[MechaJunaCannonState_Idle alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaCannonState_Fire
{
    MechaJunaCannonState_Fire* newState = [[[MechaJunaCannonState_Fire alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createMechaJunaCannonState_Dead
{
    MechaJunaCannonState_Dead* newState = [[[MechaJunaCannonState_Dead alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRampageJunaState_ApproachBarricade
{
    RampageJunaState_ApproachBarricade* newState = [[[RampageJunaState_ApproachBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRampageJunaState_AttackedByJerome
{
    RampageJunaState_AttackedByJerome* newState = [[[RampageJunaState_AttackedByJerome alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWoundedJunaState_ApproachLeo
{
    WoundedJunaState_ApproachLeo* newState = [[[WoundedJunaState_ApproachLeo alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWoundedJunaState_AttackedByJohnson
{
    WoundedJunaState_AttackedByJohnson* newState = [[[WoundedJunaState_AttackedByJohnson alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWoundedJunaState_DeathShock
{
    WoundedJunaState_DeathShock* newState = [[[WoundedJunaState_DeathShock alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createWoundedJunaState_Dead
{
    WoundedJunaState_Dead* newState = [[[WoundedJunaState_Dead alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createTankState_ApproachPlayer
{
    TankState_ApproachPlayer* newState = [[[TankState_ApproachPlayer alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createTankState_ApproachBarricade
{
    TankState_ApproachBarricade* newState = [[[TankState_ApproachBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createTankState_AttackBarricade
{
    TankState_AttackBarricade* newState = [[[TankState_AttackBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createTankState_Death
{
    TankState_Death* newState = [[[TankState_Death alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createFlierState_Spawn
{
    FlierState_Spawn* newState = [[[FlierState_Spawn alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createFlierState_Death
{
    FlierState_Death* newState = [[[FlierState_Death alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRunnerState_ApproachBarricade
{
    RunnerState_ApproachBarricade* newState = [[[RunnerState_ApproachBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRunnerState_ApproachPlayer
{
    RunnerState_ApproachPlayer* newState = [[[RunnerState_ApproachPlayer alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRunnerState_Death
{
    RunnerState_Death* newState = [[[RunnerState_Death alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRunnerState_AttackBarricade
{
    RunnerState_AttackBarricade* newState = [[[RunnerState_AttackBarricade alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}

-(EnemyState*) createRunnerState_Blank
{
    RunnerState_Blank* newState = [[[RunnerState_Blank alloc] initWithEnemyParent:enemyParent] autorelease];
    return newState;
}
@end
