//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyVejigante;

@interface VejiganteState_ApproachBarricade : NSObject<IEnemyState>
{
    EnemyVejigante* enemyParentRef;
    CCLayer* screenForegroundLayerRef;
    CCLayerColor* screenFlashEffectLayer;

    //Keeps track of arrival to barricade in order to avoid
    //performing the same AI switching of states multiple times.
    BOOL hasArrivedAtBarricade;
    
    //keeps track of how many time the screen face flash has occured
    Byte faceFlashCount;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
@end
