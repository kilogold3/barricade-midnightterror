//
//  EnemyFactory.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyFactory.h"
#import "EnemyWalker.h"
#import "EnemyVejigante.h"
#import "EnemyStateFactory.h"

@implementation EnemyFactory

-(id) initWithDependencies
{
    self = [super init];
    
    if( self != nil )
    {
    }
    
    return self;
}

-(void) dealloc
{
    [super dealloc];
}

-(EnemyBase*) createWalker
{
    return [EnemyWalker node];
}

-(EnemyBase*) createVejigante
{
    return [EnemyVejigante node];
}

-(EnemyBase*) createEnemyFromStringType: (NSString*) enemyType
{
    return [NSClassFromString(enemyType) node];
}



@end
