//
//  TestScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "BarricadeRepairLayer.h"
#import "GameplayScene.h"
#import "NotificationCenterEventList.h"
#import "GB2ShapeCache.h"
#import "BarricadeRepairFragment.h"
#import "NSMutableArray+Shuffling.h"
#import "Box2DDebugLayer.h"
#import "GB2Engine.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"

@implementation BarricadeRepairLayer

-(void) enableBox2dDebugDrawing
{
	// Using John Wordsworth's Box2DDebugLayer class now
	// The advantage is that it draws the debug information over the normal cocos2d graphics,
	// so you'll still see the textures of each object.
    
	UInt32 debugDrawFlags = 0;
	debugDrawFlags += b2Draw::e_shapeBit;
	debugDrawFlags += b2Draw::e_jointBit;
	//debugDrawFlags += b2Draw::e_aabbBit;
	//debugDrawFlags += b2Draw::e_pairBit;
	//debugDrawFlags += b2Draw::e_centerOfMassBit;
    
    Box2DDebugLayer* debugLayer = [Box2DDebugLayer debugLayerWithWorld:[GB2Engine sharedInstance].world
                                                              ptmRatio:PTM_RATIO
                                                                 flags:debugDrawFlags];
    [self addChild:debugLayer z:100];
}

-(id) init
{
	if ((self = [super init]))
	{
        [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"shapes.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"barricadeRepair.plist"];

        REGISTER_EVENT(self,
                       @selector(BarricadeRepairBlueprintLoaded_EventHandler:),
                       EventList::BARRICADE_REPAIR_BLUEPRINT_LOADED)
        REGISTER_EVENT(self,
                       @selector(BarricadeRepairFullyRepaired_EventHandler:),
                       EventList::BARRICADE_REPAIR_FULLY_REPAIRED);        
    }
    
    return self;
}

-(void) dealloc
{
    UNREGISTER_EVENT(self, EventList::BARRICADE_REPAIR_BLUEPRINT_LOADED);
    UNREGISTER_EVENT(self, EventList::BARRICADE_REPAIR_FULLY_REPAIRED);

    [super dealloc];
}

-(void) QuitBarricadeRepair: (id) sender
{
    SEND_EVENT(EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY, nil);
    
    [self removeFromParentAndCleanup:YES];
}

-(void) didLoadFromCCB
{
    //Disable repair prompt layer by default
    barricadeRepairedPromptNode.visible = NO;
    
    
    //We have now loaded the scene, now we have to configure all the
    //fragments with their respective tag ids. We do this by first generating
    //a list of 20 unique random values from 0-19. These will represent the tags.
    NSMutableArray* tagValues = [NSMutableArray arrayWithCapacity:barricadeFragmentsLayer.children.count];
    for (NSUInteger currentTag = 0; currentTag < barricadeFragmentsLayer.children.count; ++currentTag)
    {
        [tagValues addObject: [NSNumber numberWithInt:currentTag]];
    }
    
    //Shuffle the list
    [tagValues shuffle];
    
    //Now we have an unordered list. Let's start assigning the tag values and initializing fragments
    for (NSUInteger currentTag = 0; currentTag < barricadeFragmentsLayer.children.count; ++currentTag)
    {
        BarricadeRepairFragment* currentFragment = [barricadeFragmentsLayer.children objectAtIndex:currentTag];
        [currentFragment configureFragmentWithTagID:[[tagValues objectAtIndex:currentTag] intValue]];
    }
    
    //Animate instructions
    const float FADE_TIME = 0.8f;
    const Byte MIN_OPACITY = 100;
    const Byte MAX_OPACITY = 255;
    instructionsLabel.opacity = 0;
    CCFadeTo* fadeInToAction = [CCFadeTo actionWithDuration:FADE_TIME opacity:MAX_OPACITY];
    CCFadeTo* fadeOutToAction = [CCFadeTo actionWithDuration:FADE_TIME opacity:MIN_OPACITY];
    CCSequence* sequenceAction = [CCSequence actions:
                                  fadeInToAction,
                                  fadeOutToAction,
                                  nil];
    [instructionsLabel runAction: [CCRepeatForever actionWithAction:sequenceAction] ];
    
    //Draw debug data
    [KKConfig selectKeyPath:@"KKStartupConfig"];
    if( [KKConfig boolForKey:@"EnableCollisionDebugDraw"] )
    {
        [self enableBox2dDebugDrawing];
    }
}

-(void) cleanup
{
    [super cleanup];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"barricadeRepair.plist"];
}

-(void) BarricadeRepairBlueprintLoaded_EventHandler: (NSNotification*) notification
{
    //Now that the barricade is loaded, we verify if the barricade is at full health.
    const short barricadeHealth = [[[[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE] objectForKey:GSP::BARRICADE_HEALTH] intValue];
    
    //If it is... 
    if(barricadeHealth >= 100)
    {
        //We don't need to have stray pieces on the board. Let's clear the whole thing out.
        [barricadeFragmentsLayer removeAllChildrenWithCleanup:YES];
        
        //and activate the "Barricade Repaired" prompt.
        //(Let's call it directly. No need for the event)
        [self BarricadeRepairFullyRepaired_EventHandler:nil];
    }
    else
    {
        NSArray* usedFragmentIDs = [notification object];
        
        for (NSNumber* curFragmentID in usedFragmentIDs)
        {
            CCNode* fragmentToBeRemoved = [barricadeFragmentsLayer getChildByTag:curFragmentID.intValue];
            [barricadeFragmentsLayer removeChild:fragmentToBeRemoved cleanup:YES];
        }
    }
}

-(void) FreezeActivity_EventHandler:(NSNotification*) notification
{
    //Disable the fragments
    for( BarricadeRepairFragment* repairFragment in barricadeFragmentsLayer.children )
    {
        repairFragment.touchEnabled = NO;
        [repairFragment resetFragmentToOriginalState];
    }
    
    //Disable the continue button
    buttonToGameplay.enabled = NO;
}

-(void) BarricadeRepairFullyRepaired_EventHandler:(NSNotification*)notification
{
    //Disable the fragments if there are any...
    for( BarricadeRepairFragment* repairFragment in barricadeFragmentsLayer.children )
    {
        repairFragment.touchEnabled = NO;
        [repairFragment resetFragmentToOriginalState];
    }
    
    //Present the prompt
    barricadeRepairedPromptNode.visible = YES;
    [instructionsLabel stopAllActions];
    instructionsLabel.visible = NO;
}


@end
