//
//  OverdriveAnimationJeromeV1.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/14/13.
//
//

#import <Foundation/Foundation.h>
#import "CCBAnimationManager.h"
#import "OverdriveExecutionProtocol.h"

@interface OverdriveSequence : CCLayer <CCBAnimationManagerDelegate>
{
    OverdriveExecutionObject* overdriveExecution;
}
@property (assign) OverdriveExecutionObject* OverdriveExecution;
@end
