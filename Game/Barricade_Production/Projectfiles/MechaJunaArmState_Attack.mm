//
//  MechaJunaState_AttackWithRightArm.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/19/13.
//
//

#import "MechaJunaArmState_Attack.h"
#import "EnemyMechaJuna.h"
#import "SimpleAudioEngine.h"

@implementation MechaJunaArmState_Attack

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyMechaJunaArm class]],@"Wrong enemy parent type");

        armRef = static_cast<EnemyMechaJunaArm*>(enemyParentIn);
    }
    
    return self;
}

-(void) enter
{
    hasLaunched = NO;
    isReturning = NO;
}

-(void) exit
{
    
}

-(void) update: (float) deltaTime
{
    CGPoint currentTrajectory;
    
    if( YES == hasLaunched and NO == isReturning )
    {
        currentTrajectory = armRef.DestinationPosition;
    }
    else
    if( YES == hasLaunched and YES == isReturning )
    {
        currentTrajectory = armRef.OriginalPosition;
    }
    else
    if( NO == hasLaunched )
    {
        currentTrajectory = armRef.DestinationPosition;
        hasLaunched = YES;
        [[SimpleAudioEngine sharedEngine] playEffect:@"GP_SFX_RocketLaunch.wav"];
    }
    else
    {
        NSAssert(NO, @"Missing conditional case. Rethink Logic.");
    }
    
    //Perform movement
    [self stepToLocation: currentTrajectory
           withDeltaTime: deltaTime];
    
    //If at origin,
    if( YES == isReturning && 1.0f > ccpDistance( armRef.position, armRef.OriginalPosition ) )
    {
        //Let's reset the values
        isReturning = NO;
        hasLaunched = NO;        
    }
    
    //If at destination,
    else if(NO == isReturning && 1.0f > ccpDistance( armRef.position, armRef.DestinationPosition ) )
    {
        //Let's reset the values
        isReturning = YES;
    }
}


-(void) stepToLocation: (CGPoint) location
              byAmount: (Byte) amount
{
    //Grab destination position
    CGPoint destPos = location;
    
    //Grab self position
    CGPoint selfPosition = armRef.position;
    
    //Form a line from enemy to player
    CGPoint direction = ccpSub(destPos, selfPosition);
    
    //Normalize for direction
    CGPoint normailzedDirection = ccpNormalize(direction);
    
    //create amount offset
    CGPoint offset = {  amount * ((normailzedDirection.x > 0) ? 1 : -1) ,
                        amount * ((normailzedDirection.y > 0) ? 1 : -1) };
    
    //apply offset to position
    CGPoint newPosition = { armRef.position.x + offset.x ,
                            armRef.position.y + offset.y };
    
    //Set new position
    [armRef setPosition:newPosition];
}

-(void) stepToLocation: (CGPoint) location
         withDeltaTime: (float) deltaTime
{
    //Grab destination position
    CGPoint destPos = location;
    
    //Grab self position
    CGPoint selfPosition = armRef.position;
    
    //Form a line from enemy to player
    CGPoint direction = ccpSub(destPos, selfPosition);
    
    //Normalize for direction
    CGPoint normailzedDirection = ccpNormalize(direction);
    
    //create velocity
    CGPoint velocity = {    normailzedDirection.x * armRef.ArmMovementSpeed * deltaTime ,
                            normailzedDirection.y * armRef.ArmMovementSpeed * deltaTime };
    
    //apply velocity to position
    CGPoint newPosition = { armRef.position.x + velocity.x ,
                            armRef.position.y + velocity.y };
    
    //Set new position
    [armRef setPosition:newPosition];
}


@end
