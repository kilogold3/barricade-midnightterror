//
//  GameOverSceneChupacabrasLoop.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/17/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameOverSceneChupacabrasLoop.h"

@interface GameOverSceneChupacabrasLoop (PrivateMethods)
// declare private methods here
@end

@implementation GameOverSceneChupacabrasLoop

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    
    CCActionInterval* waveAction = [CCWaves3D actionWithDuration:20.f
                                                    size:CGSizeMake(5, 2)
                                                   waves:8
                                               amplitude:19.0f];
    
    [gameOverBannerSprite runAction: [CCRepeatForever actionWithAction:waveAction]];
}

@end
