//
//  GameIntroScene.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameIntroScene.h"
#import "CCBReader.h"
#import "StoryScene.h"

@interface GameIntroScene (PrivateMethods)
// declare private methods here
@end

@implementation GameIntroScene

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		[self scheduleUpdate];
        
        introLayersAnimation = [CCBReader nodeGraphFromFile:@"GameIntro.ccbi"];
        [self addChild:introLayersAnimation];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

// scheduled update method
-(void) update:(ccTime)delta
{
    if(
    [[KKInput sharedInput] isAnyTouchOnNode:introLayersAnimation
                                 touchPhase:KKTouchPhaseEnded] )
    {
        [[CCDirector sharedDirector] replaceScene:[StoryScene node]];
    }
}

@end
