//
//  AppWidescreener.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/8/13.
//
//

#import "AppWidescreener.h"
#import "kobold2d.h"

@implementation AppWidescreener

+(void) applyLetterboxToScene: (CCNode*) scene
{
    //Only do this if widescreen is present.
    if( YES == isWidescreenEnabled() )
    {
        const UInt16 BLACK_BORDER_WIDTH = 120;
        const CGSize SCREEN_SIZE = [CCDirector sharedDirector].screenSize;
        const ccColor3B BORDER_COLOR = ccBLACK;
        const float OPACTIY = 255;
        
        CCSprite* layerBorderLeft = [CCSprite spriteWithFile:@"MagicPixel.png"];
        layerBorderLeft.color = BORDER_COLOR;
        layerBorderLeft.scaleX = BLACK_BORDER_WIDTH + 1; //Just a pixel-worth of padding.
        layerBorderLeft.scaleY = SCREEN_SIZE.height;
        layerBorderLeft.anchorPoint = CGPointZero;
        layerBorderLeft.position = ccp(-BLACK_BORDER_WIDTH,0);
        layerBorderLeft.opacity = OPACTIY;
        
        CCSprite* layerBorderRight = [CCSprite spriteWithFile:@"MagicPixel.png"];
        layerBorderRight.color = BORDER_COLOR;
        layerBorderRight.scaleX = BLACK_BORDER_WIDTH + 1; //Just a pixel-worth of padding.
        layerBorderRight.scaleY = SCREEN_SIZE.height;
        layerBorderRight.anchorPoint = CGPointZero;
        layerBorderRight.position = ccp(480,0);
        layerBorderRight.opacity = OPACTIY;
        
        [scene addChild:layerBorderRight z:99];
        [scene addChild:layerBorderLeft z:99];
        
        [AppWidescreener centralizeSceneOnScreen:scene];
    }
}

+(CGSize) getSceneContentSize
{
    return CGSizeMake(480, 320);
}

+(CGPoint) getWidescreenLetterboxOffset
{
    if( [AppWidescreener isWidescreenEnabled] )
    {
        return ccp(44,0);
    }
    else
    {
        return CGPointZero;
    }
}

+(void) centralizeSceneOnScreen: (CCNode*) scene
{
    if( YES == [AppWidescreener isWidescreenEnabled] )
    {
        scene.position = [AppWidescreener getWidescreenLetterboxOffset];
    }
}

+(BOOL) isWidescreenEnabled
{
    return isWidescreenEnabled();
}


@end
