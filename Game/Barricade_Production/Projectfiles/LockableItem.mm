//
//  LockedItem.mm
//  Barricade_Production
//
//  Created by Jeb Khabir on 7/11/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "LockableItem.h"

const SignedByte UNINITIALIZED_TAP_COUNT = -1;

@implementation LockableItem
@synthesize LockedItemSpriteID=lockedItemSpriteID;
@synthesize UnlockedItemSpriteID=unlockedItemSpriteID;
@synthesize OpenItemSpriteID=openItemSpriteID;
@synthesize IsLocked=isLocked;
@synthesize TapsToUnlock=tapsToUnlock;

-(id) init
{
    if( self = [super init] )
    {
        tapsToUnlock = UNINITIALIZED_TAP_COUNT;
    }
    
    return self;
}

-(void) Interact
{
    NSAssert(NO, @"Method call reserved only for child classes");
}

@end
