//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VejiganteState_Spawn.h"
#import "GB2ShapeCache.h"
#import "NotificationCenterEventList.h"
#import "EnemyVejigante.h"
#import "EnemyStateFactory.h"
#import "Player.h"

@implementation VejiganteState_Spawn

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyVejigante class]],@"Wrong enemy parent type");
        enemyParentRef = (EnemyVejigante*) enemyParentIn;
        
        //Acquire GameplayScene's ScreenForegroundLayer
        screenForegroundLayerRef = nil;
        NSValue* screenForegroundLayerRefValue = [NSValue valueWithPointer:&screenForegroundLayerRef];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAMEPLAYSCENE_SCREENFOREGROUNDLAYER
                                                            object: screenForegroundLayerRefValue];
        NSAssert(screenForegroundLayerRef != nil, @"Failed to acquire ScreenForegroundLayer");
        
        
        //Create an invisible black screen first
        screenFlashEffectLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 0)
                                                        width:[CCDirector sharedDirector].screenSize.width
                                                       height:[CCDirector sharedDirector].screenSize.height];
        
        [screenForegroundLayerRef addChild:screenFlashEffectLayer];
    }
    
    return self;
}

-(void) enter
{
    //Change the vejigante sprite frame
    [enemyParentRef.VejiganteSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"VegifloatSprite0.png"]];
    
    //Set anchor point
    [enemyParentRef.VejiganteSprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyVejiganteShape"]
     ];
    
    //setup fadeout
    CCDelayTime* suspenseDelay = [CCDelayTime actionWithDuration:0.65f];
    
    CCCallBlock* addBlackout = [CCCallBlock actionWithBlock:^{
        
        [screenFlashEffectLayer setOpacity:255];
        [screenFlashEffectLayer setPosition:CGPointZero];
        [screenFlashEffectLayer setAnchorPoint:CGPointZero];        
    }];
    
    CCFadeOut* fadeDarkness = [CCFadeOut actionWithDuration:2.5f];
    
    CCCallBlock* cleanupFadeMemory = [CCCallBlock actionWithBlock:^{
        [screenForegroundLayerRef removeChild:screenFlashEffectLayer cleanup:YES];
    }];
    
    CCCallBlock* changeState = [CCCallBlock actionWithBlock:^{
        [enemyParentRef changeEnemyState:[enemyParentRef.AI_StateFactory createVejiganteState_ApproachBarricade]];
    }];
    
    CCSpawn* fadeDarknessAndChangeState = [CCSpawn actionOne:fadeDarkness two:changeState];
    
    CCSequence* actionSequence = [CCSequence actions:suspenseDelay, addBlackout, fadeDarknessAndChangeState, cleanupFadeMemory, nil];
    
    //Kill all enemies before running the action
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_JEROME_DAMAGE_ENEMIES
                                                        object:nil];
    
    //To avoid having the player use overdrive to screw us up, we'll just set overdrive to zero
    enemyParentRef.PlayerRef.CurrentOverdrivePercentage = 0;
    
    [screenFlashEffectLayer runAction: actionSequence];
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}
@end
