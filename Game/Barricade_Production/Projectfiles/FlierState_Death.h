//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyFlier;

@interface FlierState_Death : NSObject<IEnemyState>
{
    EnemyFlier* enemyParentRef;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

@end
