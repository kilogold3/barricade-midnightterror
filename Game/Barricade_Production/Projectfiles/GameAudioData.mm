//
//  GameAudioData.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 4/29/13.
//
//

#import "GameAudioData.h"
#import "CDAudioManager.h"
#import "GMath.h"

@implementation GameAudioData
@synthesize cdAudioManager = audioManager;

//Singleton
static GameAudioData* gameAudioDataInstance = nil;
+ (GameAudioData *)sharedGameAudioData
{
   	if (!gameAudioDataInstance) {
        gameAudioDataInstance = [[self alloc] init];
	}
    
	return gameAudioDataInstance;
}

-(void) dealloc
{
    for( int curIter = 0; curIter < MAX_SOUND_IDS; ++curIter )
    {
        [soundIdSlots[curIter] release];
    }
    
    [gameAudioDataInstance release];
    gameAudioDataInstance = nil;
    [super dealloc];
}

//Class methods
-(id) init
{
    self = [super init];
    
    if( nil != self)
    {
        return self;
        //////////////
        //////////////
        //////////////
        
        CDSoundEngine *sse = [CDAudioManager sharedManager].soundEngine;
        
        /**
         A source group is another name for a channel
         Here I have 2 channels, the first index allows for only a single effect... my background music
         The second channel I have reserved for my sound effects.  This is set to 31 because you can
         have up to 32 effects at once
         */
        NSArray *sourceGroups = [NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:31], nil];
        [sse defineSourceGroups:sourceGroups];
        
        //Initialise audio manager asynchronously as it can take a few seconds
        /** Different modes of the engine
         typedef enum {
         kAMM_FxOnly,                       //!Other apps will be able to play audio
         kAMM_FxPlusMusic,                  //!Only this app will play audio
         kAMM_FxPlusMusicIfNoOtherAudio,	//!If another app is playing audio at start up then allow it to continue and don't play music
         kAMM_MediaPlayback,				//!This app takes over audio e.g music player app
         kAMM_PlayAndRecord                 //!App takes over audio and has input and output
         } tAudioManagerMode;*/
        [CDAudioManager initAsynchronously:kAMM_FxPlusMusic];
        
        //grab a hold of the singleton instance
        audioManager = [CDAudioManager sharedManager];
        
        //init the array
        memset(soundIdSlots, 0, sizeof(NSDate*) * MAX_SOUND_IDS);
        
        //Debug
        if( [KKConfig selectKeyPath:@"KKStartupConfig"] )
        {
            printDebugData = [KKConfig boolForKey:@"EnableGameAudioDebugOutput"];
        }
    }
    
    return self;
}


-(void) updateLoadSoundBuffer: (NSString*) soundFile atSoundIdSlot: (int) slot
{
    soundIdSlots[slot] = [NSDate date];
    [soundIdSlots[slot] retain];
    
    [[CDAudioManager sharedManager].soundEngine loadBuffer:slot
                                                  filePath:soundFile];
}

-(ALuint) preloadAndGenerateNewSoundIdWithFile: (NSString*) filename
{

    //Let's scan the array and keep track of the oldest loaded sound...
    //If we get lucky, we may find an uninitialized slot. Otherwise,
    //we will have to remove the oldest sound and plug the new sound into
    //that now-vacant slot.
    
    int oldestSlotIndex = -1;
    
    //Then we cycle through all slots
    for( int curIter = 0; curIter < MAX_SOUND_IDS; ++curIter )
    {
        //If we have an uninitialized slot...
        if( nil == soundIdSlots[curIter] )
        {
            //Let's update the sound slot
            [self updateLoadSoundBuffer: filename
                          atSoundIdSlot: curIter];
            
            //Let's return the id
            if( YES == printDebugData )
            {
                CCLOG(@"New sound ID generated: [%i | %@]",curIter,filename);
            }
            return curIter;
        }
        
        //We did not find a vacant slot. Let's check if this is
        //the oldest slot.
        else
        {
            //If we don't have an "oldest slot" selected,
            //let's grab the curretn slot
            if( -1 == oldestSlotIndex )
            {
                oldestSlotIndex = curIter;
            }
            //Otherwise, let's compare the dates...
            else
            {
                //(Let's make this a bit more readable)
                NSDate* oldDate = soundIdSlots[oldestSlotIndex];
                NSDate* curIterDate = soundIdSlots[curIter];
                
                //If 'curIterDate' is older than 'oldDate' 
                if( curIterDate == [oldDate earlierDate:curIterDate] )
                {
                    //We have discovered an older date.
                    //Let's use this one instead
                    oldestSlotIndex = curIter;
                }
            }
        }
    }
    
    //If we are here, this means we did not find a vacant slot.
    //We will have to make one. Let's remove the current sound
    //in the old slot.
    [self unloadSoundId:oldestSlotIndex];
    
    //Let's add the new sound now and return the sound ID...
    
    //Update the sound slot
    [self updateLoadSoundBuffer: filename
                  atSoundIdSlot: oldestSlotIndex];
    
    //Let's return the id
    if( YES == printDebugData )
    {
        CCLOG(@"New sound ID generated: [%i | %@]",oldestSlotIndex,filename);
    }
    return oldestSlotIndex;
}

-(void) unloadSoundId: (ALuint) soundId
{    
    //Unload audio buffer
    [audioManager.soundEngine stopSound:soundId];
    [audioManager.soundEngine unloadBuffer:soundId];
    [soundIdSlots[soundId] release];
    
    if( YES == printDebugData )
    {
        CCLOG(@"Removing SoundID: %i", soundId);
    }
}

-(CDSoundSource*) getSoundSourceFromID: (ALuint) soundId
{
   return [audioManager.soundEngine soundSourceForSound:soundId
                                          sourceGroupId:kASC_Right];
}

-(void) playSound:(ALuint) soundId
{
    [audioManager.soundEngine playSound:soundId
                          sourceGroupId:kASC_Right
                                  pitch:1.0f
                                    pan:0
                                   gain:1.0f
                                   loop:NO];
}

-(void) playSound:(ALuint) soundId pitch:(float) pitch pan:(float) pan gain:(float) gain loop:(BOOL) loop
{
    [audioManager.soundEngine playSound:soundId
                          sourceGroupId:kASC_Right
                                  pitch:pitch
                                    pan:pan
                                   gain:gain
                                   loop:loop];
}



@end
