//
//  WeaponPistol.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/11/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "WeaponBase.h"

@class Player;
class RayCastCallbackLineOfFire;

@interface WeaponPistol : WeaponBase 
{
}

-(id) initWithDependencies: (Player*) gamePlayerRefIn;

@end
