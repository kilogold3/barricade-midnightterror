//
//  Aim.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Aim.h"
#import "Pair.h"
#import "Player.h"
#import "NotificationCenterEventList.h"

@interface Aim (Private)
-(void) calculateMovementEvent: (NSNotification*) notification;
-(void) processMovementEvent: (NSNotification*) notification;
@end

@implementation Aim
@synthesize AimMode=aimMode;

-(id) init
{
    self = [super init];
    
    if( nil != self )
    {
        aimMode = AIM_MODE_RELATIVE;
        screenSize = [CCDirector sharedDirector].screenSize;
        playerRef = nil;
        SEND_EVENT(EventList::COMPONENT_ACQUIRE_PLAYER_TARGET, [NSValue valueWithPointer:&playerRef]);
        NSAssert( nil != playerRef, @"PlayerRef nil. Player instance not found." );
        
        
        REGISTER_EVENT(self, @selector(GameplayPlayerMoved_EventHandler:), EventList::GAMEPLAY_PLAYER_MOVED);
        REGISTER_EVENT(self, @selector(calculateMovementEvent:), EventList::CONTROLS_NEW_AIM_DESTINATION);
        REGISTER_EVENT(self, @selector(acquireAim_EventHandler:), EventList::COMPONENT_ACQUIRE_AIM_TARGET);
        
        [self scheduleUpdate];
    }
    
    return self;
}

-(void) GameplayPlayerMoved_EventHandler: (NSNotification*) notification
{
    SEND_EVENT(EventList::PLAYER_NEW_AIM_DESTINATION, self );
}

-(void) update:(ccTime)delta
{
    //Make sure that the aim always stays in front of the player.
    //The player can move without manipulating the aim. For this reason, we keep
    //track of every frame to see if the player has moved in front of the aim.
    if( playerRef.position.x < self.position.x )
    {
        const CGPoint finalPosition = ccp(playerRef.position.x, self.position.y);

        self.position = finalPosition;

        SEND_EVENT(EventList::PLAYER_NEW_AIM_DESTINATION, self);
    }
}

-(void) calculateMovementEvent: (NSNotification*) notification
{
    KKTouch* eventArgs = [notification object];

    CGPoint finalPosition;
    
    //The touch panel only sends events when the touch location has changed
    //compared with the previous frame. Therefore, it is safe to assume that
    //we are only either beginning a touch, or moving it.
    
    //If we have a touch phase value...
    switch (aimMode)
    {
        case AIM_MODE_RELATIVE:
            //If touch is in the "Move" phase
            if ( KKTouchPhaseBegan != eventArgs.phase)
            {
                //Calculate realive aiming...
                CGPoint deltaPosition = ccpSub(eventArgs.location, eventArgs.previousLocation);
                finalPosition = ccpAdd(self.position, deltaPosition);
            }
            else
            {
                //We did not have a "Move" phase.
                //Ignore the location.
                return;
            }
            break;
            
        case AIM_MODE_ABSOLUTE:
            finalPosition = eventArgs.location;
            break;
  
        case AIM_MODE_OFFSET:
            finalPosition = eventArgs.location;
            finalPosition.y += 80;
            break;
    }
    
    //Clip aim to regions within the screen
    const short HUD_SPACE_BUFFER = 70;
    if( finalPosition.x > playerRef.position.x )    { finalPosition.x = playerRef.position.x; }
    if( finalPosition.x < 0 )                       { finalPosition.x = 0; }
    if( finalPosition.y > screenSize.height )       { finalPosition.y = screenSize.height; }
    if( finalPosition.y < HUD_SPACE_BUFFER )        { finalPosition.y = HUD_SPACE_BUFFER; }

    //Notify everyone (including ourselves) about the new position
    self.position = finalPosition;
    SEND_EVENT(EventList::PLAYER_NEW_AIM_DESTINATION, self);
}

-(void) cleanup
{
    UNREGISTER_EVENT(self, EventList::GAMEPLAY_PLAYER_MOVED);
    UNREGISTER_EVENT(self, EventList::CONTROLS_NEW_AIM_DESTINATION);
    UNREGISTER_EVENT(self, EventList::COMPONENT_ACQUIRE_AIM_TARGET);

    [super cleanup];
}

-(void) acquireAim_EventHandler: (NSNotification*) notification
{
    NSValue* enemyRefPointer = [notification object];
    Aim** targetPlayerRef = (Aim**)[enemyRefPointer pointerValue];
    (*targetPlayerRef) = self;
}


@end
