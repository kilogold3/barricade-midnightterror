//
//  HUD_DataDisplayStatsAmmo.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "HUD_DataDisplayStatsAmmo.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "Player.h"

@implementation HUD_DataDisplayStatsAmmo

-(void) setDataForWeapon: (WeaponBase*) weaponIn
{
    [self setDisplayFrame: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: weaponIn.HudDataDisplayWeaponSpriteID]];
}

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        REGISTER_EVENT(self, @selector(WeaponChanged_EventHandler:), EventList::CONTROLS_WEAPON_SELECTED)
    }
    
    return self;
}

-(void) cleanup
{
    [super cleanup];
    UNREGISTER_EVENT(self, EventList::CONTROLS_WEAPON_SELECTED)
}

-(void) WeaponChanged_EventHandler: (NSNotification*) notification
{
    Player* playerRef = nil;
    SEND_EVENT(EventList::COMPONENT_ACQUIRE_PLAYER_TARGET, [NSValue valueWithPointer:&playerRef])
    NSAssert( playerRef != nil, @"Player not found." );
    
    const int currentWeaponIndex = [[notification object] intValue];
    
    [self setDataForWeapon:[playerRef.WeaponsArray objectAtIndex:currentWeaponIndex]];
}

@end
