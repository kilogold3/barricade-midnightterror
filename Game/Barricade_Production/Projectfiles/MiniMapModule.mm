//
//  MiniMapModule.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "MiniMapModule.h"
#import "MiniMapModulePlayerData.h"
#import "NotificationCenterEventList.h"


@interface MiniMapModule (PrivateMethods)
// declare private methods here
-(int) generateNumericValueFromRoomString: (NSString*) roomIDString;
@end

@implementation MiniMapModule

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		//[self scheduleUpdate];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) didLoadFromCCB
{
    //Set position to location specified by CCB
    self.position = ccp(minimapLayerPositionX,minimapLayerPositionY);
    
    
    //Generate child tag for the correct room we are in
    NSString* const uninitializedValue = @"Uninitialized";
    NSMutableString* roomFilenameID = [NSMutableString stringWithString:uninitializedValue];
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_CURRENT_ROOM
                                                        object:roomFilenameID];
    NSAssert(NO == [roomFilenameID isEqualToString:uninitializedValue], @"Room ID is invalid");
    int roomIdNodeTag = [self generateNumericValueFromRoomString:roomFilenameID];
    
    
    //Cycle through all the children (CCLayerColor) setting the opacity to 0.
    //If we find the tagged child, we'll save it for later to animate.
    CCSprite* taggedNode = nil;
    for (CCSprite* curNode in self.children)
    {
        //If we are the player data, we should skip.
        //We don't want to mess with that here.
        if( [curNode isKindOfClass:[MiniMapModulePlayerData class]])
        {
            continue;
        }
        
        
        if( curNode.tag == roomIdNodeTag )
        {
            taggedNode = curNode;
        }
        else
        {
            [curNode setOpacity:0];
        }
    }
    NSAssert(nil != taggedNode, @"Room color layer not found");
    
    
    //Set the tagged node to alternate opacity
    const float fadeOpacity = 0.5f;
    CCFadeTo *fadeIn = [CCFadeTo actionWithDuration:fadeOpacity opacity:255];
    CCFadeTo *fadeOut = [CCFadeTo actionWithDuration:fadeOpacity opacity:0];
    CCSequence *pulseSequence = [CCSequence actionOne:fadeIn two:fadeOut];
    CCRepeatForever *repeat = [CCRepeatForever actionWithAction:pulseSequence];
    [taggedNode runAction:repeat];
}

-(int) generateNumericValueFromRoomString: (NSString*) roomIDString
{
    //Since we know out filename naming convention, we can get this in a snap.
    //Format is...
    //RG_[Progression][StoryPath][RoomID].ccbi
    //In this case, we only want RoomID.
    //RG_01A14.ccbi = 14
    
    NSString* returnStringBuffer = [NSString stringWithFormat:@"%c%c",
                                    [roomIDString characterAtIndex:6],
                                    [roomIDString characterAtIndex:7]];

    return [returnStringBuffer intValue];
}


@end
