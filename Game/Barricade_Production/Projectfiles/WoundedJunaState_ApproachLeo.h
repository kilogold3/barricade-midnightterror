//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyJuna;
@class Player;

@interface WoundedJunaState_ApproachLeo : NSObject<IEnemyState>
{
    CCAction* repeatingWalkingAnimationAction;
    EnemyJuna* enemyParent;
    Player* playerRef;
    CCLayer* screenForegroundLayerRef;
    CCLayer* gameplayForegroundLayerRef;
    CGPoint flinchFrameID;
    
    CCLayerColor* upperBar;
    CCLayerColor* lowerBar;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
@end
