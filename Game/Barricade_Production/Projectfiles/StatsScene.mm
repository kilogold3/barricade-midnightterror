//
//  StatsScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "StatsScene.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "CreditsScene.h"
#import "StatsSceneActor.h"
#import "CCBReader.h"
#import "AppWidescreener.h"

@implementation StatsScene

-(void) didLoadFromCCB
{
    // Setup a delegate method for the animationManager
    animationManagerRef = self.userObject;
    animationManagerRef.delegate = self;
    totalTimelineSequencesForScene = animationManagerRef.sequences.count;
    currentTimelineSequence = 0;
    
    
    //Call in the first animation sequence (should be TL0)
    [self scheduleOnce:@selector(runCurrentSequence) delay:1.5f];
}



- (void) completedAnimationSequenceNamed:(NSString*)name
{
    CCLOG(@"Completed Sequence %@", name);
    
    //If we've finished the final sequence, we don't need to process this function anymore
    if( currentTimelineSequence >= totalTimelineSequencesForScene - 1)
    {
        //Let's schedule the update to allow for tapping into the next scene
        [self scheduleUpdate];
        
        //Let's also enable the little hand icon, to queue the player that it's time to tap.
        CCSprite* advanceSignalSprite = [CCSprite spriteWithFile:@"CursorHand.png"];
        advanceSignalSprite.position = ccp([CCDirector sharedDirector].screenSize.width,0);
        advanceSignalSprite.anchorPoint = ccp(1.3f,-0.1f);
        advanceSignalSprite.opacity = 0;
        advanceSignalSprite.scale = 0.6f;
        
        const float fadeTime = 1.0f;
        [advanceSignalSprite runAction:[CCRepeatForever actionWithAction:
                                        [CCSequence actionOne:[CCFadeIn actionWithDuration:fadeTime]
                                                          two:[CCFadeOut actionWithDuration:fadeTime]]]];
        [self addChild:advanceSignalSprite];
        return;
    }
    
    
    //We've completed this sequence. Let's move on to the next one
    ++currentTimelineSequence;

    //We only tally up the numbers when we've reached an odd-numbered sequence.
    //We do this by checking the first bit.
    if( (currentTimelineSequence & 1) == 1 )
    {
        //Now the we know we are in the odd-numbered sequences, we must start tallying up
        //the appropriate labels. We can find the right label to modify by looking for the child
        //with the tag value equal to the currentTimelineSequence.
        currentTallyStringValue = 0;
        StatsSceneActor* actorChild = static_cast<StatsSceneActor*>([enemyActorsLayer getChildByTag:currentTimelineSequence]);
        [actorChild.KillCountLabel setString:[NSString stringWithFormat:@"%i",currentTallyStringValue]];
        actorChild.KillCountLabel.visible = YES;
        
        //Now that we have the number visible, let's perform the tally...
        //First, we get all the variables required to create the CCActions.
        const ccTime TALLY_TIME = 0.8f;
        SaveObjectPropertyList* playerSaveProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::PLAYER];
        NSMutableDictionary* enemyKillsDictionary = [playerSaveProperties objectForKey:GSP::ENEMIES_KILLED];
        
        
        //We use the variables to instantiate the actions.
        CCActionTween* incrementValue = [CCActionTween actionWithDuration:TALLY_TIME
                                                                      key:@"currentTallyStringValue"
                                                                     from:0
                                                                       to:[[enemyKillsDictionary objectForKey:actorChild.EnemyTypeSaveDataID] floatValue]];
        
        CCCallBlock* scheduleTallyString = [CCCallBlock actionWithBlock:^{
            [self schedule:@selector(refreshCurrentTallyString) interval:0.07f];
        }];
        
        
        CCCallBlock* unscheduleTallyStringAndContinue = [CCCallBlock actionWithBlock:^{
            currentTallyStringValue = 0;
            [self unschedule:@selector(refreshCurrentTallyString)];
            [self runCurrentSequence];           
        }];
                
        //Run the sequence of actions
        [self runAction:[CCSequence actions:
                         scheduleTallyString,
                         incrementValue,
                         unscheduleTallyStringAndContinue,
                         nil]];
    }
    //Otherwise... We are not tallying up numbers.
    //Let's just go straight to the sequence
    else
    {
        [self runCurrentSequence];
    }
}

-(void) update:(ccTime)delta
{
    if(
       [[KKInput sharedInput] isAnyTouchOnNode:self
                                    touchPhase:KKTouchPhaseEnded] )
    {
        [self unscheduleUpdate];
        [[CCDirector sharedDirector] replaceScene: [CCBReader sceneWithNodeGraphFromFile:@"CreditsScene.ccbi"]];
    }
}

- (void) runCurrentSequence
{
    [animationManagerRef runAnimationsForSequenceNamed:[NSString stringWithFormat:@"TL%i",currentTimelineSequence]];
}

-(void) refreshCurrentTallyString
{
    StatsSceneActor* actorChild = static_cast<StatsSceneActor*>([enemyActorsLayer getChildByTag:currentTimelineSequence]);
    [actorChild.KillCountLabel setString:[NSString stringWithFormat:@"%i",currentTallyStringValue]];
}

-(void) cleanup
{
    [super cleanup];
    
    // Clear out delegate method for the animationManager
    CCBAnimationManager* animationManager = self.userObject;
    animationManager.delegate = nil;
}

@end
