//
//  Box2dPhysicsWorld.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Box2dPhysicsWorld.h"
#import "Box2D.h"

Box2dPhysicsWorld* Box2dPhysicsWorld::instance = NULL;

Box2dPhysicsWorld::Box2dPhysicsWorld()
{
}

Box2dPhysicsWorld* Box2dPhysicsWorld::getInstance(void)
{
    if( NULL == instance )
    {
        instance = new Box2dPhysicsWorld();
        instance->init();
    }
    
    return instance;
}

void Box2dPhysicsWorld::PurgeInstance(void)
{
    if( NULL != instance )
    {
        instance->shutdown();
        delete instance;
        instance = NULL;
    }
}

void Box2dPhysicsWorld::init(void)
{
    b2Vec2 gravity(0,0);
    gamePhysicsWorld = new b2World( gravity );
}
void Box2dPhysicsWorld::shutdown(void)
{
    delete gamePhysicsWorld;
}


