//
//  GameplayLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Player;
@class Barricade;
@class EnemyField;
@class Aim;
@class CDSoundSource;

@interface GameplayLayer : CCLayer 
{
    Player* gamePlayer;
    Aim* aim;

    //Audio Components
    NSString* percussionSoundFilename;
    NSString* melodySoundFilename;
    CDSoundSource* percussionSound;

}
@property (nonatomic,readonly) CDSoundSource* PercussionSound;
@property (nonatomic,readonly) Player* GamePlayer;
@property (nonatomic,readonly) Aim* GameAim;
@property (nonatomic,readonly) Barricade* GameBarricade;

-(id) init;
-(void) GameplayEnemyStrike_EventHandler: (NSNotification*) notification;
@end
