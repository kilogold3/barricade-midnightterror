//
//  TransitionPageForward.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/27/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

/********************************************************
 This class is only designed to pass in hard coded parameters
 into the transition via the generic "transitionWithDuration"
 calls done through CocosBuilder. Essentially a bridging class.
 ********************************************************/
@interface TransitionPageForward : CCTransitionPageTurn
{
}



@end
