//
//  BarricadeRepairBlueprint.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/24/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "BarricadeRepairBlueprint.h"
#import "NotificationCenterEventList.h"
#import "GB2Engine.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "Barricade.h"
#import "NSMutableArray+Shuffling.h"
#import "SimpleAudioEngine.h"

@interface BarricadeRepairBlueprint (PrivateMethods)
// declare private methods here
@end

@implementation BarricadeRepairBlueprint

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
        
        b2World* world = [GB2Engine sharedInstance].world;
        
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        bodyDef.position = b2Vec2(0,0);
        physicsBody = world->CreateBody( &bodyDef );
        
        b2CircleShape circleShape;
        circleShape.m_p.Set(0, 0); //position, relative to body position
        circleShape.m_radius = 5; //radius
        
        b2FixtureDef myFixtureDef;
        myFixtureDef.shape = &circleShape; //this is a pointer to the shape above
        myFixtureDef.isSensor = true;
        physicsBody->CreateFixture(&myFixtureDef); //add a fixture to the body
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(barricadeRepairFragmentRepair_EventHandler:)
                                                     name:EventList::BARRICADE_REPAIR_FRAGMENT_REPAIR
                                                   object:nil];
        
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) onExit
{
    SaveObjectPropertyList* barricadePropertyList =
    [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE];
    
    [barricadePropertyList setValue: [NSNumber numberWithInt:currentBarricadeHealth]
                             forKey: GSP::BARRICADE_HEALTH];
    [super onExit];
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::BARRICADE_REPAIR_FRAGMENT_REPAIR
                                                  object:nil];
    
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:physicsBody];

	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) dealloc
{
	[super dealloc];
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    physicsBody->SetTransform( b2Vec2FromCGPoint(positionIn), 0);
    const b2Transform& transform = physicsBody->GetTransform();
    [super setPosition:CGPointFromb2Vec2(transform.p)];
}

-(CGPoint) position
{
    return CGPointFromb2Vec2( physicsBody->GetTransform().p ) ;
}


-(void) barricadeRepairFragmentRepair_EventHandler: (NSNotification*) notification
{
    CCNode* barricadeFragment = [notification object];
    BOOL foundChild = NO;
    
    for (CCSprite* child in self.children)
    {
        if( barricadeFragment.tag == child.tag )
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"RG_SFX_BarricadePiecesClick.wav"];

            //enable the fragment sprite on the blueprint
            child.visible = YES;
            foundChild = YES;
            currentBarricadeHealth += 10;
            
            //eliminate the drag/drop fragment for the scene
            [barricadeFragment removeFromParentAndCleanup:YES];
            break;
        }
    }
    
    if( NO == foundChild)
    {
        CCLOG(@"%@",[NSString stringWithFormat: @"Specified child not found. TagID:%i", barricadeFragment.tag]);
    }
    
    
    //If we reached max health, let's bring out the prompt
    if( currentBarricadeHealth >= 100)
    {
        SEND_EVENT(EventList::BARRICADE_REPAIR_FULLY_REPAIRED, nil);
    }
}

-(void) didLoadFromCCB
{
    //
    // We will always round up to the nearest 10's (not tenths, I think? Don't know the math term)
    // If we get a 91, 95, 98, 94, the we will round up. It doesn't matter if we are greater than
    // 5 to round up, we will still do it. If we have 3% health, we will round up to 10%.
    //
    
    //
    
    //Grab the value of the barricade health
    const short barricadeHealth = [[[[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE] objectForKey:GSP::BARRICADE_HEALTH] intValue];

    //We have the value. Let's assume it's 76. We now want to round up.
    //First we MOD to get a value from 0-10.
    //In this case, 76 % 10 = 6
    const short HealthTensValue = barricadeHealth % 10;
    
    //If the HealthTensValue resulted in a number non-fully-divisible
    //by 10...
    if( HealthTensValue > 0)
    {
        //We then add the difference between the Tens-Value and 10.
        //In this case, 10 - 6 = 4. We end up with:
        // 76 + 4 = 80.
        currentBarricadeHealth = barricadeHealth + (10 - HealthTensValue);
    }
    //Otherwise assign the raw value.
    else
    {
        currentBarricadeHealth = barricadeHealth;
    }
    
    //Now we want to find how many fragments should be deactivated
    const Byte fragmentsToDeactivate = 10 - (currentBarricadeHealth / 10);
    
    //We will now loop through the fragment children and disable 'n' amounts
    //of fragments randomly
    const Byte numFragments = self.children.count; //SHOULD BE 10

    //Create a list of ordered numbers from 0 to "numberOfFragments"
    NSMutableArray* fragmentsIndicies =  [NSMutableArray arrayWithCapacity:numFragments];
    for (int curIteration = 0; curIteration < numFragments; ++curIteration)
    {
        [fragmentsIndicies addObject:[NSNumber numberWithInt:curIteration]];
    }
    
    //Shuffle the list to generate random values
    [fragmentsIndicies shuffle];
    
    //Loop through the first 'fragmentsToDeactivate' amount of entries to deactivate.
    //This is how we get random children within the ranges of the arrays.
    for (int curFragment = 0; curFragment < fragmentsToDeactivate; ++curFragment)
    {
        const Byte fragmentIndex = [[fragmentsIndicies objectAtIndex: curFragment] intValue];
        
        //Obtain the fragment sprite
        const CCNode* fragmentSprite = [self getChildByTag:fragmentIndex];
        
        //Disable it.
        [fragmentSprite setVisible:NO];
    }
    
    //Send an event to notify the barricade ball is loaded.
    //We want to make sure the fragments used in the ball are not available for use.
    //As an event arg, we will send the Fragment ID's of the visible ones.
    NSMutableArray* usedFragments = [NSMutableArray arrayWithCapacity:(numFragments - fragmentsToDeactivate)];
    
    for( NSUInteger curFragment = 0; curFragment < numFragments; ++curFragment )
    {
        const CCNode* const curFragmentInstance = [self.children objectAtIndex:curFragment];
        if( YES ==  curFragmentInstance.visible )
        {
            [usedFragments addObject: [NSNumber numberWithInt:curFragmentInstance.tag] ];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::BARRICADE_REPAIR_BLUEPRINT_LOADED
                                                        object:usedFragments];
    
}
@end
