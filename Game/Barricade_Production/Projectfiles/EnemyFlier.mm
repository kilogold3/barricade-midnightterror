//
//  EnemyWalker.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnemyFlier.h"
#import "EnemyStateFactory.h"
#import "WeaponBase.h"
#import "NotificationCenterEventList.h"
#import "Barricade.h"
#import "TrapBase.h"
#import "GMath.h"

@implementation EnemyFlier

-(void) killEnemy
{
    //Notify death of enemy
    SEND_EVENT(EventList::GAMEPLAY_ENEMY_DEAD, self)
    
    //Switch to dead state
    [self changeEnemyState: [enemyStateFactory createFlierState_Death]];
}

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {
        //Order matters here, because AI states use this class' attributes
        //to set behavior. Set the basic attributes before dealing with states.
        
        //Set basic attributes
        [self loadBasicAttributes:@"EnemyFlierSettings"];
        contactWithBarricadeSegmentCounter = 0;
        
        
        //set initial state
        [self instantChangeEnemyState: [enemyStateFactory createFlierState_Spawn]];

    }
    
    return self;
}

-(void) barricadeDestroyedEventHandler: (NSNotification*) notification
{
    //Fliers are unnaffected by this.
}

-(void) OverdriveYunaClearBarricadeArea_EventHandler: (NSNotification*) notification
{
    //We override this method in order to avoid the flier from getting killed by the barricade in place.
    //The Flier is in the air. It should not be affected.
}

-(void) OverdriveYunaReinforceBarricadeStart_EventHandler: (NSNotification*) notification
{
    //We override this method in order to avoid the flier from getting killed by the barricade in place.
    //The Flier is in the air. It should not be affected.
}

-(void) OverdriveYunaReinforceBarricadeEnd_EventHandler: (NSNotification*) notification
{
    //We override this method in order to avoid the flier from getting killed by the barricade in place.
    //The Flier is in the air. It should not be affected.
}

-(void) OverdriveYunaKillEnemiesInBarricadeArea_EventHandler: (NSNotification*) notification
{
    //We override this method in order to avoid the flier from getting killed by the barricade in place.
    //The Flier is in the air. It should not be affected.
}

@end
