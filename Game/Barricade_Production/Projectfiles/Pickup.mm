//
//  Pickup.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "Pickup.h"
#import "NotificationCenterEventList.h"
#import "GameSaveData.h"
#import "Player.h"
#import "GameSaveDataPropertyList.h"
#import "Pair.h"
#import "WeaponTypes.h"
#import "SimpleAudioEngine.h"

@interface Pickup (PrivateMethods)
// declare private methods here
@end

@implementation Pickup
@synthesize IsReadyForPickup=isReadyForPickup;

-(id) init
{
    self = [super init];
    {
        //Acquire the minimap button reference
        minimapButtonRef = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_MINIMAP_BUTTON
                                                            object: [NSValue valueWithPointer:&minimapButtonRef]];
        NSAssert(minimapButtonRef != nil, @"Failed to acquire minimapButtonRef");
        
        //Default the item to be ready to get picked up
        self.IsReadyForPickup = YES;
    }
    
    return self;
}

-(void) update:(ccTime)delta
{
    //Only update if allowed to be picked up
    if( YES == self.IsReadyForPickup)
    {
        [super update:delta];
    }
}

-(void) Interact
{    
    GameSaveData* gameSaveData = [GameSaveData sharedGameSaveData];

    //Perform the effect of the pickup
    switch ( pickupType )
    {
        case PICKUP_TRAVEL_SUPPLIES:
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"Pickup_Supplies.wav"];
            
            //Grab the current value from save data
            NSDictionary* saveDataPropertyList =
                [gameSaveData getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
            NSNumber* travelSuppliesCount =
                [saveDataPropertyList valueForKey:GSP::TRAVEL_SUPPLIES_COUNT];
            
            //Set the value to the current-value + pickup-value
            [saveDataPropertyList setValue: [NSNumber numberWithInt: travelSuppliesCount.intValue + pickupValue]
                                    forKey: GSP::TRAVEL_SUPPLIES_COUNT];
        }
            break;
            
        default:
            break;
    }
    
    //Notify that the pickup was acquired
    [[NSNotificationCenter defaultCenter] postNotificationName: EventList::RESOURCE_GATHERING_PICKUP_ACQUIRED
                                                        object: self];

    //We should remove the pickup from the screen now.
    //Let's move the pickup to the Minimap button and then eliminate it.
    
    CCCallBlock* cleanupBlock = [CCCallBlock actionWithBlock:^{
        [self removeFromParentAndCleanup:YES];
    }];

    const float moveTime = 0.3f;
    CCMoveTo* moveAction = [CCMoveTo actionWithDuration:moveTime
                                               position:minimapButtonRef.position];
    
    const float scaleTime = 0.15f;
    CCScaleTo* minimapButtonScaleUp = [CCScaleTo actionWithDuration:scaleTime
                                                              scale:1.5f];
    
    CCScaleTo* minimapButtonScaleDown = [CCScaleTo actionWithDuration:scaleTime
                                                              scale:1.0f];
    
    CCCallBlock* minimapTrigger = [CCCallBlock actionWithBlock:^{
        [minimapButtonRef runAction:[CCSequence actionOne:minimapButtonScaleUp
                                                      two:minimapButtonScaleDown]];
    }];
    
    
    CCSpawn* eliminatePickupAndTriggerMiniMapButton = [CCSpawn actionOne:minimapTrigger
                                                                     two:cleanupBlock];
    
    
    [self runAction:[CCSequence actions:moveAction,eliminatePickupAndTriggerMiniMapButton, nil]];
}


@end
