//
//  GameplayBackgroundLayer_00.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 2/1/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "GameplayBackgroundLayer_02A.h"
#import "CCBAnimationManager.h"
#import "CCBSequence.h"

@interface GameplayBackgroundLayer_02A (PrivateMethods)
// declare private methods here
@end

@implementation GameplayBackgroundLayer_02A

- (void) completedAnimationSequenceNamed:(NSString*)name
{
    /*
     We want to override this method to do nothing. 
     This level is an exception where the players will not
     play till sunrise. Therefore, we don't leave when the sequence
     ends. We leave when the conditions of the level are met.
     IE. When Vejigante attacks Leo.
     */
}

@end
