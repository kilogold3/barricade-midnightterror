//
//  EnemyMechaJuna.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyBase.h"
#import "IEnemyState.h"
#import "AI_TreeNode.h"

class b2Body;

enum MECHA_JUNA_STATES {
    eIDLE,
    ePUNCH_LEFT,
    ePUNCH_RIGHT,
    eFIRE_CANNON,
    };

enum CANNON_TARGET_BLOCK {
    eBLOCK1,
    eBLOCK2,
    eBLOCK3
    };

struct tCannonTargetBlock {
    CGPoint start;
    CGPoint end;
};

typedef tCannonTargetBlock CannonTargetBlock;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface EnemyMechaJunaArm : EnemyBase
{
    CCSprite* armSprite;
    CCSprite* armLightSprite;
    CGPoint originalPosition; //original position from load
    CGPoint destinationPosition; //where the projectile is headed
    Byte armAttackRange;
    Byte armMovementSpeed;
}

@property (nonatomic, readwrite) CGPoint OriginalPosition;
@property (nonatomic, readwrite) CGPoint DestinationPosition;
@property (readonly) Byte ArmAttackRange;
@property (readonly) Byte ArmMovementSpeed;
@property (readonly) CCSprite* ArmSprite;
@property (readonly) CCSprite* ArmLightSprite;
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;
@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface EnemyMechaJunaCannonBeam : CCNode
{
    b2Body* beamPhysicsBody;
    Byte contactWithPlayerCounter;
    CCSprite* beamSprite;
    float physicsBasedScaleX;
    
    //Flag to determine if the contact allows the cannon
    //to kill the player.
    BOOL isHarmful;
}
@property (nonatomic, readwrite) BOOL IsHarmful;
@property (readonly) CCSprite* BeamSprite;

-(void) syncPhysicsBodyRotationWithParentRotation;
-(void) beginContactWithPlayer: (GB2Contact*)contact;
-(void) endContactWithPlayer: (GB2Contact*)contact;
@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface EnemyMechaJunaCannon : EnemyBase
{
    EnemyMechaJunaCannonBeam* cannonBeam;
    CCSprite* cannonSprite;
    CCSprite* cannonLightSprite;
    CGPoint cannonAimPointStart;
    CGPoint cannonAimPointEnd;
    CGPoint cannonRootTargetBlockStart; //Also known as Target Block 1 Start (IN WORLD-SPACE)
    Byte cannonTargetBlockHeight;
    BOOL isVulnerable;
    float cannonRotationSpeed;
}
@property (nonatomic, readwrite) CGPoint CannonAimPointStart;
@property (nonatomic, readwrite) CGPoint CannonAimPointEnd;
@property (readonly) Byte CannonTargetBlockHeight;
@property (readonly) EnemyMechaJunaCannonBeam* CannonBeam;
@property (readonly) CCSprite* CannonSprite;
@property (readonly) CCSprite* CannonLightSprite;
@property (readonly) float CannonRotationSpeed;
@property (nonatomic, readwrite) BOOL IsVulnerable;

//NOTE: The coords data here is in world-space
-(CannonTargetBlock) getTargetBlock: (CANNON_TARGET_BLOCK) targetBlockIn;

@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface EnemyMechaJuna : CCNode
{
    Player* playerRef;
    
    AI_TreeNode* rootNode;
    
    EnemyMechaJunaArm* leftArm;
    EnemyMechaJunaArm* rightArm;
    
    EnemyMechaJunaCannon* cannonWeapon;
    
    MECHA_JUNA_STATES currentStateID;
    
    float cannonRotationSpeed;
    float thinkingTimeMax;
    float thinkingTimeMin;
    float thinkingTimeCurrent;
}
@property (readonly) EnemyMechaJunaArm* LeftArm;
@property (readonly) EnemyMechaJunaArm* RightArm;
@property (readonly) EnemyMechaJunaCannon* CannonWeapon;
@property (readonly) MECHA_JUNA_STATES CurrentStateID;

-(void) GameplayMechaJunaLaserFireEnd_EventHandler: (NSNotification*) notification;
-(void) GameplayMechaJunaDefeated_EventHandler: (NSNotification*) notification;
-(void) GameplayMechaJunaArmDestroyed_EventHandler: (NSNotification*) notification;
@end