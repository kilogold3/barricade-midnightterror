//
//  GameplayForegroundLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
/**********************************************************
 The purpose of this class is to serve as the container/manager
 of any special effects we might want to play on top of the gameplay 
 scene.
 **********************************************************/
#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameplayForegroundLayer : CCLayer
{
    
}

//In: NSNotification* - holds a string to the special effect animation CCBI file
//Return: void
-(void) eventHandler_playSpecialEffectAnimation: (NSNotification*) notification;
-(void) ComponentAcquireGameplayLayerForeground_EventHandler:(NSNotification*) notification;
@end
