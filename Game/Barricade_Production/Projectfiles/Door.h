//
//  Door.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "LockableItem.h"

@interface Door : LockableItem
{
    NSString* destinationCCBI;
}

-(void) Interact;

@end
