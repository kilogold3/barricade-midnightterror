//
//  ToBeContinuedScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ToBeContinuedScene.h"
#import "GameIntroScene.h"
#import "SimpleAudioEngine.h"
#import "GameSaveData.h"

@implementation ToBeContinuedScene

-(void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"SlowHeartbeat.wav" loop:YES];

    [self scheduleUpdate];
}

-(void) onEnter
{
    [super onEnter];
}

-(void) update:(ccTime)delta
{
    KKInput* input = [KKInput sharedInput];
    
    if( [input anyTouchEndedThisFrame] )
    {
        [self unscheduleUpdate];
        
        [[GameSaveData sharedGameSaveData] ResetSaveDataToDefaults];

        [[CCDirector sharedDirector] replaceScene:[GameIntroScene node]];
    }
}

-(void) cleanup
{
    [super cleanup];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}
@end
