//
//  HUD_DataDisplayStatsAmmo.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
@class WeaponBase;

@interface HUD_DataDisplayStatsAmmo : CCSprite
{
}

-(void) setDataForWeapon: (WeaponBase*) weaponIn;
@end
