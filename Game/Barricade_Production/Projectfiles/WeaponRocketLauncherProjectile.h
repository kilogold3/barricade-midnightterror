//
//  WeaponRocketLauncherProjectile.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <map>

@class GB2Contact;
@class WeaponRocketLauncher;

typedef std::map<b2Body*, short> CollisionCounterLookupTable;

@interface WeaponRocketLauncherProjectile : CCNode
{
    CollisionCounterLookupTable enemyTargetContactCounters;

    bool hasDetonated;
    bool printDebugData;
    b2Body* projectileBody;
    WeaponRocketLauncher* rocketLauncherParentRef;
    CCSprite* rocketSprite;
    CCParticleSystemQuad* particlePropulsionEffect;
}

//Direction at which the projectile is headed.
@property (nonatomic,readwrite) CGPoint ProjectileDirection;
@property (nonatomic,readwrite) float ProjectileSpeed;

-(id) initWithDependencies: (WeaponRocketLauncher*) rocketLauncherParentRefIn;

@end
