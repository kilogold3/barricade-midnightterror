//
//  SearchTimerClock.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class ResourceGatheringScene;
@class ActivityLauncher;

@interface SearchTimerClock : CCNode
{
    BOOL isEnabled;

    ActivityLauncher* dummySearchTimerClockTransitionActivityLauncher;
    CCSprite* arrowHandSprite;
    ResourceGatheringScene* resourceGatheringSceneRef;
    float miniScale;
    float largeScale;
    
    //Label representing how many days are left
    CCLabelTTF* daysLeftLabel;
    
    //Resource gathering timer
    float levelTimeRemaining;
    float levelTimeMax;
}
@property (readonly) float MiniScale;
@property (readonly) float LargeScale;
@property (readonly) BOOL IsEnabled;
@property (readonly) float LevelTimeRemaining;
@property (readonly) float LevelTimeMax;

-(void) componentAcquireResourceGatheringSearchTimerClock_EventHandler: (NSNotification*) notification;
-(void) resourceGatheringTimerClockEnable_EventHandler: (NSNotification*) notification;
@end
