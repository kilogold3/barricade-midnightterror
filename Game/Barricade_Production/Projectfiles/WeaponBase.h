//
//  WeaponBase.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/11/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "WeaponTypes.h"
#import "RayCastCallbackLineOfFire.h"

@class Player;
@class EnemyBase;
@class Aim;

@interface WeaponBase : NSObject 
{
    //Player reference for positional info
    Player* gamePlayerRef;

    //Reference for aiming to the reticle.
    Aim* gameAimRef;
    
    //Anchor for arms sprite (different for each weapon)
    CGPoint weaponSpriteAnchor;
    
    //Arm sprite ID's
    NSString* rearArmSpriteID;
    NSString* frontArmSpriteID;
    
    //Weapon pannel sprite ID's
    NSString* weaponPanelActiveSpriteID;
    NSString* weaponPanelInactiveSpriteID;
    
    //The sprite frame name for the weapon's icon
    NSString* hudDataDisplayWeaponSpriteID;
    
    //Basic weapon attributes
    WEAPON_TYPES weaponType;
    short damage;
    
    //Target acquisition
    EnemyBase* lastAcquiredTarget;
    
    //Playing field
    CCLayer* gameplayLayerRef;
    
    //Used to determine the magnitude distance between
    //the player shoulder and the barrel of the weapon
    float weaponBarrelOffsetMagnitude;
    
    //Debug
    BOOL printDebugData;
}
@property (readonly) NSString* RearArmSpriteID;
@property (readonly) NSString* FrontArmSpriteID;
@property (readonly) NSString* WeaponPanelActiveSpriteID;
@property (readonly) NSString* WeaponPanelInactiveSpriteID;
@property (readonly) NSString* HudDataDisplayWeaponSpriteID;
@property (readonly) short Damage;
@property (readonly) WEAPON_TYPES WeaponType;
@property (readonly) CGPoint WeaponBarrelPosition;
@property (readonly) CGPoint WeaponSpriteAnchor;

-(id) initWithDependencies: (Player*) gamePlayerRefIn;

-(void) pullTrigger: (NSNotification*) notification;
-(void) releaseTrigger: (NSNotification*) notification;
-(void) update:(ccTime) delta;
-(void) enter;
-(void) exit;
-(void) fireWeaponFromOrigin: (CGPoint) originPosition;

-(void) updateLineOfFire: (NSNotification*) notification;
-(void) GameplayWeaponFired_EventHandler:(NSNotification*) notification;
-(void) OverdriveYunaReinforceBarricadeStart_EventHandler:(NSNotification*) notification;
-(void) GameplayEnemyDead_EventHandler:(NSNotification*) notification;



-(CGPoint) generateScreenLeftBoundProjectedPointFromLineOfFire: (CGPoint) originPosition;
-(void) GetWeaponBarrelPositionBasedOnTarget: (CGPoint) targetPosition
                                   andOrigin: (CGPoint) originPosition
                       withBarrelPositionOut: (CGPoint*) barrelPositionOut
                          withArmRotationOut: (float*) armRotationOut;


@end
