//
//  ParticlesRain.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ParticlesRain.h"
#import "SimpleAudioEngine.h"

@implementation ParticlesRain

-(void) onEnter
{
    [super onEnter];
    
    soundSourceRain = [[SimpleAudioEngine sharedEngine] soundSourceForFile: @"RainLoop.wav"];
    [soundSourceRain retain];
    soundSourceRain.looping = YES;
    soundSourceRain.gain = 0.2f;
    [soundSourceRain play];
}

-(void) cleanup
{
    [super cleanup];
    [soundSourceRain stop];
    [soundSourceRain release];
}

@end
