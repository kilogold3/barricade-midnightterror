//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlierState_Spawn.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2Engine.h"
#import "EnemyFlier.h"

const float HOVER_SPEED = 20.0f;
const float MAX_HOVER_HEIGHT_OFFSET = 5.0f;

@implementation FlierState_Spawn

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyFlier class]],@"Wrong enemy parent type");
        enemyParent = (EnemyFlier*) enemyParentIn;
    }
    
    return self;
}

-(void) enter
{
    hasInitiallyAcquiredHoverHeight = NO;
    defaultHoverHeight = 0;
    movementDirection = hoverDirection = 1;
    enemyParent.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyFlierFly0.png"];
    
    //Load animation
    const short animationFrameCount = 4;
    float animationDelay = (15 / enemyParent.MovementSpeed) / animationFrameCount;
    
    CCAnimation* walkingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyFlierFly%d.png"
                                                                   numFrames:animationFrameCount
                                                                       delay:animationDelay];
    
    CCAnimate* animateAction = [CCAnimate actionWithAnimation:walkingAnimation];
    CCAction* repeatingFlyingAnimationAction = [CCRepeatForever actionWithAction:animateAction];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyParent.EnemyPhysicsBody
                                           forShapeName:@"enemyFlierShape"];
    //Set anchor point
    [enemyParent.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyFlierShape"]
     ];
    
    //Load the sprite
    [enemyParent addChild:enemyParent.EnemySprite];
    
    //Animate sprite
    [enemyParent.EnemySprite runAction:repeatingFlyingAnimationAction];
}

-(void) exit
{
    [enemyParent.EnemySprite stopAllActions];
    [enemyParent removeChild:enemyParent.EnemySprite cleanup:YES];
    [[GB2Engine sharedInstance] addBodyToPostWorldStepCompleteFixtureRemovalList:enemyParent.EnemyPhysicsBody];
}

-(void) update: (float) deltaTime
{
    //Get initial hover height (only happens once per instance)
    if( NO == hasInitiallyAcquiredHoverHeight )
    {
        hasInitiallyAcquiredHoverHeight = YES;
        defaultHoverHeight = enemyParent.position.y;
    }
    
    
    //We want to create a hovering effect, so we'll move the object up an down
    //the Y-Axis by a couple of points.
    self.HoverHeight += ((HOVER_SPEED * deltaTime) * hoverDirection);
    
    //Check to see if we need to invert the VERTICAL direction
    if(
       (self.HoverHeight >= MAX_HOVER_HEIGHT_OFFSET && 1 == hoverDirection) ||
       (self.HoverHeight <= -MAX_HOVER_HEIGHT_OFFSET && -1 == hoverDirection)
       )
    {
        hoverDirection = -hoverDirection;
    }
    
    //Check to see if we need to invert the HORIZONTAL direction
    const CGSize screenSize = [CCDirector sharedDirector].screenSize;
    
    if( enemyParent.position.x > screenSize.width )
    {
        movementDirection = -movementDirection;
        enemyParent.position = ccp(screenSize.width, enemyParent.position.y);
    }
    else if( enemyParent.position.x < 0 )
    {
        movementDirection = -movementDirection;
        enemyParent.position = ccp(0, enemyParent.position.y);
    }
    
    //Advance the enemy
    enemyParent.position = ccp(enemyParent.position.x + (enemyParent.MovementSpeed * deltaTime) * movementDirection,
                               defaultHoverHeight + self.HoverHeight );
}
@end
