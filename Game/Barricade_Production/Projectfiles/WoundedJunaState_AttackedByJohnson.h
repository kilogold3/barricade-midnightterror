//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"


class b2Fixture;
@class EnemyJuna;
@class GB2Contact;
@class Barricade;

@interface WoundedJunaState_AttackedByJohnson : NSObject<IEnemyState>
{
    Barricade* barricadeRef;
    CCAction* repeatingWalkingAnimationAction;
    b2Fixture* physicsBodyFixture;
    EnemyJuna* enemyParent;
    CCLayer* gameplayLayerForeground;
    
    float movementSpeed;
    
    //Collision counters
    short contactWithBarricadeCounter;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
@end
