//
//  IPlayerState.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IPlayerState <NSObject>
-(void) Enter;
-(void) Exit;
-(void) Update: (float) deltaTime;
@end

typedef NSObject<IPlayerState> PlayerState;