//
//  TitleScene.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"
#import "CCBAnimationManager.h"

@class CCControlButton;
@class TitleSceneCreatureEyesLayer;
@class ParticlesRain;

@interface TitleSceneInstance : CCLayer <CCBAnimationManagerDelegate>
{
    CCLayer* menuButtonsLayer;
    CCNode* saveFilePromptLayer;
    CCLabelTTF* touchToStartLabel;
    CCControlButton* btnContinue;
    CCControlButton* btnNewGame;
    TitleSceneCreatureEyesLayer* titleSceneCreatureEyesLayer;
    CCLayer* waves3DEffectLayer;
    ParticlesRain* particlesRain;
    
}
-(void) startGameContinue;
-(void) startGameNew;
@end

@interface TitleScene : CCScene
{
    TitleSceneInstance* titleSceneInstance;
}

@end
