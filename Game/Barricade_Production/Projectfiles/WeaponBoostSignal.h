//
//  WeaponBoostSignal.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 9/15/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@interface WeaponBoostSignal : CCParticleSystemQuad
{
    
}
-(void) uiUpdateWeaponBoostSignalLevel_EventHandler: (NSNotification*) notification;

@end
