//
//  AI_ActionNode.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 6/20/13.
//
//

#import "AI_TreeNode.h"

typedef void (^ ActionBlock)(void);

@interface AI_ActionNode : NSObject<AI_TreeNodeProtocol>
{
    ActionBlock actionBlock;
}

-(id) initWithActionBlock: (ActionBlock) actionBlockIn;

-(void) doAction;

@end
