//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WalkerState_Death.h"
#import "EnemyWalker.h"
#import "CCAnimation+SequenceLoader.h"
#import "GB2ShapeCache.h"
#import "GMath.h"
#import "GameAudioList.h"
#import "SimpleAudioEngine.h"

@implementation WalkerState_Death

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn
{
    self = [super init];
    
    if( self != nil)
    {
        NSAssert([enemyParentIn isKindOfClass:[EnemyWalker class]],@"Wrong enemy parent type");
        enemyParentRef = enemyParentIn;
    }

    return self;
}

-(void) dealloc
{
    [animateAction release];
    [super dealloc];
}

-(void) enter
{
    int randValue = gRangeRand(0,40);
    
    if( randValue <= 20 )
        [self setDeathAnimationC:enemyParentRef];
    else
        [self setDeathAnimationD:enemyParentRef];
    
    CCSequence* animationSequence = [CCSequence actionOne:animateAction
                                                      two:[CCTintTo actionWithDuration:1.0f
                                                                                   red:100
                                                                                 green:128
                                                                                  blue:100]
                                     ];
    
    [enemyParentRef.EnemySprite runAction:animationSequence];
    [[SimpleAudioEngine sharedEngine] playEffect:GAL::SND_ENEMY_WALKER_DEATH];
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}

-(void) setDeathAnimationA: (EnemyBase*) enemyParentRefIn
{
    enemyParentRef.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyWalkerDeathA_0.png"];
    
    //Load animation
    const short animationFrameCount = 9;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyWalkerDeathA_%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParentRef.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    [enemyParentRefIn addChild:enemyParentRef.EnemySprite];
}

-(void) setDeathAnimationB: (EnemyBase*) enemyParentRefIn
{
    enemyParentRef.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyWalkerDeathB_0.png"];
    
    //Load animation
    const short animationFrameCount = 9;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyWalkerDeathB_%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParentRef.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    [enemyParentRefIn addChild:enemyParentRef.EnemySprite];
}

-(void) setDeathAnimationC: (EnemyBase*) enemyParentRefIn
{
    enemyParentRefIn.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyWalkerDeathC_0.png"];
    
    //Load animation
    const short animationFrameCount = 4;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyWalkerDeathC_%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParentRefIn.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    [enemyParentRefIn addChild:enemyParentRef.EnemySprite];
}

-(void) setDeathAnimationD: (EnemyBase*) enemyParentRefIn
{
    enemyParentRefIn.EnemySprite = [CCSprite spriteWithSpriteFrameName:@"enemyWalkerDeathD_0.png"];
    
    //Load animation
    const short animationFrameCount = 4;
    float animationDelay = 0.6f / animationFrameCount;
    
    CCAnimation* dyingAnimation = [CCAnimation animationWithSpriteSequence:@"enemyWalkerDeathD_%d.png"
                                                                 numFrames:animationFrameCount
                                                                     delay:animationDelay];
    
    animateAction = [CCAnimate actionWithAnimation:dyingAnimation];
    [animateAction retain];
    
    //Set anchor point
    [enemyParentRefIn.EnemySprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyWalkerWalkShape"]
     ];
    
    [enemyParentRefIn addChild:enemyParentRef.EnemySprite];
}

@end
