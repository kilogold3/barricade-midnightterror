//
//  SearchTimerClockTransition.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "ActivityBase.h"

@class SearchTimerClock;

@interface SearchTimerClockTransition : ActivityBase
{
    SearchTimerClock* searchTimerClockRef;
    CCLayerColor* backdropLayerColor;
}

@end
