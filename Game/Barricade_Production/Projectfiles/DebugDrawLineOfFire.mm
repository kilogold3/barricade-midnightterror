//
//  DebugDrawLineOfFire.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "DebugDrawLineOfFire.h"
#import "Aim.h"
#import "Player.h"
#import "PlayerState_Alive.h"
#import "PlayerArms.h"

@implementation DebugDrawLineOfFire

-(id) initWithDependencies: (Player*) gamePlayerRefIn
                          : (Aim*) gameAimRefIn
{
    self = [super init];
    
    if( self != nil )
    {
        gamePlayerRef = gamePlayerRefIn;
        gameAimRef = gameAimRefIn;
    }
    
    return self;
}

-(void) draw
{
    [super draw];
    
    //Draw line for aiming
    //If we are in the correct state...
    if( [gamePlayerRef.CurrentPlayerState class] ==  [PlayerState_Alive class])
    {
        //Bring all coords to a common space (this node's local coords)
        PlayerState_Alive* playerState = (PlayerState_Alive*)gamePlayerRef.CurrentPlayerState;
        const CGPoint playerArmsPosition = [gamePlayerRef convertToWorldSpace:playerState.PlayerArmsRef.position];
        const CGPoint aimPosition = [gameAimRef.parent convertToWorldSpace:gameAimRef.position];
        //Draw line
        ccDrawLine(playerArmsPosition, aimPosition);
    }
}

@end
