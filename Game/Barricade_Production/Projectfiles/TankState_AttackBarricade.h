//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"
@class EnemyTank;
@class Barricade;

@interface TankState_AttackBarricade : NSObject<IEnemyState>
{
    Barricade* barricadeRef;
    EnemyBase* enemyParent;
    CCAnimate* animateAction;
    CCAction* repeatingAttackingAnimationAction;
    CGPoint attackFrameID;
    BOOL readyToAttack;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;

@end
