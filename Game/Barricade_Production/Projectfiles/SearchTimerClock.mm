//
//  SearchTimerClock.m
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "SearchTimerClock.h"
#import "NotificationCenterEventList.h"
#import "ResourceGatheringScene.h"
#import "ActivityLauncher.h"
#import "GameSaveDataPropertyList.h"
#import "GameSaveData.h"

inline static double lerp(double a, double b, double t)
{
    return a + (b - a) * t;
}

inline static float lerpf(float a, float b, float t)
{
    return a + (b - a) * t;
}

@implementation SearchTimerClock
@synthesize MiniScale=miniScale;
@synthesize LargeScale=largeScale;
@synthesize IsEnabled=isEnabled;
@synthesize LevelTimeRemaining=levelTimeRemaining;
@synthesize LevelTimeMax=levelTimeMax;

-(void) setIsEnabled:(BOOL)IsEnabledIn
{
    isEnabled = IsEnabledIn;
    
    if( isEnabled )
    {
        [self resumeSchedulerAndActions];
    }
    else
    {
        [self pauseSchedulerAndActions];
    }

    self.visible = isEnabled;
}

-(void) didLoadFromCCB
{
    //Set initial level time
    if( [KKConfig selectKeyPath:@"ResourceGatheringSceneConfig"] )
    {
        levelTimeMax = [KKConfig floatForKey:@"MaxLevelTime"];
    }
    else
    {
        NSAssert(false, @"Did [NOT] find config.lua keypath");
    }
    
    levelTimeRemaining = levelTimeMax;
    
    //Load days left label
    //Configure only if we are past 01A progression.
    SaveObjectPropertyList* gameProgressProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    if( [[gameProgressProperties valueForKey:GSP::CURRENT_PROGRESSION] intValue] >= 1 )
    {
        const int currentOverallDay = [[gameProgressProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
        [KKConfig selectKeyPath:@"GameSettings"];
        const int maxDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
        [daysLeftLabel setString: [NSString stringWithFormat:@"%i Days Left", maxDaysLimit - currentOverallDay] ];
    }
    else
    {
        daysLeftLabel.visible = NO;
    }
    
    //Acquire ResourceGatheringScene reference
    resourceGatheringSceneRef = nil;
    NSValue* resourceGatheringSceneRefValue = [NSValue valueWithPointer:&resourceGatheringSceneRef];
    [[NSNotificationCenter defaultCenter] postNotificationName:EventList::COMPONENT_ACQUIRE_GAME_PROGRESS
                                                        object: resourceGatheringSceneRefValue];
    NSAssert(resourceGatheringSceneRefValue != nil, @"Failed to acquire ResourceGatheringScene");
    
    //Set default scales
    miniScale = 0.25f;
    largeScale = 1.0f;
    
    //Load the dummy activity launcher
    dummySearchTimerClockTransitionActivityLauncher = [ActivityLauncher node];
    dummySearchTimerClockTransitionActivityLauncher.ActivityToLaunch = @"SearchTimerClockTransitionActivity.ccbi";
    [self addChild:dummySearchTimerClockTransitionActivityLauncher];
    
    
    //Register event listeners
    REGISTER_EVENT(self,
                   @selector(componentAcquireResourceGatheringSearchTimerClock_EventHandler:),
                   EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_SEARCHTIMERCLOCK )
    
    REGISTER_EVENT(self,
                   @selector(resourceGatheringTimerClockEnable_EventHandler:),
                   EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE)
    
    
    //set other misc data
    [self setScale:miniScale];
    isEnabled = YES;
    [self scheduleUpdate];
}

-(void) cleanup
{
    UNREGISTER_EVENT(self, EventList::COMPONENT_ACQUIRE_RESOURCE_GATHERING_SEARCHTIMERCLOCK)
    UNREGISTER_EVENT(self, EventList::RESOURCE_GATHERING_TIMER_CLOCK_ENABLE)

    [super cleanup];
}

-(void) update:(ccTime)delta
{
    //Check to see if we begin the transition activity
    if (self.LevelTimeRemaining <= 0)
    {
        //Notifiy the ResourceGatheringScene that the time is up and that
        //it is not allowed to let the player continue interaction.
        //(Essentially, just freeze the running activities, since the rest
        //of the scene is already disabled by other components)
        SEND_EVENT(EventList::RESOURCE_GATHERING_FREEZE_RUNNING_ACTIVITIES, nil);
        
        //launch the mini map activity with the dummy activity launcher
        SEND_EVENT(EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY,
                   dummySearchTimerClockTransitionActivityLauncher);
       
        //We don't need to update anymore. The clock's job is done here.
        [self unscheduleUpdate];
    }
    else
    {
        //Tick the timer
        levelTimeRemaining -= delta;
        
        if(levelTimeRemaining <= 0)
        {
            levelTimeRemaining = 0;
        }
        
        //Calculate the percentage of how much time is left
        float percentageTimeRemaining = self.LevelTimeRemaining / self.LevelTimeMax;
        
        //Calculate rotation value according to percentage
        const float dayRotationLimit = -90.0f;
        const float nightRotationLimit = 90.0f;
        
        float currentRotation = lerpf(dayRotationLimit, nightRotationLimit, percentageTimeRemaining);
        
        //Assign new rotational value
        //Setting to negative here to rotate CCW for Cocos2D.
        [arrowHandSprite setRotation:-currentRotation];
    }
}

-(void) componentAcquireResourceGatheringSearchTimerClock_EventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    SearchTimerClock** requestorRef = (SearchTimerClock**)[requestorRefPointer pointerValue];
    (*requestorRef) = self;
}

-(void) resourceGatheringTimerClockEnable_EventHandler: (NSNotification*) notification
{
    self.IsEnabled = [[notification object] boolValue];
}


@end
