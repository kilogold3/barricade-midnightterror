//
//  ResourceGatheringInteractiveObject.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 3/12/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ResourceGatheringInteractiveObject.h"
#import "NotificationCenterEventList.h"

@interface ResourceGatheringInteractiveObject (PrivateMethods)
// declare private methods here
@end

@implementation ResourceGatheringInteractiveObject

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		
		// uncomment if you want the update method to be executed every frame
		[self scheduleUpdate];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resourceGatheringLaunchActivity_EventHandler:)
                                                     name:EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resourceGatheringSuspendActivity_EventHandler:)
                                                     name:EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY
                                                   object:nil];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	 // add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_LAUNCH_ACTIVITY
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::RESOURCE_GATHERING_SUSPEND_ACTIVITY
                                                  object:nil];
    
	 // specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) dealloc
{
	[super dealloc];
}

// scheduled update method
-(void) update:(ccTime)delta
{
    KKInput* touchInput = [KKInput sharedInput];
    
    //If we are touching a CCNode...
    if( [touchInput isAnyTouchOnNode:self touchPhase:KKTouchPhaseBegan] )
    {
        //We will invalidate all touches first
        //(avoids processing a touch event on the same location in the next room.)
		KKTouch* touch;
		CCARRAY_FOREACH(touchInput.touches, touch)
		{
            [touch invalidate];
        }
        
        //then we interact
        [self Interact];
    }
}

-(void) Interact
{
    NSAssert(false, @"Method must be overriden");
}

-(void) resourceGatheringLaunchActivity_EventHandler: (NSNotification*) notification;
{
    [self unscheduleUpdate];
}

-(void) resourceGatheringSuspendActivity_EventHandler: (NSNotification*) notification
{
    [self scheduleUpdate];
}


@end
