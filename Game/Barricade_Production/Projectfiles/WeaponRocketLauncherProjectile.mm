//
//  WeaponRocketLauncherProjectile.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "WeaponRocketLauncherProjectile.h"
#import "GB2Engine.h"
#import "GB2Contact.h"
#import "EnemyBase.h"
#import "WeaponRocketLauncher.h"
#import "CCAnimation+SequenceLoader.h"
#import "CCBReader.h"
#import "SimpleAudioEngine.h"

@implementation WeaponRocketLauncherProjectile
@synthesize ProjectileDirection;
@synthesize ProjectileSpeed;

-(id) initWithDependencies: (WeaponRocketLauncher*) rocketLauncherParentRefIn
{
    self = [super init];
    
    if (self)
    {
        rocketLauncherParentRef = rocketLauncherParentRefIn;
        
        //Create physics body
        b2BodyDef bodyDef;
        bodyDef.position = b2Vec2(0,0);
        bodyDef.type = b2_dynamicBody;
        bodyDef.userData = self;
        projectileBody = [GB2Engine sharedInstance].world->CreateBody(&bodyDef);
        
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:projectileBody
                                               forShapeName:@"Rocket"];
    }
    return self;
}

-(void) onEnter
{
    NSAssert( NO == CGPointEqualToPoint(self.ProjectileDirection, CGPointZero ),
             @"Uninitialized projectile direction");
    NSAssert( 0 != self.ProjectileSpeed,
             @"Uninitialized projectile speed");
    
    
    [super onEnter];
    
    //Load propulsion effect
    particlePropulsionEffect = (CCParticleSystemQuad*)[CCBReader nodeGraphFromFile:@"GameplayRocketSmoke.ccbi"];
    [self addChild:particlePropulsionEffect];
    
    //Load sprite
    rocketSprite = [CCSprite spriteWithSpriteFrameName:@"Rocket.png"];
    rocketSprite.anchorPoint = [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"Rocket"];
    [self addChild:rocketSprite];
    
    //Set debug draw flag
    [KKConfig selectKeyPath:@"KKStartupConfig"];
    printDebugData = [KKConfig boolForKey:@"EnableProximityMineDebugOutput"];
    
    [self scheduleUpdate];
}

-(void) update:(ccTime)delta
{
    [super update:delta];
    
    //newPosition = currentPosition + ( ( Direction * speed ) * delta )
    //NOTE: We only move if our physics body is available. Otherwise,
    //      we'll invoke a crash.
    if( nil != projectileBody )
    {
        self.position = ccpAdd(self.position,
                               ccpMult( ccpMult(self.ProjectileDirection,
                                                self.ProjectileSpeed),
                                       delta)
                               );
        
        //If the projectile is out of the screen, we shouldn't be using it anymore.
        if( NO == CGRectContainsPoint([CCDirector sharedDirector].screenRect, self.position) )
        {
            [self removeFromParentAndCleanup:YES];
        }
    }
}

-(void) cleanup
{
    [super cleanup];
    
    //There's a chance we haven't hit anything and reached the end of the screen.
    //At that point we haven't destroyed the physics body, so let's do it now.
    //Delete the physics body, we don't need it anymore
    if( nil != projectileBody )
    {
        [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:projectileBody];
        projectileBody = nil;
    }
    
    [rocketSprite stopAllActions]; //just being paranoid here
    [self stopAllActions];
}

-(void) dealloc
{
    CCLOG(@"WeaponRocketLauncherProjectile deleted");
    [super dealloc];
}

-(void) beginContact: (GB2Contact*)contact
{
    //Early-out:
    //We will only process here if the contact object is of type EnemyBase
    //and if we expect the projectile to still be in play.
    if ( NO == [contact.otherObject isKindOfClass:[EnemyBase class]] ||
         nil == projectileBody )
    {
        return;
    }
    
    //Find out if we have a counter stored for this collision
    b2Body* collidingBody = contact.otherFixture->GetBody();
    int collisionCounter;
    
    if( enemyTargetContactCounters.find( collidingBody ) != enemyTargetContactCounters.end() )
        collisionCounter = enemyTargetContactCounters[collidingBody] + 1;
    else
        collisionCounter = 1;
    
    enemyTargetContactCounters[collidingBody] = collisionCounter;
    
    //Found a collision
    if( 1 == collisionCounter && YES == printDebugData)
    {
        CCLOG(@"Enemy: [%@] WITHIN Blast Radius Of Trap: [%@]",
              (EnemyBase*)collidingBody->GetUserData(),
              self
              );
    }
    
    
    //Find out if we have not detonated AND if hit the detection radius
    if( NO == hasDetonated &&
       YES == [@"RocketBody" isEqualToString: (NSString*)contact.ownFixture->GetUserData() ] )
    {
        
        if( YES == printDebugData )
        {
            CCLOG(@"Detonating Trap: %@", self);
        }
        
        //Mark trap as detonated (We don't want to detonate twice).
        hasDetonated = YES;
        
        //Damage the enemies within the mine's blast radius
        for(CollisionCounterLookupTable::iterator iterator = enemyTargetContactCounters.begin();
            iterator != enemyTargetContactCounters.end();
            iterator++)
        {
            // iterator->first = key
            // iterator->second = value
            b2Body* enemyPhysicsBody = iterator->first;
            EnemyBase* enemyRef = static_cast<EnemyBase*>( enemyPhysicsBody->GetUserData() );
            
            [enemyRef reactToWeaponDamage:rocketLauncherParentRef];
        }
        
        //Stop moving (hopefully not exploding)
        [self stopAllActions];
        
        //No more propulsion
        [particlePropulsionEffect stopSystem];
        
        //Delete the physics body, we don't need it anymore
        [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:projectileBody];
        projectileBody = nil;

        //Play detonation sequence
        //Load animation
        const short animationFrameCount = 7;
        float animationDelay = 0.085f;
        
        CCAnimation* explodeAnimation = [CCAnimation animationWithSpriteSequence:@"mineSprite_Explode_b%d.png"
                                                                       numFrames:animationFrameCount
                                                                           delay:animationDelay];
        
        //Configure actions
        CCAnimate* explosionAnimateAction = [CCAnimate actionWithAnimation:explodeAnimation];
        CCCallBlock* removeProjectileInstanceAction = [CCCallBlock actionWithBlock:^{
            [self removeFromParentAndCleanup:YES];
        }];

        [rocketSprite runAction:[CCSequence actions:
                                 explosionAnimateAction,
                                 removeProjectileInstanceAction,
                                 nil]];
        
        //Play explosion sound
        [[SimpleAudioEngine sharedEngine] playEffect:@"OD_SFX_MiniExplosion"];
    }
}

-(void) endContact: (GB2Contact*)contact
{
    //Early-out:
    //We will only process here if the contact object is of type EnemyBase
    //and if we expect the projectile to still be in play.
    if ( NO == [contact.otherObject isKindOfClass:[EnemyBase class]] ||
        nil == projectileBody )
    {
        return;
    }
    
    //Assumes there has been a previous contact before
    b2Body* collidingBody = contact.otherFixture->GetBody();
    
    int collisionCounter = enemyTargetContactCounters[collidingBody] - 1;
    enemyTargetContactCounters[collidingBody] = collisionCounter;
    if( 0 == collisionCounter)
    {
        if( YES == printDebugData )
        {
            CCLOG(@"Enemy: [%@] OUTSIDE Blast Radius Of Trap: [%@]",
                  (EnemyBase*)collidingBody->GetUserData(),
                  self
                  );
        }
        
        //Remove from map.
        enemyTargetContactCounters.erase( collidingBody );
    }
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    //Set the CCNode's position (world & local should match)...
    [super setPosition:positionIn];
    
    CGPoint physicsCoordinates = positionIn;
    
    if( nil != self.parent )
    {
        physicsCoordinates = [self.parent convertToWorldSpace:positionIn];
    }
    
    projectileBody->SetTransform( b2Vec2FromCGPoint(physicsCoordinates), CC_DEGREES_TO_RADIANS(-self.rotation));
}

-(CGPoint) position
{
    CGPoint positionValue = CGPointFromb2Vec2( projectileBody->GetTransform().p );
    
    //If we are in a hierarchy
    if( nil != self.parent )
    {
        //convert to node-coords
        positionValue = [self.parent convertToNodeSpace: positionValue];
    }
    
    return  positionValue;
}
@end
