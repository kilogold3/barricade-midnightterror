//
//  OverdriveExecutionYunaV1.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverdriveExecutionProtocol.h"

@interface OverdriveExecutionLeoV1 : CCNode<OverdriveExecutionProtocol>
{
    
}
-(void) executeOverdrive;
-(void) resetOverdrive;
@end
