//
//  TitleSceneCreatureEyesLayer.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/25/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface TitleSceneCreatureEyes : CCSprite
{
}
@property (nonatomic,readwrite) float OpenEyeDelay; //how long do eyes stay open.
@property (nonatomic,readwrite) float BlinkSpeed;   //how fast do eyes open (and close afterwards).
@property (nonatomic,readwrite) float IntendedScale;//the overall size of the eyes (reference for scaling)

@end

@interface TitleSceneCreatureEyesLayer : CCNode
{
    Byte creatureEyeCount;
}

-(void) startEyes; //Starts loading the eyes

@end
