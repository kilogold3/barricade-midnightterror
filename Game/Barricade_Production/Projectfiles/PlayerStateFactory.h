//
//  PlayerStateFactory.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPlayerState.h"

@class Player;

@interface PlayerStateFactory : NSObject
{
    Player* playerRef;
}

-(id) initWithDependencies: (Player*) playerRefIn;
-(PlayerState*) createPlayerState_Alive;
-(PlayerState*) createPlayerState_Dead;

@end
