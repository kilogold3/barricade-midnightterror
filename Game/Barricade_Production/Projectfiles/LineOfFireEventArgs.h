//
//  LineOfFireEventArgs.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WeaponBase;

@interface LineOfFireEventArgs : NSObject
{
    CGPoint lineOfFireOrigin;
    CGPoint lineOfFireDirection;
    WeaponBase* firingWeapon;
}
@property (readonly) CGPoint LineOfFireOrigin;
@property (readonly) CGPoint LineOfFireDirection;
@property (readonly) WeaponBase* FiringWeapon;

-(id) initWithDependencies: (CGPoint) lineOfFireOriginIn
                          : (CGPoint) lineOfFireDirectionIn
                          : (WeaponBase*) firingWeaponIn;
@end
