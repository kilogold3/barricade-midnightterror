//
//  EnemyVejigante.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 5/30/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnemyVejigante.h"
#import "GMath.h"
#import "EnemyStateFactory.h"
#import "NotificationCenterEventList.h"
#import "CCBReader.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "SimpleAudioEngine.h"

@interface EnemyVejigante (PrivateMethods)
// declare private methods here
@end

@implementation EnemyVejigante
@synthesize VejiganteSprite=vejiganteSprite;
@synthesize Invulnerable=invulnerable;

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
		movementSpeed = 20.0f;
        
        //We don't really use health for the vejigante...
        currentHealth = maxHealth = 0;
        allowHealthDisplay = NO;
        
        //By default, we allow the Vejigante to get hit.
        self.Invulnerable = NO;
                
        [self instantChangeEnemyState: [enemyStateFactory createVejiganteState_Spawn]];
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];
    
    //Load default initial sprite
    vejiganteSprite = [CCSprite spriteWithSpriteFrameName:@"VegifloatSprite0.png"];
    [self addChild:vejiganteSprite];
    
    //Set anchor point
    [vejiganteSprite setAnchorPoint:
     [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"enemyVejiganteShape"]
     ];
    
    //Load shape
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:enemyPhysicsBody
                                           forShapeName:@"enemyVejiganteShape"];
    
    //knock out player's special controls
    SEND_EVENT(EventList::CONTROLS_RESTRICT_CONTROLS, nil);
}

-(void) cleanup
{
	[super cleanup];

	// any cleanup code goes here
    [self removeChild:vejiganteSprite cleanup:YES];
	
	// specifically release/nil any references that could cause retain cycles
	// since dealloc might not be called if this class retains another node that is
   // either a sibling or in a different branch of the node hierarchy
}

-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon
{
    if( NO == self.Invulnerable )
    {
        [self phasingDisplace:ccp(self.position.x, gRangeRand(90,250))];
    }
}

-(void) phasingDisplace: (CGPoint) position
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"VejigantePhasingDisplace.wav"];

    const float FADE_TIME = 0.2f;
    
    CCFadeOut* fadeEnemyOut = [CCFadeOut actionWithDuration:FADE_TIME];
    CCFadeIn* fadeEnemyIn = [CCFadeIn actionWithDuration:FADE_TIME];
    CCCallBlock* moveEnemy = [CCCallBlock actionWithBlock:^{
        [self setPosition:position];
    }];
    
    CCSequence* actionSequence = [CCSequence actions:fadeEnemyOut, moveEnemy, fadeEnemyIn, nil];
    
    
    [vejiganteSprite runAction:actionSequence];
}

/***********************************************************
 Truth be told, these methods should remain empty. We are simply overriding them
 to do nothing. This is because we inherit from EnemyBase, and EnemyBase gives you
 a game over when in contact with the player. We don't want this. We could forward
 the behavior to all enemy's AI-states so they can call "GameOver" instead, but
 every enemy, except this one does that.
 ************************************************************/
-(void) beginContactWithPlayer: (GB2Contact*)contact
{
    ++contactWithPlayerCounter;
    if( 1 == contactWithPlayerCounter )
    {
        //We've collided with the player
    }
}
-(void) endContactWithPlayer: (GB2Contact*)contact
{
    --contactWithPlayerCounter;
}
@end
