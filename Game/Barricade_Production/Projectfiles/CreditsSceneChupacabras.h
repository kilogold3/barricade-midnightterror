//
//  CreditsSceneChupacabras.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "kobold2d.h"

@interface CreditsSceneChupacabras : CCNode
{
    //Actor instance of the chupacabras.
    //It also contains the type of enemy it is.
    CCNode* chupacabraActor;
    
    //Walking direction to see whether it moves from
    //left to right, or right to left.
    SignedByte movementDirection;
    float movementSpeed;
}

-(id) initWithEnemyTypeName: (NSString*) enemyTypeName;


@end
