//
//  WalkerState_AproachBarricade.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WalkerState_Blank.h"

@implementation WalkerState_Blank

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
{
    self = [super init];
    
    if( self != nil)
    {
    }
    
    return self;
}

-(void) enter
{
}

-(void) exit
{
}

-(void) update: (float) deltaTime
{
}
@end
