//
//  BarricadeReinforcer.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/19/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "BarricadeReinforcer.h"
#import "GB2ShapeCache.h"
#import "NotificationCenterEventList.h"

const ccTime REINFORCER_EFFECT_TIME_LENGTH = 10.0f;

@interface BarricadeReinforcer (PrivateMethods)
// declare private methods here
@end

@implementation BarricadeReinforcer

-(id) init
{
	self = [super init];
	if (self)
	{
		// add init code here (note: self.parent is still nil here!)
	}
	return self;
}

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    currentTimer = REINFORCER_EFFECT_TIME_LENGTH;
    reinforcerSprite = [CCSprite spriteWithSpriteFrameName:@"BarricadeReinforcer.png"];
    reinforcerSprite.anchorPoint = [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"barricade"];

    [self addChild:reinforcerSprite];
    
    isConfigured = NO;
    isTransitioningOut = NO;
    [self configureReinforcement];
    
    [self scheduleUpdate];
    
    
    
    REGISTER_EVENT(self,
                   @selector(OverdriveExecutionFinish_EventHandler:),
                   EventList::OVERDRIVE_FINISH_EXECUTION);
    REGISTER_EVENT(self,
                   @selector(OverdriveAnimationStart_EventHandler:),
                   EventList::OVERDRIVE_START_ANIMATION);
    REGISTER_EVENT(self,
                   @selector(OverdriveAnimationFinish_EventHandler:),
                   EventList::OVERDRIVE_FINISH_ANIMATION);
    REGISTER_EVENT(self,
                   @selector(ControlsPauseGameButtonPressed_EventHandler:),
                   EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED);
    REGISTER_EVENT(self,
                   @selector(ControlsResumeGameButtonPressed_EventHandler:),
                   EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED);
}

-(void) cleanup
{
    //Stop everything we were doing
    [self stopAllActions];
    
    [self removeChild:reinforcerSprite cleanup:YES];
    
    UNREGISTER_EVENT(self,
                     EventList::OVERDRIVE_FINISH_EXECUTION);
    UNREGISTER_EVENT(self,
                     EventList::OVERDRIVE_START_ANIMATION);
    UNREGISTER_EVENT(self,
                     EventList::OVERDRIVE_FINISH_ANIMATION);
    UNREGISTER_EVENT(self,
                     EventList::CONTROLS_PAUSEGAME_BUTTON_PRESSED);
    UNREGISTER_EVENT(self,
                     EventList::CONTROLS_RESUMEGAME_BUTTON_PRESSED);
    
    [super cleanup];
}

-(void) OverdriveExecutionFinish_EventHandler: (NSNotification*) notification
{
    isOverdrivePlaying = NO;
}
-(void) OverdriveAnimationStart_EventHandler: (NSNotification*) notification
{
    isOverdrivePlaying = YES;
}

-(void) OverdriveAnimationFinish_EventHandler: (NSNotification*) notification
{
    //If it's a hit, we are still playing up to the execution.
    const BOOL isOverdriveHit = [[notification object] boolValue];
    isOverdrivePlaying = isOverdriveHit;
    
}
-(void) ControlsPauseGameButtonPressed_EventHandler: (NSNotification*) notification
{
    isGamePaused = YES;
}
-(void) ControlsResumeGameButtonPressed_EventHandler: (NSNotification*) notification
{
    isGamePaused = NO;
}

-(void) configureReinforcement
{
    NSAssert(NO == isConfigured, @"Attempting to configure, but already configured.");
    
    //Let's stop the out-transitioning effect, and reset the opacity
    //in case it has begun to fade out.
    [reinforcerSprite stopAllActions];
    reinforcerSprite.opacity = 255;
    
    //Reset the timer.
    currentTimer = REINFORCER_EFFECT_TIME_LENGTH;
    isConfigured = YES;
    isTransitioningOut = NO;
}

-(void) update:(ccTime)delta
{
    //Early-out
    if( isOverdrivePlaying || isGamePaused )
    {
        return;
    }

    if( currentTimer <= 0 && NO == isTransitioningOut )
    {
        isTransitioningOut = YES;
        
        CCCallBlock* finalizeEffect = [CCCallBlock actionWithBlock:^{
            //Remove the reinforcement from the hierarchy
            [self removeFromParentAndCleanup:YES];
            
            //Now that we have been removed from the hierarchy, we can safely call this event which will invoke
            //multiple objects that ultimately check for Barricade::IsInvincible. We want to make sure
            //that function returns the proper value, which is produced by checking if we are
            //in the hierarchy.
            SEND_EVENT(EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_END, nil);
        }];
        
        
        [reinforcerSprite runAction:[CCSequence actions:
                                     [CCDelayTime actionWithDuration:0.3],
                                     [CCFadeOut actionWithDuration:0.3],
                                     finalizeEffect,
                                     nil]];
    }
    else
    {
        currentTimer -= delta;
        
        if( reinforcerSprite.color.r == ccWHITE.r &&
            reinforcerSprite.color.g == ccWHITE.g &&
           reinforcerSprite.color.b == ccWHITE.b )
        {
            reinforcerSprite.color = ccYELLOW;
        }
        else
        {
            reinforcerSprite.color = ccWHITE;
        }
    }
}

-(void) restartReinforcement
{
    //Stop everything we were doing
    [reinforcerSprite stopAllActions];
    
    //reconfigure
    isConfigured = NO;
    [self configureReinforcement];
}

@end
