//
//  InputTouchPanel.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InputTouchPanel.h"
#import "kobold2d.h"

@implementation InputTouchPanel
@synthesize AcquiredTouch=acquiredTouch;

-(id) initWithRect: (CGRect) touchRectIn
{
    self = [super init];
    
    if( self != nil)
    {
        acquiredTouch = nil;
        touchRect = touchRectIn;
    }
    return self;
}

-(KKTouch*) processTouchesWithException: (KKTouch*) TouchException
                         relativeToNode: (CCNode*) node
{
    KKInput* input = [KKInput sharedInput];
    
    //Do an early-out test.
    //If there are no touches in the screen right now, it's pretty clear
    //that there is no acquired touch.
    if( NO == input.touchesAvailable )
    {
        acquiredTouch = nil;
        return acquiredTouch;
    }
    
    for (KKTouch* curTouch in input.touches) 
    {
        //First, check to see if this is a valid exception touch...
        //If it is, we skip it.
        if( nil != TouchException && TouchException.touchID == curTouch.touchID )
            continue;
        
        //Generate the touch (world) coords relative to a CCNode's (local) coords
        const CGPoint nodeLocalTouchLocation = [node convertToNodeSpace:curTouch.location];
                
        //If we have no touch, AND we are starting the touch...
        if( nil == acquiredTouch && curTouch.phase == KKTouchPhaseBegan )
        {
            //if the touch is within the touch panel
            if( CGRectContainsPoint( touchRect, nodeLocalTouchLocation ) )
            {
                //assign the new touch
                acquiredTouch = curTouch;
                
                //leave function
                return acquiredTouch;
            }
        }
        else //we have a touch... 
        {
            //if id's match, AND ( the touch has ended OR touch is released OR is out of the area)
            if( acquiredTouch.touchID == curTouch.touchID )
            {
                if( curTouch.phase == KKTouchPhaseEnded || 
                    curTouch.phase == KKTouchPhaseLifted ||
                    NO == CGRectContainsPoint( touchRect, nodeLocalTouchLocation )
                   )
                {
                    //remove touch 
                    acquiredTouch = nil;
                    return acquiredTouch;
                }                
            }
        }
    }
    
    //If we are here, we have a previously acquired touch past the "Began" phase
    //which is still within the range of the panel.
    //we just return it.
    return acquiredTouch;
}

@end