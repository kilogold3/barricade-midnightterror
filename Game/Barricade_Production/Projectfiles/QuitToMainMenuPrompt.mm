//
//  QuitToMainMenuPrompt.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 11/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "QuitToMainMenuPrompt.h"
#import "NotificationCenterEventList.h"
#import "CCBReader.h"
#import "TitleScene.h"

@implementation QuitToMainMenuPrompt

-(void) toMainMenu
{
    [[CCDirector sharedDirector] replaceScene: [TitleScene node]];
}

-(void) toPreviousScreen
{
    SEND_EVENT(EventList::UI_DISMISS_QUIT_PROMPT, nil);
    [self removeFromParentAndCleanup:YES];
}

@end
