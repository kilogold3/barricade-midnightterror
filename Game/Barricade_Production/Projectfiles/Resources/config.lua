--[[
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
--]]


--[[
		* Need help with the KKStartupConfig settings?
		* ------ http://www.kobold2d.com/x/ygMO ------
--]]


local config =
{
	KKStartupConfig = 
	{
		-- load first scene from a class with this name, or from a Lua script with this name with .lua appended
		FirstSceneClassName = "TitleScene",

		MaxFrameRate = 60,
		DisplayFPS = NO,

		EnableUserInteraction = YES,
		EnableMultiTouch = YES,

		-- Render settings
		DefaultTexturePixelFormat = TexturePixelFormat.RGBA8888,
		GLViewColorFormat = GLViewColorFormat.RGBA565, --Enabled for 3D CCActions
		GLViewDepthFormat = GLViewDepthFormat.DepthNone,
		GLViewMultiSampling = NO,
		GLViewNumberOfSamples = 2,

		Enable2DProjection = NO,
		EnableRetinaDisplaySupport = YES,
		EnableGLViewNodeHitTesting = NO,
		EnableStatusBar = NO,

        -- Debug Render Settings
        EnableCollisionDebugDraw = NO,
        EnableLineOfFireDebugDraw = NO,
        EnableControlTouchPanelDebugDraw = NO,
        EnableProximityMineDebugOutput = NO,
        EnableWeaponFireDebugOutput = NO,
        EnableEnemyHitDebugOutput = NO,
        EnableCCSpriteFrameCacheDebugOutput = NO,

		-- Orientation & Autorotation
		-- Kobold2D uses the supported orientations from the Target's Summary pane: http://cl.ly/2l132Z2f463H2O3r0M1O
		-- (same as Info.plist key UISupportedInterfaceOrientations aka "Supported interface orientations")
	
		-- iAd setup
		EnableAdBanner = NO,
		PlaceBannerOnBottom = YES,
		LoadOnlyPortraitBanners = NO,
		LoadOnlyLandscapeBanners = YES,
		AdProviders = "iAd",	-- comma seperated list -> "iAd, AdMob" means: use iAd if available, otherwise AdMob
		AdMobRefreshRate = 15,
		AdMobFirstAdDelay = 5,
		AdMobPublisherID = "a15225686735299", -- how to get an AdMob Publisher ID: http://developer.admob.com/wiki/PublisherSetup
		AdMobTestMode = YES,

		-- Mac OS specific settings
		AutoScale = NO,
		AcceptsMouseMovedEvents = NO,
		EnableFullScreen = NO,
	},
	
	-- you can create your own config sections using the same mechanism and use KKConfig to access the parameters
	-- or use the KKConfig injectPropertiesFromKeyPath method

    ResourceGatheringSceneConfig =
    {
        MaxLevelTime = 18,
    },

	GameplaySceneConfig_00A = 
	{		
        EnemyAppearanceDay1_Types = "EnemyWalker|EnemyFlier",
        EnemyAppearanceDay1_Probabilities = "95|5",

        EnemyAppearanceDay3_Types = "EnemyWalker|EnemyFlier",
        EnemyAppearanceDay3_Probabilities = "60|40",

        EnemyAppearanceDay5_Types = "EnemyWalker|EnemyFlier",
        EnemyAppearanceDay5_Probabilities = "50|50",

        SpawnBaseTimerMin = 				0.5,
        SpawnBaseTimerMax = 				1.5,
        SpawnModifier_WeaponUpgradeLevel2 = -0.5,
        SpawnModifier_WeaponUpgradeLevel3 = -1,
        SpawnModifier_DaySpent = 			-0.15,
        SpawnModifier_DaySpentLimit = 		0.5,
        MinCountPerSpawn = 					1,
        MaxCountPerSpawn = 					1,
	},

	GameplaySceneConfig_01A = 
	{		
        EnemyAppearanceDay1_Types = "EnemyWalker|EnemyFlier|EnemyTank",
        EnemyAppearanceDay1_Probabilities = "50|49|1",

        EnemyAppearanceDay3_Types = "EnemyWalker|EnemyFlier|EnemyTank",
        EnemyAppearanceDay3_Probabilities = "49|48|3",

        EnemyAppearanceDay5_Types = "EnemyWalker|EnemyFlier|EnemyTank",
        EnemyAppearanceDay5_Probabilities = "48|47|5",

        SpawnBaseTimerMin = 				0.5,
        SpawnBaseTimerMax = 				1.3,
        SpawnModifier_WeaponUpgradeLevel2 = -0.5,
        SpawnModifier_WeaponUpgradeLevel3 = -1,
        SpawnModifier_DaySpent = 			-0.15,
        SpawnModifier_DaySpentLimit = 		0.4,
        MinCountPerSpawn = 					1,
        MaxCountPerSpawn = 					2,
	},

    -- This is the Vejigante Battle Scene.
    -- Attributes here are a little different because
    -- we use a special spawner
	GameplaySceneConfig_02A = 
	{		
        SpawnBaseTimerMin = 				0.5,
        SpawnBaseTimerMax = 				0.8,
        SpawnModifier_WeaponUpgradeLevel2 = -0.5,
        SpawnModifier_WeaponUpgradeLevel3 = -1,
        SpawnModifier_DaySpent = 			-0.15,
        SpawnModifier_DaySpentLimit = 		0.3,
        MinCountPerSpawn = 					1,
        MaxCountPerSpawn = 					1,
	},

	GameplaySceneConfig_03A = 
	{		
        EnemyAppearanceDay1_Types =  "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay1_Probabilities = "52|25|3|20",

        EnemyAppearanceDay3_Types = "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay3_Probabilities = "45|27|5|25",

        EnemyAppearanceDay5_Types = "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay5_Probabilities = "30|35|8|30",

        SpawnBaseTimerMin = 				0.5,
        SpawnBaseTimerMax = 				1.1,
        SpawnModifier_WeaponUpgradeLevel2 = -0.5,
        SpawnModifier_WeaponUpgradeLevel3 = -1,
        SpawnModifier_DaySpent = 			-0.15,
        SpawnModifier_DaySpentLimit = 		0.4,
        MinCountPerSpawn = 					1,
        MaxCountPerSpawn = 					2,
	},

	GameplaySceneConfig_04A = 
	{		
        EnemyAppearanceDay1_Types = "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay1_Probabilities = "35|20|5|40",

        EnemyAppearanceDay3_Types = "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay3_Probabilities = "35|20|10|35",

        EnemyAppearanceDay5_Types = "EnemyWalker|EnemyFlier|EnemyTank|EnemyRunner",
        EnemyAppearanceDay5_Probabilities = "32|15|15|38",

        SpawnBaseTimerMin = 				0.5,
        SpawnBaseTimerMax = 				0.9,
        SpawnModifier_WeaponUpgradeLevel2 = -0.5,
        SpawnModifier_WeaponUpgradeLevel3 = -1,
        SpawnModifier_DaySpent = 			-0.15,
        SpawnModifier_DaySpentLimit = 		0.4,
        MinCountPerSpawn = 					1,
        MaxCountPerSpawn = 					2,
	},

	EnemyWalkerSettings =
	{
        MinStartHealth = 					16,
	    MaxStartHealth = 					20,
        AttackDamage = 						1,
        MinMovementSpeed = 					35,
        MaxMovementSpeed = 					40,    
        OverdriveReward = 					5,
        WeaponBoostReward = 				10,
	},

	EnemyTankSettings =
	{
		MaxStartHealth = 					80,
		MinStartHealth = 					50,
		AttackDamage = 						10,
		MinMovementSpeed = 					20,
		MaxMovementSpeed = 					30,
		OverdriveReward = 					25,
        WeaponBoostReward = 				35,
	},
	
	EnemyFlierSettings =
	{
		MaxStartHealth = 					16,
		MinStartHealth = 					20,
		AttackDamage = 						10,
		MinMovementSpeed = 					45,
		MaxMovementSpeed = 					50,
		OverdriveReward = 					5,
        WeaponBoostReward = 				10,
	},

	EnemyRunnerSettings =
	{
        MinStartHealth = 					10,
	    MaxStartHealth = 					20,
        AttackDamage = 						15,
        MinMovementSpeed = 					80,
        MaxMovementSpeed = 					100,
        OverdriveReward = 					3,
        WeaponBoostReward = 				10,
	},
	
    PlayerSettings =
	{
		MaxWeaponBoostTime = 3,
	},

    WeaponSettings =
    {
        PistolDamage = 8,
        RifleDamage = 9,
        RocketDamage = 11,
    },

    OverdriveSettings =
    {
        JeromeOverdriveDamageMin = 20,
        JeromeOverdriveDamageMax = 40,
    },

    GameSettings =
    {
        OleanderCityDaysDeadline = 20,
    },
}

return config
