//
//  PlayerStateFactory.m
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerStateFactory.h"
#import "PlayerState_Alive.h"
#import "PlayerState_Dead.h"

@implementation PlayerStateFactory

-(id) initWithDependencies: (Player*) playerRefIn
{
    self = [super init];
    
    if( self != nil)
    {
        playerRef = playerRefIn;
    }
    return self;
}

-(PlayerState*) createPlayerState_Alive
{
    PlayerState* newPlayerState = [[[PlayerState_Alive alloc] initWithDependencies:playerRef] autorelease];
    return newPlayerState;
}

-(PlayerState*) createPlayerState_Dead
{
    PlayerState* newPlayerState = [[[PlayerState_Dead alloc] init] autorelease];
    return newPlayerState;
}

@end
