//
//  OverdriveExecutionYunaV1.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "OverdriveExecutionYunaV1.h"
#import "NotificationCenterEventList.h"
#import "Barricade.h"
#import "SimpleAudioEngine.h"

@implementation OverdriveExecutionYunaV1

-(void) executeOverdrive
{

    const ccTime FLASH_FADEOUT_DURATION = 0.4f;
    const ccTime ENEMY_DEATH_ANIMATION_DURATION = 1.5f;
    const CGSize screenSize = [CCDirector sharedDirector].screenSize;
    

    
    CCCallBlock* flashFadeOutAction = [CCCallBlock actionWithBlock:
                                       ^{
                                           //Clear out the enemies in the barricade area
                                           SEND_EVENT(EventList::OVERDRIVE_YUNA_CLEAR_BARRICADE_AREA, nil);
                                           
                                           //Add a white flash to the screen
                                           CCLayerColor* whiteFlashLayer = [CCLayerColor layerWithColor:ccc4(255, 255, 255, 255)
                                                                                                  width:screenSize.width
                                                                                                 height:screenSize.height];
                                           [self addChild:whiteFlashLayer];
                                           
                                           //Repair the barricade hiding in the flash
                                           [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START
                                                                                               object:[NSValue valueWithPointer:nil]];
                                           
                                           //Play barricade repair sound
                                           [[SimpleAudioEngine sharedEngine] playEffect:@"BarricadeRepaired.wav"];
                                                                                      
                                           //fade out the white flash
                                           [whiteFlashLayer runAction:[CCFadeOut actionWithDuration:FLASH_FADEOUT_DURATION]];
                                           
                                           //Finish the overdrive
                                           [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_FINISH_EXECUTION
                                                                                               object:nil];
                                           
                                           //Reset the overdrive for the next use
                                           [self resetOverdrive];
                                           
                                       }];
    
    

    
    CCCallBlock* killAllEnemiesInBarricadeArea = [CCCallBlock actionWithBlock:
                                                  ^{
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:EventList::OVERDRIVE_YUNA_KILL_ENEMIES_IN_BARRICADE_AREA
                                                                                                          object:nil];
                                                  }];
    
    /***************************************************
     *  1) Kill any enemies in the barricade area
     *  2) Wait a few for the death animations to play
     *  3) Flash the screen
     *  4) Repair the barricade
     *  5) Resume game
     ***************************************************/
    CCSequence* actionSequence = [CCSequence actions:
                                  killAllEnemiesInBarricadeArea,
                                  [CCDelayTime actionWithDuration:ENEMY_DEATH_ANIMATION_DURATION],
                                  flashFadeOutAction,
                                  [CCDelayTime actionWithDuration:FLASH_FADEOUT_DURATION],
                                  [CCCallFunc actionWithTarget:self selector:@selector(cleanupOverdrive)],
                                  nil];

    [self runAction:actionSequence];
}
-(void) resetOverdrive
{
    
}

-(void) cleanupOverdrive
{
    //Remove the execution so it can be added again later.
    //The execution is persistent and will rely on a 'reset'
    //feature to manage itself. The execution removes itself
    //when the player is deleted.
    [self.parent removeChild:self cleanup:YES];
    
    CCLOG(@"YUNA Overdrive Execution Complete!");
}
@end
