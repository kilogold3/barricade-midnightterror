//
//  DialogueScene.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 8/9/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "DialogueScene.h"
#import "DialogueSceneInstance.h"
#import "CCBReader.h"
#import "GameSaveData.h"
#import "GameSaveDataPropertyList.h"
#import "AppWidescreener.h"

@implementation DialogueScene

-(id) init
{
    if( self = [super init])
    {
    }
    
    return self;
}

-(void) onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    //Offsetting the scene must occur before any objects are added.
    //This is because objects that do not want to be offset (like box2d debug draw layer)
    //will be added with the scene's negated position in order to counteract the offset,
    //thus leaving the object at the origin.
    //
    //An object negating the position would have the same x/y-components but in negative value.
    //To offset the screen, we do it via the AppWidescreener
    //[AppWidescreener applyLetterboxToScene:self];
    
    /////////////////////////////////////////////////////////////
    //Get all the mumbo-jumbo to load the DialogueSceneInstance
    /////////////////////////////////////////////////////////////
    
    //First, find out if we're loading a standard, tutorial, or an ending dialogue
    SaveObjectPropertyList* resourceGatheringProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::RESOURCE_GATHERING_SCENE];
    SaveObjectPropertyList* gameProgressProperties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::GAME_PROGRESS];
    const int currentOverallDay = [[gameProgressProperties objectForKey:GSP::CURRENT_OVERALL_DAY] intValue];
    [KKConfig selectKeyPath:@"GameSettings"];
    const int oleanderCityDaysLimit = [KKConfig intForKey:@"OleanderCityDaysDeadline"];
    
    NSString* CCB_DialogueSceneFilename = nil;
    if( currentOverallDay > oleanderCityDaysLimit )
    {
        CCB_DialogueSceneFilename = [NSString stringWithFormat:@"DS_BadEnding%@.ccbi",
                                     [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    }
    else if (YES == [[resourceGatheringProperties valueForKey:GSP::TUTORIAL_TO_BE_EXECUTED] boolValue])
    {
        CCB_DialogueSceneFilename = [NSString stringWithFormat:@"DS_Tutorial%@.ccbi",
                                     [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    }
    else
    {
        int currentlyLoadedStoryChapter = [[gameProgressProperties objectForKey:GSP::CURRENT_PROGRESSION] intValue];
        CCB_DialogueSceneFilename = [NSString stringWithFormat:@"DS_%02iA%@.ccbi",
                                     currentlyLoadedStoryChapter,
                                      [AppWidescreener isWidescreenEnabled] ? @"__Wide" : @""];
    }
    
    CCLOG(@"Loading Dialogue Scene: %@", CCB_DialogueSceneFilename);
    CCNode* ccbNode = [CCBReader nodeGraphFromFile:CCB_DialogueSceneFilename];
    NSAssert([ccbNode isKindOfClass: [DialogueSceneInstance class]], @"Incorrect Custom Class");
    dialogueSceneInstance = static_cast<DialogueSceneInstance*>(ccbNode);
    
    //Load the dialogue-advance icon
    advanceSignalSprite = [CCSprite spriteWithFile:@"CursorHand.png"];
    advanceSignalSprite.position = ccp([CCDirector sharedDirector].screenSize.width,0);
    advanceSignalSprite.anchorPoint = ccp(1.3f,-0.1f);
    advanceSignalSprite.opacity = 0;
    advanceSignalSprite.scale = 0.6f;
    
    const float fadeTime = 1.0f;
    [advanceSignalSprite runAction:[CCRepeatForever actionWithAction:
                                    [CCSequence actionOne:[CCFadeIn actionWithDuration:fadeTime]
                                                      two:[CCFadeOut actionWithDuration:fadeTime]]]];
    
    //initialize proceedures here...
    [self addChild:dialogueSceneInstance];
    [self addChild:advanceSignalSprite];
    [self scheduleUpdate];
}

// scheduled update method
-(void) update:(ccTime)delta
{
    if(
       [[KKInput sharedInput] isAnyTouchOnNode:self
                                    touchPhase:KKTouchPhaseBegan] )
    {
        if( YES == dialogueSceneInstance.IsSequencePaused )
        {
            [dialogueSceneInstance resumeSequence];
        }
    }
}

@end
