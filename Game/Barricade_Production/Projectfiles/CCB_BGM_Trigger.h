//
//  CCBSoundTrigger.h
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@interface CCB_BGM_Trigger : CCNode
{
    NSString* soundFilename;
    BOOL isLooping;
}

@end
