//
//  CCBSoundTrigger.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/6/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "CCB_SFX_Trigger.h"
#import "SimpleAudioEngine.h"

@implementation CCB_SFX_Trigger

-(void) didLoadFromCCB
{
    soundSource = [[SimpleAudioEngine sharedEngine] soundSourceForFile: soundFilename];
    [soundSource retain];
    
    if( nil == soundSource )
    {
        CCLOG(@"WARNING: Loading SFX failed for %@",soundFilename);
    }
}

-(void) setVisible:(BOOL)visible
{
    //If we are setting the same state it is already in,
    //we will ignore.
    //NOTE:
    //Keep in mind that this means we need to know when the sound
    //stopped if we want to play it again. Otherwise, it will get
    //ignored.
    if(self.visible == visible)
    {
        return;
    }
    
    [super setVisible:visible];
    
    if( self.visible )
    {
        soundSource.looping = isLooping;
        [soundSource play];
    }
    else
    {
        [soundSource stop];
    }
}

-(void) setPosition:(CGPoint)position
{
    [super setPosition:position];
    
    if( soundSource != nil)
    {
        [soundSource stop];
    }
}

-(void) dealloc
{
    [super dealloc];
    if( soundSource != nil)
    {
        [soundSource stop];
        [soundSource release];
        soundSource= nil;
    }
}

-(void) setRotation:(float)rotation
{
    NSAssert( rotation >= 0 and rotation <= 100, @"Rotation/Volume out of bounds");
    
    [super setRotation:rotation];
    
    soundSource.gain = rotation/100.0f;
}
@end