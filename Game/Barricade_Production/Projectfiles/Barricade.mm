//
//  Barricade.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Barricade.h"
#import "Box2D.h"
#import "GB2ShapeCache.h"
#import "GB2Engine.h"
#import "NotificationCenterEventList.h"
#import "EnemyBase.h"
#import "GameSaveDataPropertyList.h"
#import "GameSaveData.h"
#import "SimpleAudioEngine.h"
#import "BarricadeReinforcer.h"

const int BARRICADE_ENFORCER_TAG = 1;

///////////////////////
//SECRET CLASS
///////////////////////
@interface BarricadeSegment : CCSprite
{
    b2Body* physicsBody;
}
-(id) initWithSpriteFrameName:(NSString *)spriteFrameName;
@end

@implementation BarricadeSegment

-(id) initWithSpriteFrameName:(NSString *)spriteFrameName
{
    self = [super initWithSpriteFrameName:spriteFrameName];
    
    if( self != nil )
    {
        b2BodyDef bodyDefinition;
        bodyDefinition.position = b2Vec2(0,0);
        bodyDefinition.userData = self;
        physicsBody = [GB2Engine sharedInstance].world->CreateBody( &bodyDefinition );
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:physicsBody forShapeName:@"barricade"];
    }
    
    return self;
}

//Method overriding:
//These methods override the set/get position from Cocos2D.
//This is supposed to manipulate
-(void) setPosition:(CGPoint)positionIn
{
    //Set the CCNode's position (world & local should match)...
    [super setPosition:positionIn];
    
    CGPoint physicsCoordinates = positionIn;
    
    if( nil != self.parent )
    {
        physicsCoordinates = [self.parent convertToWorldSpace:positionIn];
    }
    
    physicsBody->SetTransform( b2Vec2FromCGPoint(physicsCoordinates), 0);
}

-(CGPoint) position
{
    CGPoint positionValue = CGPointFromb2Vec2( physicsBody->GetTransform().p );
    
    //If we are in a hierarchy
    if( nil != self.parent )
    {
        //convert to node-coords
        positionValue = [self.parent convertToNodeSpace: positionValue];
    }
    
    return  positionValue;
}

-(void) cleanup
{
    physicsBody->SetUserData(NULL);
    [[GB2Engine sharedInstance] addBodyToPostWorldStepBodyRemovalList:physicsBody];
    physicsBody = NULL;
    [super cleanup];
}

-(void) dealloc
{
    CCLOG(@"Dealloc: Barricade Segment");
    [super dealloc];
}

@end

///////////////////////
//ACTUAL CLASS
///////////////////////
@implementation Barricade
@synthesize MaxHealth=maxHealth;
@synthesize CurrentHealth=currentHealth;
@synthesize BarricadeSprite=barricadeSegment;
@synthesize IsDefending;

-(BOOL) IsInvincible
{
    return (nil != [self getChildByTag:BARRICADE_ENFORCER_TAG]);
}

-(BOOL) IsDefending
{
    return ( self.CurrentHealth > 0 || YES == self.IsInvincible);
}

-(void) setCurrentHealth:(int)CurrentHealth
{
    //Early-Out:
    //If we are attempting to lower the health value, we will
    //only do so if we are NOT invincible.
    if( YES == self.IsInvincible )
    {
        return;
    }
    
    //Make sure we are not setting health beyond max
    NSAssert(CurrentHealth <= maxHealth, @"The selected health value is not within range");
        
    //Set health
    int previousCurrentHealth = currentHealth;
    currentHealth = CurrentHealth;
    
    CCSpriteFrameCache* spriteFrameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
                
        //Fully repaired: 51-100
        if (currentHealth > 51 && currentHealth <= 100)
        {
            CCSpriteFrame* spriteFrame = [spriteFrameCache spriteFrameByName:barricadeSpriteFrameNameA];
            
            //If we don't have this frame set...
            if( NO == CGPointEqualToPoint(spriteFrame.rect.origin, barricadeSegment.displayFrame.rect.origin) )
            {
                barricadeSegment.opacity = 255;
                [barricadeSegment setDisplayFrame: spriteFrame];
            }
        }
        
        //mild damage: 11-50
        else if (currentHealth >= 11 && currentHealth <= 50)
        {
            CCSpriteFrame* spriteFrame = [spriteFrameCache spriteFrameByName:barricadeSpriteFrameNameB];
            
            //If we don't have this frame set...
            if( NO == CGPointEqualToPoint(spriteFrame.rect.origin, barricadeSegment.displayFrame.rect.origin) )
            {
              barricadeSegment.opacity = 255;
              [barricadeSegment setDisplayFrame: spriteFrame];
            }
        }
        
        //severe damage: 1 - 10
        else if (currentHealth >= 1 && currentHealth <= 10)
        {
            CCSpriteFrame* spriteFrame = [spriteFrameCache spriteFrameByName:barricadeSpriteFrameNameC];
            
            //If we don't have this frame set...
            if( NO == CGPointEqualToPoint(spriteFrame.rect.origin, barricadeSegment.displayFrame.rect.origin) )
            {
                barricadeSegment.opacity = 255;
                [barricadeSegment setDisplayFrame: spriteFrame];
            }
        }
        
        //destroyed: 0
        else if (currentHealth <= 0)
        {
            //At this point we assume barricade is dead. Let's lock health at zero.
            currentHealth = 0;
            
            //If we just hit zero HP on this frame...
            if( previousCurrentHealth > 0 )
            {
                //Post a notification about the destruction of the barricade
                [[NSNotificationCenter defaultCenter] postNotificationName: EventList::GAMEPLAY_BARRICADE_DESTROYED
                                                                    object: nil];
                
                //Play the transitional sound effect of the barricade powering down.
                [[SimpleAudioEngine sharedEngine] playEffect:@"BarricadeDown.wav"];
                
                //Instead of setting a new frame here... let's just fade out.
                [barricadeSegment runAction:[CCFadeOut actionWithDuration:0.3f]];
            }
            //Otherwise, we hit zero previously. Since we only hit zero once per round, we can
            //assume that we hit zero last round. So all we need to do here is make sure the proper
            //visual representation of the barricade is displayed (in this case, it's invisible)
            else
            {
                barricadeSegment.opacity = 0;
            }
            
            //At the end of it all, let's make sure we have the proper frameID for the dead barricade.
            CCSpriteFrame* spriteFrame = [spriteFrameCache spriteFrameByName:barricadeSpriteFrameNameD];
            if( NO == CGPointEqualToPoint(spriteFrame.rect.origin, barricadeSegment.displayFrame.rect.origin) )
            {
                [barricadeSegment setDisplayFrame: spriteFrame];
            }
        }
    
    //Notify the game that the health has changed
    if( previousCurrentHealth != currentHealth )
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:EventList::GAMEPLAY_BARRICADE_HEALTH_CHANGED
                                                            object:[NSNumber numberWithInt:currentHealth]];
    }
}

-(void) setPosition:(CGPoint)position
{
    [super setPosition:position];
    [barricadeSegment setPosition:CGPointZero];
}

-(id) init
{
    self = [super init];
    
    if( self != nil )
    {        
        //Load save data
        [self loadBarricadeSaveData];
        
        //Set strings for sprite filenames
        //The defacto name goes like this:
        //
        //      Stage1_TypeA_BarricadeSprite1.png
        //
        NSString* frameNameStagePrefix = @"Stage1_";
        NSString* frameNameTypePrefix = @"TypeD_";
        NSString* frameName = @"BarricadeSprite";
        NSString* frameNameExtentionSuffix = @".png";
        barricadeSpriteFrameNameA = [NSString stringWithFormat:@"%@%@%@1%@",
                                     frameNameStagePrefix,
                                     frameNameTypePrefix,
                                     frameName,
                                     frameNameExtentionSuffix];
        [barricadeSpriteFrameNameA retain];
        
        barricadeSpriteFrameNameB = [NSString stringWithFormat:@"%@%@%@2%@",
                                     frameNameStagePrefix,
                                     frameNameTypePrefix,
                                     frameName,
                                     frameNameExtentionSuffix];
        [barricadeSpriteFrameNameB retain];
        
        barricadeSpriteFrameNameC = [NSString stringWithFormat:@"%@%@%@3%@",
                                     frameNameStagePrefix,
                                     frameNameTypePrefix,
                                     frameName,
                                     frameNameExtentionSuffix];
        [barricadeSpriteFrameNameC retain];
        
        barricadeSpriteFrameNameD = [NSString stringWithFormat:@"%@%@%@4%@",
                                     frameNameStagePrefix,
                                     frameNameTypePrefix,
                                     frameName,
                                     frameNameExtentionSuffix];
        [barricadeSpriteFrameNameD retain];
        
        
        //instantiate
        barricadeSegment = [[[BarricadeSegment alloc] initWithSpriteFrameName:barricadeSpriteFrameNameA] autorelease];

        
        //Add to parent
        [self addChild:barricadeSegment];

        
        //set anchors
        CGPoint anchorPoint = [[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"barricade"];
        [barricadeSegment setAnchorPoint:anchorPoint ];
        
        //Listen to events
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(OverdriveUnaRestoreBarricade_EventHandler:)
                                                     name:EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(enemyStrikeEventHandler:) 
                                                     name:EventList::GAMEPLAY_ENEMY_STRIKE_BARRICADE 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(acquireBarricadeEventHandler:) 
                                                     name:EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(saveBarricadeSaveData:)
                                                     name:EventList::DATA_SAVE_BARRICADE_HEALTH
                                                   object:nil];
        
        //Set currentHealth to currentHealth
        //I know it's redundant, but it's to invoke the correct sprite
        //images based on the health value. This can be done a better way, I suppose.
        [self setCurrentHealth:currentHealth];
    }
    
    return self;
}

-(void) cleanup
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::OVERDRIVE_YUNA_REINFORCE_BARRICADE_START
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::GAMEPLAY_ENEMY_STRIKE_BARRICADE  
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:EventList::COMPONENT_ACQUIRE_BARRICADE_TARGET 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EventList::DATA_SAVE_BARRICADE_HEALTH
                                                  object:nil];    
    [barricadeSpriteFrameNameA release];
    [barricadeSpriteFrameNameB release];
    [barricadeSpriteFrameNameC release];
    [barricadeSpriteFrameNameD release];
    
    [self removeAllChildrenWithCleanup:YES];
    [super cleanup];
}

-(void) dealloc
{
    CCLOG(@"Dealloc: Barricade");
    [super dealloc];
}

-(void) enemyStrikeEventHandler: (NSNotification*) notification
{
    int enemyAttackDamage = [[notification object] intValue];
    [self setCurrentHealth: self.CurrentHealth - enemyAttackDamage];
}

-(void) OverdriveUnaRestoreBarricade_EventHandler: (NSNotification*) notification
{
    BarricadeReinforcer* barricadeEnforcerRef = static_cast<BarricadeReinforcer*>([self getChildByTag:BARRICADE_ENFORCER_TAG]);
    
    //If we did not find a barricade reinforcer, let's create one
    if( nil == barricadeEnforcerRef )
    {
        [self addChild:[BarricadeReinforcer node] z:1 tag:BARRICADE_ENFORCER_TAG];
    }
    //Otherwise, restart the reinforcer
    else
    {
        [barricadeEnforcerRef restartReinforcement];
    }
    
}

-(void) onEnter
{
    [super onEnter];
    
    //Redundant call, but used to invoke the underlying mechanics of synchronizing
    //the physics body from Box2D with the barricade object from Cocos2D.
    self.position = self.position;
}

-(void) acquireBarricadeEventHandler: (NSNotification*) notification
{
    NSValue* requestorRefPointer = [notification object];
    Barricade** targetBarricadeRef = (Barricade**)[requestorRefPointer pointerValue];
    (*targetBarricadeRef) = self;
}

-(void) loadBarricadeSaveData
{
    //Set base variables
    SaveObjectPropertyList* properties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE];
    currentHealth  = [[properties objectForKey:GSP::BARRICADE_HEALTH] intValue];
    maxHealth = 100;
}

-(void) saveBarricadeSaveData: (NSNotification*) notification
{
    //Write base variables
    SaveObjectPropertyList* properties = [[GameSaveData sharedGameSaveData] getSaveObjectProperties:GSC::BARRICADE];
    [properties setValue: [NSNumber numberWithInt:currentHealth] forKey:GSP::BARRICADE_HEALTH];
}
@end
