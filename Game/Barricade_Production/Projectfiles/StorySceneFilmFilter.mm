//
//  StorySceneFilmFilter.mm
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 12/10/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "StorySceneFilmFilter.h"
#import "AppWidescreener.h"

@implementation StorySceneFilmFilter

-(void) onEnter
{
	[super onEnter];

	// add init code here where you need to use the self.parent reference
	// generally recommended to run node initialization here
    
    if( YES == [AppWidescreener isWidescreenEnabled] )
    {
        //self.scaleX = 1.17f;
    }
}


@end
