//
//  ItemContainer.mm
//  Barricade_Production
//
//  Created by KelvinBonilla on 7/14/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "ItemContainer.h"
#import "Pickup.h"

@implementation ItemContainer

-(void) didLoadFromCCB
{
    //Look for the associated pickup and deactivate it,
    //so it doesn't get picked up before the container is destroyed.
    [self getAssociatedPickupItemInstanceReference].IsReadyForPickup = NO;
}

-(void) setIsLocked:(BOOL)IsLocked
{
    [super setIsLocked:IsLocked];
    
    if( NO == isLocked )
    {
        [self runAction:[CCSequence actionOne:[CCFadeOut actionWithDuration:0.4f]
                                          two:[CCCallBlock actionWithBlock:
                                               ^{
                                                   //Activate the associated pickup
                                                   [self getAssociatedPickupItemInstanceReference].IsReadyForPickup = YES;
                                                   
                                                   //Eliminate the container from the scene.
                                                   [self removeFromParentAndCleanup:YES];
                                                }]
                         ]
         ];
    }
}

-(void) Interact
{
    [self LaunchActivity];
}

-(Pickup*) getAssociatedPickupItemInstanceReference
{
    CCNode* associatedPickupRef = [self.parent getChildByTag:associatedPickupTagID];
    
    NSAssert(associatedPickupRef != nil, @"Associated pickup is not found.");
    NSAssert([associatedPickupRef isKindOfClass:[ResourceGatheringInteractiveObject class]], @"Wrong class type for container pickup item");
    
    return ( static_cast<Pickup*>(associatedPickupRef) );
}

@end
