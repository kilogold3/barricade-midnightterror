//
//  WalkerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyWalker;
@class WeaponBase;

@interface WalkerState_ApproachPlayer : NSObject<IEnemyState>
{
    EnemyWalker* enemyParent;
    CCAction* repeatingWalkingAnimationAction;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;

@end
