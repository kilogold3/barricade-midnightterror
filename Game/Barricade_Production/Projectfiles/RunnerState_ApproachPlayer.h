//
//  RunnerState_AproachBarricade.h
//  Barricade_Production
//
//  Created by Kelvin Bonilla on 10/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IEnemyState.h"

@class EnemyRunner;
@class WeaponBase;

@interface RunnerState_ApproachPlayer : NSObject<IEnemyState>
{
    EnemyRunner* enemyParent;
    CCAction* repeatingWalkingAnimationAction;
}

-(id) initWithEnemyParent: (EnemyBase*) enemyParentIn;
-(void) reactToWeaponDamage: (WeaponBase*) firingWeapon;

@end
